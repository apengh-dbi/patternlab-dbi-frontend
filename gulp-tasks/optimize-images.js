/*
*
* Optimize images in /wcsstore/images folder
*
*/


const gulp                = require('gulp');
const path                = require('path');
const image               = require('gulp-image');

var config = require('../patternlab-config.json'),
  patternlab = require('patternlab-node')(config);
function paths() {
  return config.paths;
}
function getConfiguredCleanOption() {
  return config.cleanPublic;
}
function normalizePath() {
  return path
    .relative(
      process.cwd(),
      path.resolve.apply(this, arguments)
    )
    .replace(/\\/g, "/");
}




const IMAGE_OPTIONS = {
  "pngquant": true,
  "optipng": false,
  "zopflipng": false,
  "jpegRecompress": false,
  "jpegoptim": true,
  "mozjpeg": false,
  "gifsicle": true,
  "svgo": true,
  "quiet": false
};


// Image Optimization
gulp.task('optimize-images', () => {
  return gulp.src( normalizePath(paths().source.images + '**/*') )
    .pipe( image(IMAGE_OPTIONS) )
    .pipe(gulp.dest( normalizePath(paths().source.images) ));
});
