/*
*
* Clean Public Folder
*
*/

const gulp                = require('gulp');
const path                = require('path');
const argv                = require('minimist')(process.argv.slice(2));

const del                 = require('del');


var config = require('../patternlab-config.json'),
  patternlab = require('patternlab-node')(config);
function paths() {
  return config.paths;
}
function getConfiguredCleanOption() {
  return config.cleanPublic;
}
function normalizePath() {
  return path
    .relative(
      process.cwd(),
      path.resolve.apply(this, arguments)
    )
    .replace(/\\/g, "/");
}


// Clean Public Folder
gulp.task('clean', () => {
  var options = (argv.dryRun == 'true') ? {dryRun:true} : {dryRun:false}
  return del( normalizePath(paths().public.root + '/**/*'), options );
});