/*
*
* Compile SCSS files
*
*/

const gulp                = require('gulp');
const path                = require('path');

const autoprefixer        = require('gulp-autoprefixer');
const sass                = require('gulp-sass');
const sourcemaps          = require('gulp-sourcemaps');


var config = require('../patternlab-config.json'),
  patternlab = require('patternlab-node')(config);
function paths() {
  return config.paths;
}
function getConfiguredCleanOption() {
  return config.cleanPublic;
}
function normalizePath() {
  return path
    .relative(
      process.cwd(),
      path.resolve.apply(this, arguments)
    )
    .replace(/\\/g, "/");
}


const CONFIG_SASS = {
  outputStyle: 'nested',
  precision: 5,
  includePaths: ['.'],
  onError: console.error.bind(console, 'Sass error:')
}

const CONFIG_AUTOPREFIXER = {
  browsers: [
    'last 2 versions',
    '> 2%',
    'ie >= 11'
  ]
}


// Compile sass
gulp.task('sass', () => {
  return gulp.src( normalizePath(paths().source.scss + '**/*.scss'), {base: normalizePath(paths().source.scss)} )
    
    // Compile SASS files
    .pipe( sass(CONFIG_SASS) )
    
    // Create Source Maps
    .pipe( sourcemaps.write() )
    
    // Auto-prefix css styles for cross-browser compatibility
    .pipe( autoprefixer(CONFIG_AUTOPREFIXER) )
    
    // Output
    .pipe(gulp.dest( normalizePath(paths().source.css) ));
});