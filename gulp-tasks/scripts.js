const gulp                = require('gulp');
const path                = require('path');


const concat              = require('gulp-concat');
const rename              = require('gulp-rename');
const sourcemaps          = require('gulp-sourcemaps');
const uglify              = require('gulp-uglify');

const babel               = require('gulp-babel');
const polyfill            = require('babel-polyfill');


var config = require('../patternlab-config.json'),
  patternlab = require('patternlab-node')(config);
function paths() {
  return config.paths;
}
function getConfiguredCleanOption() {
  return config.cleanPublic;
}
function normalizePath() {
  return path
    .relative(
      process.cwd(),
      path.resolve.apply(this, arguments)
    )
    .replace(/\\/g, "/");
}


// Javascript files
gulp.task('scripts', () => {
  return gulp.src( ['./source/assets/js/**/*.js', '!./source/assets/js/**/*.min.js'], {base: normalizePath(paths().source.js)} )
    .pipe(babel({
      "presets": ["es2015"]
    }))
    .pipe( uglify() )
    .pipe( rename(function(path) {
      path.dirname += '/min';
      path.extname = '.min.js'
    }) )
    .pipe(gulp.dest( normalizePath(paths().source.js) ));
});