/*
*
* Download WCS Assets (CSS / JS files)
*
*/

const gulp                = require('gulp');
const path                = require('path');
const argv                = require('minimist')(process.argv.slice(2));

const remoteSrc           = require('gulp-remote-src');


var config = require('../patternlab-config.json'),
  patternlab = require('patternlab-node')(config);
function paths() {
  return config.paths;
}
function getConfiguredCleanOption() {
  return config.cleanPublic;
}
function normalizePath() {
  return path
    .relative(
      process.cwd(),
      path.resolve.apply(this, arguments)
    )
    .replace(/\\/g, "/");
}


const WCS_ASSETS = [
  "/AuroraPlusStorefrontAssetStore/css/common1_1.css",
  "/AuroraPlusStorefrontAssetStore/images/colors/color1/loading.gif",
  "/AuroraPlusStorefrontAssetStore/images/colors/color1/transparent.gif",
  "/AuroraPlusStorefrontAssetStore/javascript/Common/ShoppingActions.js",
  "/AuroraPlusStorefrontAssetStore/javascript/Common/ShoppingActionsServicesDeclaration.js",
  "/AuroraPlusStorefrontAssetStore/javascript/CommonContextsDeclarations.js",
  "/AuroraPlusStorefrontAssetStore/javascript/CommonControllersDeclaration.js",
  "/AuroraPlusStorefrontAssetStore/javascript/UserArea/OrderLookup.js",
  "/AuroraPlusStorefrontAssetStore/javascript/compiled-js/DBI-my-account.js",
  "/AuroraPlusStorefrontAssetStore/javascript/compiled-js/DBILocator.js",
  "/AuroraPlusStorefrontAssetStore/javascript/compiled-js/DBIcommon-global.js",
  "/AuroraPlusStorefrontAssetStore/javascript/compiled-js/OmniPDP.js",
  "/AuroraPlusStorefrontAssetStore/javascript/compiled-js/common-global.js",
  "/AuroraPlusStorefrontAssetStore/javascript/compiled-js/mod-enq.js",
  "/AuroraPlusStorefrontAssetStore/javascript/compiled-js/omniPLP.js",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/images/general/Calendar.png",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/images/general/davids-bridal-logo.png",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/images/general/findastore.png",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/images/header/close.svg",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/images/header/menu.svg",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/images/header/search.svg",
  "/wcsstore/AuroraPlusStorefrontAssetStore/compiled-css/static.css",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIcompiled-css/header.css",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIcompiled-css/screen.css",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIcompiled-js/UIjs/modernizr.min.js",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIcompiled-js/UIjs/script.min.js",
  "/wcsstore/AuroraPlusStorefrontAssetStore/images/apple-touch-icon-precomposed.png",
  "/wcsstore/AuroraPlusStorefrontAssetStore/images/apple-touch-icon.jpg",
  "/wcsstore/AuroraPlusStorefrontAssetStore/images/favicon.ico",
  "/wcsstore/Widgets_701/com.dbi.commerce.store.widgets.CatalogEntryList/javascript/SearchBasedNavigationDisplay.js",
  "/wcsstore/Widgets_701/com.dbi.commerce.store.widgets.PDP_AddToRequisitionLists/javascript/AddToRequisitionLists.js",
  "/wcsstore/Widgets_701/com.dbi.commerce.store.widgets.PDP_SKUList/javascript/SKUList.js",
  "/wcsstore/Widgets_701/com.ibm.commerce.store.widgets.CatalogEntryRecommendation/javascript/CatalogEntryRecommendation.js",
  "/wcsstore/dojo18-cv/dojo/dojo.js",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/opensans-regular-webfont.woff2",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/opensans-regular-webfont.woff",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/opensans-regular-webfont.ttf",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/opensans-bold-webfont.woff2",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/opensans-bold-webfont.woff",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/domainedisplayweb-semibold-webfont.woff2",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/opensans-bold-webfont.ttf",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/domainedisplayweb-mediumitalic-webfont.woff2",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/domainedisplayweb-semibold-webfont.woff",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/domainedisplayweb-mediumitalic-webfont.woff",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/domainedisplayweb-semibold-webfont.ttf",
  "/wcsstore/AuroraPlusStorefrontAssetStore/DBIUILayer/styleguide/fonts/domainedisplayweb-mediumitalic-webfont.ttf",
  "/wcsstore/AuroraPlusStorefrontAssetStore/fonts/icomoon/icomoon.woff",
  "/wcsstore/AuroraPlusStorefrontAssetStore/fonts/icomoon/icomoon.ttf",
  "/AuroraPlusStorefrontAssetStore/images/intflags/US.png",
  "/AuroraPlusStorefrontAssetStore/images/colors/color1/footer/now_accepting_pp.png"
]


// Download WCS assets
gulp.task('download-wcs-assets', () => {
  var assets = WCS_ASSETS.sort();
  //console.log(assets);
  return remoteSrc(assets, {
    base: 'https://www.davidsbridal.com'
  })
  .pipe(gulp.dest('./source'))
});