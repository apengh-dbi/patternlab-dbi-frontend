$(function(){

  var offerBoxes = $('.offerBox');
  var offers = ["FREE SHIPPING on All Orders $100 & Up"];
  var link = 'http://www.davidsbridal.com/Content_HelpFAQ_ShippingGuide';

  var html = '<div class="offerDate">Ends 4/14/18</div>\
    <div class="offerHead"><a href="' + link + '">FREE SHIPPING, NO MINIMUM on All Online Orders</a></div>\
    <div class="offerBody">Discount applied automatically at checkout.</div>\
    <div class="offerFoot">Receive free standard ground shipping in the continental US for orders placed from April 14, 2018 from 12:00 am ET to April 14, 2018 11:59 pm ET at davidsbridal.com only. This offer cannot be applied to any prior purchases. Free shipping offer is not valid for orders placed outside of the continental US (including AK and HI). <a href="' + link + '">See additional Shipping and Returns Information</a>.</div>';

  $.each(offerBoxes, function(index) { // Loop through offers
    var message = $(this).find('.offerHead').text();
    if( offers.indexOf(message) !== -1 ) { // If offer matches the search offer, update html
      $(this).html(html);
    }
  });

});
