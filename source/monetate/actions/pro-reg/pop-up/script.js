(function($){
  console.log('MONETATE - PROGRESSIVE REGISTRATION POP-UP');

  var debug = true;
  var showEveryXDays = 30;

  var $modal = $('#proRegModal');
  var $overlay = $('.proreg-modal__overlay');
  var $dialog = $('.proreg-modal__dialog');
  var $imageLink = $('#proRegModalImageLink');
  var $buttonLink = $('#proRegModalButton');
  var $closeButton = $('#proRegModalCloseButton');
  var delay = 2000;
  var timeout = delay + 15000;

  function info(message) {
    if(debug === true) {console.log(message);}
  }
  function error(message) {
    if(debug === true) {console.error(message);}
  }


  function initProRegModal() {

    // Show Modal
    function showModal() {
      $modal.fadeIn().attr('aria-hidden', 'false');
      $overlay.fadeIn();
      $imageLink.focus();
      if (typeof(Storage) !== "undefined") {
        var now = new Date();
        localStorage.setItem("proRegModalViewed", true);
        localStorage.setItem("proRegModalLastViewed", now);
      }
    };

    // Hide Modal
    function hideModal() {
      $modal.fadeOut().attr('aria-hidden', 'true');
      $overlay.fadeOut();
    };

    setTimeout(showModal, delay);
    // If no interaction with modal for X seconds, close the modal
    var closeIfNoInteration = setTimeout(function() {
      hideModal();
    }, timeout);

    $closeButton.on('click', function(event) {
      info('Close Button Clicked');
      event.preventDefault();
      hideModal();
    });

    $overlay.on('click', function(event) {
      info('Overlay Clicked');
      event.preventDefault();
      hideModal();
    });


    // Keep focus in the modal (ADA compliance)
    $modal.on('keydown', function (e) {
      var which = e.which;
      var target = e.target;
      var focusedElementId = $( document.activeElement )[0].id;
      info(focusedElementId);
      if (which === 9 && e.shiftKey) { // SHIFT + TAB progressive
        // shift+tab from Image to the Close Button
        if ( focusedElementId === 'proRegModalImageLink' || target === $modal[0]) {
          e.preventDefault();
          $closeButton.focus();
        }
        // shift+tab from Button to the Image Link
        if ( focusedElementId === 'proRegModalButton' || target === $modal[0]) {
          e.preventDefault();
          $imageLink.focus();
        }
      } else if (which === 9) { // TAB PRESS
        // tab from Close Button to the Image
        if ( focusedElementId === 'proRegModalCloseButton' || target === $modal[0]) {
          e.preventDefault();
          $imageLink.focus();
        }
      } else if (which === 27) { // ESCAPE
        hideModal();
      }
    });
  }




  // Use localStorage to determine last time modal was shown
  if (typeof(Storage) !== "undefined") {

    var viewed = localStorage.getItem("proRegModalViewed");
    var lastViewed = new Date( localStorage.getItem("proRegModalLastViewed") );
    // var lastViewed = new Date( 'June 2, 2018 1:46:00 PM' );
    var now = new Date();
    var seconds = showEveryXDays * 24 * 60 * 60 * 1000;
    info('viewed: ' + viewed);
    info('lastViewed: ' + lastViewed);

    // If viewed == false or viewed greater than the number of days ago, show modal
    if( viewed === "undefined" || viewed == "false" || now.getTime() - lastViewed.getTime() >= seconds ) {
      info('Show Modal');
      initProRegModal();
    }

  } else {
    // Sorry! No Web Storage support..
    info('Sorry! No Web Storage support..');
    initProRegModal();
  }

}(jQuery));
