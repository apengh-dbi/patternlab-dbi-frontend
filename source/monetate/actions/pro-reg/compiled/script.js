'use strict';

var proRegURL = 'http://dbqa.davidsbridal.com/newregistration';
var loginURL = 'https://dbqa.davidsbridal.com/UserRegistrationForm?new=Y&amp;catalogId=10051&amp;myAcctMain=1&amp;myAcctMain=1&amp;langId=&amp;storeId=10052&amp;regSource=&amp;login=yes';
var loggedInCookie = 'db_userType';

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function progressiveRegistration() {
  var windowWidth = $(window).width();

  // ========================================================
  // MOBILE/TABLET - NOT LOGGED IN
  // ========================================================
  if (windowWidth < 1024 && getCookie(loggedInCookie) != 'R') {

    // Hide 'MENU'
    //$('.header__main-nav-trigger-text').text('');

    // Hide 'Sign In' Text
    $('.header-SignIn-new').text('');

    // Add Sign Up | Login
    var mobileCount = $('.signup-login__mobile').length;
    if (mobileCount == 0) {
      var signupLogin = '<div class="signup-login signup-login__mobile">\
        <a class="signup-login__signup" href="' + proRegURL + '" onclick="(ga(\'tealium_0.send\', \'event\', \'Monetate\', \'Prog Reg - Edit Signup/Login - 7/26/17\', \'Sign Up Click - Experiment\', {nonInteraction: true}))">Sign Up</a>\
        <span>&nbsp;|&nbsp;</span>\
        <a class="signup-login__signup" href="' + loginURL + '" onclick="(ga(\'tealium_0.send\', \'event\', \'Monetate\', \'Prog Reg - Edit Signup/Login - 7/26/17\', \'Login Click - Experiment\', {nonInteraction: true}))">Login</a>\
      </div>';

      $('.header__primary-dropdown').after(signupLogin);
    }

    // ========================================================
    // DESKTOP - NOT LOGGED IN
    // ========================================================
  } else if (windowWidth >= 1024 && getCookie(loggedInCookie) != 'R') {

    var desktopCount = $('.signup-login__desktop').length;
    if (desktopCount == 0) {
      $('.header__personal-nav-account-icon').remove();
      var signupLogin = '<div class="signup-login signup-login__desktop">\
        <a class="header__personal-nav-account-icon-link signup" href="' + proRegURL + '" aria-label="press enter to access sign up" onclick="(ga(\'tealium_0.send\', \'event\', \'Monetate\', \'Prog Reg - Edit Signup/Login - 7/26/17\', \'Sign Up Click - Experiment\', {nonInteraction: true}))">\
          <span class="header__personal-nav-account-text sans-xs">Sign Up</span>\
        </a>\
        <span>&nbsp;|&nbsp;</span>\
        <a class="header__personal-nav-account-icon-link login" href="' + loginURL + '" onclick="(ga(\'tealium_0.send\', \'event\', \'Monetate\', \'Prog Reg - Edit Signup/Login - 7/26/17\', \'Login Click - Experiment\', {nonInteraction: true}))" aria-label="press enter to access log in">\
          <span class="header__personal-nav-account-text sans-xs">Login</span>\
        </a>\
      </div>';

      $('.header__personal-nav-account.relative').html(signupLogin);
    }

    // ========================================================
    // DESKTOP - LOGGED IN
    // ========================================================
    // } else if (windowWidth >= 1024 && getCookie(loggedInCookie) == 'R') {
    //
    //   var desktopCount = $('.signup-login__desktop').length;
    //   if(desktopCount == 0) {
    //     $('.header__personal-nav-account-icon').remove();
    //
    //     var html = $('.header__personal-nav-account.relative').html();
    //     var signupLogin = '<div class="signup-login signup-login__desktop">' + html + '<span>&nbsp;|&nbsp;</span>\
    //       <a class="header__personal-nav-account-icon-link login" href="'+loginURL+'" onclick="(ga(\'tealium_0.send\', \'event\', \'Monetate\', \'Prog Reg - Edit Signup/Login - 7/26/17\', \'Login Click - Experiment\', {nonInteraction: true}))" aria-label="press enter to access log in">\
    //         <span class="header__personal-nav-account-text sans-xs">Login</span>\
    //       </a>\
    //     </div>';
    //     $('.header__personal-nav-account.relative').html(signupLogin);
    //   }
  }
}

jQuery(document).ready(function () {
  progressiveRegistration();
});
jQuery(window).on('resize', function () {
  progressiveRegistration();
});
