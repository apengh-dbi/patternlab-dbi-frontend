var latitude = 'WC_SLLATITUDE';
var longitude = 'WC_SLLONGITUDE';

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

alert('LATITUDE: ' + getCookie(latitude) + ', LONGITUDE: ' + getCookie(longitude));

// var restAPI = '//www.davidsbridal.com/wcs/resources/store/10052/storelocator';
// var request = restAPI + '/latitude/' + getCookie(latitude) + '/longitude/' + getCookie(longitude) + '?maxItems=1&siteLevelStoreSearch=false&responseFormat=json&radius=1000';
// console.log(request);
//
// $.ajax({
//   url: request,
//   method: 'GET'
// }).success(function(data) {
//   console.log(data);
//
//   var response = data.PhysicalStore[0];
//   console.log(response);
//
//   var address = response.addressLine[0];
//   var city = response.city;
//   var state = response.stateOrProvinceName;
//
//   var html = '<table width="50%" border="1" cellpadding="3" cellspacing="0">\
//     <thead>\
//       <tr>\
//         <th>LATITUDE</th>\
//         <th>LONGITUDE</th>\
//         <th>ADDRESS1</th>\
//         <th>CITY</th>\
//         <th>STATE</th>\
//       </tr>\
//     </thead>\
//     <tbody>\
//       <tr>\
//         <td>'+getCookie(latitude)+'</td>\
//         <td>'+getCookie(longitude)+'</td>\
//         <td>'+address+'</td>\
//         <td>'+city+'</td>\
//         <td>'+state+'</td>\
//       </tr>\
//     </tbody>\
//   </table>';
//
//   $('.ctnr-maincontent').html(html);
});
