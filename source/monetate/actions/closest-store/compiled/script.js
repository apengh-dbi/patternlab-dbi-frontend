'use strict';

// Redirect to HTTPS
// if(window.location.protocol == 'http:') {
//   window.location = 'https://' + window.location.host + window.location.pathname + window.location.search;
// }

var genericAppointmentBanner = '<style media="screen">\
.appointment-banner {\
  max-width: 1262px;\
  margin: 10px auto;\
  background-color: #ffbbb3;\
  position: relative;\
  text-align: center;\
  height: 125px; }\
  @media screen and (min-width: 768px) {\
    .appointment-banner {\
      height: 155px;\
      margin: 10px auto; } }\
  @media screen and (min-width: 1024px) {\
    .appointment-banner {\
      height: 120px; } }\
.appointment-banner__inner {\
  position: absolute;\
  top: 8px;\
  bottom: 8px;\
  right: 8px;\
  left: 8px;\
  border: 1px solid #fff;\
  padding: 20px 0; }\
@media screen and (min-width: 500px) {\
  .appointment-banner__inner {\
    padding: 10px 0; } }\
@media screen and (min-width: 500px) {\
  .appointment-banner__inner {\
    padding: 18px 0; } }\
.appointment-banner__text {\
  padding: 0; }\
.appointment-banner__headline {\
  display: block; }\
@media screen and (min-width: 768px) {\
  .appointment-banner__headline {\
    display: block; } }\
@media screen and (min-width: 1024px) {\
  .appointment-banner__headline {\
    display: inline-block; } }\
.appointment-banner__headline p {\
  margin-bottom: 0; }\
.appointment-banner__subheadline {\
  display: none;\
  padding: 8px 50px 8px; }\
@media screen and (min-width: 768px) {\
  .appointment-banner__subheadline {\
    display: block;\
    padding: 8px 50px 8px; } }\
@media screen and (min-width: 1024px) {\
  .appointment-banner__subheadline {\
    display: inline-block;\
    padding: 24px 20px 0; } }\
.appointment-banner__subheadline p {\
  line-height: 20px !important;\
  margin-bottom: 0; }\
.appointment-banner__btn {\
  display: block;\
  margin: 0;\
  padding: 10px 10px; }\
@media screen and (min-width: 768px) {\
  .appointment-banner__btn {\
    padding: 0 10px; } }\
@media screen and (min-width: 1024px) {\
  .appointment-banner__btn {\
    display: inline-block; } }\
.appointment-banner .btn {\
  font-family: "Open Sans", Helvetica Neue, Arial, sans-serif;\
  font-weight: 600;\
  font-size: 14px;\
  color: #fff;\
  display: inline-block;\
  margin: 0;\
  padding: 0;\
  text-decoration: none;\
  text-align: center;\
  text-transform: uppercase;\
  vertical-align: middle;\
  white-space: nowrap;\
  border: none;\
  background-color: transparent; }\
.appointment-banner .btn-secondary a {\
  outline: none;\
  color: #52535a;\
  background-color: #ffffff;\
  border: 1px solid #52535a;\
  display: block;\
  text-align: center;\
  text-decoration: none;\
  width: 240px;\
  margin: 0 auto; }\
.appointment-banner .btn-secondary a:hover,\
.appointment-banner .btn-secondary a:focus {\
  background-color: #ffffff;\
  border: 1px solid #e0e0e7;\
  outline: none; }\
</style>\
<div class="appointment-banner">\
  <div class="appointment-banner__inner">\
    <div class="appointment-banner__text">\
      <div class="appointment-banner__headline">\
        <p class="serif-m-italic">Meet with a stylist</p>\
      </div>\
      <div class="appointment-banner__subheadline">\
        <p class="sans-xs">We&#39;d love to help you put together your look, from dress to jewelry.</p>\
      </div>\
      <div class="appointment-banner__btn">\
        <p class="btn btn-secondary"><a class="sans-bold-uppercase" href="/DBIMakeAnAppointmentView?catalogId=10051&amp;langId=-1&amp;storeId=10052">Make an Appointment</a></p>\
      </div>\
    </div>\
  </div>\
</div>';

var targetElement = $('.ctnr-maincontent');

// ========================================================
// HTML GEOLOCATION IS SUPPORTED
// ========================================================
if (navigator.geolocation) {
  var success = function success(pos) {
    var crd = pos.coords;
    var latitude = crd.latitude;
    var longitude = crd.longitude;
    var accuracy = crd.accuracy;

    console.log('Your current position is:');
    console.log('Latitude', latitude);
    console.log('Longitude', longitude);
    console.log('Accuracy', accuracy);

    // Build URL
    //http://www.davidsbridal.com/wcs/resources/store/10052/storelocator/latitude/40.0809275/longitude/-75.2874923?maxItems=1&siteLevelStoreSearch=false&responseFormat=json&radius=1000
    var restAPI = 'https://www.davidsbridal.com/wcs/resources/store/10052/storelocator';
    var request = restAPI + '/latitude/' + latitude + '/longitude/' + longitude + '?maxItems=1&siteLevelStoreSearch=false&responseFormat=json&radius=1000';
    console.log(request);

    // AJAX Request
    $.ajax({
      url: request
    }).done(function (data) {
      console.log(data);

      var response = data.PhysicalStore[0];
      console.log(response);

      console.log(response.addressLine[0]);
      console.log(response.city);
      console.log(response.stateOrProvinceName);

      var closestStoreBanner = '<style media="screen">\
      .closest-store-banner {\
        background: #fdccd0;\
        display: block;\
        margin: 0 auto;\
        max-width: 1215px;\
        position: relative;\
        text-align: center; }\
        @media screen and (min-width: 768px) {\
          .closest-store-banner {\
            background-image: url("http://sb.monetate.net/img/1/564/717050.jpg");\
            background-repeat: no-repeat;\
            background-position: 0% 0%;\
            height: 200px; } }\
        .closest-store-banner__inner {\
          display: block;\
          height: 100%;\
          width: 100%; }\
        .closest-store-banner__text {\
          padding: 20px; }\
          @media screen and (min-width: 768px) {\
            .closest-store-banner__text {\
              padding: 0;\
              display: inline-block;\
              position: absolute;\
              top: 50%;\
              -webkit-transform: translateY(-50%);\
                      transform: translateY(-50%);\
              -webkit-transform-style: preserve-3d;\
                      transform-style: preserve-3d;\
              left: 30%;\
              right: 10%; } }\
          @media screen and (min-width: 1024px) {\
            .closest-store-banner__text {\
              left: initial;\
              right: 25%; } }\
        .closest-store-banner p {\
          margin: 0; }\
        .closest-store-banner .sans-l {\
          font-family: "Open Sans", Verdana, Helvetica, Arial, "Droid Sans", sans-serif;\
          font-size: 16px;\
          line-height: 25px;\
          font-weight: normal; }\
        @media screen and (min-width: 768px) {\
          .closest-store-banner .sans-l {\
            font-size: 18px;\
            line-height: 30px; } }\
        .closest-store-banner .btn {\
          font-family: "Open Sans",Helvetica Neue,Arial,sans-serif;\
          display: inline-block;\
          text-decoration: none;\
          text-align: center;\
          text-transform: uppercase;\
          vertical-align: top;\
          white-space: nowrap;\
          border: none;\
          padding: 0;\
          cursor: pointer;\
          width: auto;\
          margin-top: 15px; }\
          .closest-store-banner .btn-secondary a.sans-bold-uppercase {\
            display: inline-block;\
            padding: 9px 32px;\
            border: 1px solid #74736d;\
            color: #333;\
            background-color: #fff;\
            outline: none; }\
            .closest-store-banner .btn-secondary a.sans-bold-uppercase:hover, .closest-store-banner .btn-secondary a.sans-bold-uppercase:active, .closest-store-banner .btn-secondary a.sans-bold-uppercasefocus {\
              border: 1px solid #e0e0e7; }\
            @media screen and (min-width: 768px) {\
              .closest-store-banner .btn-secondary a.sans-bold-uppercase {\
                padding: 11px 50px !important; } }\
      </style>\
      <div class="closest-store-banner">\
        <a href="/DBIMakeAnAppointmentView?catalogId=10051&langId=&storeId=10052">\
          <div class="closest-store-banner__inner"></div>\
        </a>\
        <div class="closest-store-banner__text">\
          <p class="sans-l">Visit your store</p>\
          <p class="sans-l">\
            <strong>\
              <span>at </span>\
              <span id="closestStoreAddress">' + response.addressLine[0] + ', </span>\
              <span id="closestStoreCity"><br class="tablet-hidden desktop-hidden">' + response.city + ', </span>\
              <span id="closestStoreState">' + response.stateOrProvinceName + '</span>\
              <span></span>\
            </strong>\
          </p>\
          <p class="sans-l"><strong>for help finding your perfect dress.</strong></p>\
          <p class="btn btn-secondary">\
            <a href="/DBIMakeAnAppointmentView?catalogId=10051&langId=&storeId=10052" class="sans-bold-uppercase">Make An Appointment</a>\
          </p>\
        </div>\
      </div>';

      targetElement.prepend(closestStoreBanner);
    });
  };

  var error = function error(err) {
    var errorCode = err.code;
    var errorMessage = err.message;
    console.warn('Error: ' + errorCode + ', ' + errorMessage);

    targetElement.prepend(genericAppointmentBanner);
  };

  var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  ;

  ;

  navigator.geolocation.getCurrentPosition(success, error, options);

  // ========================================================
  // HTML Geolocation is NOT supported
  // ========================================================
} else {
  console.log("Geolocation is not supported by this browser.");
  targetElement.prepend(genericAppointmentBanner);
}
