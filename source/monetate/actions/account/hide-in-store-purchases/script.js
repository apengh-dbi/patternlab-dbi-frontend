(function($) {
  $(document).ready(function() {
    console.log('MONETATE - HIDE IN-STORE PURCHASES');

    var debug = false;
    function info(message) {
      if(debug) {console.log(message);}
    }

    function hideInStorePurchases() {
      info('HIDE IN-STORE PURCHASES');
      $('#ProcessedOrdersStatusDisplay > div.order_status_table').remove();

      $('.order_status_select').on('change', function() {
        $('#ProcessedOrdersStatusDisplay > div.order_status_table').remove();
      });
    }
    hideInStorePurchases();

    var targetNode = document.getElementById('ProcessedOrdersStatusDisplay');
    var config = {
      attributes: true,
      childList: true,
      characterData: false
    };

    var callback = function(mutationsList) {
      info(mutationsList);
      for(var i = 0; i < mutationsList.length; i++) {
        info(mutationsList[i]);
        var mutation = mutationsList[i];
        if (mutation.type == 'attributes') {
          info('The ' + mutation.attributeName + ' attribute was modified.');
          hideInStorePurchases();
        } else if(mutation.type == 'childList') {
          info('The ' + mutation.attributeName + ' attribute was modified.');
          hideInStorePurchases();
        }
      }
    };

    var observer = new MutationObserver(callback);
    observer.observe(targetNode, config);

  });
}(jQuery));
