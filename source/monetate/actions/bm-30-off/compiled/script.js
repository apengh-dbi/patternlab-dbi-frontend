'use strict';

// PLP
if (document.activeL1Topic == 'BRIDESMAIDS') {
  $('.pd_origprice.listing__grid-item-price-standard').hide();
}

$('.listing__grid-item').has('.listing__grid-item-product').each(function () {
  $(this).find('.pd_origprice.listing__grid-item-price-standard').hide();
});

// PDP
$('.detail__meta-price.detail__meta-price--standard.line-through').hide();

// Cart


// Checkout
