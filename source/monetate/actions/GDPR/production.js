!function(e,t,n,c,o){
  var r,s;e[c]=function(t){
    e[c]._token=t.token,e[c]._env=t.env
  },
  e[c].l=1*new Date,r=t.createElement("script"),
  s=t.getElementsByTagName("script")[0],
  r.async=1,
  r.src="https://cross-border-tag.borderfree.com/v1/dist/cbt.js",
  s.parentNode.insertBefore(r,s)
}
(window,document,0,"cbt"),
cbt(
  {token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjaElkIjoxMzYxLCJjcmVhdGVkRGF0ZSI6IjIwMTgtMDUtMDhUMDk6Mjg6MTIuNTkyWiIsImlhdCI6MTUyNTc3MTY5Mn0.zgJVHmrBZVULK8IIe798Dvvb4Y4TpvqEsZk0guTEtkA",
  env:"prd"});
