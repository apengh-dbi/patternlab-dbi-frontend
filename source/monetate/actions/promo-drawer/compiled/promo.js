"use strict";

var _promoDrawerJSON;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Color Name Options
// https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#colors_table

var promoDrawerJSON = (_promoDrawerJSON = {
  promo: {
    startDate: "April 1, 2018 12:00 AM",
    end: "April 10, 2018 11:59 PM",
    date: "Limited time",
    img: {
      src: "/wcsstore/images/wwcm/sfg/I&G_Module_Events_414x284.jpg",
      alt: ""
    },
    gradientColorName: "",
    message: "Free shipping $100+",
    link: [{
      url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
      text: "See details",
      hasAsterisk: true
    }]
  }

}, _defineProperty(_promoDrawerJSON, "promo", {
  "startDate": "April 1, 2018 12:00 AM",
  "end": "April 10, 2018 11:59 PM",
  "date": "Ends March 29",
  "img": {
    "src": "",
    "alt": ""
  },
  "gradientColorName": "",
  "message": "Select wedding dresses originally $300-$600, now $99",
  "link": [{
    "url": "http://www.davidsbridal.com/wedding-dresses/all-wedding-dresses",
    "text": "Shop now",
    "hasAsterisk": false
  }]
}), _defineProperty(_promoDrawerJSON, "promo", {
  "startDate": "April 1, 2018 12:00 AM",
  "end": "April 10, 2018 11:59 PM",
  "date": "Ends March 28",
  "img": {
    "src": "/wcsstore/images/wwcm/sfg/Inspiration_ColorHub_414x284.jpg",
    "alt": ""
  },
  "gradientColorName": "",
  "message": "$30 off bridesmaid dresses",
  "link": [{
    "url": "http://www.davidsbridal.com/bridesmaid-dresses/all-bridesmaid-dresses",
    "text": "Shop now",
    "hasAsterisk": false
  }, {
    "url": "http://www.davidsbridal.com/Content_BuyOnline_offers",
    "text": "See details",
    "hasAsterisk": true
  }]
}), _defineProperty(_promoDrawerJSON, "promo", {
  "startDate": "April 1, 2018 12:00 AM",
  "end": "April 10, 2018 11:59 PM",
  "date": "Limited time",
  "img": {
    "src": "/wcsstore/images/wwcm/sfg/I&G_Module_Events_414x284.jpg",
    "alt": ""
  },
  "gradientColorName": "",
  "message": "Free shipping $100+",
  "link": [{
    "url": "http://www.davidsbridal.com/Content_BuyOnline_offers",
    "text": "See details",
    "hasAsterisk": true
  }]
}), _defineProperty(_promoDrawerJSON, "promo", {
  "startDate": "April 1, 2018 12:00 AM",
  "end": "April 10, 2018 11:59 PM",
  "date": "Ends April 8",
  "img": {
    "src": "/wcsstore/images/wwcm/sfg/I&G_Module_Events_414x284.jpg",
    "alt": ""
  },
  "gradientColorName": "",
  "message": "",
  "link": [{
    "url": "http://www.davidsbridal.com/Content_BuyOnline_offers",
    "text": "See details",
    "hasAsterisk": true
  }]
}), _defineProperty(_promoDrawerJSON, "promo", {
  "startDate": "April 1, 2018 12:00 AM",
  "end": "April 10, 2018 11:59 PM",
  "date": "Limited time",
  "img": {
    "src": "/wcsstore/images/wwcm/sfg/I&G_Module_Events_414x284.jpg",
    "alt": ""
  },
  "gradientColorName": "blueviolet",
  "message": "Wedding dresses originally $700-$1600, now $499",
  "link": [{
    "url": "http://www.davidsbridal.com/Content_BuyOnline_offers",
    "text": "See details",
    "hasAsterisk": true
  }]
}), _promoDrawerJSON);
