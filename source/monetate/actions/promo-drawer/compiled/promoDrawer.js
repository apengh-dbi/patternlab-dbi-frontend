'use strict';

(function ($) {

  $.fn.linkify = function (options) {

    var settings = $.extend({
      // Defaults
      color: 'green',
      backgroundColor: 'blue'
    }, options);

    return this.css({
      color: settings.color,
      backgroundColor: settings.backgroundColor
    });
  };
})(jQuery);

$('a').linkify({
  color: 'orange',
  backgroundColor: 'green'
});
