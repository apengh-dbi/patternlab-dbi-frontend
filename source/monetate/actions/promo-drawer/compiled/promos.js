"use strict";

// Color Name Options
// https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#colors_table

var promoDrawerJSON = [

// PROMO
// $20 Off Any Regular Price Dress for Mom
{
  startDate: "April 1, 2018 12:00 AM",
  endDate: "May 7, 2018 11:59 PM",
  date: "Ends May 7",
  img: {
    src: "http://placehold.it/870x617",
    alt: "Alt Text"
  },
  gradientColorName: "",
  message: [{
    text: "Up to $150 Off Regular Price Wedding Dresses, Including Designer Styles"
  }],
  links: [{
    url: "http://www.davidsbridal.com/wedding-dresses/all-wedding-dresses",
    text: "Shop now",
    hasAsterisk: false
  }, {
    url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
    text: "See details",
    hasAsterisk: true
  }]
},

// PROMO
// Select Designer Wedding Dresses Originally $700-$1600, Now $499
{
  startDate: "April 1, 2018 12:00 AM",
  endDate: "May 7, 2018 11:59 PM",
  date: "Ends May 7",
  img: {
    src: "http://placehold.it/870x617",
    alt: "Alt Text"
  },
  gradientColorName: "",
  message: [{
    text: "Select Designer Wedding Dresses Originally $700-$1600, Now $499"
  }],
  links: [{
    url: "http://www.davidsbridal.com/wedding-dresses-under-100",
    text: "Shop now",
    hasAsterisk: false
  }, {
    url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
    text: "See details",
    hasAsterisk: true
  }]
},

// PROMO
// Select Wedding Dresses Originally $300-$600, Now $99
{
  startDate: "April 1, 2018 12:00 AM",
  endDate: "May 7, 2018 11:59 PM",
  date: "Ends May 7",
  img: {
    src: "http://placehold.it/870x617",
    alt: "Alt Text"
  },
  gradientColorName: "",
  message: [{
    text: "Select Wedding Dresses Originally $300-$600, Now $99"
  }],
  links: [{
    url: "http://www.davidsbridal.com/wedding-dresses/all-wedding-dresses",
    text: "Shop now",
    hasAsterisk: false
  }, {
    url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
    text: "See details",
    hasAsterisk: true
  }]
},

// PROMO
// $20 Off Any Regular Price Dress for Mom
{
  startDate: "April 1, 2018 12:00 AM",
  endDate: "June 30, 2018 11:59 PM",
  date: "Ends June 30",
  img: {
    src: "http://placehold.it/870x617",
    alt: "Alt Text"
  },
  gradientColorName: "",
  message: [{
    text: "$20 Off Any Regular Price Dress for Mom"
  }, {
    text: "Use code:",
    promoCode: "GOMOM2"
  }],
  links: [{
    url: "http://www.davidsbridal.com/wedding-dresses/all-wedding-dresses",
    text: "Shop now",
    hasAsterisk: false
  }, {
    url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
    text: "See details",
    hasAsterisk: true
  }]
},

// PROMO
// FREE SHIPPING on All Orders $100 & Up
{
  startDate: "April 1, 2018 12:00 AM",
  endDate: "June 30, 2018 11:59 PM",
  date: "Limited Time",
  img: {
    src: "http://placehold.it/870x617",
    alt: "Alt Text"
  },
  gradientColorName: "",
  message: [{
    text: "FREE SHIPPING on All Orders $100 & Up"
  }],
  links: [{
    url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
    text: "See details",
    hasAsterisk: true
  }]
}];
