'use strict';

// ========================================================
// GET
// /store/{storeId}/cart/@self/assigned_promotion_code
// http://www.davidsbridal.com/wcs/resources/store/10052/cart/@self/assigned_promotion_code
// ========================================================
$.ajax({
  url: 'http://www.davidsbridal.com/wcs/resources/store/10052/cart/@self/assigned_promotion_code',
  method: 'GET'
}).success(function (response) {
  console.log(response);
});

// ========================================================
// POST
// /store/{storeId}/cart/@self/assigned_promotion_code
// http://www.davidsbridal.com/wcs/resources/store/10052/cart/@self/assigned_promotion_code
// ========================================================
var data = {
  "URL": "",
  "catalogId": "10051",
  "langId": "-1",
  "promoCode": "FUN40",
  "requesttype": "ajax",
  "storeId": "10052",
  "taskType": "A"
};

$.ajax({
  type: "POST",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  url: 'http://www.davidsbridal.com/wcs/resources/store/10052/cart/@self/assigned_promotion_code',
  data: JSON.stringify(data),
  dataType: 'json'
}).success(function (response) {
  console.log(response);
});
