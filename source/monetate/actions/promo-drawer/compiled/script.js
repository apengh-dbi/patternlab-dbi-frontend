"use strict";

$(function () {
  console.log('MONETATE - DAILY DEALS DRAWER');

  // ========================================================
  // PROMOS
  // ========================================================
  var promoDrawerJSON = [

  // PROMO
  // $20 Off Any Regular Price Dress for Mom
  {
    startDate: "April 1, 2018 12:00 AM",
    endDate: "Decmeber 31, 2999 11:59 PM",
    date: "Limited Time",
    img: {
      link: "http://www.davidsbridal.com/Content_BuyOnline_offers",
      src: "http://sb.monetate.net/img/1/564/1508831.jpg",
      alt: "Alt Text"
    },
    gradientColorName: "",
    message: [{
      text: "Free shipping 100+"
    }],
    links: [{
      url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
      text: "See details",
      hasAsterisk: true
    }]
  },

  // PROMO
  // Select Designer Wedding Dresses Originally $700-$1600, Now $499
  {
    startDate: "April 1, 2018 12:00 AM",
    endDate: "May 7, 2018 11:59 PM",
    date: "Ends May 7",
    img: {
      link: "http://www.davidsbridal.com/wedding-dresses/all-wedding-dresses",
      src: "http://sb.monetate.net/img/1/564/1508834.jpg",
      alt: "Select Designer Wedding Dresses Originally $700-$1600, Now $499"
    },
    gradientColorName: "",
    message: [{
      text: "Select Designer Wedding Dresses Originally $700-$1600, Now $499"
    }],
    links: [{
      url: "http://www.davidsbridal.com/wedding-dresses/all-wedding-dresses",
      text: "Shop now",
      hasAsterisk: false
    }, {
      url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
      text: "See details",
      hasAsterisk: true
    }]
  },

  // PROMO
  // Select Wedding Dresses Originally $300-$600, Now $99
  {
    startDate: "April 1, 2018 12:00 AM",
    endDate: "May 7, 2018 11:59 PM",
    date: "Ends May 7",
    img: {
      link: "http://www.davidsbridal.com/wedding-dresses-under-100",
      src: "http://sb.monetate.net/img/1/564/1508835.jpg",
      alt: "Select Wedding Dresses Originally $300-$600, Now $99"
    },
    gradientColorName: "",
    message: [{
      text: "Select Wedding Dresses Originally $300-$600, Now $99"
    }],
    links: [{
      url: "http://www.davidsbridal.com/wedding-dresses-under-100",
      text: "Shop now",
      hasAsterisk: false
    }, {
      url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
      text: "See details",
      hasAsterisk: true
    }]
  },

  // PROMO
  // $30 Off Regular Price Bridesmaid Dresses
  {
    startDate: "April 1, 2018 12:00 AM",
    endDate: "May 7, 2018 11:59 PM",
    date: "Ends May 7",
    img: {
      link: "http://www.davidsbridal.com/bridesmaid-dresses/all-bridesmaid-dresses",
      src: "http://sb.monetate.net/img/1/564/1508817.jpg",
      alt: "$30 Off Regular Price Bridesmaid Dresses"
    },
    gradientColorName: "",
    message: [{
      text: "$30 Off Regular Price Bridesmaid Dresses"
    }, {
      text: "",
      promoCode: ""
    }],
    links: [{
      url: "http://www.davidsbridal.com/bridesmaid-dresses/all-bridesmaid-dresses",
      text: "Shop now",
      hasAsterisk: false
    }, {
      url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
      text: "See details",
      hasAsterisk: true
    }]
  },

  // PROMO
  // $30 Off Regular Price Bridesmaid Dresses
  {
    startDate: "April 1, 2018 12:00 AM",
    endDate: "May 7, 2018 11:59 PM",
    date: "Ends May 7",
    img: {
      link: "http://www.davidsbridal.com/dresses/mother-of-the-bride-dresses",
      src: "http://sb.monetate.net/img/1/564/1508816.jpg",
      alt: "$20 Off Any Regular Price Dress for Mom"
    },
    gradientColorName: "",
    message: [{
      text: "$20 Off Any Regular Price Dress for Mom"
    }, {
      text: "Use code:",
      promoCode: "GOMOM2"
    }],
    links: [{
      url: "http://www.davidsbridal.com/dresses/mother-of-the-bride-dresses",
      text: "Shop now",
      hasAsterisk: false
    }, {
      url: "http://www.davidsbridal.com/Content_BuyOnline_offers",
      text: "See details",
      hasAsterisk: true
    }]
  }]; // END OF PROMOS


  /*
  *
  * DON'T EDIT ANYTHING BELOW
  *
  */

  // ========================================================
  // GET THIRD-PARTY ASSETS
  // ========================================================
  // $.get('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js');


  // ========================================================
  // VARIABLES
  // ========================================================
  var $body = $('body');
  var $promoDrawer = $('.promo-drawer__drawer');
  var $header = $('.promo-drawer__header');
  var $closeButton = $('.promo-drawer__close-button');
  var $carousel = $('.promo-drawer__carousel--slick');
  var $overlay = $('.promo-drawer__overlay');

  // ========================================================
  // FUNCTIONS
  // ========================================================
  function openDrawer() {
    $header.find('p').hide();
    $body.addClass('no-scroll');
    $overlay.fadeIn();
    $promoDrawer.attr('data-drawer-status', 'open').focus();

    // Send custom API event
    window.monetateQ = window.monetateQ || [];
    window.monetateQ.push(["trackEvent", ["daily_deals_drawer_opened"]]);
  }

  function closeDrawer() {
    $promoDrawer.attr('data-drawer-status', 'closed');
    $header.find('p').show();
    $body.removeClass('no-scroll');
    $overlay.fadeOut();
  }

  function colorToRGBA(color) {
    // Returns the color as an array of [r, g, b, a] -- all range from 0 - 255
    // color must be a valid canvas fillStyle. This will cover most anything
    // you'd want to use.
    // Examples:
    // colorToRGBA('red')  # [255, 0, 0, 255]
    // colorToRGBA('#f00') # [255, 0, 0, 255]
    var cvs, ctx;
    cvs = document.createElement('canvas');
    cvs.height = 1;
    cvs.width = 1;
    ctx = cvs.getContext('2d');
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, 1, 1);
    return ctx.getImageData(0, 0, 1, 1).data;
  }

  function rgbaToHex(gradient) {
    var rgba = colorToRGBA(gradient);
    var r = rgba[0];
    var g = rgba[1];
    var b = rgba[2];
    var a = rgba[3];

    return '#' + r.toString(16) + g.toString(16) + b.toString(16) + (a * 255).toString(16).substring(0, 2);
  }

  function byteToHex(num) {
    // Turns a number (0-255) into a 2-character hex number (00-ff)
    return ('0' + num.toString(16)).slice(-2);
  }

  function colorToHex(color) {
    // Convert any CSS color to a hex representation
    // Examples:
    // colorToHex('red')            # '#ff0000'
    // colorToHex('rgb(255, 0, 0)') # '#ff0000'
    var rgba, hex;
    rgba = colorToRGBA(color);
    hex = [0, 1, 2].map(function (idx) {
      return byteToHex(rgba[idx]);
    }).join('');
    return "#" + hex;
  }

  function LightenDarkenColor(colorCode, amount) {
    var usePound = false;
    if (colorCode[0] == "#") {
      colorCode = colorCode.slice(1);
      usePound = true;
    }
    var num = parseInt(colorCode, 16);
    var r = (num >> 16) + amount;
    if (r > 255) {
      r = 255;
    } else if (r < 0) {
      r = 0;
    }
    var b = (num >> 8 & 0x00FF) + amount;
    if (b > 255) {
      b = 255;
    } else if (b < 0) {
      b = 0;
    }
    var g = (num & 0x0000FF) + amount;
    if (g > 255) {
      g = 255;
    } else if (g < 0) {
      g = 0;
    }
    return (usePound ? "#" : "") + (g | b << 8 | r << 16).toString(16);
  }

  function colorContrast(gradient) {
    var rgba = colorToRGBA(gradient);
    var yiq = (rgba[0] * 299 + rgba[1] * 587 + rgba[2] * 114) / 1000;
    return yiq;
  }

  function timezoneAdjust(date, timezoneOffest) {
    var timezoneOffest = timezoneOffest || -4;
    return new Date(new Date(date).getTime() + timezoneOffest * 3600 * 1000).toUTCString().replace(/ GMT$/, "");
  }

  // ========================================================
  // Open/Close Drawer
  // ========================================================
  $header.on('click', function () {
    if ($promoDrawer.attr('data-drawer-status') == 'closed') {
      openDrawer();
    } else {
      closeDrawer();
    }
  });

  // Click outside of drawer, close drawer
  $overlay.on('click', function () {
    closeDrawer();
  });

  // keyboard events on the modal itself
  $promoDrawer.on('keydown', function (e) {
    var which = e.which;
    var target = e.target;
    if (which === 27) {
      // ESCAPE
      closeDrawer();
    }
  });

  $closeButton.on('click', function (e) {
    e.preventDefault();
    closeDrawer();
  });

  // ========================================================
  // Build HTML Promos
  // ========================================================
  function generateSlides() {
    for (var i = 0; i < promoDrawerJSON.length; i++) {
      var timeOffset = -4;
      var startDate = new Date(promoDrawerJSON[i].startDate);
      var endDate = new Date(promoDrawerJSON[i].endDate);
      var today = new Date().toLocaleString('en-US', { timeZone: 'America/New_York' });
      var now = new Date(today);

      var startTime = new Date(timezoneAdjust(startDate)).getTime();
      var endTime = new Date(timezoneAdjust(endDate)).getTime();
      var currentTime = new Date(timezoneAdjust(now)).getTime();

      // If promo is active, add promo
      if (startTime <= currentTime && currentTime <= endTime) {

        // --------------------------------------------------------
        // Image/Gradient
        // --------------------------------------------------------
        // Image
        var img = '';
        if (promoDrawerJSON[i].img.src !== "" && promoDrawerJSON[i].img.src !== undefined) {
          img = '<div class="promo-drawer__slide-img">\
          <a href="' + promoDrawerJSON[i].img.link + '">\
          <img src="' + promoDrawerJSON[i].img.src + '" alt="' + promoDrawerJSON[i].img.alt + '">\
          </a>\
          </div>';

          // Custom Gradient w/ Text
        } else if (promoDrawerJSON[i].gradientColorName !== "" && promoDrawerJSON[i].gradientColorName !== undefined) {
          var gradient = promoDrawerJSON[i].gradientColorName;
          var hex = rgbaToHex(colorToHex(gradient));
          var lighten = LightenDarkenColor(colorToHex(gradient), 50);
          var darken = LightenDarkenColor(colorToHex(gradient), -50);

          var rgb = colorToRGBA(gradient);
          console.log(gradient, hex, rgb, lighten, darken);
          var lightenText = '';
          if (colorContrast(gradient) >= 128) {
            lightenText = ' dark-text';
          }
          img = '<div class="promo-drawer__slide-gradient" style="background-image: linear-gradient(135deg, ' + lighten + ', ' + darken + ');">\
          <a href="' + promoDrawerJSON[i].img.link + '">\
          <div class="promo-drawer__slide-gradient-message">\
          <p class="serif-m-italic' + lightenText + '">' + promoDrawerJSON[i].message + '</p>\
          </div>\
          </a>\
          </div>';

          // Default Gradient w/ Text
        } else {
          img = '<div class="promo-drawer__slide-gradient">\
          <a href="' + promoDrawerJSON[i].img.link + '">\
          <div class="promo-drawer__slide-gradient-message">\
          <p class="serif-m-italic">' + promoDrawerJSON[i].message + '</p>\
          </div>\
          </a>\
          </div>';
        }

        // --------------------------------------------------------
        // Description
        // --------------------------------------------------------
        var description = '';
        for (var d = 0; d < promoDrawerJSON[i].message.length; d++) {
          // Has Promo Code
          if (promoDrawerJSON[i].message[d].promoCode) {
            description += '<p class="sans-m-bold">' + promoDrawerJSON[i].message[d].text + ' <span class="sans-bold-uppercase highlight">' + promoDrawerJSON[i].message[d].promoCode + '</span></p>';
            // No Promo Code
          } else {
            description += '<p class="sans-m-bold">' + promoDrawerJSON[i].message[d].text + '</p>';
          }
        }

        // --------------------------------------------------------
        // Links
        // --------------------------------------------------------
        var links = '';
        // One Link
        if (promoDrawerJSON[i].links.length == 1) {
          links += '<a href="' + promoDrawerJSON[i].links[0].url + '" class="sans-m">' + promoDrawerJSON[i].links[0].text + '</a>';
          if (promoDrawerJSON[i].links[0].hasAsterisk === true) {
            links += '<span>*</span>';
          }
          // Multiple Links
        } else if (promoDrawerJSON[i].links.length > 1) {
          for (var a = 0; a < promoDrawerJSON[i].links.length; a++) {
            links += '<a href="' + promoDrawerJSON[i].links[a].url + '" class="sans-m">' + promoDrawerJSON[i].links[a].text + '</a>';
            if (promoDrawerJSON[i].links[a].hasAsterisk === true) {
              links += '<span>*</span>';
            }
          }
        }

        // --------------------------------------------------------
        // Generate Slides HTML
        // --------------------------------------------------------
        var slide = '<div class="slick-slide">\
        <div class="promo-drawer__slide">\
        <div class="promo-drawer__slide-date">\
        <p class="sans-m-bold">' + promoDrawerJSON[i].date + '</p>\
        </div>' + img + '<div class="promo-drawer__slide-description">\
        <div class="promo-drawer__slide-description-title">' + description + '</div>\
        <div class="promo-drawer__slide-description-links">' + links + '</div>\
        </div>\
        </div>\
        </div>';
        $carousel.append(slide);
      }
    } // End loop
  }
  generateSlides();

  $carousel.slick({
    autoplay: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: false,
    infinite: true,
    centerMode: false,
    dots: false,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 420,
      settings: {
        slidesToShow: 1
      }
    }]
  });
  $('.promo-drawer__carousel-button').on("click", function (event) {
    event.preventDefault();
  });
});
