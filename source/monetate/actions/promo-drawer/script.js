$(function() {


  // ========================================================
  // PROMOTIONS
  // ========================================================
  var promotions = [


    // --------------------------------------------------------
    // PROMO
    // FLASH SALE - $40 Off Your Order of $175 & Up
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "July 31, 2018 8:45 AM",
      endDate: "August 4, 2018 11:59 PM",
      date: "Ends Tonight",
      img: {
        link: "/Content_FlashSale_flashsale180731",
        src: "//sb.monetate.net/img/1/564/1512437.jpg",
        alt: "$40 Off Your Order of $175 & Up"
      },
      message: [
        {
          text: "$40 Off Your Order of $175 & Up"
        },
        {
          text: "Use Code:",
          promoCode: "SAVE40"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180731",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


    
    // --------------------------------------------------------
    // PROMO
    // FLASH SALE - Buy More, Save More $10 Off $50, $25 Off $100
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "August 5, 2018 3:45 PM",
      endDate: "August 6, 2018 3:00 AM",
      date: "Ends Tonight",
      img: {
        link: "/Content_FlashSale_flashsale180805",
        src: "//sb.monetate.net/img/1/564/1620808.jpg",
        alt: "Buy More, Save More - $10 Off $50, $25 Off $100"
      },
      message: [
        {
          text: "Buy More, Save More"
        },
        {
          text: "Use Code:",
          promoCode: "SUMMERSAVINGS"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180805",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
   

      // --------------------------------------------------------
    // PROMO
    // FLASH SALE - $40 Off Your Order of $175 & Up
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "August 7, 2018 8:45 AM",
      endDate: "August 7, 2018 11:59 PM",
      date: "Ends Tonight",
      img: {
        link: "/Content_FlashSale_flashsale180807",
        src: "//sb.monetate.net/img/1/564/1512437.jpg",
        alt: "$40 Off Your Order of $175 & Up"
      },
      message: [
        {
          text: "$40 Off Your Order of $175 & Up"
        },
        {
          text: "Use Code:",
          promoCode: "SUMMER40"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180807",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
    
    
    
    // --------------------------------------------------------
    // PROMO
    // FLASH SALE - Buy More, Save More $10 Off $50, $25 Off $100
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "August 12, 2018 3:45 PM",
      endDate: "August 13, 2018 3:00 AM",
      date: "Ends Tonight",
      img: {
        link: "/Content_FlashSale_flashsale180812",
        src: "//sb.monetate.net/img/1/564/1620808.jpg",
        alt: "Buy More, Save More - $10 Off $50, $25 Off $100"
      },
      message: [
        {
          text: "Buy More, Save More"
        },
        {
          text: "Use Code:",
          promoCode: "SWEETSAVINGS"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180812",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
   
    // --------------------------------------------------------
    // PROMO
    // FLASH SALE - $40 Off Your Order of $175 & Up
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "August 14, 2018 8:45 AM",
      endDate: "August 15, 2018 11:59 PM",
      date: "August 15",
      img: {
        link: "/Content_FlashSale_flashsale180814",
        src: "//sb.monetate.net/img/1/564/1512437.jpg",
        alt: "$40 Off Your Order of $175 & Up"
      },
      message: [
        {
          text: "$40 Off Your Order of $175 & Up"
        },
        {
          text: "Use Code:",
          promoCode: "MIDWEEKTREAT"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180814",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
    
    
      // --------------------------------------------------------
    // PROMO
    // FLASH SALE - $40 Off Your Order of $175 & Up
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "August 19, 2018 3:45 PM",
      endDate: "August 20, 2018 3:00 AM",
      date: "Ends Tonight",
      img: {
        link: "/Content_FlashSale_flashsale180819",
        src: "//sb.monetate.net/img/1/564/1512437.jpg",
        alt: "$40 Off Your Order of $175 & Up"
      },
      message: [
        {
          text: "$40 Off Your Order of $175 & Up"
        },
        {
          text: "Use Code:",
          promoCode: "LOVE40"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180819",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
     // --------------------------------------------------------
    // PROMO
    // FLASH SALE - $30 Off Your Order of $150 & Up
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "August 21, 2018 8:45 AM",
      endDate: "August 21, 2018 11:59 PM",
      date: "Ends Tonight",
      img: {
        link: "/Content_FlashSale_flashsale180821",
        src: "//sb.monetate.net/img/1/564/1508812.jpg",
        alt: "$30 Off Your Order of $150 & Up"
      },
      message: [
        {
          text: "$30 Off Your Order of $150 & Up"
        },
        {
          text: "Use Code:",
          promoCode: "TAKE30"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180821",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
    
    // --------------------------------------------------------
    // PROMO
    // FLASH SALE - Buy More, Save More $10 Off $50, $25 Off $100
    // --------------------------------------------------------
    {
      flashSale: true,
      startDate: "August 23, 2018 8:45 AM",
      endDate: "August 24, 2018 3:00 AM",
      date: "Ends Tonight",
      img: {
        link: "/Content_FlashSale_flashsale180823",
        src: "//sb.monetate.net/img/1/564/1620808.jpg",
        alt: "Buy More, Save More - $10 Off $50, $25 Off $100"
      },
      message: [
        {
          text: "Buy More, Save More"
        },
        {
          text: "Use Code:",
          promoCode: "HAPPYSAVINGS"
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_FlashSale_flashsale180823",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
   
   
    // --------------------------------------------------------
    // PROMO
    // FLASH SALE - FREE SHIPPING, NO MINIMUM
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "May 28, 2018 12:01 AM",
      endDate: "May 28, 2018 11:59 PM",
      date: "Ends May 28",
      img: {
        link: "/Content_BuyOnline_offers#oo",
        src: "//sb.monetate.net/img/1/564/1572344.jpg",
        alt: "Free Shipping, No Minimum"
      },
      message: [
        {
          text: "Free Shipping, No Minimum"
        },
        {
          text: "Discount applied in checkout",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers#oo",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


   

    // --------------------------------------------------------
    // PROMO
    // Up to $800 off select wedding dresses
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 20, 2018 12:00 AM",
      endDate: "July 23, 2018 11:59 PM",
      date: "Ends Today",
      img: {
        link: "/sale/sale-wedding-dresses",
        src: "//sb.monetate.net/img/1/564/1644784.jpg",
        alt: "Up to $800 off select wedding dresses"
      },
      message: [
        {
          text: ""
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/sale/sale-wedding-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
    
    
    
    // --------------------------------------------------------
    // PROMO
    // Extra $50 off sample sale wedding dresses
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 20, 2018 12:00 AM",
      endDate: "July 23, 2018 11:59 PM",
      date: "Ends Today",
      img: {
        link: "/sale/sample-sale",
        src: "//sb.monetate.net/img/1/564/1645631.jpg",
        alt: "Extra $50 off sample sale wedding dresses"
      },
      message: [
        {
          text: "Online Only"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/sale/sample-sale",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },
    
    
    
    
    
    
    // --------------------------------------------------------
    // PROMO
    // Select wedding dresses $99
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 24, 2018 12:00 AM",
      endDate: "August 13, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/sale/sale-wedding-dresses",
        src: "//sb.monetate.net/img/1/564/1648281.jpg",
        alt: "Select clearance wedding dresses $99"
      },
      message: [
        {
          text: "Select clearance wedding dresses $99"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/sale/sale-wedding-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },
    
    
   
    
     // --------------------------------------------------------
    // PROMO
    // Gorgeous gowns up to $500 off
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "May 10, 2018 12:00 AM",
      endDate: "May 24, 2018 11:59 PM",
      date: "Ends May 24",
      img: {
        link: "/wedding-dresses/all-wedding-dresses",
        src: "//sb.monetate.net/img/1/564/1540287.jpg",
        alt: "Gorgeous gowns up to $500 off"
      },
      message: [
        {
          text: ""
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/wedding-dresses/all-wedding-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },

    
    
    
    // --------------------------------------------------------
    // PROMO
    // style steals! 50% off select bridesmaid dresses
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "August 3, 2018 8:00 AM",
      endDate: "August 3, 2018 8:01 AM",
      date: "Limited Time",
      img: {
        link: "/sale/bridesmaid-dresses",
        src: "//sb.monetate.net/img/1/564/1540290.jpg",
        alt: "style steals! 50% off select bridesmaid dresses"
      },
      message: [
        {
          text: ""
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/sale/bridesmaid-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


    // --------------------------------------------------------
    // PROMO
    // style steals! 50% off select bridesmaid dresses
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "August 16, 2018 12:00 AM",
      endDate: "September 9, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/sale/bridesmaid-dresses",
        src: "//sb.monetate.net/img/1/564/1540290.jpg",
        alt: "style steals! 50% off select bridesmaid dresses"
      },
      message: [
        {
          text: ""
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/sale/bridesmaid-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


   
    // --------------------------------------------------------
    // PROMO
    // $30 Off Regular Price Bridesmaid Dresses
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "August 3, 2018 8:00 AM",
      endDate: "August 13, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/bridesmaid-dresses/all-bridesmaid-dresses",
        src: "//sb.monetate.net/img/1/564/1648282.jpg",
        alt: "$30 Off Regular Price Bridesmaid Dresses"
      },
      message: [
        {
          text: "$30 Off Regular Price Bridesmaid Dresses"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/bridesmaid-dresses/all-bridesmaid-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


    
    
    // --------------------------------------------------------
    // PROMO
    // $100 off regular priced designer wedding dresses
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "August 2, 2018 8:00 AM",
      endDate: "August 2, 2018 11:59 PM",
      date: "Ends August 13",
      img: {
        link: "/wedding-dresses/all-designer-wedding-dresses",
        src: "//sb.monetate.net/img/1/564/1645632.jpg",
        alt: "$100 off regular priced designer wedding dresses"
      },
      message: [
        {
          text: "$100 off regular priced designer wedding dresses"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/wedding-dresses/all-designer-wedding-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },
    
    
    
    
    // --------------------------------------------------------
    // PROMO
    // Free Shipping $100+
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "May 29, 2018 12:01 AM",
      endDate: "July 30, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/Content_BuyOnline_offers#oo",
        src: "//sb.monetate.net/img/1/564/1512336.jpg",
        alt: "Free Shipping $100+"
      },
      message: [
        {
          text: "Free Shipping $100+"
        },
        {
          text: "Discount applied in checkout",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers#oo",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


    
    
     // --------------------------------------------------------
    // PROMO
    // Clearance bridesmaid dresses starting at $29.99
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "May 1, 2018 12:00 AM",
      endDate: "May 9, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/sale/bridesmaid-dresses",
        src: "//sb.monetate.net/img/1/564/1540288.jpg",
        alt: "Clearance bridesmaid dresses starting at $29.99"
      },
      message: [
        {
          text: ""
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/bridesmaid-dresses/all-bridesmaid-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


   

    // --------------------------------------------------------
    // PROMO
    // $20 Off Any Regular Price Dress for Mom
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 1, 2018 12:00 AM",
      endDate: "September 30, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/dresses/mother-of-the-bride-dresses",
        src: "//sb.monetate.net/img/1/564/1508816.jpg",
        alt: "$20 Off Any Regular Price Dress for Mom"
      },
      message: [
        {
          text: "$20 Off Any Regular Price Dress for Mom"
        },
        {
          text: "Use code:",
          promoCode: "GOMOM3"
        }
      ],
      links: [
        {
          url: "/dresses/mother-of-the-bride-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },





    // --------------------------------------------------------
    // PROMO
    // Buy one, get one 50% off - shoes
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 24, 2018 12:00 AM",
      endDate: "July 31 , 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/shoes/all-shoes",
        src: "//sb.monetate.net/img/1/564/1508810.jpg",
        alt: "Buy One, Get One 50% Off - Shoes"
      },
      message: [
        {
          text: "Buy One, Get One 50% Off - Shoes"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/shoes/all-shoes",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },




    // --------------------------------------------------------
    // PROMO
    // Buy one, get one 50% off - jewelry
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 1, 2018 12:00 AM",
      endDate: "July 31, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/accessories/jewelry",
        src: "//sb.monetate.net/img/1/564/1508808.jpg",
        alt: "Buy One, Get One 50% Off - Jewelry"
      },
      message: [
        {
          text: "Buy One, Get One 50% Off - Jewelry"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/accessories/jewelry",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


    // --------------------------------------------------------
    // PROMO
    // Buy one, get one 50% off - hair accessories
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 1, 2018 12:00 AM",
      endDate: "July 31, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/accessories/hair-accessories",
        src: "//sb.monetate.net/img/1/564/1508807.jpg",
        alt: "Buy One, Get One 50% Off - Hair Accessories"
      },
      message: [
        {
          text: "Buy One, Get One 50% Off - Hair Accessories"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/accessories/hair-accessories",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },



    // --------------------------------------------------------
    // PROMO
    // Buy one, get one 50% off - veils
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "July 1, 2018 12:00 AM",
      endDate: "July 31, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/accessories/veils",
        src: "//sb.monetate.net/img/1/564/1508811.jpg",
        alt: "Buy One, Get One 50% Off - Veils"
      },
      message: [
        {
          text: "Buy One, Get One 50% Off - Veils"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/accessories/veils",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },




    // --------------------------------------------------------
    // PROMO
    // DBCC - Take 6 months to pay with the David's Bridal Credit Card*
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "April 1, 2018 12:00 AM",
      endDate: "December 31, 2999 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/Content_Finance_dbiCreditCard",
        src: "//sb.monetate.net/img/1/564/1520556.jpg",
        alt: "Take 6 months to pay with the David's Bridal Credit Card*"
      },
      message: [
        {
          text: "Take 6 months to pay"
        },
        {
          text: "with the David's Bridal Credit Card*",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "",
          text: "",
          hasAsterisk: false
        },
        {
          url: "/Content_Finance_dbiCreditCard",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },


    // --------------------------------------------------------
    // PROMO
    // Clearance bridesmaid dresses from $29.99
    // --------------------------------------------------------
    {
      flashSale: false,
      startDate: "April 1, 2018 12:00 AM",
      endDate: "May 7, 2018 11:59 PM",
      date: "Limited Time",
      img: {
        link: "/sale/bridesmaid-dresses",
        src: "//sb.monetate.net/img/1/564/1520546.jpg",
        alt: "Clearance bridesmaid dresses from $29.99"
      },
      message: [
        {
          text: "Clearance bridesmaid dresses from $29.99"
        },
        {
          text: "",
          promoCode: ""
        }
      ],
      links: [
        {
          url: "/sale/bridesmaid-dresses",
          text: "Shop now",
          hasAsterisk: false
        },
        {
          url: "/Content_BuyOnline_offers",
          text: "See details",
          hasAsterisk: true
        }
      ]
    },



  ]; // END OF PROMOS






  /*
  *
  * DON'T EDIT ANYTHING BELOW
  *
  */

  // ========================================================
  // VARIABLES
  // ========================================================
  var $promoDrawer = $('.promo-drawer__drawer');
  var $header = $('.promo-drawer__header');
  var $closeButton = $('.promo-drawer__close-button');
  var $carousel = $('.promo-drawer__carousel--slick');
  var $overlay = $('.promo-drawer__overlay');
  var delay = 2000;
  var timeout = delay + 5000;


  var originalHeaderText = $header.find('h3').text().trim();
  $header.find('h3').attr('data-original-text', originalHeaderText);


  // ========================================================
  // FUNCTIONS
  // ========================================================
  function updateHeaderText() {
    var currentHeaderText = $header.find('h3').text().trim();
    var originalHeaderText = $header.find('h3').attr('data-original-text');
    console.log(currentHeaderText, originalHeaderText);
    $header.find('h3').text(originalHeaderText);
    $header.find('h3').attr('data-original-text', currentHeaderText);
  }


  // Open Drawer / Slide Up
  function openDrawer() {
    $header.find('p').hide();
    //$('html').addClass('no-scroll');
    $overlay.fadeIn();
    $promoDrawer.attr('data-drawer-status', 'open').focus();
    updateHeaderText();


    // Send custom API event
    window.monetateQ = window.monetateQ || [];
    window.monetateQ.push([
      "trackEvent",
      ["daily_deals_drawer_opened"]
    ]);
  }

  function openDrawerOnFirstView() {
    $header.find('p').hide();
    //$('html').addClass('no-scroll');
    $overlay.fadeIn();
    $promoDrawer.attr('data-drawer-status', 'open');
    $header.focus();
    updateHeaderText();
  }

  // Close Drawer / Slide Down
  function closeDrawer() {
    $promoDrawer.attr('data-drawer-status', 'closed');
    $header.find('p').show();
    //$('html').removeClass('no-scroll');
    $overlay.fadeOut();
    updateHeaderText();
  }

  // Adjust start/end date/time to Eastern time
  function timezoneAdjust(date, timezoneOffest) {
    var timezoneOffest = timezoneOffest || -4;
    return new Date( new Date(date).getTime() + timezoneOffest * 3600 * 1000).toUTCString().replace( / GMT$/, "" );
  }


  // ========================================================
  // Open Drawer on First Page View
  // ========================================================
  if(window.sessionStorage) {
    if( sessionStorage.getItem('dailyDealsDrawerViewed') == undefined || sessionStorage.getItem('dailyDealsDrawerViewed') == false) {
      setTimeout(openDrawerOnFirstView, delay);

      // If no interaction with drawer for X seconds, close the drawer
      var closeIfNoInteration = setTimeout(function() {
        closeDrawer();
      }, timeout);
      $promoDrawer.on('click', function() {
        clearInterval(closeIfNoInteration);
      });
      $promoDrawer.on('keydown', function(event) {
        clearInterval(closeIfNoInteration);
      });

    }
    sessionStorage.setItem('dailyDealsDrawerViewed', true);
  }


  // ========================================================
  // Open/Close Drawer
  // ========================================================
  // Click on promo drawer header
  $header.on('click', function() {
    if( $promoDrawer.attr('data-drawer-status') == 'closed' ) {
      openDrawer();
    } else {
      closeDrawer();
    }
  });

  // Click outside of drawer, close drawer
  $overlay.on('click', function() {
    closeDrawer();
  });

  // Keyboard events on the drawer itself
  $promoDrawer.on('keydown', function (e) {
    var which = e.which;
    if (which === 27) { // ESCAPE
      closeDrawer();
    }
  });

  // Click on close (X) button
  $closeButton.on('click', function(e) {
    e.preventDefault();
    closeDrawer();
  });



  // ========================================================
  // Build HTML Promo Slides
  // ========================================================
  function generateSlides() {

    // Loop through promotions
    for( var i = 0; i < promotions.length; i++) {
      var timeOffset = -4; // Eastern time (GMT -4 hours)
      var startDate = new Date(promotions[i].startDate);
      var endDate = new Date(promotions[i].endDate);
      var now = new Date();

      var startTime = new Date( timezoneAdjust(startDate) ).getTime();
      var endTime = new Date( timezoneAdjust(endDate) ).getTime();
      var currentTime = new Date( timezoneAdjust(now) ).getTime();

      // If promo is active, add promo
      if ( startTime <= currentTime && currentTime <= endTime ) {

        // --------------------------------------------------------
        // Flash Sale
        // --------------------------------------------------------
        if( promotions[i].flashSale === true ) {
          console.log('Flash Sale = true');
          var text = promotions[i].message[0].text;
          $header.find('h3').text(text);
        }


        // --------------------------------------------------------
        // Image
        // --------------------------------------------------------
        var img = '';
        var altText = '';
        if( promotions[i].img.src !== "" && promotions[i].img.src !== undefined ) {
          if( promotions[i].img.alt !== "" && promotions[i].img.alt !== undefined && promotions[i].img.alt != promotions[i].message[0].text[0]) {
            altText = promotions[i].img.alt;
          } else {
            altText = promotions[i].message[0].text[0];
          }
          img = '<div class="promo-drawer__slide-img">\
            <a href="' + promotions[i].img.link + '">\
              <img src="' + promotions[i].img.src + '" alt="' + altText + '">\
            </a>\
          </div>';
        }

        // --------------------------------------------------------
        // Messages
        // --------------------------------------------------------
        var messages = '';
        for ( var d = 0; d < promotions[i].message.length; d++ ) {
          if( promotions[i].message[d].text !== "" && promotions[i].message[d].text !== undefined ) {
            // Has Promo Code
            if( promotions[i].message[d].promoCode ) {
              messages += '<p class="sans-m-bold">' + promotions[i].message[d].text + ' <span class="sans-bold-uppercase highlight">' + promotions[i].message[d].promoCode +  '</span></p>';
              // No Promo Code
            } else {
              messages += '<p class="sans-m-bold">' + promotions[i].message[d].text + '</p>';
            }
          }
        }

        // --------------------------------------------------------
        // Links
        // --------------------------------------------------------
        var links = '';
        for( var a = 0; a < promotions[i].links.length; a++ ) {
          if( promotions[i].links[a].url !== "" && promotions[i].links[a].url !== undefined ) {
            links += '<a href="' + promotions[i].links[a].url + '" class="sans-m">' + promotions[i].links[a].text + '</a>';
            if( promotions[i].links[a].hasAsterisk === true ) {
              links += '<span>*</span>';
            }
          }
        }


        // --------------------------------------------------------
        // Generate Slides HTML
        // --------------------------------------------------------
        var slide = '<div class="slick-slide">\
          <div class="promo-drawer__slide">\
            <div class="promo-drawer__slide-date">\
              <p class="sans-m-bold">' + promotions[i].date +'</p>\
            </div>'
              + img +
            '<div class="promo-drawer__slide-description">\
              <div class="promo-drawer__slide-description-title">'
                + messages +
              '</div>\
              <div class="promo-drawer__slide-description-links">'
                + links +
              '</div>\
            </div>\
          </div>\
        </div>';
        $carousel.append(slide);
      } // If promo is active, add promo
    } // End loop
  } // close generateSlides function

  generateSlides();



  $carousel.slick({
    autoplay: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: false,
    infinite: true,
    centerMode: false,
    dots: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 420,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  $('.promo-drawer__carousel-button').on("click", function(event){
    event.preventDefault();
  });

});
