require(["dojo/dom-construct"], function(domConstruct){
  (function(){
    var step2Appt = document.getElementById('step2-appt')
    , step3Appt = document.getElementById('step3-appt')
    , appointmentFooterSteps = document.getElementById('appointment-footer-steps')
    , appointmentHeaderSteps = document.getElementById('appointment-header-steps');

    if(typeof enquire !== "undefined"){
      enquire.register(breakpoint_small, {
        match: function(){
          if(step2Appt != null){
            domConstruct.place(step2Appt, appointmentHeaderSteps, "last");
            if(dojo.query(".appointment-step",step2Appt)[0]) {
              dojo.query(".appointment-step",step2Appt)[0].innerHTML = "2. Reserve";
            }
          } else {
            if(dojo.query(".appointment-step")[1]) {
              dojo.query(".appointment-step")[1].innerHTML = "2. Reserve";
            }
          }
            if(step3Appt != null){ domConstruct.place(step3Appt, appointmentHeaderSteps, "last");}

          },
          unmatch: function(){

            if(step2Appt != null){
              if(dojo.query(".appointment-step",step2Appt)[0]) {
                dojo.query(".appointment-step",step2Appt)[0].innerHTML = "2. Reserve appointment";
              }
            } else {
              if(dojo.query(".appointment-step")[1]) {
                dojo.query(".appointment-step")[1].innerHTML = "2. Reserve appointment";
              }
            }
          }
        });
      }
    })();
  });
