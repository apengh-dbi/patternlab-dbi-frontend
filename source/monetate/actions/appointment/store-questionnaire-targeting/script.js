/*
* https://dbstage7.davidsbridal.com/DBIApptCalConfView?storeId=10052&catalogId=10051&langId=-1&type=bridal&apptType=bridal&confNumber=VL384189&storeTz=America%2FNew_York&dur=90&storeNum=0088&comingFrom=Appointments&physicalStoreId=100&startDate=2018-07-21T10%3A30%3A00&endTime=+12%3A00+PM&firstName=k2pGtWFgqUU%3D&email1=rYT6R9ov1PV4aPXWLlP23w%3D%3D&currentLat=&currentLong=&radius=&zipCityInput=
* URL: DBIApptCalConfView
* ESPOT: ES_Appointment_Confirmation_Ad_bridal
*
*/

(function($) {

  var debug = false;
  var targetStores = [19, 48, 62];
  var storeNumber = parseInt(utag_data["appointment_store_number"]);
  var $element = $('#appointmentConfirmation > div.row-fluid > div.span6');
  var $heading = $element.find('h1.apptHd');
  var $subheading = $element.find('p.guestMsg');
  var subheadingText = $subheading.text();

  function info(message) {
    if(debug) { console.log(message); }
  }

  // Extract email from string
  function extractEmail(string) {
    return string.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
  }

  // Extract name from string
  function extractName(string) {
    var name = string.replace(/[\n\r]+/g, '').trim().split(',');
    name = name[0].split(/\s+/g);
    return name[1];
  }



  if( targetStores.includes(storeNumber) ) {
    // Get email address
    var email = extractEmail(subheadingText).join('\n');
    var length = email.length - 1;
    var lastCharacter = email.charAt(length);
    if(lastCharacter == '.') {
      email = email.substring(0, length);
    }
    info('EMAIL: ' + email);

    // Get name
    var name = extractName(subheadingText);
    info('NAME: ' + name);

    // Generate new text
    var headingHtml = '<style>.apptHd{font-size:24px;}</style><h2 style="margin:0; padding:5px 0 15px; font-family:\'Open Sans\',\'Helvetica Neue\',Arial,sans-serif; font-size:32px;">1 Last Step Before Your Appointment - Complete Your Style Profile</h2>';
    var subheadingHtml = 'Hi ' + name + ', we sent an <strong><em>email confirmation</em></strong> and a <strong><em>style profile questionnaire</em></strong> to <u>' + email + '</u>. Please take a few minutes to fill out your style profile so that we can personalize your appointment and make it even better!';

    // Update Heading and Subheading
    $heading.after(headingHtml);
    $subheading.html(subheadingHtml);


    // Update espot image
    // var storeImg = 'https://www.davidsbridal.com/wcsstore/images/wwcm/storepages/Store'+parseInt(storeNumber)+'_658x438.jpg';
    // $element.find('div.espotBlock').find('img').attr('src', storeImg);
  }

}(jQuery));
