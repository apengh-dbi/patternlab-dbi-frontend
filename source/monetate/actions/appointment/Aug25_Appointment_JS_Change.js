dojo.addOnLoad(function(){
  if(dojo.query('.ctnr-header-espots')[0]) {
    dojo.query('.ctnr-header-espots')[0].scrollIntoView(true);
  }
});

if (window.jQuery){
  jQuery(document).ready(function() {

    jQuery("#timeSelectButton").on("click", function() {
      var htime = jQuery("#timesForm input:radio[name=availableTimesButton]:checked").val();
      if(htime != undefined || htime != null) {
        jQuery("#holdAppt").removeClass("disabled");
        document.getElementById("Date-Time").scrollIntoView(true);
        //jQuery("#holdAppt").attr("href","#");
      }
    });

    jQuery("#holdAppt").on("click",function(){
      var htime = jQuery("#timesForm input:radio[name=availableTimesButton]:checked").val();
      if(htime == undefined || htime == null) {
        document.getElementById("appointment-header-steps").scrollIntoView(true);
      }
    });

    jQuery("#holdAppt").attr("id","holdAppt_monetate");
    jQuery("#holdAppt").addClass("disabled");
    dojo.query(".appt .apptTypeButton").forEach(function(node, index, arr){
      node.innerHTML = "<span>"+ node.innerHTML +"</span>";

    });

  });

}
