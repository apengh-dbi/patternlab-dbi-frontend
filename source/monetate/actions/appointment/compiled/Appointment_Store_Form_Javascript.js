"use strict";

dojo.query(".apptTypeButton").on("click", function () {
  dojo.query(".apptTypeButton").forEach(function (node, index, arr) {
    dojo.removeClass(node, "selected");
  });
  dojo.addClass(this, "selected");
});

var zipInput = document.getElementById("zipCityInput");
if (zipInput != null) {
  zipInput.placeholder = "Enter street, city and state, or zip.";
  zipInput.onkeypress = "javascript;";
  zipInput.removeAttribute("maxLength");
}
if (document.getElementById("cityGo")) {
  document.getElementById("cityGo").innerHTML = "Find Store";
}

if (document.getElementById("appt-label-zip")) {
  document.getElementById("appt-label-zip").style.display = "none";
}

if (document.getElementById("state-appt")) {
  document.getElementById("state-appt").style.display = "none";
}

if (dojo.query(".input-holder-appt .or-appt")[0]) {
  dojo.query(".input-holder-appt .or-appt")[0].style.display = "none";
}

jQuery(document).ready(function () {
  if (document.getElementById("noStoresResults") != null && document.getElementById("noStoresResults").value > 0) {
    if (document.getElementById("selectStoreHd")) {
      document.getElementById("selectStoreHd").style.display = "block";
    }
  }
});

if (document.getElementById("searchByGeoNodeForm")) {
  document.getElementById("searchByGeoNodeForm").action = "MakeAnAppointmentView#locationPage";
}

if (document.getElementById("bridalButton")) {
  document.getElementById("bridalButton").href = "#selectAppt";
}

if (document.getElementById("dressButton")) {
  document.getElementById("dressButton").href = "#selectAppt";
}

if (document.getElementById("occasionButton")) {
  document.getElementById("occasionButton").href = "#selectAppt";
}

if (document.getElementById("quinceButton")) {
  document.getElementById("quinceButton").href = "#selectAppt";
}

if (dojo.query("#page > div.ctnr-maincontent > div.appointment-type.sign_in_registration.clearfix > div.row-fluid#locationPage > h2")[0]) {
  var locateHd = dojo.query("#page > div.ctnr-maincontent > div.appointment-type.sign_in_registration.clearfix > div.row-fluid#locationPage > h2")[0];
  locateHd.innerHTML = "Locate a store for your appointment";
  locateHd.className += " appt_hd";
}

if (dojo.query("#page > div.ctnr-maincontent > div.appointment-type.sign_in_registration.clearfix > div.row-fluid h2")[0]) {
  locateHd = dojo.query("#page > div.ctnr-maincontent > div.appointment-type.sign_in_registration.clearfix > div.row-fluid h2")[0];
  locateHd.innerHTML = "Select appointment type (click icon below)";
  locateHd.className += " appt_hd";
  locateHd.id = "selectAppt";
}
