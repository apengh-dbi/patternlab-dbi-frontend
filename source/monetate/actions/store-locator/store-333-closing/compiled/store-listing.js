'use strict';

$(document).ready(function () {
  console.log('DOCUMENT READY');

  var storeHours = '<h3>Store hours</h3>\
    M:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    T:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    W-F:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    Sat:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    Sun:&nbsp;XX:XX pm - XX:XX pm';
  console.log(storeHours);

  var storeListings = $('.scrollSearchResultsConatiner').find('.row-body');
  storeListings.each(function (index) {
    var storeNum = $(this).find('input[id^=title]').val();
    console.log(index, storeNum);
    if (storeNum == "David's Bridal #333") {
      // Hide Make Appt Button
      $(this).find('.btn-7.mk-appt').hide();

      // Update Store Hours
      $(this).find('.store-hours').html(storeHours);
    }
  });
});
