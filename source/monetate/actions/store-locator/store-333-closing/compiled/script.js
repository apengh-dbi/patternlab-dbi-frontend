'use strict';

// http://www.davidsbridal.com/DBIAjaxStoreLocatorDirectionsView?currentLat=&selectCity=Westminster&langId=-1&latitude=33.72345&zipCode=92683&longitude=-117.98903&radius=50&catalogId=10051&distance=4.452425274669687&selectState=CA&currentLong=&ShowBreadCrumbInStoreLocator=true&stLocId=269&pageName=DBIAjaxStoreLocatorDirectionsView&storeId=10052&zipCityInput=huntington+beach

// Hide Make Appt Button
$('#ctnr-mkAppt').find('a').hide();

// Update Store Hours
var hours = 'M-F:&nbsp;XX:XX am - XX:XX pm\
  <br>\
  Sat:&nbsp;XX:XX am - XX:XX pm\
  <br>\
  Sun:&nbsp;XX:XX pm - XX:XX pm';

$('#desk-str-hd').find('h3').next('div').html(hours);
