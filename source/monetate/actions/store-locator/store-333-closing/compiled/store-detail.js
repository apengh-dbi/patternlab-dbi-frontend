'use strict';

// /DBIAjaxStoreLocatorDirectionsView?currentLat=&selectCity=Westminster&langId=-1&latitude=33.72345&zipCode=92683&longitude=-117.98903&radius=50&catalogId=10051&distance=4.452425274669687&selectState=CA&currentLong=&ShowBreadCrumbInStoreLocator=true&stLocId=269&pageName=DBIAjaxStoreLocatorDirectionsView&storeId=10052&zipCityInput=huntington+beach
// /DBIAjaxStoreLocatorDirectionsView?stLocId=269


$(document).ready(function () {
  var storeHours = '<h3>Store hours</h3>\
    M:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    T:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    W-F:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    Sat:&nbsp;XX:XX am - XX:XX pm\
    <br>\
    Sun:&nbsp;XX:XX pm - XX:XX pm';
  console.log(storeHours);

  // Hide Make Appt Button
  $('#ctnr-mkAppt').find('a').hide();

  // Update Store Hours
  $('#desk-str-hd').find('h3').next('div').html(storeHours);
});
