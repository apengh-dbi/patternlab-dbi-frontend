$(document).ready(function() {
  console.log('MONETATE - Hide Store Listing');

  // ========================================================
  // Hide Single Store
  // ========================================================
  var targetStores = ["David's Bridal #249"];
  
  // ========================================================
  // Hide Multiple Stores (delete backslashes to use)
  // ========================================================
  //var targetStores = ["David's Bridal #28", "David's Bridal #249"];
  
  
  var debug = true;
  function info(message) {
    if(debug) {console.log(message);}
  }
  
  
  function hideStoreListing() {
    var stores = $('#storeLocatorResults_aplus').find('.row-body');
    $.each(stores, function(index) {
      var store = $(this).find('input[id*="title"]').val();
      
      // If store is in targetStores, hide
      if(targetStores.includes(store)) {
        $(this).hide();
      }
    });
  }
  hideStoreListing();

  var targetNode = document.getElementById('storeLocatorResults_aplus');
  var config = {
    attributes: true,
    childList: true,
    characterData: false
  };

  var callback = function(mutationsList) {
    info(mutationsList);
    for(var i = 0; i < mutationsList.length; i++) {
      info(mutationsList[i]);
      var mutation = mutationsList[i];
      if (mutation.type == 'attributes') {
        info('The ' + mutation.attributeName + ' attribute was modified.');
        hideStoreListing();
      } else if(mutation.type == 'childList') {
        info('The ' + mutation.attributeName + ' attribute was modified.');
        hideStoreListing();
      }
    }
  };

  var observer = new MutationObserver(callback);
  observer.observe(targetNode, config);

});
  
