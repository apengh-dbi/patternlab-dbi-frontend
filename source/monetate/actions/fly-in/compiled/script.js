'use strict';

$(function () {
  function flyInSlideUp() {
    $('.appointment-flyin').show().animate({ bottom: '20px' }, 1000);
    $('.appointment-flyin__close-button').on('click', function (event) {
      event.preventDefault();
      $('.appointment-flyin').animate({ bottom: '-240px' }, 1000);
    });
  }
  setTimeout(flyInSlideUp, 2000);
});
