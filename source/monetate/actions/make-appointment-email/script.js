$('.slick-slide').not(':first-child').find('img').attr('src', 'http://placehold.it/1600x615');
$('.slick-slide').not(':first-child').remove();


$('.slick-initialized').slick('slickRemove', 0, false);
$('.slick-initialized').slick('slickRemove', 1);
$('.slick-initialized').slick('slickRemove', 2);
$('.slick-initialized').slick('unslick');

var removeFlashSaleBanner = null;
function checkForFlashSaleBanner() {
  if(removeFlashSaleBanner) {
    clearInterval(removeFlashSaleBanner);
  }
  removeFlashSaleBanner = setInterval(function() {
    $('div[id^=monetate_selectorBanner]').remove();
  }, 1000);
}
checkForFlashSaleBanner();
