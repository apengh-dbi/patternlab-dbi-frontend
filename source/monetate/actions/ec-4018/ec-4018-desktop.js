var cssAndHtml = '<style>' +
  'body {margin: 0; padding: 0; }' +
  '.overflow-hidden {overflow: hidden;}' +
  '#temporaryOverlay {position: absolute; background: rgba(0, 0, 0, 0.5); width: 100%; height: 100%; z-index: 100; }' +
  '#continue-dialog {display: none; position: absolute; background-color: #fff; border: none; border-radius: 0; line-height: 1.5em; margin: 0 auto; text-align: center; top: 50%; left: 50%; -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%); width: 670px; z-index: 2000; }' +
  '#continue-dialog h2 {margin: 0; padding: 5px; background-color: #eee; border-radius: 5px 5px 0 0; }' +
  '#continue-dialog button {position: absolute; border: none; background: none; width: 60px; height: 60px; top: 0; right: 0; z-index: 1; }' +
  '#continue-dialog button:focus {outline: -webkit-focus-ring-color auto 5px; }' +
  '#dialog-inner-wrapper {padding: 0; }' +
  '#dialog-inner-wrapper p {margin: 0; }' +
  '.sr-only {position: absolute; width: 1px; height: 1px; padding: 0; margin: -1px; overflow: hidden; clip: rect(0, 0, 0, 0); border: 0; }' +
  '</style>' +

  '<div id="continue-dialog" role="dialog" aria-labelledby="admessagehead1" aria-describedby="admessagebody1">' +
    '<button type="button" id="button-close" aria-label="close"></button>' +
      '<div id="dialog-inner-wrapper">' +
        '<div id="admessagebody1" tabindex="-1" class="sr-only">Registration Modal</div>' +
        '<div>' +
          '<p><img src="http://sb.monetate.net/img/1/c/static/2804/444360/58b21d90b1948ec0cf9a9180454c850b14318eb4.png" alt="Registration Modal" usemap="#registrationModal"></p>' +
          '<map id="registrationModal" name="registrationModal">' +
            '<area shape="rect" id="Brides" coords="337,199,588,239" href="https://www.davidsbridal.com/BrideSignupFormView?catalogId=10051&amp;langId=-1&amp;storeId=10052&amp;regSource=PaidSearch" alt="For Brides" target="_self">' +
            '<area shape="rect" id="Bridesmaid" coords="337,249,588,289" href="https://www.davidsbridal.com/UserSignupView?storeId=10052&amp;catalogId=10051&amp;roleName=Bridesmaid" alt="For Bridesmaids" target="_self">' +
            '<area shape="rect" id="Mother" coords="337,299,588,339" href="https://www.davidsbridal.com/UserSignupView?storeId=10052&amp;catalogId=10051&amp;roleName=Mother" alt="For Mothers of the Bride" target="_self">' +
            '<area shape="rect" id="Other" coords="337,351,588,391" href="https://www.davidsbridal.com/UserSignupView?storeId=10052&amp;catalogId=10051&amp;roleName=Other" alt="For Party Goers" target="_self">' +
            '<area shape="rect" id="Prom" coords="337,401,588,441" href="https://www.davidsbridal.com/UserSignupView?storeId=10052&amp;catalogId=10051&amp;roleName=Prom" alt="For Prom Goers" target="_self">' +
          '</map>' +
        '</div>' +
  	'</div>' +
  '</div>';

$('body').prepend(cssAndHtml);


var $modal = $('#continue-dialog');
var $container = $('#fullContainer');
var $close = $('#button-close');

function showModal() {
  $container.attr('aria-hidden', 'true');
  $('body').prepend('<div id="temporaryOverlay"></div>');
  $modal.show();
  setTimeout(function () {
    // $modal[0].focus();
    $('#registrationModal area')[0].focus(); // Focus on first button
  }, 0);
  $('body').toggleClass('overflow-hidden');
}

function hideModal() {
  $container.removeAttr('aria-hidden');
  $('#temporaryOverlay').remove();
  $modal.hide();
  $('body').toggleClass('overflow-hidden');
}



// On load, open modal dialog
showModal();

// Clicking on (X), closes modal
$close.on('click', function(){
  hideModal();
});

// Keep focus in the modal
$modal.on('keydown', function (e) {
  var which = e.which;
  var target = e.target;
  var focusedElementId = $( document.activeElement )[0].id;
  if (which === 9 && e.shiftKey) { // SHIFT + TAB
    // shift+tab from "For Brides" button to the (close) button
    if ( focusedElementId === 'Brides' || target === $modal[0]) {
      e.preventDefault();
      $close.focus();
    }
    // shift+tab from "(X)" button to the Prom button
    if ( focusedElementId === 'button-close' || target === $modal[0]) {
      e.preventDefault();
      $('#Prom').focus();
    }
  } else if (which === 9) { // TAB
    if ( focusedElementId === 'Prom' || target === $modal[0]) {
      e.preventDefault();
      $close.focus();
    }
  } else if (which === 27) { // ESCAPE
    hideModal();
  }
});


// clicking outside of the modal while the modal is
// open will close the dialog and focus the trigger
$(document).on('click', function (e) {
  if ($modal.is(':visible') && !$(e.target).closest('#continue-dialog')[0]){
    hideModal();
  }
});
