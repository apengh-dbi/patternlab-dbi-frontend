var $modal = $('#continue-dialog');
var $container = $('#fullContainer');
var $close = $('#button-close');

function showModal() {
  $container.attr('aria-hidden', 'true');
  $('body').prepend('<div id="temporaryOverlay"></div>');
  $modal.show();
  setTimeout(function () {
    // $modal[0].focus();
    $('#registrationModal area')[0].focus(); // Focus on first button
  }, 0);
  $('body').toggleClass('overflow-hidden');
}

function hideModal() {
  $container.removeAttr('aria-hidden');
  $('#temporaryOverlay').remove();
  $modal.hide();
  $('body').toggleClass('overflow-hidden');
}



// On load, open modal dialog
$(window).load(function(){
  showModal();
});

// Clicking on (X), closes modal
$close.on('click', function(){
  hideModal();
});

// Keep focus in on the modal
$modal.on('keydown', function (e) {
  var which = e.which;
  var target = e.target;
  var focusedElementId = $( document.activeElement )[0].id;
  if (which === 9 && e.shiftKey) { // SHIFT + TAB
    // shift+tab from "For Brides" button to the (close) button
    if ( focusedElementId === 'Brides' || target === $modal[0]) {
      e.preventDefault();
      $close.focus();
    }
    if ( focusedElementId === 'button-close' || target === $modal[0]) {
      e.preventDefault();
      $('#Prom').focus();
    }
  } else if (which === 9) { // TAB
    if ( focusedElementId === 'Prom' || target === $modal[0]) {
      e.preventDefault();
      $close.focus();
    }
  } else if (which === 27) { // ESCAPE
    hideModal();
  }
});


// clicking outside of the modal while the modal is
// open will close the dialog and focus the trigger
$(document).on('click', function (e) {
  if ($modal.is(':visible') && !$(e.target).closest('#continue-dialog')[0]){
    hideModal();
  }
});
