(function( $ ){

  var $countdown = $('.flash-sale-banner__timer');
  var endDate = new Date($countdown.attr('data-end-date'));
  var today = new Date();
  //var endDate = new Date('May 28, 2018 11:59:59 PM');
  //var today = new Date('May 28, 2018 12:00:00 AM');
  var secondsInDay = 1000 * 60 * 60 * 24;

  // Adjust start/end date/time to Eastern time
  function timezoneAdjust(date, timezoneOffest) {
    var timezoneOffest = timezoneOffest || -4;
    return new Date( new Date(date).getTime() + timezoneOffest * 3600 * 1000).toUTCString().replace( / GMT$/, "" );
  }

  var endTime = new Date( timezoneAdjust(endDate) ).getTime();
  var currentTime = new Date( timezoneAdjust(today) ).getTime();

  // Countdown Timer
  var days, hours, minutes, seconds; // variables for time units


  function countdownTimer(){
    var countdown = $('#promCountdown');

  	// find the amount of "seconds" between now and target
  	var currentDate = new Date().getTime();
  	var secondsLeft = (endDate - currentDate) / 1000;

  	days = parseInt(secondsLeft / 86400);
  	secondsLeft = secondsLeft % 86400;

  	hours = parseInt(secondsLeft / 3600);
  	secondsLeft = secondsLeft % 3600;

  	minutes = parseInt(secondsLeft / 60);
  	seconds = pad( parseInt( secondsLeft % 60 ) );

    // format countdown string + set tag value
    var html = '<span class="days">' + days +' days</span>'+
      '<span>&nbsp;:&nbsp;</span>'+
      '<span class="hours">' + hours + ' hrs</span>'+
      '<span>&nbsp;:&nbsp;</span>'+
      '<span class="minutes">' + minutes + ' mins</span>'+
      '<span>&nbsp;:&nbsp;</span>'+
      '<span class="seconds">' + seconds + ' secs</span>';
  	countdown.html(html);
  }

  countdownTimer();
  setInterval(function () { countdownTimer(); }, 1000);


  var params = {
    'endDate': endDate,
    'currentTime': currentTime
  }
  console.log(params);

}( jQuery ));
