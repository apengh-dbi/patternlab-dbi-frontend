(function ( $ ) {

  try {
    var catgroupId = window.categoryPageParams.categoryId;
    var url = 'http://www.davidsbridal.com/wcs/resources/store/10052/categoryview/byId/' + catgroupId;
    $.ajax({
      url: url,
      method: 'GET'
    }).done(function(response) {
      var response = response.CatalogGroupView[0];
      var message = '<style>\
        table { font-family: "Open Sans", Helvetica, Arial, sans-serif; white-space:normal; table-layout:fixed; width:100%; box-shadow:2px 2px 4px #666; }\
        td, th { padding:10px; text-align:left; font-weight:700; }\
        th { background:#333; color:#fff; white-space:nowrap; }\
        td { word-wrap:break-word; }\
      </style>\
      <section style="background:#f5f5f5; padding:20px; overflow-x:auto;">\
        <table width="100%" border="1" cellpadding="5" cellspacing="0">\
          <tbody>\
            <tr>\
              <th width="200">CATEGORY</th>\
              <td>' + response.name + '</td>\
            </tr>\
            <tr>\
              <th>CATGROUP ID</th>\
              <td>' + response.uniqueID + '</td>\
            </tr>\
            <tr>\
              <th>IDENTIFIER</th>\
              <td>' + response.identifier + '</td>\
            </tr>\
            <tr>\
              <th>TITLE</th>\
              <td>' + response.title + '</td>\
            </tr>\
          </tbody>\
        </table>\
      </section>';
      $('body').prepend(message);
    });
  } catch(err) {
    console.log(err.message);
  }

}( jQuery ));
