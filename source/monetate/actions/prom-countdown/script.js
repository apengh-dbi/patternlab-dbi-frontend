$(function() {
  var body = $('body');
  var promModal = $('.prom-countdown');
  var closeButton = promModal.find('.prom-countdown__close > a');

  // Close modal
  closeButton.on('click', function(event) {
    event.preventDefault();
    promModal.fadeOut('slow');
  });

  // Countdown Timer
  var endDate = new Date('November 20, 2017 23:59:59');
  var days, hours, minutes, seconds; // variables for time units


  function pad(n) {
  	return (n < 10 ? '0' : '') + n;
  }

  function countdownTimer(){
    var countdown = $('#promCountdown');

  	// find the amount of "seconds" between now and target
  	var currentDate = new Date().getTime();
  	var secondsLeft = (endDate - currentDate) / 1000;

  	days = parseInt(secondsLeft / 86400);
  	secondsLeft = secondsLeft % 86400;

  	hours = parseInt(secondsLeft / 3600);
  	secondsLeft = secondsLeft % 3600;

  	minutes = parseInt(secondsLeft / 60);
  	seconds = pad( parseInt( secondsLeft % 60 ) );

    // format countdown string + set tag value
    var html = '<span class="days">' + days +' days</span>'+
      '<span>&nbsp;:&nbsp;</span>'+
      '<span class="hours">' + hours + ' hrs</span>'+
      '<span>&nbsp;:&nbsp;</span>'+
      '<span class="minutes">' + minutes + ' mins</span>'+
      '<span>&nbsp;:&nbsp;</span>'+
      '<span class="seconds">' + seconds + ' secs</span>';
  	countdown.html(html);
  }

  countdownTimer();
  setInterval(function () { countdownTimer(); }, 1000);

});
