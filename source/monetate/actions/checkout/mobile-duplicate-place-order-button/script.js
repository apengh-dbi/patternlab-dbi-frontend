
$(document).ready(function() {
  console.log('MONETATE - DUPLICATE PLACE ORDER BUTTON');

  // ========================================================
  //For Normal CC Orders
  // ========================================================
  var observer = new MutationObserver(function (mutationRecords, observer) {
    mutationRecords.forEach(function (mutation) {
      var orderReview = mutation.target.id;
      var count = $('#orderTotalsSummarySectionDiv').find('.paymentForm_nextButtonContainer').length;
      if(count == 1) {
        if( $('#paymentBillingForm_nextButton_orderSummary').is(":visible") ) {
          if( $('#OrderReviewHeadingTop').find('span.paymentForm_nextButtonContainer').length == 0 ) {
            $('#OrderReviewHeadingTop h2').find('span.paymentForm_nextButtonContainer').remove();
            var html = $('.paymentForm_nextButtonContainer').html();
            html = '<span class="paymentForm_nextButtonContainer">' + html + '</span>';
            $('#OrderReviewHeadingTop h2').after(html);
          }
        }
      }
    });
  });

  var target = document.getElementById("orderTotalsSummarySectionDiv");
  var config = {
    childList: true,
    subtree: true,
    attributes: true,
    characterData: true
  };
  observer.observe(target, config);



  // ========================================================
  // For Paypal Orders
  // ========================================================
  if( $('#PayPalOrderReviewForm').is(":visible") ) {
    var count = $('#orderTotalsSummarySectionDiv').find('.paymentForm_nextButtonContainer').length;
    if(count == 1) {
      if( $('#singleOrderSummaryPayPal').is(":visible") ) {
        if( $('#OrderReviewHeadingTop').find('span.paymentForm_nextButtonContainer').length == 0 ) {
          $('#OrderReviewHeadingTop').find('span.paymentForm_nextButtonContainer').remove();
          var html = $('.paymentForm_nextButtonContainer').html();
          html = '<span class="paymentForm_nextButtonContainer" style="margin-bottom: 15px;">' + html + '</span>';
          $('#OrderReviewHeadingTop').append(html);
        }
      }
    }
  }  

});
