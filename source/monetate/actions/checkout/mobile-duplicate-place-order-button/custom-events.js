$(document).ready(function() {

  // ========================================================
  // Custom Events
  // ========================================================

  // Initialize the monetateQ object. If the monetateQ object is
  // already present on the page, you don't have to do this again.
  //window.monetateQ = window.monetateQ || [];

  // Send the event key when the event occurs
  //"Event Key" is the name of the key that you want to send.

  // Default Button
  $('#orderSummary').find('span.paymentForm_nextButtonContainer').on('click', function() {
    console.log('DEFAULT BUTTON CLICKED 1');
    window.monetateQ.push(["trackEvent", ["checkout_duplicate_place_order_button_default_button"]]);
  });
  $('#orderSummary').find('#paymentBillingForm_nextButton_orderSummary').on('click', function() {
    console.log('DEFAULT BUTTON CLICKED 2');
    window.monetateQ.push(["trackEvent", ["checkout_duplicate_place_order_button_default_button"]]);
  });

  // Duplicate Button
  $('#OrderReviewHeadingTop').find('span.paymentForm_nextButtonContainer').on('click', function() {
    console.log('DUPLICATE BUTTON CLICKED 1');
    window.monetateQ.push(["trackEvent", ["checkout_duplicate_place_order_button_duplicate_button"]]);
  });
  $('#OrderReviewHeadingTop').find('#paymentBillingForm_nextButton_orderSummary').on('click', function() {
    console.log('DUPLICATE BUTTON CLICKED 2');
    window.monetateQ.push(["trackEvent", ["checkout_duplicate_place_order_button_duplicate_button"]]);
  });

});



require(["dojo/request", "dojo/request/notify"], function(request, notify){
  // DOJO AJAX Call - Done
  notify("done", function(responseOrError){
    // If error, log error
    if(responseOrError instanceof Error){
      console.log('Dojo - Notify - Failed');
    } else {
      console.log('Dojo - Notify - Success');
      console.log('TRACK CUSTOM EVENTS');

      // Default Button
      $('#orderSummary').find('span.paymentForm_nextButtonContainer').on('click', function() {
        console.log('DEFAULT BUTTON CLICKED');
        window.monetateQ.push(["trackEvent", ["checkout_duplicate_place_order_button_default_button"]]);
      });

      // Duplicate Button
      $('#OrderReviewHeadingTop').find('span.paymentForm_nextButtonContainer').on('click', function() {
        console.log('DUPLICATE BUTTON CLICKED 1');
        window.monetateQ.push(["trackEvent", ["checkout_duplicate_place_order_button_duplicate_button"]]);
      });

    }
  });
});
