(function($) {

  // Variables
  var debug = true;
  var key = 'browsingHistory';
  var browsingHistory = [];

  // Functions
  function info(message = '') {
    if(debug) {console.log(message);}
  }
  function error(message = 'There was an error.') {
    if(debug) {console.error(message);}
  }
  function mixedMediaSet(styleNum, productId, color) {
    return 'https://img.davidsbridal.com/is/image/DavidsBridalInc/Set-'+styleNum+'-'+productId+'-'+color;
  }

  function lsGet(key) {
    if (localStorage.getItem(key)) {
      var value = localStorage.getItem(key);
      value = JSON.parse(value);
    }
    info(value);
    return value;
  }


  // Check if localStorage is available
  if (window.localStorage) {
    info('localStorage Available');

    lsGet(key);


    // Get PDP info
    var timestamp = new Date();
    var styleNum = s7DBIData.mfPartNumber;
    var productId = s7DBIData.productId;
    var color = s7DBIData.mainColor;
    var url = window.location.href;
    var mixedMediaSet = mixedMediaSet(styleNum, productId, color);

    var data = {
      'timestamp': timestamp,
      'url': url,
      'styleNum': styleNum,
      'productId': productId,
      'color': color,
      'mixedMediaSet': mixedMediaSet
    };
    data = JSON.stringify(data);
    info(data);
    browsingHistory.push(data);
    info(data);

    localStorage.setItem(key, data);




  } else {
    error('localStorage is not available');
  }

}(jQuery));
