(function() {
  var links = [
    {
      topic: "BRIDES",
      href: "/wedding-dresses/all-wedding-dresses"
    },
    {
      topic: "BRIDESMAIDS",
      href: "/bridesmaid-dresses/all-bridesmaid-dresses"
    },
    {
      topic: "DRESSES",
      href: "/dresses/all-dresses"
    },
    {
      topic: "ACCESSORIES",
      href: "/accessories/all-accessories"
    },
    {
      topic: "DECORATIONS-GIFTS",
      href: "/wedding-decorations/favors-decorations"
    },
    {
      topic: "SHOES",
      href: "/shoes/all-shoes"
    },
    {
      topic: "PROM",
      href: "/prom-dresses/all-prom-dresses"
    },
    {
      topic: "SALE",
      href: "/sale/sale-all"
    }
  ];

  for(var i = 0; i < links.length; i++) {
    var menu = document.querySelector('.header__main-nav-topic[data-menu-topic="' + links[i].topic + '"]');
    if(menu) {
      // menu.querySelector('.header__main-nav-topic-section-link--all > a').href = links[i].href;
      menu.querySelector('.header__main-nav-topic-heading-link').href = links[i].href;
    }
  }
})();
