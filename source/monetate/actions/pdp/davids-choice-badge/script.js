(function($) {

  var targetData = {
    price: 99.95,
    avgRating: 4.5,
    numRatings: 25
  };

  var productId = s7DBIData.productId;
  var target = $('div[data-slot-id="3"]');
  var $ratingsData = $('#BVRRRatingOverall_Rating_Summary_1');

  var html = '<style media="screen">\
  .davids-pick {\
    margin-top: 5px; }\
    .davids-pick span {\
      background: #f04874;\
      background: -webkit-gradient(linear, left top, right top, from(#ec1950), to(#f47798));\
      background: linear-gradient(to right, #ec1950, #f47798);\
      color: #fff !important;\
      border: none !important;\
      border: 1px solid #f04874; }\
  </style>\
  <p class="btn btn-secondary btn-secondary--small no-hover davids-pick">\
    <span class="sans-s btn-link btn-link--small">David\'s Choice</span>\
  </p>';


  // Get Average Review Rating
  var avgRating = $ratingsData.find('[itemprop="ratingValue"]').text();
  if(avgRating) {
    avgRating = parseFloat(avgRating);
  }


  // Get Number of Reviews
  var numRatings = $ratingsData.find('[itemprop="reviewCount"]').attr('content');
  if(numRatings) {
    numRatings = parseInt(numRatings);
  }


  // Get Price
  var price = productPriceArray[productId].usdMinPrice;
  if(price) {
    price = parseFloat(price);
  }

  var conditions =
    avgRating
    && avgRating >= targetData.avgRating
    && numRatings != undefined
    && numRatings >= targetData.numRatings
    && price <= targetData.price


  console.log('avgRating:', avgRating);
  console.log('numRatings:', numRatings);
  console.log('price', price);
  console.log('conditions', conditions);
  if(conditions) {
    target.prepend(html);
  }

}(jQuery));
