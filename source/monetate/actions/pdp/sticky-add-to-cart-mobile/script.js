var $selectionCost = $('.detail__price.border-bottom--gray-lt.spacing-top');
var $buttonContainer = $('.detail__fieldset.detail__actions.spacing-top');

// add position attribute
$selectionCost.attr('data-position', 'relative');
$buttonContainer.attr('data-position', 'relative');


var $quantityPrice = $('#quantity_price');
var quantityPriceText = $quantityPrice.text();
if(quantityPriceText != '') {
  $selectionCost.attr('data-position', 'fixed');
  $buttonContainer.attr('data-position', 'fixed');
}


// Check if color swatch is selected
var $colors = $('.detail__color-swatch-item-input');
console.log($colors.length);

if ($colors.is(':checked')) {
  console.log('Color Selected');
} else {
  console.log('Color Not Selected');
}


// Check if size is selected
var $sizes = $('.detail__size-swatch-item-input');
console.log($sizes.length);

if ($sizes.is(':checked')) {
  console.log('Size Selected');
} else {
  console.log('Size Not Selected');
}
