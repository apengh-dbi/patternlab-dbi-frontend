(function($) {

  var category = 'Jewelry';
  var link = '/accessories/jewelry';
  var image = 'https://sb.monetate.net/img/1/564/1601912.jpg';
  var altText = 'Filigree and Crystal Drop Earrings | Style 60250E';

  var html = '<style media="screen" type="text/css">\
  .designed-to-match {\
    display: block;\
    position: relative;\
    margin-bottom: 20px;\
    padding-top: 10px; }\
    .designed-to-match__heading {\
      padding-bottom: 5px; }\
      .designed-to-match__heading p {\
        display: block;\
        width: 100%;\
        margin: 0; }\
        @media screen and (min-width: 500px) {\
          .designed-to-match__heading p {\
            display: inline-block;\
            width: auto;\
            margin-right: 10px; } }\
        .designed-to-match__heading p.serif-s-italic {\
          margin-bottom: 5px; }\
        .designed-to-match__heading p.sans-bold-uppercase a {\
          text-decoration: none; }\
          .designed-to-match__heading p.sans-bold-uppercase a:hover {\
            text-decoration: none; }\
  </style>\
  <div class="designed-to-match">\
    <div class="designed-to-match__heading">\
      <p class="serif-s-italic">Designed to match</p>\
      <p class="sans-bold-uppercase">\
        <a class="designed-to-match__link" href="' + link + '">Shop ' + category + '&nbsp;&gt;</a>\
      </p>\
    </div>\
    <div class="designed-to-match__img">\
      <a class="designed-to-match__link" href="' + link + '">\
        <img alt="' + altText + '" src="' + image + '" />\
      </a>\
    </div>\
  </div>';

  $('#widget_minishopcart_popup_1 > div.actions').before(html);

}(jQuery));
