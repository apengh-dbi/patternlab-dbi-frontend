// Selector
// #widget_minishopcart_popup_1 > div.actions


if (L1 == 'Shoes') {
  console.log('SHOES!');

  var target = $('#widget_minishopcart_popup_1 > div.actions');

  var cssAndHtml = '<style media="screen">\
  .designed-to-match {\
    display: block;\
    position: relative;\
    margin-bottom: 20px;\
    padding-top: 10px; }\
    .designed-to-match__heading {\
      padding-bottom: 5px; }\
      .designed-to-match__heading p {\
        display: block;\
        width: 100%;\
        margin: 0; }\
        @media screen and (min-width: 500px) {\
          .designed-to-match__heading p {\
            display: inline-block;\
            width: auto;\
            margin-right: 10px; } }\
        .designed-to-match__heading p.serif-s-italic {\
          margin-bottom: 5px; }\
        .designed-to-match__heading p.sans-bold-uppercase a {\
          text-decoration: none; }\
          .designed-to-match__heading p.sans-bold-uppercase a:hover {\
            text-decoration: none; }\
  </style>\
  <div class="designed-to-match">\
    <div class="designed-to-match__heading">\
      <p class="serif-s-italic">Designed to match</p>\
      <p class="sans-bold-uppercase">\
        <a href="/accessories/handbags">Shop Handbags&nbsp;&gt;</a>\
      </p>\
    </div>\
    <div class="designed-to-match__img">\
      <a href="/accessories/handbags">\
        <img src="http://sb.monetate.net/img/1/564/1376289.jpg" alt="Iridescent Glitter Minaudiere | Style HBARIEL">\
      </a>\
    </div>\
  </div>';

  target.before(cssAndHtml);
}


// Custom Click Event
'.designed-to-match a'
