function slickSplitLarge() {
  if (!$(".slick-split-2-4-large").hasClass("slick-initialized")) {
    $(".slick-split-2-4-large").slick({
      mobileFirst: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      prevArrow: ".silhouette-arrow.previous",
      nextArrow: ".silhouette-arrow.next"
    });
    $('.silhouette-arrow').on('click', function(e) {
      e.preventDefault();
    });
  }
}
jQuery(window).on({
  load: function() {
    if ($(window).width() >= 768) {
      $(".slick-split-2-4-large").slick('unslick');
    } else {
      slickSplitLarge();
    }
  },
  resize: function() {
    if ($(window).width() >= 768) {
      $(".slick-split-2-4-large").slick('unslick');
    } else {
      slickSplitLarge();
    }
  }
});

(function() {
  jQuery(document).ready(function() {
    console.log('ROUND OUT THE PARTY JS LOADED');
    var scene7Url = 'http://img.davidsbridal.com/is/image/DavidsBridalInc/';
    var imagePreset = '?$plpproductimgdesktop_3up$';
    // On swatch click, get the color name
    $('.detail__color-swatch-item-input').on('click', function(){
      console.log('Color Swatch Click Event');
      var swatchColor = $(this).attr('swatch-value');
      // Find all images in complementary products
      var complementaryProductImages = $('.complementary-products').find('.complementary-image').find('img');
      $.each(complementaryProductImages, function() {
        var imageSrc = $(this).attr('src');
        var parts = imageSrc.split('/');
        var lastSegment = parts.pop() || parts.pop();  // handle potential trailing slash
        var segments = lastSegment.split('-');
        var path = encodeURIComponent( segments[0] +'-'+ segments[1] +'-'+ segments[2] +'-'+ swatchColor );
        var url = scene7Url + path + imagePreset;
        console.log(url);
        $(this).attr('src', url);
      });
    });
  });
})();
