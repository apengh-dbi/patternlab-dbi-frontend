// Product_satin-flower-girl-dress-with-pleated-waist-and-hem-wg1372_flower-girl-dresses

$(function() {
  var debug = 1;
  var inserted = 0;

  function log(message) {
    if(debug == 1) {
      console.log(message);
    }
  }

  if(inserted === 0) {

    // --------------------------------------------------------
    // Update Drop-Down
    // --------------------------------------------------------
    // Get current list from drop-down
    var currentDropdown = $('.detail__color-dropdown-select').find('option.soColorsClass');

    var data = [];
    $.each( currentDropdown, function(index) {
      var list = [];
      var color = $(this).attr('swatch-value');
      var classes = $(this).attr('class');
      var entitledItem = $(this).attr('entitleditem');
      list = [color, classes, entitledItem];
      data.push(list);
    });
    data.sort();
    log(data);

    // Update Drop-Down
    var options = '';
    for(var i = 0; i < data.length; i++) {
      options += '<option class="'+data[i][1]+'" value="swatch--'+data[i][0].toLowerCase()+'" data-swatch-slug="'+data[i][0].toLowerCase()+'" swatch-name="color" swatch-value="'+data[i][0]+'" entitledItem="'+data[i][2]+'" do-not-disable="color">'+data[i][0]+'</option>';
    }
    var dropdownHtml = '<option class="detail__color-dropdown-option" value="swatch--none" data-swatch-slug="none">Select color</option>' + options;
    currentDropdown.parent('select').html(dropdownHtml);


    // --------------------------------------------------------
    // Update Color Swatches
    // --------------------------------------------------------
    var colorSwatchList = $('.detail__color-swatch-item-list > .detail__color-swatch-item');

    // Get swatch-value
    var data = [];
    $.each(colorSwatchList, function(index) {
      var list = [];
      var name = $(this).find('input.detail__color-swatch-item-input').attr('swatch-value');
      nameSplit = name.split('/');
      var primary = nameSplit[0];
      var accent = nameSplit[1];
      var hex = $(this).attr('data-color');
      var classes = $(this).attr('class');
      var entitledItem = $(this).find('input.detail__color-swatch-item-input').attr('entitleditem');
      list = [name, primary, accent, hex, classes, entitledItem];
      data.push(list);
    });
    data.sort();
    log(data);


    // Get unique primary colors
    var uniquePrimaryColors = [];
    var prev;
    $.each(data, function(index) {
      var primary = data[index][1];
      if(prev != primary) {
        uniquePrimaryColors.push(primary);
      }
      prev = primary;
    });
    log(uniquePrimaryColors);


    // Hide current color swatch list
    $('.detail__color-swatch-item-list').eq(0).hide();

    // Build out new HTML
    $.each(uniquePrimaryColors, function(index) {
      var primaryColor = uniquePrimaryColors[index];
      console.log(primaryColor);

      var html = '';
      var swatchesHtml = '';
      $.each(data, function(index) {
        var name = data[index][0];
        var primary = data[index][1];
        var accent = data[index][2];
        var hex = data[index][3];
        var classes = data[index][4];
        var entitledItem = data[index][5];

        if(primary == primaryColor) {
          swatchesHtml += '<li class="'+classes+'" data-color-delivery-slugs="" data-color="'+hex+'">\
            <input class="detail__color-swatch-item-input" type="radio" name="detail-color-swatch" id="detail-color-swatch--'+name.toLowerCase()+'" value="color-swatch--'+name.toLowerCase()+'" data-swatch-slug="'+name.toLowerCase()+'" swatch-name="color" swatch-value="'+name+'" entitleditem="'+entitledItem+'" do-not-disable="color" number-of-price-breakes="1">\
            <label class="detail__color-swatch-label '+hex+' dark" for="detail-color-swatch--'+name.toLowerCase()+'" title="'+name+'">\
              <span class="detail__color-swatch-bg swatch-bg" data-swatch-slug="'+name.toLowerCase()+'" style="background-color:#'+hex+';"></span>\
              <span class="detail__color-swatch-title">'+name+'</span>\
            </label>\
            <script>\
              pdpUtils.getContrast50(\''+hex+'\');\
            </script>\
            <script>\
              switchDetailColor.setColorSwatchValues("'+name+'");\
              </script>\
            </li>';
        }
      });

      // Insert HTML
      html = '<p class="sans-m-bold">'+primaryColor+' Base</p>'+
        '<ul class="detail__color-swatch-item-list">'+
        swatchesHtml+
        '</ul>';
      $('.detail__color-swatch-item-list').last().after(html);
    });

  }
  inserted = 1;

});
