$(function() {

  var debug = 1;
  function log(message) {
    if(debug == 1) {
      console.log(message);
    }
  }

  // --------------------------------------------------------
  // Update Drop-Down
  // --------------------------------------------------------

  // Get current list from drop-down
  var currentDropdown = $('.detail__color-dropdown-select').find('option.soColorsClass');
  var primaryColors = [];
  var uniquePrimaryColors = [];
  var accentColors = [];
  var uniqueAccentColors = [];

  $.each( currentDropdown, function(index) {
    var color = $(this).attr('swatch-value');
    color = color.split('/');
    primaryColors.push(color[0]);
    accentColors.push(color[1]);
  });
  log(primaryColors);
  log(accentColors);

  // Get unique primary colors
  $.each(primaryColors, function(i, el) {
    if($.inArray(el, uniquePrimaryColors) === -1) {
      uniquePrimaryColors.push(el);
    }
  });
  uniquePrimaryColors.sort(); // Sort alphabetically
  log(uniquePrimaryColors);




  // Update Drop-Down
  var options = '';
  for(var i = 0; i < uniquePrimaryColors.length; i++) {
    options += '<option class="detail__color-dropdown-option quickShipClass soColorsClass" value="swatch--'+uniquePrimaryColors[i].toLowerCase()+'" data-swatch-slug="'+uniquePrimaryColors[i].toLowerCase()+'" swatch-name="color" swatch-value="'+uniquePrimaryColors[i]+'" entitleditem="entitledItem_10277801" do-not-disable="color">'+uniquePrimaryColors[i]+'</option>';
  }
  var dropdownHtml = '<option class="detail__color-dropdown-option" value="swatch--none" data-swatch-slug="none">Select color</option>' + options;
  currentDropdown.parent('select').html(dropdownHtml);






  // --------------------------------------------------------
  // Update Color Swatches
  // --------------------------------------------------------
  var colorSwatchList = $('.detail__color-swatch-item-list > .detail__color-swatch-item');
  console.log(colorSwatchList);

  var swatches = [];
  $.each( colorSwatchList, function(index) {
    var classes = $(this).attr('class');
    var name = $(this).find('input.detail__color-swatch-item-input').attr('swatch-value');
    var color = $(this).attr('data-color');
    var currentSwatch = []
    currentSwatch["name"] = name;
    currentSwatch["color"] = color;
    currentSwatch["classes"] = classes;
    console.log(currentSwatch);
    swatches.push(currentSwatch);
  });
  console.log(swatches);


  // Get unique accent colors
  $.each(accentColors, function(i, el) {
    if($.inArray(el, uniqueAccentColors) === -1) {
      uniqueAccentColors.push(el);
    }
  });


  uniqueAccentColors.sort(); // Sort alphabetically
  log(uniqueAccentColors);

  $.each(colorSwatchList, function(index) {

  });



  var html = '<li class="detail__color-swatch-item quickShipClass soColorsClass" data-color-delivery-slugs="" data-color="ebb8af">\
    <input class="detail__color-swatch-item-input" type="radio" name="detail-color-swatch" id="detail-color-swatch--ivory/ballet" value="color-swatch--ivory/ballet" data-swatch-slug="ivory/ballet" swatch-name="color" swatch-value="Ivory/Ballet" entitleditem="entitledItem_10919164" do-not-disable="color" number-of-price-breakes="1">\
      <label class="detail__color-swatch-label ebb8af" for="detail-color-swatch--ivory/ballet" title="Ivory/Ballet">\
        <span class="detail__color-swatch-bg swatch-bg" data-swatch-slug="ivory/ballet" style="background-color:#ebb8af;"></span>\
        <span class="detail__color-swatch-title">Ivory/Ballet</span>\
      </label>\
  </li>';







});
