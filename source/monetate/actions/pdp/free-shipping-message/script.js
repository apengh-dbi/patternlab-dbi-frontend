(function ( $ ) {
  var debug = true;
  var productId = $('#productIdForQtyPrices').val();
  var minPrice = productPriceArray[productId].usdMinPrice;

  function info(message) {
    if(debug === true) { console.log(message); }
  }

  function showMessage() {
    $('.product_free-shipping-message').show();
  }
  function hideMessage() {
    $('.product_free-shipping-message').hide();
  }

  info(minPrice);

  if(minPrice >= 100) {
    showMessage();
    info('Free Shipping');
  } else {
    hideMessage();
  }

}( jQuery ));
