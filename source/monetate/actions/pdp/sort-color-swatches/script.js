
function rgbTohsv(r, g, b) {
  r /= 255, g /= 255, b /= 255;

  var max = Math.max(r, g, b);
  var min = Math.min(r, g, b);

  var h, s, l = (max + min) / 2;

  if(max == min) {
    h = s = 0;
  } else {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

    switch(max) {
      case r: h = (g - b) / d + (g < b ? 6 : 0); break;
      case g: h = (b - r) / d + 2; break;
      case b: h = (r - g) / d + 4; break;
    }

    h /= 6;
  }

  return [ h, s, l ];
}

function rgb2hsv () {
    var rr, gg, bb,
        r = arguments[0] / 255,
        g = arguments[1] / 255,
        b = arguments[2] / 255,
        h, s,
        v = Math.max(r, g, b),
        diff = v - Math.min(r, g, b),
        diffc = function(c){
            return (v - c) / 6 / diff + 1 / 2;
        };

    if (diff == 0) {
        h = s = 0;
    } else {
        s = diff / v;
        rr = diffc(r);
        gg = diffc(g);
        bb = diffc(b);

        if (r === v) {
            h = bb - gg;
        }else if (g === v) {
            h = (1 / 3) + rr - bb;
        }else if (b === v) {
            h = (2 / 3) + gg - rr;
        }
        if (h < 0) {
            h += 1;
        }else if (h > 1) {
            h -= 1;
        }
    }
    // return {
    //     h: Math.round(h * 360),
    //     s: Math.round(s * 100),
    //     v: Math.round(v * 100)
    // };

    h = Math.round(h * 360);
    s = Math.round(s * 100);
    v = Math.round(v * 100)

    return [ h, s, v ];
}


// var colors = {
//   0: {
//     name: "Amethyst",
//     red: 66,
//     green: 38,
//     blue: 79,
//     hue: 0.7804878048780489,
//     saturation: 0.3504273504273504,
//     value: 0.22941176470588237
//   },
//   1: {
//     name: "Amethyst",
//     red: 66,
//     green: 38,
//     blue: 79,
//     hue: 0.7804878048780489,
//     saturation: 0.3504273504273504,
//     value: 0.22941176470588237
//   }
// };
// console.log(colors);

var colors = [];

$('.detail__color-swatch-item').each(function() {
  var classValues = $(this).attr('class');
  var hex = $(this).attr('data-color');

  var color = $(this).find('.detail__color-swatch-item-input').attr('swatch-value');
  var rgb = $(this).find('.detail__color-swatch-bg').css('background-color');
  rgb = rgb.split("(")[1].split(")")[0];
  rgb = rgb.split(',');
  var red = rgb[0];
  var green = rgb[1];
  var blue = rgb[2];

  var hsv = rgb2hsv(red, green, blue);
  var hue = hsv[0];
  var saturation = hsv[1];
  var value = hsv[2];

  var currentColor = {
    "classValues": classValues,
    "dataColor": hex,
    "name": color,
    "red": red,
    "green": green,
    "blue": blue,
    "hue": hue,
    "saturation": saturation,
    "value": value
  }
  //console.log(currentColor);
  colors.push(currentColor);
});

//console.log(colors);



var firstBy = (function() {
  function identity(v){return v;}
  function ignoreCase(v){return typeof(v)==="string" ? v.toLowerCase() : v;}
  function makeCompareFunction(f, opt){
    opt = typeof(opt)==="number" ? {direction:opt} : opt||{};
    if(typeof(f)!="function"){
      var prop = f;
      // make unary function
      f = function(v1){return !!v1[prop] ? v1[prop] : "";}
    }
    if(f.length === 1) {
      // f is a unary function mapping a single item to its sort score
      var uf = f;
      var preprocess = opt.ignoreCase?ignoreCase:identity;
      f = function(v1,v2) {return preprocess(uf(v1)) < preprocess(uf(v2)) ? -1 : preprocess(uf(v1)) > preprocess(uf(v2)) ? 1 : 0;}
    }
    if(opt.direction === -1) return function(v1,v2){return -f(v1,v2)};
    return f;
  }
  /* adds a secondary compare function to the target function (`this` context)
  which is applied in case the first one returns 0 (equal)
  returns a new compare function, which has a `thenBy` method as well */
  function tb(func, opt) {
    var x = typeof(this) == "function" ? this : false;
    var y = makeCompareFunction(func, opt);
    var f = x ? function(a, b) {
      return x(a,b) || y(a,b);
    }
    : y;
    f.thenBy = tb;
    return f;
  }
  return tb;
})();

colors.sort(
  firstBy(function(v1, v2) { return v1.red - v2.red })
  .thenBy(function(v1, v2) { return v1.green - v2.green })
  .thenBy(function(v1, v2) { return v1.blue - v2.blue })
);






var html = '';
for(var i = 0; i < colors.length; i++) {
  $('.detail__color-swatch-item').each(function() {
    var currentDataColor = $(this).attr('data-color');
    if(currentDataColor == colors[i].dataColor) {
      console.log(colors[i].name, colors[i].dataColor);
      var listItemHtml = $(this).html();
      console.log(listItemHtml);
      html += '<li class="'+colors[i].classValues+'" data-color-delivery-slugs data-color="'+colors[i].dataColor+'">'+listItemHtml+'</li>';
    }
  });
}

console.log(html);

$('.detail__color-swatch-item-list').html(html);
