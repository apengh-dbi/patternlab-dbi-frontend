(function ( $ ) {

  var debug = true;
  var styleNum = $('h2.detail__style-id').text();
  var imageURL = 'http://img.davidsbridal.com/is/image/' + encodeURIComponent(s7DBIData.fullImage);
  var productURL = encodeURIComponent(window.location.href);

  var button = '<style>\
  #notifyMeBtn {\
    margin-top: 8px; }\
  @media screen and (min-width: 1250px) {\
    #notifyMeBtn {\
      margin-top: 0; } }</style>\
  <button class="btn btn-secondary button button-column find-a-store-btn" type="button" name="button" id="notifyMeBtn" onclick="window.open(\'http://pages.em.davidsbridal.com/NotifyMe?StyleNum=' + styleNum + '&imageURL=' + imageURL + '&productURL=' + productURL +'\')" wairole="button" role="button" title="Item Unavailable - Notify Me" style="display: none;">\
    <p class="sans-bold-uppercase btn-link" id="productPageFindAStore" style="padding-top: 10px; padding-bottom: 10px;">\
      <span class="btn-label" aria-label="Item Unavailable - Notify Me">&#10003; Item Unavailable - Notify Me</span>\
    </p>\
  </button>';

  // INSERT BUTTON HIDDEN (only insert once)
  var buttonCount = $('#notifyMeBtn').length;
  if(buttonCount == 0) {
    $('.pdp-favorite-container').after(button);
  }

  function info(message) {
    if(debug === true) { console.log(message); }
  }

  function showButton() {
    $('#notifyMeBtn').show();
  }

  function hideButton() {
    $('#notifyMeBtn').hide();
  }


  // GA EVENT
  function GAFASEVENT() {
    $('#notifyMeBtn').click(function(){
      ga('send', 'event', 'PDP Page', styleid, 'Out of Stock Button', {nonInteraction: true});
    });
  }
  $(document).ready(function() {
    GAFASEVENT();
  });


  // CHECK FOR DISABLED COLOR SWATCHES
  function colorSwatches() {
    var outOfStockCount = 0;

    // Loop through color swatches
    $.each( $('.detail__color-swatch-item'), function() {
      var input = $(this).find('input.detail__color-swatch-item-input');
      var swatchValue = input.attr("swatch-value");
      if( input.is(':disabled') || input.hasClass('disabled') || $(this).hasClass('unAvailable') ) {
        info(swatchValue + ' - Out of stock');
        outOfStockCount++;
      } else {
        info(swatchValue + ' - In stock');
      }
      return outOfStockCount;
    });

    var isMarkedDown = $('.detail__meta-price.detail__meta-price--marked-down').text().indexOf('.99') !== -1;
    info(outOfStockCount);
    info('isMarkedDown: ' + isMarkedDown);
    if( outOfStockCount > 0 && !isMarkedDown ) {
      showButton();
    } else {
      hideButton();
    }
  }
  colorSwatches();


  // CHECK FOR DISABLED SIZE SWATCHES
  function sizeSwatches() {
    var outOfStockCount = 0;

    // Loop through size swatches
    $.each( $('.detail__size-item'), function() {
      var input = $(this).find('input.detail__size-swatch-item-input');
      var swatchValue = input.attr("swatch-value");
      if( input.is(':disabled') || input.hasClass('disabled') || $(this).hasClass('unAvailable') ) {
        info(swatchValue + ' - Out of stock');
        outOfStockCount++;
      } else {
        info(swatchValue + ' - In stock');
      }
      return outOfStockCount;
    });

    var isMarkedDown = $('.detail__meta-price.detail__meta-price--marked-down').text().indexOf('.99') !== -1;
    info(outOfStockCount);
    info('isMarkedDown: ' + isMarkedDown);
    if( outOfStockCount > 0 && !isMarkedDown ) {
      showButton();
    } else {
      hideButton();
    }
  }
  sizeSwatches();


  // COLOR SWATCH CLICKED
  $.each( $('.detail__color-swatch-item-input'), function() {
    var color = $(this).attr("swatch-value");
    $(this).on('change', function() {
      info('Color: ' + color);
      sizeSwatches();
    });
  });

  // SIZE SWATCH CLICKED
  $.each( $('.detail__size-swatch-item-input'), function() {
    var size = $(this).attr("swatch-value");
    $(this).on('change', function() {
      info('Size: ' + size);
      colorSwatches();
    });
  });

  // DROP DOWN COLOR MENU CHANGE
  $('.detail__color-dropdown-select').on('change', function() {
    info( $(this).val() );
    sizeSwatches();
  });

  // COMING SOON PRODUCTS WITHOUT SWATCHES
  if( $('.detail__meta-price-notice.titlePrefix').text() == 'Coming Soon!' ) {
    info('COMING SOON');
    showButton();
  }

}( jQuery ));
