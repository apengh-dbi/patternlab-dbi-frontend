(function ( $ ) {

  var outOfStockEspot = $('.detail__meta-form.spacing-top-one-and-half-at-large').find('.genericESpot');
  var outOfStockMessage = $('#errorMessageLarg, #errorMessageSmall');
  if ( outOfStockMessage.length > 0 ) {
    var comingSoonMessage = $('.detail__meta-price-notice.titlePrefix').text();
    if ( comingSoonMessage == 'Coming Soon!' ) {
      outOfStockEspot.hide();
      outOfStockMessage.hide();
    }
  }

}( jQuery ));
