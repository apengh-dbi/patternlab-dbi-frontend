/*
* Try.com button on PDP
* Who:
*   Device Type: Desktop
*   Browser: Chrome
*   Exclude Internal IP
*/
$(function() {
  console.log('MONETATE - TRY.COM BUTTON TEST');

  var target = $('.column.detail__meta.extra-left-gutter');
  var tryButton = $('#TRY-button-sdk');
  var html = '<div id="TRY-button-sdk" style="width:100%; display:none;"><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%;"><div id="TRY-button-sdk-button" onclick="window.open(\'https://try-com.app.link/?referralCode=davidsbridal&deeplink=\'+encodeURIComponent(window.location.href)+\'&utm_source=native_html_try_button&utm_campaign=davidsbridal_web\');" style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: column; flex: 0 0 50%; justify-content: flex-start; align-content: flex-start; width: 100%; height: 50%; border-radius: 3px; box-shadow: none;"><button type="button" style="height: auto; user-select: none; letter-spacing: normal; word-spacing: normal; text-indent: 0px; text-decoration: none; outline: 0px; cursor: pointer; width: 100%; position: relative; padding: 0px; margin: 0px; transition: background-color 250ms ease-out; border: 1px solid rgb(47, 127, 208); background-color: #6e62ee; border-radius: 3px;"><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex-shrink: 0; justify-content: space-between; align-content: flex-start; width: 100%; border-radius: 3px;"><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex: 0 0 auto; justify-content: center; align-content: flex-start; width: auto; padding-left: 10px;"><div style="width: 24px; height: 24px; background-color: transparent; border-radius: 2px; overflow: hidden; cursor: pointer;"><img style="width:24px;height:24px" src="https://s3-us-west-1.amazonaws.com/trycom/assets/favicons/favicon@2x.png"/></div></div><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex: 1 1 auto; justify-content: center; align-content: flex-start; width: 100%; padding: 10px;"><span style="margin: 0px; padding: 0px; text-decoration: none; background-color: transparent; font-size: 12px; font-family: Gotham-Book, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(255, 255, 255); line-height: 1.5; -webkit-font-smoothing: antialiased; text-transform: none; display: inline-block; font-weight: normal; width: 100%; flex: 1 1 auto; text-align: center;">Try for Free</span></div></div></button></div><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: column; flex: 1 1 auto; justify-content: flex-start; align-content: flex-start; width: 100%; height: auto;"><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: column; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%; height: auto; padding-left: 10px;"><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: row; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%;"><span style="margin: 0px; padding: 0px; text-decoration: none; background-color: transparent; font-size: 11px; font-family: Gotham-Bold, Helvetica, Arial, sans-serif; color: rgb(51, 54, 56); line-height: 1.5; -webkit-font-smoothing: antialiased; text-transform: none; display: inline-block; font-weight: bold; text-align: left;">Get 7 days to Try</span></div><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: row; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%;"><span style="margin: 0px; padding: 0px; text-decoration: none; background-color: transparent; font-size: 11px; font-family: Gotham-Book, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(144, 148, 152); line-height: 1.5; -webkit-font-smoothing: antialiased; text-transform: none; display: inline-block; font-weight: normal; text-align: left;">via Try.com</span></div></div></div></div></div>';

  // Insert Button (Hidden)
  console.log('Add Try.com Button');
  target.append(html);


  function showButton(el) {
    console.log('Show Button');
    el.show();
  }
  function hideButton(el) {
    console.log('Hide Button');
    el.hide();
  }

  function checkFinalSaleMessage() {
    var $finalSaleMessage = $('#finalSaleMessage').is(':visible');
    if($finalSaleMessage) {
      console.log('FINAL SALE MESSAGE IS VISIBLE');
    } else {
      console.log('FINAL SALE MESSAGE IS HIDDEN');
    }
  }

  function checkQuickShipProductAvail() {
    var $quickShipProductAvail = $('#quickShipProductAvail');
    if( $quickShipProductAvail.css('display') == 'block' ) {
      console.log('QUICK SHIP MESSAGE IS VISIBLE');
    } else {
      console.log('QUICK SHIP MESSAGE IS HIDDEN');
    }
  }


  // Color Dropdown Change
  $('.detail__color-dropdown-select').on('change', function() {
    console.log('COLOR DROPDOWN CHANGED');
    checkFinalSaleMessage();
    checkQuickShipProductAvail();
  });

  // Color Swatch Click
  $('.detail__color-swatch-item-input').on('click', function() {
    console.log('COLOR SWATCH CLICKED');
    checkFinalSaleMessage();
    checkQuickShipProductAvail();
  });

  // Color Swatch Click
  $('.detail__size-swatch-item-input').on('click', function() {
    console.log('SIZE SWATCH CLICKED');
    checkFinalSaleMessage();
    checkQuickShipProductAvail();
  });




}());
