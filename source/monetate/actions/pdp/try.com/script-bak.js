/*
* Try.com button on PDP
* Who:
*   Device Type: Desktop
*   Browser: Chrome
*   Exclude Internal IP
*/
$(function() {
  var target = $('.column.detail__meta.extra-left-gutter');
  var tryButtonHtml = '<div id="TRY-button-sdk" style="width:100%; display:none;"><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%;"><div id="TRY-button-sdk-button" onclick="window.open(\'https://try-com.app.link/?referralCode=davidsbridal&deeplink=\'+encodeURIComponent(window.location.href)+\'&utm_source=native_html_try_button&utm_campaign=davidsbridal_web\');" style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: column; flex: 0 0 50%; justify-content: flex-start; align-content: flex-start; width: 100%; height: 50%; border-radius: 3px; box-shadow: none;"><button type="button" style="height: auto; user-select: none; letter-spacing: normal; word-spacing: normal; text-indent: 0px; text-decoration: none; outline: 0px; cursor: pointer; width: 100%; position: relative; padding: 0px; margin: 0px; transition: background-color 250ms ease-out; border: 1px solid rgb(47, 127, 208); background-color: #6e62ee; border-radius: 3px;"><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex-shrink: 0; justify-content: space-between; align-content: flex-start; width: 100%; border-radius: 3px;"><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex: 0 0 auto; justify-content: center; align-content: flex-start; width: auto; padding-left: 10px;"><div style="width: 24px; height: 24px; background-color: transparent; border-radius: 2px; overflow: hidden; cursor: pointer;"><img style="width:24px;height:24px" src="https://s3-us-west-1.amazonaws.com/trycom/assets/favicons/favicon@2x.png"/></div></div><div style="box-sizing: border-box; display: flex; align-items: center; flex-direction: row; flex: 1 1 auto; justify-content: center; align-content: flex-start; width: 100%; padding: 10px;"><span style="margin: 0px; padding: 0px; text-decoration: none; background-color: transparent; font-size: 12px; font-family: Gotham-Book, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(255, 255, 255); line-height: 1.5; -webkit-font-smoothing: antialiased; text-transform: none; display: inline-block; font-weight: normal; width: 100%; flex: 1 1 auto; text-align: center;">Try for Free</span></div></div></button></div><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: column; flex: 1 1 auto; justify-content: flex-start; align-content: flex-start; width: 100%; height: auto;"><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: column; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%; height: auto; padding-left: 10px;"><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: row; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%;"><span style="margin: 0px; padding: 0px; text-decoration: none; background-color: transparent; font-size: 11px; font-family: Gotham-Bold, Helvetica, Arial, sans-serif; color: rgb(51, 54, 56); line-height: 1.5; -webkit-font-smoothing: antialiased; text-transform: none; display: inline-block; font-weight: bold; text-align: left;">Get 7 days to Try</span></div><div style="box-sizing: border-box; display: flex; align-items: flex-start; flex-direction: row; flex-shrink: 0; justify-content: flex-start; align-content: flex-start; width: 100%;"><span style="margin: 0px; padding: 0px; text-decoration: none; background-color: transparent; font-size: 11px; font-family: Gotham-Book, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(144, 148, 152); line-height: 1.5; -webkit-font-smoothing: antialiased; text-transform: none; display: inline-block; font-weight: normal; text-align: left;">via Try.com</span></div></div></div></div></div>';

  // Insert Button (Hidden)
  target.append(tryButtonHtml);


  function showButton() {
    $('#TRY-button-sdk').show();
  }
  function hideButton() {
    $('#TRY-button-sdk').hide();
  }

  // ========================================================
  // Show button when condition are met
  // ========================================================
  function checkButtonConditions() {

    // Loop through color swatches
    var colorsSelected = 0;
    $.each( $('.detail__color-swatch-item'), function() {
      var input = $(this).find('input.detail__color-swatch-item-input');
      if( input.is(':checked') && $(this).hasClass('quickShipClass') ) { // Checked and is not special order, i.e. has class quickShipClass
        colorsSelected++;
      }
      return colorsSelected;
    });


    // Loop through size swatches
    var sizesSelected = 0;
    $.each( $('.detail__size-item'), function() {
      var input = $(this).find('input.detail__size-swatch-item-input');
      if( input.is(':checked') ) {
        sizesSelected++;
      }
      return sizesSelected;
    });

    // Exclude "CTO Error", Final Sale Message products, Final Sale Badge
    var soCtoWrapper = $('#soCtoWrapper').is(':visible');
    var finalSaleMessage = $('#finalSaleMessage').is(':visible');
    var finalSaleBadge = $('.detail__meta-price-notice.titlePrefix').text() == 'FINAL SALE' ? true : false;

    var debug = {
      'soCtoWrapper': soCtoWrapper,
      'finalSaleMessage': finalSaleMessage,
      'finalSaleBadge': finalSaleBadge,
      'sizesSelected': sizesSelected,
      'colorsSelected': colorsSelected
    }
    console.log(debug);

    // If conditions are met, show button, else hide button
    var conditions = (sizesSelected > 0 && colorsSelected > 0 && soCtoWrapper == false && finalSaleMessage == false && finalSaleBadge == false);
    conditions ? showButton() : hideButton();
  }



  // Color swatch or size swatch click
  $('.detail__color-swatch-item-input, .detail__size-swatch-item-input').on('click', function() {
    checkButtonConditions();
  });

  // Drop down color menu change
  $('.detail__color-dropdown-select').on('change', function() {
    checkButtonConditions();
  });

  // Delivery method change, Hide button
  $('.detail__color-delivery-radio').on('click', function() {
    hideButton();
  });

}());
