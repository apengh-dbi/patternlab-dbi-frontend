console.log('MONETATE - FIT/SIZING IMPROVISATION');

// Select the node that will be observed for mutations
var targetNode = document.getElementById('BVRRSummaryContainer');

// Options for the observer (which mutations to observe)
var config = { attributes: true, childList: true };

// Callback function to execute when mutations are observed
var callback = function(mutationsList) {
  for(var mutation of mutationsList) {

    if (mutation.type == 'childList') {
      var fitSizingContainer = $('.detail__main').find('.BVRRSecondaryRatingsContainer');
      var fitSizingContainerHtml = fitSizingContainer.html();
      if(fitSizingContainerHtml != undefined) {
        fitSizingContainerHtml = '<div class="BVRRSecondaryRatingsContainer">' + fitSizingContainerHtml + '</div>';

        var target = $('.detail__fieldset.detail__attribute.detail__size');
        if( target.length > 0) {

          // Hide fitSizingContainer
          fitSizingContainer.hide();

          // Insert fitSizingContainer above "Size"
          target.before(fitSizingContainerHtml);
          target.css({
            'display': 'block',
            'position': 'relative',
            'clear': 'both',
            'padding-top': '10px'
          });
          $('.detail__main').find('.BVRROverallRatingContainer').css({
            'width': '90px',
            'margin-bottom': '10px'
          });
          $('.detail__main').find('.BVImgOrSprite').css('width', '200px');
        }
      }

      observer.disconnect();

    }
  }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

// Start observing the target node for configured mutationsList
observer.observe(targetNode, config);
