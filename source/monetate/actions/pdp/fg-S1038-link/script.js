var html = '<p class="detail__fieldset detail__view-additional-colors">\
  <a href="#" class="sans-bold-uppercase">View Additional Colors&nbsp;&gt;</a>\
</p>';
var target = $('fieldset.detail__fieldset.detail__attribute.detail__size');
target.before(html);

// Styling
$('.detail__fieldset.detail__view-additional-colors').css({
  'margin-bottom': '0'
});
$('.detail__fieldset.detail__detail__color').css({
  'margin-bottom': '0',
  'padding-bottom': '0'
});
