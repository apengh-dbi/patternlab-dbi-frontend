

// GET Associated Promotion by Product ID - REST API
// http://www.davidsbridal.com/wcs/resources/store/10052/associated_promotion?q=byProduct&qProductId=10613584&responseFormat=json

// GET Associated Promotion by Category ID - REST API
// http://www.davidsbridal.com/wcs/resources/store/10052/associated_promotion?q=byCategory&qCategoryId=3002633&responseFormat=json

$(document).ready(function() {
  $.each( $('.listing__grid-item').has('.listing__grid-item-product'), function(index) {

    var listingGridItemPromotion = $(this).find('.listing__grid-item-promotion').length;
    if(listingGridItemPromotion == 0) {
      var parent = $(this).closest('.listing__grid-item');
      var catEntryParams = parent.find('#catEntryParams').val();
      var json = JSON.stringify(eval("(" + catEntryParams + ")"));
      var obj = JSON.parse(json);
      var productId = obj.id;
      var styleNum = obj.name;


      $.ajax({
        url: 'http://www.davidsbridal.com/wcs/resources/store/10052/associated_promotion?q=byProduct&qProductId=' + productId + '&responseFormat=json',
        method: 'GET',
        dataType: 'text'
      }).success(function(response) {
        var response = response.replace(',"");', '');
        var json = JSON.stringify(eval("(" + response + ")"));
        var obj = JSON.parse(json);
        if (obj.associatedPromotions != null) {
          var promotion = obj.associatedPromotions[0].description.shortDescription;
          console.log(index+1, styleNum, promotion);
          var target = parent.find('.listing__grid-item-description');
          var html = '<div class="listing__grid-item-promotion sans-xs-bold">' + promotion + '</div>';
          target.before(html);
          $('.listing__grid-item-promotion').css({
            'display': 'block',
            'padding': '4px 9px 0',
            'color': 'rgb(170, 23, 13)',
            'margin-top': '4px',
            'margin-bottom': '-10px',
            'text-align': 'center'
          });
        }
      });
    }

  });
});



var start = 1;
var end = start + 1;
for(var i = start; i < end; i++) {
  var request = 'http://www.davidsbridal.com/wcs/resources/store/10052/storelocator/byStoreId/' + i;
  $.ajax({
    url: request,
    method: 'GET'
  }).success(function(data, textStatus, xhr) {
    var store = data.PhysicalStore[0];
    var storeNum = store.storeName;
    var address1 = store.addressLine[0];
    var address2 = store.addressLine[1];
    var city = store.city;
    var state = store.stateOrProvinceName;
    var country = store.country;
    var zip = store.postalCode;
    var latitude = store.latitude;
    var longitude = store.longitude;
    console.log(storeNum);
    console.log(address1 + ', ' + address2);
    console.log(city + ', ' + state, country, zip);
    console.log(latitude + ', ' + longitude);
  });
}





var start = Math.floor(Math.random() * 2000000);
var end = start + 10000;
for(var i = start; i <= end; i++) {
  var request = 'http://dbstage7.davidsbridal.com/?time=' + i;
  $.ajax({
    url: request,
    method: 'GET'
  }).success(function(data, textStatus, xhr) {
    console.log(xhr.status);
    console.log(textStatus);
  });
}
