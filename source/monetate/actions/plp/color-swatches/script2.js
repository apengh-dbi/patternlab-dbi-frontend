/*
*
* Add color swatches to PLP
*
*/

// REST API to get products, colors, color codes by Category ID
// http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?pageSize=30&pageNumber=1
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?pageSize=30&pageNumber=1&facet=ads_f_e3_fabric_ntk_cs:Charmeuse&facet=ads_f_colorfamily_ntk_cs:Yellow'
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?pageSize=30&pageNumber=1&filterFacet=Fabric:Charmeuse'
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?facet=ads_f1_ntk_cs%3A%22Sunset%22'
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?facet=ads_f1_ntk_cs%3A%22Apricot%22&facet=ads_f1_ntk_cs%3A%22Sunset%22'
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3002803?facet=ads_f_e3_strpsl_ntk_cs%3A%22Off+the+Shoulder%22'



console.log('MONETATE - ADD COLOR SWATCHES TO PLP');

var filterList = $('#facetFilterList');

function initInsertColorSwatches() {

  function isSearchDisplay() {
    var currentUrl = window.location.href;
    var isSearchDisplay = currentUrl.indexOf("SearchDisplay");
    return isSearchDisplay;
  }

  function getEnabledProductFacets() {
    var facetForm = document.forms['productsFacets'] != null ? document.forms['productsFacets'] : document.forms['productsFacetsHorizontal'];
    var elementArray = facetForm.elements;

    var facetArray = new Array();
    var facetIds = new Array();
    if(_searchBasedNavigationFacetContext != 'undefined') {
      for(var i=0; i< _searchBasedNavigationFacetContext.length; i++) {
        facetArray.push(_searchBasedNavigationFacetContext[i]);
        //facetIds.push();
      }
    }

    var facetLimits = new Array();
    for (var i=0; i < elementArray.length; i++) {
      var element = elementArray[i];

      if(element.type != null && element.type.toUpperCase() == "CHECKBOX" && !$(element).hasClass("select_all") ) {
        if(element.title == "MORE") {
          // scan for "See More" facet enablement.
          if(element.checked) {
            facetLimits.push(element.value);
          }
        }
        else {
          // disable the checkbox while the search is being performed to prevent double clicks
          //element.disabled = true;

          if(element.checked ) {
            if(facetArray.indexOf(element.value) == -1) {
              facetArray.push(element.value);
            }
            var id = element.id;
            var push =true;
            if(id.indexOf("_") != -1) {
              id = id.split("_")[0];
              if(facetIds.indexOf(id) != -1) {
                push = false;
              }
            }
            if(push)
            facetIds.push(id);
          }
        }
      }
    }

    var results = new Array();
    results.push(facetArray);
    results.push(facetLimits);
    results.push(facetIds);
    return results;
  }


  function getSelectedFacets() {
    var facets = getEnabledProductFacets();
    var selectedFacets = facets[0];
    var facetsURL = '';
    for(var i = 0; i < selectedFacets.length; i++) {
      var counter = i + 1;
      //console.log(selectedFacets[i]);
      if( (i + 1) === selectedFacets.length) {
        facetsURL += 'facet=' + selectedFacets[i];
      } else {
        facetsURL += 'facet=' + selectedFacets[i] + '&';
      }
    }
    return facetsURL;
  }


  function getPageNumber() {
    var pageNumber = document.getElementsByClassName('listing-pager__page-item--current')[0];
    if(pageNumber != undefined) {
      currentPage = parseInt(pageNumber.textContent);
    }
    if (isNaN(currentPage)) {
      return 1;
    } else {
      return currentPage;
    }
  }




  if( typeof SearchBasedNavigationDisplayJS != "undefined") { // Check if page is PLP

    var api = 'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/';
    var categoryId = categoryPageParams.categoryId; // Category ID
    var productsPerPage = 30;
    var currentPage = 1;
    var request = api + categoryId + '?' + 'pageSize=30&pageNumber=';

    function buildResponse() {
      // Is a '/SearchDisplay' URL
      if( isSearchDisplay() !== -1 ) {
        if(filterList.length > 0) {
          console.log('IS SEARCH DISPLAY - FILTERS SELECTED');
          request += getPageNumber() + '&' + getSelectedFacets();
        } else {
          console.log('IS SEARCH DISPLAY - NO FILTERS SELECTED');
          request += getPageNumber();
        }

      // Is not a '/SearchDisplay' URL
      } else {
        console.log('IS NOT SEARCH DISPLAY');
        request += getPageNumber();
      }
      return request;
    }
    buildResponse();


    // Submit AJAX Request
    console.log(request);
    $.ajax({
      url: request,
      method: 'GET',
      dataType: 'text'
    }).success(function(response) {
      var response = JSON.parse(JSON.stringify(eval("(" + response.replace(',"");', '') + ")")));
      var responseCount = parseInt(response.recordSetTotal);
      var listingCount = parseInt($('.listing__item-count-total').text());
      //console.log(response);
      //console.log(responseCount);
      //console.log(listingCount);

      // Check if record counts match
      if( responseCount === listingCount) {
        console.log('RESULT SET COUNT MATCHES');
        var colorFamilyIndex = 0;
        $.each( response.FacetView, function(index) {
          // console.log( response.FacetView[index].name );
          if( response.FacetView[index].name == "color" ) {
            //console.log(index);
            colorFamilyIndex = index;
            return false;
          }
          return colorFamilyIndex;
        });
        //console.log('colorFamilyIndex', colorFamilyIndex);

        var hexValues = response.FacetView[colorFamilyIndex].Entry;
        //console.log(hexValues);

        var colorNameHex = {};
        for(var i = 0; i < hexValues.length; i++) {
          var name = hexValues[i].label;
          var hex = hexValues[i].hex;
          colorNameHex[name] = hex;
        }


        // Loop through products
        $.each( $('.listing__grid-item').has('.listing__grid-item-product'), function(index) {
          //console.log(colorNameHex);

          // Products
          var product = response.CatalogEntryView[index];
          //console.log(product);
          var catentryId = product.uniqueID;
          var styleNum = product.name;
          var colorsNames = product.xcatentry_color;
          if(colorsNames != undefined) { // If product doesn't have any colors, skip
            colorsNames = colorsNames.split(';'); // Split color names to form an array
            console.log(catentryId, styleNum, colorsNames.length);

            // Generate HTML for color swatches
            var swatches = '';
            for(var i = 0; i < 4; i++) {
              if(colorsNames[i] != undefined) {
                swatches += '<li class="listing__grid-swatch" data-color="'+colorNameHex[ colorsNames[i] ]+'">';
                swatches +=   '<input class="listing__grid-swatch-input" type="radio" name="listing__grid-color-swatch" value="color-swatch--'+colorsNames[i].toLowerCase()+'" data-swatch-slug="'+colorsNames[i].toLowerCase()+'" swatch-name="color" swatch-value="'+colorsNames[i]+'">';
                swatches +=   '<label class="listing__grid-swatch-label" for="listing__grid-color-swatch--'+colorsNames[i].toLowerCase()+'" title="'+colorsNames[i]+'">';
                swatches +=     '<span class="listing__grid-swatch-bg" data-swatch-slug="'+colorsNames[i].toLowerCase()+'" style="background-color: '+colorNameHex[ colorsNames[i] ]+';"></span>';
                swatches +=     '<span class="listing__grid-swatch-title">'+colorsNames[i]+'</span>';
                swatches +=   '</label>';
                swatches += '</li>';
              }
            }
            // console.log(swatches);

            // HTML if there are move than 4 colors
            var href = $(this).find('.listing__grid-item-product-image').attr('href');
            //console.log('href', href);
            var moreColorsHTML = '';
            if(colorsNames.length > 4) {
              moreColorsHTML = '<li class="listing__grid-swatch listing__grid-swatch-plus">'+
                '<a href="'+href+'">'+
                  '<svg class="" xmlns="http://www.w3.org/2000/svg" viewBox="-494 495 11 11">'+
                    '<polygon points="-483,500 -488,500 -488,495 -489,495 -489,500 -494,500 -494,501 -489,501 -489,506 -488,506 -488,501 -483,501 "></polygon>'+
                  '</svg>'+
                '</a>'+
              '</li>';
            }

            // Generate HTML to be inserted
            var html = '<div class="listing__grid-swatches-container">'+
              '<ul class="listing__grid-swatches">'+
              swatches+
              moreColorsHTML+
              '</ul>'+
            '</div>';

            // Insert HTML
            $(this).find('.listing__grid-item-description').before(html);

          } // If product doesn't have any colors, skip
        }); // Loop through products

      } else {
        console.log('RESULT SET COUNT DO NOT MATCH');
      }

    }).fail(function() {
      console.error('AJAX REQUEST FAILED');
    }); // End AJAX Request


    var plpColorSwatchClick = null;
    function initPlpColorSwatchClick() {
      console.log('initPlpColorSwatchClick');
      var lastProductSwatchesCount = 0;

      plpColorSwatchClick = setInterval(function() {

        lastProductSwatchesCount = $('.listing__grid-item').has('.listing__grid-item-product').last().find('.listing__grid-swatches-container').length;
        if(lastProductSwatchesCount > 0) {
          clearInterval(plpColorSwatchClick);
          console.log('plpColorSwatchClick interval cleared');

          var urlPrefix = 'http://img.davidsbridal.com/is/image/DavidsBridalInc/';
          var urlSuffix = '?$plpproductimgdesktop_3up$';

          $.each( $('.listing__grid-item').has('.listing__grid-item-product'), function(index) {
            var parent = $(this).closest('.listing__grid-item');
            var catEntryParams = parent.find('#catEntryParams').val();
            var json = JSON.stringify(eval("(" + catEntryParams + ")"));
            var obj = JSON.parse(json);
            var productId = obj.id;
            var styleNum = obj.name;
            // console.log(productId, styleNum);

            $.each( $(this).find('.listing__grid-swatch-bg'), function() {
              $(this).on('click', function() {
                var windowWidth = $(window).width();
                if( windowWidth <= 768 ) {
                  urlSuffix = '?$plpproductimgmobile_1up$';
                } else if( windowWidth > 768 && windowWidth <= 1024) {
                  urlSuffix = '?$plpproductimgtablet_1up$';
                } else {
                  urlSuffix = '?$plpproductimgdesktop_3up$';
                }

                var mediaSetColor = $(this).parent('.listing__grid-swatch-label').attr('title'); // Get color name
                mediaSetColor = mediaSetColor.replace(/\//g, "_"); // Replace forward slash with underscore
                mediaSetColor = mediaSetColor.replace(/ /g,"%20"); // Replace space with URL encoded space
                //console.log(mediaSetColor);
                var mixedMediaSet = 'Set-' + styleNum + '-' + productId + '-' + mediaSetColor;
                //console.log(mixedMediaSet);
                var imageRequest = urlPrefix + mixedMediaSet + urlSuffix; // Build new img src URL
                //console.log(imageRequest);
                parent.find('img').attr('src', imageRequest); // Update img src
              });
            });
          });
        }
      }, 1000);
    }
    initPlpColorSwatchClick();

  } // Check if page is PLP
} // initInsertColorSwatches()

initInsertColorSwatches();
