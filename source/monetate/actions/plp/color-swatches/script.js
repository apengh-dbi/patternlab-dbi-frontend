// REST API to get products, colors, color codes
// http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?pageSize=30&pageNumber=1

// All Bridesmaid Dresses > Length: Tea Length
//http://www.davidsbridal.com/SearchDisplay?categoryId=3003367&storeId=10052&langId=-1#!&facet:1016584101973276101110103116104&productBeginIndex:0&orderBy:&pageView:grid&minPrice:&maxPrice:&pageSize:30&productSize:large&isPriceFacet:false&facetValues:,Tea Length&

// All Bridesmaids Dresses > Fabric:Charmeuse and Color Family: Yellow
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?pageSize=30&pageNumber=1&facet=ads_f_e3_fabric_ntk_cs:Charmeuse&facet=ads_f_colorfamily_ntk_cs:Yellow'
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?pageSize=30&pageNumber=1&filterFacet=Fabric:Charmeuse'


// Loop Through Facets
var api = 'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/';
var categoryId = categoryPageParams.categoryId; // Category ID

var facets = SearchBasedNavigationDisplayJS.getEnabledProductFacets();
var selectedFacets = facets[0];
var facetsURL = '';
for(var i = 0; i < selectedFacets.length; i++) {
  var counter = i + 1;
  console.log(selectedFacets[i]);
  if( (i + 1) === selectedFacets.length) {
    facetsURL += 'facet=' + selectedFacets[i];
  } else {
    facetsURL += 'facet=' + selectedFacets[i] + '&';
  }
}
console.log(facetsURL);
var request = api + categoryId + '?' + facetsURL;
console.log(request);
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?facet=ads_f1_ntk_cs%3A%22Sunset%22'
//'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3003367?facet=ads_f1_ntk_cs%3A%22Apricot%22&facet=ads_f1_ntk_cs%3A%22Sunset%22'
'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/3002803?facet=ads_f_e3_strpsl_ntk_cs%3A%22Off+the+Shoulder%22'


try {
  var api = 'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/';
  var categoryId = categoryPageParams.categoryId; // Category ID
  console.log('categoryId', categoryId);

  // Get Select Facets
  var facetsURL = '';
  function getSelectedFacets() {
    var facets = SearchBasedNavigationDisplayJS.getEnabledProductFacets();
    var selectedFacets = facets[0];
    //console.log(selectedFacets.length);
    if(selectedFacets.length > 0) {
      for(var i = 0; i < selectedFacets.length; i++) {
        var counter = i + 1;
        if( (i + 1) === selectedFacets.length) {
          facetsURL += 'facet=' + selectedFacets[i];
        } else {
          facetsURL += 'facet=' + selectedFacets[i] + '&';
        }
      }
    }
    return facetsURL;
  }
  getSelectedFacets();
  console.log('facetsURL length', facetsURL.length);

  // Set Request
  var request = api + categoryId;
  if(facetsURL.length > 0) {
    request += '?' + facetsURL;
  } else {
    var currentPage = 1;
    var pageNumber = $('.listing-pager__page-item-text.listing-pager__page-item--current').text();
    console.log(pageNumber.length);
    if(pageNumber.length > 0) {
      currentPage = pageNumber;
    }
    console.log('currentPage', currentPage);
    request += '?pageSize=30&pageNumber=' + currentPage;
  }

  console.log('request', request);

} catch(e) {
  console.log(e.message);
}








$(function() {
  console.log('MONETATE - ADD COLOR SWATCHES TO PLP');

  var debug = 1; // 0 -> debugging off, 1 -> degugging on

  function initInsertColorSwatches() {
    try {
      var categoryId = categoryPageParams.categoryId; // Category ID
      var currentPage = 1;
      var pageNumber = $('.listing-pager__page-item-text.listing-pager__page-item--current');
      if(pageNumber != undefined) {
        currentPage = pageNumber.text();
      }
      var url = 'http://www.davidsbridal.com/wcs/resources/store/10052/productview/byCategory/'+categoryId+'?pageSize=30&pageNumber='+currentPage;
      if(debug == 1) { console.log(url); };

      $.ajax({
        url: url,
        method: 'GET',
        dataType: 'text'
      }).success(function(response) {
        var response = JSON.parse(JSON.stringify(eval("(" + response.replace(',"");', '') + ")")));
        if(debug == 1) { console.log(response); };

        var colorFamilyIndex = 0;
        $.each( response.FacetView, function(index) {
          // console.log( response.FacetView[index].name );
          if( response.FacetView[index].name == "color" ) {
            if(debug == 1) { console.log(index); };
            colorFamilyIndex = index;
            return false;
          }
          return colorFamilyIndex;
        });
        if(debug == 1) { console.log('colorFamilyIndex', colorFamilyIndex); };

        var hexValues = response.FacetView[colorFamilyIndex].Entry;
        if(debug == 1) { console.log(hexValues); };

        var colorNameHex = {};
        for(var i = 0; i < hexValues.length; i++) {
          var name = hexValues[i].label;
          var hex = hexValues[i].hex;
          colorNameHex[name] = hex;
        }


        // Loop through products
        $.each( $('.listing__grid-item').has('.listing__grid-item-product'), function(index) {
          //console.log(colorNameHex);

          // Products
          var product = response.CatalogEntryView[index];
          var catentryId = product.uniqueID;
          var colorsNames = product.xcatentry_color;
          if(colorsNames != undefined) { // If product doesn't have any colors, skip
            colorsNames = colorsNames.split(';'); // Split color names to form an array
            if(debug == 1) { console.log(catentryId, colorsNames.length); };

            // Generate HTML for color swatches
            var swatches = '';
            for(var i = 0; i < 4; i++) {
              if(colorsNames[i] != undefined) {
                swatches += '<li class="listing__grid-swatch" data-color="'+colorNameHex[ colorsNames[i] ]+'">';
                swatches +=   '<input class="listing__grid-swatch-input" type="radio" name="listing__grid-color-swatch" value="color-swatch--'+colorsNames[i].toLowerCase()+'" data-swatch-slug="'+colorsNames[i].toLowerCase()+'" swatch-name="color" swatch-value="'+colorsNames[i]+'">';
                swatches +=   '<label class="listing__grid-swatch-label" for="listing__grid-color-swatch--'+colorsNames[i].toLowerCase()+'" title="'+colorsNames[i]+'">';
                swatches +=     '<span class="listing__grid-swatch-bg" data-swatch-slug="'+colorsNames[i].toLowerCase()+'" style="background-color: '+colorNameHex[ colorsNames[i] ]+';"></span>';
                swatches +=     '<span class="listing__grid-swatch-title">'+colorsNames[i]+'</span>';
                swatches +=   '</label>';
                swatches += '</li>';
              }
            }
            // console.log(swatches);

            // HTML if there are move than 4 colors
            var href = $(this).find('.listing__grid-item-product-image').attr('href');
            if(debug == 1) { console.log('href', href); };
            var moreColorsHTML = '';
            if(colorsNames.length > 4) {
              moreColorsHTML = '<li class="listing__grid-swatch listing__grid-swatch-plus">'+
                '<a href="'+href+'">'+
                  '<svg class="" xmlns="http://www.w3.org/2000/svg" viewBox="-494 495 11 11">'+
                    '<polygon points="-483,500 -488,500 -488,495 -489,495 -489,500 -494,500 -494,501 -489,501 -489,506 -488,506 -488,501 -483,501 "></polygon>'+
                  '</svg>'+
                '</a>'+
              '</li>';
            }

            // Generate HTML to be inserted
            var html = '<div class="listing__grid-swatches-container">'+
              '<ul class="listing__grid-swatches">'+
              swatches+
              moreColorsHTML+
              '</ul>'+
            '</div>';

            // Insert HTML
            $(this).find('.listing__grid-item-description').before(html);

          } // If product doesn't have any colors, skip
        }); // Loop through products
      }); // AJAX success



      var plpColorSwatchClick = null;
      function initPlpColorSwatchClick() {
        console.log('initPlpColorSwatchClick');
        var lastProductSwatchesCount = 0;

        plpColorSwatchClick = setInterval(function() {

          lastProductSwatchesCount = $('.listing__grid-item').has('.listing__grid-item-product').eq(29).find('.listing__grid-swatches-container').length;
          if(lastProductSwatchesCount > 0) {
            clearInterval(plpColorSwatchClick);
            console.log('plpColorSwatchClick interval cleared');

            var urlPrefix = 'http://img.davidsbridal.com/is/image/DavidsBridalInc/';
            var urlSuffix = '?$plpproductimgdesktop_3up$';

            $.each( $('.listing__grid-item').has('.listing__grid-item-product'), function(index) {
              var parent = $(this).closest('.listing__grid-item');
              var catEntryParams = parent.find('#catEntryParams').val();
              var json = JSON.stringify(eval("(" + catEntryParams + ")"));
              var obj = JSON.parse(json);
              var productId = obj.id;
              var styleNum = obj.name;
              // console.log(productId, styleNum);

              $.each( $(this).find('.listing__grid-swatch-bg'), function() {
                $(this).on('click', function() {
                  var windowWidth = $(window).width();
                  if( windowWidth <= 768 ) {
                    urlSuffix = '?$plpproductimgmobile_1up$';
                  } else if( windowWidth > 768 && windowWidth <= 1024) {
                    urlSuffix = '?$plpproductimgtablet_1up$';
                  } else {
                    urlSuffix = '?$plpproductimgdesktop_3up$';
                  }

                  var mediaSetColor = $(this).parent('.listing__grid-swatch-label').attr('title'); // Get color name
                  mediaSetColor = mediaSetColor.replace(/\//g, "_"); // Replace forward slash with underscore
                  mediaSetColor = mediaSetColor.replace(/ /g,"%20"); // Replace space with URL encoded space
                  if(debug == 1) { console.log(mediaSetColor); };
                  var mixedMediaSet = 'Set-' + styleNum + '-' + productId + '-' + mediaSetColor;
                  if(debug == 1) { console.log(mixedMediaSet); };
                  var imageRequest = urlPrefix + mixedMediaSet + urlSuffix; // Build new img src URL
                  if(debug == 1) { console.log(imageRequest); };
                  parent.find('img').attr('src', imageRequest); // Update img src
                });
              });
            });
          }
        }, 1000);
      }
      initPlpColorSwatchClick();


    } catch(e) {
      console.log(e.message);
    }
  }
  initInsertColorSwatches();
});
