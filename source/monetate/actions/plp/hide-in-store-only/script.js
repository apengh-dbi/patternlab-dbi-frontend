$.each( $('.listing__grid-item'), function() { // Loop through products
  var comingSoonText = $(this).find('.grid-item-product__meta > .listing__grid-item-coming-soon').text(); // Search for 'COMING SOON' text
  console.log(comingSoonText);
  if( comingSoonText === 'COMING SOON') {
    $('.listing__grid-item-availability[data-listing-item-availability-slug="instore-only"]').hide(); // if 'coming soon', hide in-store only
  }
});




var removeInStoreOnly = null;
function initRemoveInStoreOnly() {
  console.log('MONETATE - REMOVE IN-STORE ONLY FOR COMING SOON');

  removeInStoreOnly = setInterval(function() {
    var inStoreOnlyTextCount = 0;

    $.each( $('.listing__grid-item'), function() { // Loop through products
      var comingSoonText = $(this).find('.grid-item-product__meta > .listing__grid-item-coming-soon').text(); // Search for 'COMING SOON' text
      if( comingSoonText == 'COMING SOON') {
        console.log('COMING SOON');
        var inStoreOnlyText = $('.listing__grid-item-availability[data-listing-item-availability-slug="instore-only"]');
        if(inStoreOnlyText.is(":visible")) {
          console.log('In-Store Only is visible');
          inStoreOnlyText.hide(); // if 'coming soon', hide in-store only
          inStoreOnlyTextCount++;
        }
      }
    });

    console.log(inStoreOnlyTextCount);
    if(inStoreOnlyTextCount == 0) {
      clearInterval(removeInStoreOnly);
    }

  }, 1000);
}
initRemoveInStoreOnly();
