'use strict';

// API Key: caM5iAOQAcSOWx0vOkU41xOJsFLjxeBlvVVjrWj4VnWWw
// http://api.bazaarvoice.com/data/products.json?apiversion=5.4&passkey=caM5iAOQAcSOWx0vOkU41xOJsFLjxeBlvVVjrWj4VnWWw&stats=reviews&filter=id:10277801,10278110

$(function () {
  console.log('MONETATE | ADD BV REVIEWS TO PLP');

  var reviewsAdded = $('.listing__grid-item-rating').length;
  if (reviewsAdded === 0) {

    // Variables
    var passkey = 'caM5iAOQAcSOWx0vOkU41xOJsFLjxeBlvVVjrWj4VnWWw';
    var api = 'http://api.bazaarvoice.com/data/products.json?apiversion=5.4&passkey=' + passkey + '&stats=reviews&limit=30&sort=isActive:asc&filter=id:';
    //console.log(api);

    // ========================================================
    // Get all Product IDs
    // ========================================================
    var productIdArray = [];
    $('.listing__grid-item').has('.listing__grid-item-product').each(function () {
      var parent = $(this).closest('.listing__grid-item');
      var catEntryParams = parent.find('#catEntryParams').val();
      var json = JSON.stringify(eval("(" + catEntryParams + ")"));
      var obj = JSON.parse(json);
      var productId = parseInt(obj.id);
      productIdArray.push(productId); // Push to array
    });
    var productIdString = productIdArray.toString(); // Convert to string, separated by commas
    //console.log(productIdString);


    // ========================================================
    // Send API AJAX request
    // ========================================================
    var request = api + productIdString;
    //console.log(request);
    $.ajax({
      url: request,
      method: 'GET'
    }).success(function (data) {
      var results = data.Results;
      //console.log(results);

      // --------------------------------------------------------
      // Loop through PLP products again
      // Get Product ID
      // Loop through BV Ratings result set
      // If product IDs match and rating is not null, insert rating
      // --------------------------------------------------------
      $('.listing__grid-item').has('.listing__grid-item-product').each(function () {
        // Loop through products, excluding inline espots
        var parent = $(this).closest('.listing__grid-item');
        var catEntryParams = parent.find('#catEntryParams').val();
        var json = JSON.stringify(eval("(" + catEntryParams + ")"));
        var obj = JSON.parse(json);
        var productId = parseInt(obj.id); // Get product Id as integer
        //console.log(productId);

        for (var i = 0; i < results.length; i++) {
          // Loop through BV ratings
          var id = results[i].Id;
          var rating = results[i].ReviewStatistics.AverageOverallRating;
          var totalReviews = results[i].TotalReviewCount;
          if (productId == id && rating != null) {
            // If product Ids match AND rating is not null, insert rating
            //console.log(productId, id, rating);
            var roundedRating = Math.round(rating * 10) / 10; // Round rating to 1 decimal
            var width = Math.round(roundedRating / 5 * 100 * 10) / 10;
            //console.log(width);
            var html = '<div class="listing__grid-item-rating">\
              <div class="listing__grid-item-rating-stars" title="' + roundedRating + ' out of 5 stars">\
                <div class="listing__grid-item-rating-stars-top" style="width: ' + width + '%;"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>\
                <div class="listing__grid-item-rating-stars-bottom"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>\
              </div>\
              <div class="listing__grid-item-rating-reviews">(' + totalReviews + ')</div>\
            </div>';
            $(this).find('.listing__grid-item-description').before(html);
          }
        }
      });
    }).error(function () {
      console.error('AJAX ERROR');
    });
  }
}());
