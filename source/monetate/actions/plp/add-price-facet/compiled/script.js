'use strict';

// ========================================================
// Price Slider
// ========================================================

// Lower
$('input[name="listing__price-range-low-input"]').attr('value', '100').change();

// Upper
$('input[name="listing__price-range-high-input"]').val();
