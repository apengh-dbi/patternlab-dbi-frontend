(function($) {
  $(document).ready(function() {
    console.log('MONETATE - HIDE "NEW" BADGE ON PLP');

    var urlParamter = '?newBadge=removed';
    var $listingGridItems = $('.listing__grid-item').has('.listing__grid-item-product');

    function addParameter(link, href, parameter) {
      href = href + parameter;
      link.attr('href', href);
    }

    // Loop through products to see if it has a "New" badge
    $listingGridItems.each(function(index) {
      if( $(this).find('.listing__grid-item-coming-soon') !== undefined && $(this).find('.listing__grid-item-coming-soon').text().trim() == 'NEW' ) {

        // Hide "New" badge
        $(this).find('.listing__grid-item-coming-soon').hide();

        // Append URL parameter to image link
        var imageLink = $(this).find('.listing__grid-item-product-image');
        var imageHref = imageLink.attr('href');
        addParameter(imageLink, imageHref, urlParamter);

        // Append URL paramter to description link
        var descriptionLink = $(this).find('.listing__grid-item-description > a');
        var descriptionHref = descriptionLink.attr('href');
        addParameter(descriptionLink, descriptionHref, urlParamter);
      }
    });

  });
}(jQuery));
