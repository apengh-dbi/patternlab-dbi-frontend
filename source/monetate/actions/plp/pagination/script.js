(function ( $ ) {
  var debug = true;
  var $listingGridSelect = $('#listing__grid-select');
  var $listingPager = $('.listing-pager');
  var $listingPagerControls = $('.listing-pager__controls');

  // Add message above controls
  var message = '<p class="listing-pager__message serif-m" style="display:none;">Shop more great styles</p>';
  $listingPagerControls.before(message);

  function info(message) {
    if(debug === true) { console.log(message); }
  }

  function getProductAndEspotCount() {
    var count = $('.listing__grid-item').length;
    count = parseInt(count);
    info('Product Count: ' + count);
    return count;
  }


  function getLayoutSize() {
    var layout = 3;
    var currentLayout = $('.listing__grid-layout').attr('data-listing-current-large-layout');
    if(currentLayout == 'large-smaller-layout') {
      layout = 4;
    }
    layout = parseInt(layout);
    info('Layout: ' + layout);
    return layout;
  }

  function checkProductCount() {
    var remainder = getProductAndEspotCount() % getLayoutSize();
    info('Remainder: ' + remainder);
    if(remainder !== 0) {
      $listingPager.addClass('centered');
      $('.listing-pager__message').show();
    } else if (remainder === 0) {
      $listingPager.removeClass('centered');
      $('.listing-pager__message').hide();
    }
    return remainder;
  }

  $(document).ready(function() {
    checkProductCount();
  });


  var targetNode = document.getElementById('listingGridLayout');
  var config = {
    attributes: true,
    childList: false,
    characterData: false
  };

  var callback = function(mutationsList) {
    for(var mutation of mutationsList) {
      if (mutation.type == 'attributes') {
        info('The ' + mutation.attributeName + ' attribute was modified.');
        checkProductCount();
      }
    }
  };

  var observer = new MutationObserver(callback);
  observer.observe(targetNode, config);

}( jQuery ));
