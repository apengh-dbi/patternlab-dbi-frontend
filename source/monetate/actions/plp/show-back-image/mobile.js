// http://img.davidsbridal.com/is/image/DavidsBridalInc/Set-VW360214-10277801-Amethyst?req=set,json


var documentHeight = document.body.scrollHeight;
var windowHeight = window.innerHeight;
var counter = 0;
var urlPrefix = 'http://img.davidsbridal.com/is/image/DavidsBridalInc/';
var urlSuffix = '?$plpproductimgmobile_1up$';


function initSlickSlider() {
  $.each( $('.listing__grid-item').has('.listing__grid-item-product').find('.listing__grid-item-product-image'), function(index) {

    var slickInitialized = $(this).find('.slick').length;
    if( slickInitialized === 0 && $(this).find('img').attr('src') != undefined ) {

      var img = $(this).find('img');
      var imgSrc = img.attr('src');

      var parent = $(this).closest('.listing__grid-item');
      var catEntryParams = parent.find('#catEntryParams').val();
      var json = JSON.stringify(eval("(" + catEntryParams + ")"));
      var obj = JSON.parse(json);
      var productId = obj.id;
      var mixedMediaSet = obj.image;
      var styleNum = obj.name;

      $.ajax({
        url: mixedMediaSet + '?req=set,json',
        method: 'GET',
        dataType: 'text'
      }).success(function(response) {
        var response = response.replace('/*jsonp*/s7jsonResponse(', '');
        var response = response.replace(',"");', '');
        var json = JSON.stringify(eval("(" + response + ")"));
        var obj = JSON.parse(json);
        if (obj.set.item.length != undefined) {
          var backImage = obj.set.item[1].i.n;
          backImage = backImage.replace('DavidsBridalInc/', '');
          backImage = urlPrefix + backImage + urlSuffix;

          // Remove current HTML
          parent.find('.listing__grid-item-product-image').html('');

          // Build HTML
          var imgAlt = img.attr('alt');
          var imgTitle = img.attr('title');
          var html = '<div class="listing__grid-carousel-'+productId+'">'+
            '<div class="slick" data-slick=\'{"prevArrow": ".carousel-prev-'+productId+'", "nextArrow": ".carousel-next-'+productId+'" }\'>'+
              '<div>'+
                '<img class="box-shadow" alt="'+imgAlt+'" title="'+imgTitle+'" src="'+imgSrc+'">'+
              '</div>'+
              '<div class="slick-slide">'+
                '<img class="box-shadow" alt="'+imgAlt+'" title="'+imgTitle+'" src="'+backImage+'">'+
              '</div>'+
            '</div>'+
            '<div class="listing__grid-carousel-buttons">'+
              '<a href="#" class="carousel-button carousel-prev-'+productId+'">'+
                '<svg class="carousel-button-prev" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="62px" height="62px" viewBox="5.5 -5.5 62 62" style="enable-background:new 5.5 -5.5 62 62;" xml:space="preserve">'+
                  '<polygon class="carousel-buttons-fill" points="39.6,11.5 41.7,13.7 29,25.5 41.7,37.3 39.6,39.5 24.6,25.5 " />'+
                  '<path class="carousel-buttons-fill" d="M36.5-5C53.4-5,67,8.7,67,25.5S53.4,56,36.5,56S6,42.4,6,25.5S19.7-5,36.5-5z M36.5,53C51.7,53,64,40.7,64,25.5 S51.7-2,36.5-2S9,10.3,9,25.5S21.3,53,36.5,53z" />'+
                '</svg>'+
              '</a>'+
              '<a href="#" class="carousel-button carousel-next-'+productId+'">'+
                '<svg class="carousel-button-next" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="62px" height="62px" viewBox="5.5 -5.5 62 62" style="enable-background:new 5.5 -5.5 62 62;" xml:space="preserve">'+
                  '<polygon class="carousel-buttons-fill" points="33.4,39.5 31.4,37.3 44.1,25.5 31.4,13.7 33.4,11.5 48.5,25.5 " />'+
                  '<path class="carousel-buttons-fill" d="M36.5,56C19.7,56,6,42.4,6,25.5S19.7-5,36.5-5S67,8.7,67,25.5S53.4,56,36.5,56z M36.5-2C21.3-2,9,10.3,9,25.5 S21.3,53,36.5,53S64,40.7,64,25.5S51.7-2,36.5-2z" />'+
                '</svg>'+
              '</a>'+
            '</div>'+
          '</div>';
          parent.find('.listing__grid-item-product-image').html(html);

          // Initialize Slick Carousel
          $('.listing__grid-carousel-'+productId).find(".slick").slick({
            slidesToShow: 1,
            slidesToScroll: 1
          });
          $('.listing__grid-carousel-'+productId).find('a.carousel-button').on('click', function(event) {
            event.preventDefault();
          });
        }
      }); // end AJAX
    }

  });
}
initSlickSlider();

// On Scroll
$(window).on('scroll', function() {
  var scrollPosition = $(window).scrollTop(); // Get scroll position
  var scrollFactor = scrollPosition / windowHeight;
  if( scrollFactor >= counter) {
    counter++;
    initSlickSlider();
  }
});

// View Smaller/View Larger Click
$('.listing__grid-select').find('a').on('click', function() {
  $.each( $('.listing__grid-item').find('.slick'), function() {
    var slickWidth = Math.ceil( $(this).width() );
    var slickTrackWidth = Math.floor(slickWidth * 4);
    var slickTrackTransform = Math.floor(slickWidth * 2);
    $(this).find('.slick-track').css({
      'width': slickTrackWidth + 'px',
      'transform': 'translate3d(-'+slickTrackTransform+'px, 0px, 0px)'
    });
    $(this).find('.slick-slide').css('width', slickWidth+'px');
  });
});

require(["dojo/request", "dojo/request/notify"], function(request, notify){
  // DOJO AJAX Call - Done
  notify("done", function(responseOrError){
    // If error, log error
    if(responseOrError instanceof Error){
      console.log('Dojo - Notify - Failed');
    } else {
      console.log('Dojo - Notify - Success');
      initSlickSlider();
    }
  });
});
