// http://img.davidsbridal.com/is/image/DavidsBridalInc/Set-VW360214-10277801-Amethyst?req=set,json

function productImageSwap() {
  console.log('productImageSwap');
  var urlPrefix = 'http://img.davidsbridal.com/is/image/DavidsBridalInc/';
  var urlSuffix = '?$plpproductimgdesktop_3up$';

  $('.listing__grid-item').has('.listing__grid-item-product').find('.listing__grid-item-product-image').hover(

    // Mouse In
    function() {
      var img = $(this).find('img');
      var originalImgSrc = img.attr('src'); // Get original image source

      var parent = $(this).closest('.listing__grid-item');
      var catEntryParams = parent.find('#catEntryParams').val();
      var json = JSON.stringify(eval("(" + catEntryParams + ")"));
      var obj = JSON.parse(json);
      var productId = obj.id;
      var mixedMediaSet = obj.image;
      var styleNum = obj.name;

      if( img.attr('data-alt-src') === undefined ) { // check if data-alt-src attribute is set on the img

        $.ajax({
          url: mixedMediaSet + '?req=set,json',
          method: 'GET',
          dataType: 'text'
        }).success(function(response) {
          var response = response.replace('/*jsonp*/s7jsonResponse(', '');
          var response = response.replace(',"");', '');
          var json = JSON.stringify(eval("(" + response + ")"));
          var obj = JSON.parse(json);
          if (obj.set.item.length != undefined) {
            var backImage = obj.set.item[1].i.n;
            backImage = backImage.replace('DavidsBridalInc/', '');
            backImage = urlPrefix + backImage + urlSuffix;
            img.attr('data-alt-src', originalImgSrc);
            img.attr('src', backImage);
          }
        });

      } else {
        var img = $(this).find('img');
        var imgSrc = img.attr('src');
        var dataAltSrc = img.attr('data-alt-src');
        img.attr('src', dataAltSrc);
        img.attr('data-alt-src', imgSrc);
      }

    },

    // Mouse Out
    function() {
      var img = $(this).find('img');
      var imgSrc = img.attr('src');
      var dataAltSrc = img.attr('data-alt-src');
      img.attr('src', dataAltSrc);
      img.attr('data-alt-src', imgSrc);
    }

  );
}

jQuery(document).ready(function() {
  var target = $('.productListingWidget');
  if( target.length > 0 ) {
    productImageSwap();
  }
});



require(["dojo/request", "dojo/request/notify"], function(request, notify){
  // DOJO AJAX Call - Done
  notify("done", function(responseOrError){
    // If error, log error
    if(responseOrError instanceof Error){
      console.log('Dojo - Notify - Failed');
    } else {
      console.log('Dojo - Notify - Success');
      productImageSwap();
    }
  });
});
