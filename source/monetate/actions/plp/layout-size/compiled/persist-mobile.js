'use strict';

$(function () {

  if (typeof categoryPageParams != "undefined") {

    // ========================================================
    // Functions
    // ========================================================
    var setCookie = function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    var getCookie = function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    };

    // ========================================================
    // Check if cookie is not set, set to default value
    // ========================================================


    // ========================================================
    // Variables
    // ========================================================
    var categoryId = categoryPageParams.categoryId; // Category ID / PLP
    var cookie = 'listingGridMobile';
    var defaultSize = 'larger';
    var days = 30;if (!getCookie(cookie)) {
      setCookie(cookie, defaultSize, days);
    }

    var visibleOption = $('.listing__grid-select').find('a:visible').attr('class');

    // Page loads
    // View is smaller
    // Cookie is larger
    // Change to larger
    if (visibleOption.indexOf('larger') > 0 && getCookie(cookie) == 'larger') {
      $('.listing__grid-select').find('.listing__grid-select-larger.listing__grid-select-larger--small-layout').click();

      // Page loads
      // View is larger
      // Cookie is smaller
      // Change to smaller
    } else if (visibleOption.indexOf('smaller') > 0 && getCookie(cookie) == 'smaller') {
      $('.listing__grid-select').find('.listing__grid-select-smaller.listing__grid-select-smaller--small-layout').click();
    }

    // ========================================================
    // On View Smaller/View Larger click, set cookie
    // ========================================================
    $('.listing__grid-select').find('a').on('click', function () {
      var className = $(this).attr('class');
      var size = '';
      if (className.indexOf('smaller') > 0) {
        size = 'smaller';
      } else if (className.indexOf('larger') > 0) {
        size = 'larger';
      }
      setCookie(cookie, size, 1);
    });
  }
}());
