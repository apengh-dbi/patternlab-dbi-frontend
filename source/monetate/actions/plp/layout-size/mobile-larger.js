/*
*
* Phase 2 of the Default to View Smaller Test
*
* Control: Defaults Larger - does not retain preference (current production)
*
* Test A: Defaults Larger - switching preference is retained throughout site
*
* Test B: Defaults Smaller - switching preference is retained throughout site
*
*/


$(function() {
  if( utag_data["page_type"] == 'L3' ) {
    var cookie = 'listingGridMobile';
    var defaultSize = 'larger';

    function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+ d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    // Check if cookie is not set, set to default value
    if( !getCookie(cookie) ) {
      setCookie(cookie, defaultSize, 1);
    }

    var visibleOption = $('.listing__grid-select').find('a:visible').attr('class');
    // Page loads, view is smaller, cookie is larger, change to larger
    if( visibleOption.indexOf('larger') > 0 && getCookie(cookie) == 'larger') {
      $('.listing__grid-select').find('.listing__grid-select-larger.listing__grid-select-larger--small-layout').click();

    // Page loads, view is larger, cookie is smaller, change to smaller
    } else if( visibleOption.indexOf('smaller') > 0 && getCookie(cookie) == 'smaller') {
      $('.listing__grid-select').find('.listing__grid-select-smaller.listing__grid-select-smaller--small-layout').click();
    }


    // On View Smaller/View Larger click, set cookie
    $('.listing__grid-select').find('a').on('click', function() {
      var className = $(this).attr('class');
      var size = '';
      if( className.indexOf('smaller') > 0 ) {
        size = 'smaller';
      } else if( className.indexOf('larger') > 0 ) {
        size = 'larger';
      }
      console.log(className);
      console.log(size);
      setCookie(cookie, size, 1);
    });

  }
});
