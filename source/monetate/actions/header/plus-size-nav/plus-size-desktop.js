(function($) {

  var plusSizeTrigger = '<span class="header__main-nav-trigger-text-topic" data-trigger-topic="PLUSSIZE">Plus Size</span>\
  <style>\
    [data-menu-active-topic=\'PLUSSIZE\'] .header__main-nav-trigger-text-topic[data-trigger-topic=\'PLUSSIZE\'] {\
      display: block; }\
    @media screen and (max-width: 1023px) {\
      [data-menu-active-topic=\'PLUSSIZE\'] .header__main-nav-topic[data-menu-topic=\'PLUSSIZE\'] {\
        display: block; }\
      [data-menu-active-topic=\'PLUSSIZE\'] [data-menu-topic=\'PLUSSIZE\'] .header__main-nav-topic-section-list {\
        display: block;\
        padding-left: 0; } }\
    @media screen and (min-width: 1024px) {\
      [data-topic-status=\'open\'] .header__main-nav-topic-heading-link,\
      [data-menu-active-topic=\'PLUSSIZE\'] [data-menu-topic=\'PLUSSIZE\'] .header__main-nav-topic-heading-link {\
        background-color: #ffffff;\
        border-left: 1px solid #74736d;\
        border-right: 1px solid #74736d;\
        border-top: 3px solid #F3665C; } }\
  </style>';

  var plusSizeMenu = '<li class="header__main-nav-topic" data-menu-topic="PLUSSIZE" data-topic-status="closed" role="menuitem">\
    <script>allL1Cats.push("PlusSize");</script>\
    <div class="header__main-nav-topic-heading">\
      <a class="header__main-nav-topic-heading-link sans-bold-uppercase" href="/plussize">Plus Size</a>\
    </div>\
    <ul class="header__main-nav-topic-section-list">\
      <li class="hide-at-large">\
        <a class="header__main-nav-topic-section-link--main sans-bold-uppercase" href="#" data-menu-back="">Main Menu</a>\
      </li>\
      <li class="header__main-nav-topic-section-link--all sans-bold-uppercase hide-at-large">\
        <a class="sans-bold-uppercase" href="/plussize">Plus Size&nbsp;Homepage</a>\
      </li>\
      <li class="header__main-nav-topic-section header__main-explore-nav-topic-section spacing-half" espot-name="ES_EXPLORE_PLUSSIZE">\
        <div class="header__main-nav-topic-section-heading serif-m-italic--main-nav-topic-section-heading serif-xs-italic">\
          <a class="header__main-nav-topic-section-heading-link">Explore</a>\
        </div>\
        <ul>\
          <li><a href="/Content_Bridal_dbwomanabout">Plus Size Guide</a></li>\
          <li><a href="/Content_StyleandFashionGuide_WeddingOuterwearGuide">Outerwear Guide</a></li>\
          <li><a href="/Content_StyleandFashionGuide_styleguideenhanceyourshape">Slips &amp; Shapewear Guide</a></li>\
          <li><a href="/Content_StyleandFashionGuide_WeddingDressesGuide">Wedding Dresses 101</a></li>\
          <li><a href="/Content_StyleandFashionGuide-styleguidedressguide_index">Silhouette Guide</a></li>\
          <li><a href="/Content_StyleandFashionGuide_WeddingPlanningGuide">Wedding Planning Guide</a></li>\
          <li><a href="/Content_Catalog_onlinecatalog">Look Books</a></li>\
          <li><a href="/Content_StyleandFashionGuide_RealWeddings">Real Weddings</a></li>\
        </ul>\
      </li>\
      <li class="header__main-nav-topic-section spacing-half">\
        <div class="header__main-nav-topic-section-heading serif-m-italic--main-nav-topic-section-heading serif-xs-italic">\
          <a class="header__main-nav-topic-section-heading-link">Dresses</a>\
        </div>\
        <ul class="header__main-nav-topic-section-item-list">\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/wedding-dresses/plus-size-wedding-dresses"> Plus Size Wedding Dresses</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/bridesmaid-dresses/plus-size-bridesmaid-dresses"> Plus Size Bridesmaid Dresses</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/plus-size-mother-of-the-bride-dresses"> Plus Size Mother of the Bride</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/plus-size-party-dresses"> Plus Size Party Dresses</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/prom-dresses/plus-size-prom-dresses"> Plus Size Prom</a>\
          </li>\
        </ul>\
      </li>\
      <li class="header__main-nav-topic-section spacing-half">\
        <div class="header__main-nav-topic-section-heading serif-m-italic--main-nav-topic-section-heading serif-xs-italic">\
          <a class="header__main-nav-topic-section-heading-link">Extended Sizes</a>\
        </div>\
        <ul class="header__main-nav-topic-section-item-list">\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/shapewear-and-slips"> Slips & Shapewear</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/bras"> Bras</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/shoes/wide-width-shoes"> Wide Shoes</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/wedding-robes-clothing-gifts"> Robes</a>\
          </li>\
        </ul>\
      </li>\
      <li class="header__main-nav-topic-section spacing-half">\
        <div class="header__main-nav-topic-section-heading serif-m-italic--main-nav-topic-section-heading serif-xs-italic">\
          <a class="header__main-nav-topic-section-heading-link">Accessories </a>\
        </div>\
        <ul class="header__main-nav-topic-section-item-list">\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/veils"> Veils</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/hair-accessories"> Hair Accessories</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/jewelry"> Jewelry</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/handbags"> Handbags</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/sashes-and-belts"> Sashes & Belts</a>\
          </li>\
          <li class="header__main-nav-topic-section-item">\
            <a class="header__main-nav-topic-section-item-link sans-xs" href="/accessories/jackets-and-wraps"> Jacket & Wraps</a>\
          </li>\
        </ul>\
      </li>\
      <li class="header__main-nav-topic-section spacing-half" espot-name="ES_SWIMLANE_PLUSSIZE">\
        <figure class="show-at-large header__main-nav-topic-section-figure spacing" data-swimlane-category="PlusSize">\
          <a href="/Content_StyleandFashionGuide_styleguideenhanceyourshape" data-swimlane-category="PlusSize" data-swimlane-position="1">\
            <img class="header__main-nav-topic-section-image" src="https://sb.monetate.net/img/1/564/1588008.jpg" alt="ride holding dress wearing slip" nopin="nopin">\
            <figcaption class="sans-bold-uppercase">Slips &amp; Shapewear Guide&nbsp;&gt;</figcaption>\
          </a>\
        </figure>\
      </li>\
    </ul>\
  </li>';

  $(document).ready(function() {
    $('.header__main-nav-trigger-text-topic[data-trigger-topic="DRESSES"]').after(plusSizeTrigger);
    $('.header__main-nav-topic[data-menu-topic="DRESSES"]').after(plusSizeMenu);

    $('li.header__main-nav-topic[data-menu-topic="PLUSSIZE"]').hover(
      function() {
        $(this).attr('data-topic-status', 'open');
      },
      function() {
        $(this).attr('data-topic-status', 'closed');
      }
    );
  });

}(jQuery));
