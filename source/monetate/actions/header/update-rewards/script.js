(function($) {

  $('[data-trigger-topic="Rewards"]').text('Plan & Save');
  
  // Desktop
  $('.header__business-nav-link-item[data-menu-topic="Rewards"] > a').text('Plan & Save');
  $('div.header__business-nav-link-dropdown[data-dropdown-menu="Rewards"] > ul.drop-down-menu > li.drop-down-menu-item  > a[href="/Content_WeddingPlanning_main"]').text('All Plan & Save');
  
  // Mobile
  $('.header__main-nav-topic-business[data-menu-topic="Rewards"] > ul.header__main-nav-topic-section-list > li.header__main-nav-topic-section-link--all > a').text('Plan & Save Homepage');
  $('.header__main-nav-topic-business[data-menu-topic="Rewards"] > ul.header__main-nav-topic-section-list > li.header__main-nav-topic-section > div.header__main-nav-topic-section-heading >  a.header__main-nav-topic-section-heading-link').text('Plan & Save');
  $('li.header__main-nav-topic-business[data-menu-topic="Rewards"] > ul.header__main-nav-topic-section-list > li.header__main-nav-topic-section > ul.header__main-nav-topic-business-section-item-list > li.header__main-nav-topic-section-item > a.header__main-nav-topic-section-item-link[href="/Content_WeddingPlanning_main"]').text('All Plan & Save');

}(jQuery));



// Drop Down Clicks - Mobile
// var dropDownLinks = $('li.header__main-nav-topic-business[data-menu-topic="Rewards"] > ul.header__main-nav-topic-section-list > li.header__main-nav-topic-section > ul.header__main-nav-topic-business-section-item-list > li.header__main-nav-topic-section-item > a.header__main-nav-topic-section-item-link');
// $.each(dropDownLinks, function(index) {
//   console.log(index, $(this).text());
//   $(this).on('click', function(event) {
//     event.preventDefault();
//     alert('Dropdown link clicked');
//   });
// });

// Drop Down Clicks - Desktop
// var DesktopLinks = $('div.header__business-nav-link-dropdown[data-dropdown-menu="Rewards"] > ul.drop-down-menu > li.drop-down-menu-item  > a');
// $.each(DesktopLinks, function(index) {
//   console.log(index, $(this).text());
//   $(this).on('click', function(event) {
//     event.preventDefault();
//     alert('Dropdown link clicked');
//   });
// });