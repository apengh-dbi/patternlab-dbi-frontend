(function($){
  var column = 3;
  var target = $('#contentRecommendationWidget_1_-2012_3074457345618265117').find('div.left_espot').find('div.layout-split-3').find('div.column').eq(parseInt(column - 1));

  var imageUrl = 'https://sb.monetate.net/img/1/564/1637218.jpg';
  var text = 'Introducing Tadashi Shoji Bridal';
  var cta = 'SHOP CASUAL WEDDING & RECEPTION DRESSES >';

  // Update Image
  target.find('.media-block-img').attr('src', imageUrl);

  // Update Text
  target.find('.media-block-content__title').text(text);

  // Update CTA
  target.find('.media-block-content__cta').text(cta);
}(jQuery));
