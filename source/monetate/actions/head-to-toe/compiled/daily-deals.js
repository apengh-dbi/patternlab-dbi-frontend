'use strict';

var offerBoxes = $('.offerBox');
var offers = ["FREE SHIPPING on All Orders $100 & Up"];

var html = '<div class="offerDate">Ends 4/8/18</div>\
  <div class="offerHead"><a href="/Content_BuyOnline_freeshipfreereturns">Free Shipping, No Minimum, and Free Returns</a></div>\
  <div class="offerBody">Discount applied automatically at checkout.</div>\
  <div class="offerFoot">Discount applied automatically at checkout. Receive free standard ground shipping and return shipping in the continental US for orders placed from Thursday, April 5, 2018 from 12:00 am ET to Sunday, April 8, 2018 11:59 pm ET at davidsbridal.com only. This offer cannot be applied to any prior purchases or returns. Free shipping offer is not valid for orders placed outside of the continental US (including AK and HI). <a href="/Content_BuyOnline_freeshipfreereturns">See additional Shipping and Returns Information</a>.</div>';

$.each(offerBoxes, function (index) {
  var message = $(this).find('.offerHead').text();
  if (offers.indexOf(message) !== -1) {
    $(this).html(html);
  }
});
