//********************************************************************/
/* Licensed Materials - Property of CrossView						 */
/*																     */
/* (C) Copyright CrossView Inc 2013 All Rights Reserved.			 */
/*																	 */
//********************************************************************/

/**
 * @fileOverview This javascript is used by TrackOrderDisplay.jsp.
 * @version 1.0
 * @author Ashutosh Srivastav
 */

  /* Import dojo classes. */
dojo.require("wc.service.common");

OrderLookup = {
    /**
     * This function validates the track order form and submits.
     */
    submitForm: function (form) {
        reWhiteSpace = new RegExp(/^\s+$/);

        if(form.orderNumber != null && reWhiteSpace.test(form.orderNumber.value) || form.orderNumber.value == ""){
            MessageHelper.formErrorHandleClient(form.orderNumber.id, MessageHelper.messages["TRACK_ORDER_EMPTY_ORDER_ID"]);
            return;
        } else {
            form.orderNumber.value = trim(form.orderNumber.value);
        }

        if(form.trackEmail != null && reWhiteSpace.test(form.trackEmail.value) || form.trackEmail.value == ""){
            MessageHelper.formErrorHandleClient(form.trackEmail.id, MessageHelper.messages["TRACK_ORDER_EMPTY_EMAIL"]);
            return;

        } else {
            form.trackEmail.value = trim(form.trackEmail.value.toLowerCase());
        }

        if(!MessageHelper.isValidEmail(form.trackEmail.value)){
            MessageHelper.formErrorHandleClient(form.trackEmail.id, MessageHelper.messages["TRACK_ORDER_INVALID_EMAIL"]);
            return;
        }

        /*For Handling multiple clicks. */
        if(!submitRequest()){
            return;
        }

        form.submit();
    }
}