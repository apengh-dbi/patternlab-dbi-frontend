//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2008, 2012 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

/**
 * @fileOverview This class contains declarations of AJAX services used by the Madisons store pages.
 */

dojo.require("wc.service.common");

/**
 * @class This class stores common parameters needed to make the service call.
 */
shoppingActionsServicesDeclarationJS = {
    langId: "-1", /* language of the  store */
    storeId: "", /*numeric unique identifier of the store */
    catalogId: "", /*catalog of the store that is currently in use */

    /**
     * Sets common parameters used by the services
     * @param (int) langId The language of the store.
     * @param (int) storeId The store currently in use.
     * @param (int) catalogId The catalog of the store currently in use.
     */
    setCommonParameters:function(langId,storeId,catalogId){
            this.langId = langId;
            this.storeId = storeId;
            this.catalogId = catalogId;
    }
};

    /**
     * Add an item to a shopping cart in Ajax mode. A message is displayed after
     * the service call.
     * @constructor
     */
    wc.service.declare({
        id: "AddOrderItem",
        actionId: "AddOrderItem",
        url: getAbsoluteURL() + "AjaxOrderChangeServiceItemAdd",
        formId: ""

     /**
     * display a success message
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */

        ,successHandler: function(serviceResponse) {

            MessageHelper.hideAndClearMessage();
            // Do not show this message. When item is added, we open up mini cart to display the currently added item.
            // MessageHelper.displayStatusMessage(storeNLS["SHOPCART_ADDED"]);
            //cursor_clear();
            if(shoppingActionsJS){

                var attributes = document.getElementsByName("attrValue");

                var singleSKU = true;

                for(var i=0; i<attributes.length; i++){
                    if (attributes[i].options.length > 1)
                    {
                        singleSKU = false;
                    }
                }

                if (!singleSKU)
                {
                    shoppingActionsJS.selectedAttributes = new Object();
                    dojo.publish('attributesChanged_'+ shoppingActionsJS.baseCatalogEntryId, [dojo.toJson(shoppingActionsJS.selectedAttributes)]);
                    for(var i=0; i<attributes.length; i++){
                        if(attributes[i] != null){
                            attributes[i].value = "";
                            if(attributes[i].onchange != null){
                                attributes[i].onchange();
                            }else{
                                $('#'+attributes[i].id).trigger('change');
                            }
                        }
                    }
                }

                shoppingActionsJS.editAction = "false";
                shoppingActionsJS.orderItemId2Delete = "-1";

                wc.render.updateContext('MiniShoppingCartContext');
            }
            if(typeof(ShipmodeSelectionExtJS)!= null && typeof(ShipmodeSelectionExtJS)!='undefined'){
                ShipmodeSelectionExtJS.setOrderItemId(serviceResponse.orderItemId[0]);
            }

            // get qty object set up for utag vars
            var qtyObj = [];
            var qtyString = serviceResponse.quantityAdded;
            var qtySplit = qtyString.split(',');
            var qtyLength = qtySplit.length;
            for (var i = 0; i < qtyLength; i++) {
                var newNumber = Math.floor(qtySplit[i]);
                newNumber = newNumber.toString();
                qtyObj.push(newNumber);
            }
            // get special order object set up for utag vars
            var specOrderObj = [];
            var specOrderString = serviceResponse.specialOrderFlagsAdded;
            var specOrderSplit = specOrderString.split(',');
            var specOrderLength = specOrderSplit.length;
            for (var i = 0; i < specOrderLength; i++) {
                var newNumber = specOrderSplit[i];
                specOrderObj.push(newNumber);
            }

            var typeObj = [];
            var typeString = serviceResponse.soType;
            var typeSplit = typeString.split(',');
            var typeLength = typeSplit.length;
            for (var i = 0; i < typeLength; i++) {
                var newType = typeSplit[i];
                typeObj.push(newType);
            }

            var productFinidingMethod;
            var breadCrumbLevels = shoppingActionsJS.breadCrumb.split(":");
            if (breadCrumbLevels.length > 0 && breadCrumbLevels[0] == breadCrumbLevels[1] && breadCrumbLevels[1] == breadCrumbLevels[2]) {
                productFinidingMethod = breadCrumbLevels[0];
            } else {
                productFinidingMethod = shoppingActionsJS.breadCrumb.replace(/:/g, '/');
            }
            console.log(serviceResponse);
            //DBI-9358
            if(typeof(utag) != "undefined" && typeof utag.view != "undefined") {
                utag.link({
                    customer_city: utag.data.customer_city,
                    customer_country: utag.data.customer_country,
                    customer_email_address: utag.data.customer_email_address,
                    customer_id: utag.data.customer_id,
                    customer_state: utag.data.customer_state,
                    customer_zip: utag.data.customer_zip,
                    event_type: 'AddToCart',
                    logged_in_state: utag.data.logged_in_state,
                    product_availability: utag.data.product_availability,
                    product_avg_review_rating: utag.data.product_avg_review_rating,
                    product_base_price: utag.data.product_base_price,
                    product_brand: utag.data.product_brand,
                    product_category_level1: utag.data.product_category_level1,
                    product_category_level2: utag.data.product_category_level2,
                    product_category_level3: utag.data.product_category_level3,
                    product_finding_method: utag.data.product_finding_method,
                    product_is_quick_ship: [shoppingActionsJS.quickShipFlag],
                    product_isSpecialOrder: specOrderObj,
                    product_name: utag.data.product_name,
                    product_quantity: qtyObj,
                    product_review_count: utag.data.product_review_count,
                    product_sale_price: utag.data.product_sale_price,
                    product_style: utag.data.product_style,
                    product_type: typeObj,
                    product_wsc_category: utag.data.product_wsc_category,
                    site_type: utag.data.site_type,
                    visitor_device: utag.data.visitor_device,
                    visitor_selected_country: utag.data.visitor_selected_country,
                    product_finding_method: [productFinidingMethod]
                });
            }
            dojo.publish("CMAddToCart");

        }
     /**
     * display an error message
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
        ,failureHandler: function(serviceResponse) {

            if (serviceResponse.errorMessage) {
                 if(serviceResponse.errorMessageKey == "_ERR_NO_ELIGIBLE_TRADING"){
                     MessageHelper.displayErrorMessage(storeNLS["ERROR_CONTRACT_EXPIRED_GOTO_ORDER"]);
                 } else if (serviceResponse.errorMessageKey == "_ERR_RETRIEVE_PRICE") {
                    var tempString = storeNLS["GENERICERR_MAINTEXT"];
                    tempString = dojo.string.substitute(tempString,{0:storeNLS["GENERICERR_CONTACT_US"]});
                     MessageHelper.displayErrorMessage(tempString);
                 } else {
                     MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
                 }
            }
            else {
                 if (serviceResponse.errorMessageKey) {
                    MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
                 }
            }
            cursor_clear();
        }

    });
    
    
    
    /**
     * Add an item to a shopping cart in Ajax mode. A message is displayed after
     * the service call.
     * @constructor
     */
    wc.service.declare({
        id: "DBIAddOrderItem",
        actionId: "DBIAddOrderItem",
        url: getAbsoluteURL() + "DBIAjaxOrderItemAdd",
        formId: ""

     /**
     * display a success message
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */

        ,successHandler: function(serviceResponse) {

            MessageHelper.hideAndClearMessage();
            // Do not show this message. When item is added, we open up mini cart to display the currently added item.
            // MessageHelper.displayStatusMessage(storeNLS["SHOPCART_ADDED"]);
            //cursor_clear();
            if(shoppingActionsJS){

                var attributes = document.getElementsByName("attrValue");

                var singleSKU = true;

                for(var i=0; i<attributes.length; i++){
                    if (attributes[i].options.length > 1)
                    {
                        singleSKU = false;
                    }
                }

                if (!singleSKU)
                {
                    shoppingActionsJS.selectedAttributes = new Object();
                    dojo.publish('attributesChanged_'+ shoppingActionsJS.baseCatalogEntryId, [dojo.toJson(shoppingActionsJS.selectedAttributes)]);
                    for(var i=0; i<attributes.length; i++){
                        if(attributes[i] != null){
                            attributes[i].value = "";
                            if(attributes[i].onchange != null){
                                attributes[i].onchange();
                            }else{
                                $('#'+attributes[i].id).trigger('change');
                            }
                        }
                    }
                }

                shoppingActionsJS.editAction = "false";
                shoppingActionsJS.orderItemId2Delete = "-1";

                wc.render.updateContext('MiniShoppingCartContext');
            }
            if(typeof(ShipmodeSelectionExtJS)!= null && typeof(ShipmodeSelectionExtJS)!='undefined'){
                ShipmodeSelectionExtJS.setOrderItemId(serviceResponse.orderItemId[0]);
            }

            // get qty object set up for utag vars
            var qtyObj = [];
            var qtyString = serviceResponse.quantityAdded;
            var qtySplit = qtyString.split(',');
            var qtyLength = qtySplit.length;
            for (var i = 0; i < qtyLength; i++) {
                var newNumber = Math.floor(qtySplit[i]);
                newNumber = newNumber.toString();
                qtyObj.push(newNumber);
            }
            // get special order object set up for utag vars
            var specOrderObj = [];
            var specOrderString = serviceResponse.specialOrderFlagsAdded;
            var specOrderSplit = specOrderString.split(',');
            var specOrderLength = specOrderSplit.length;
            for (var i = 0; i < specOrderLength; i++) {
                var newNumber = specOrderSplit[i];
                specOrderObj.push(newNumber);
            }

            var typeObj = [];
            var typeString = serviceResponse.soType;
            var typeSplit = typeString.split(',');
            var typeLength = typeSplit.length;
            for (var i = 0; i < typeLength; i++) {
                var newType = typeSplit[i];
                typeObj.push(newType);
            }

            var productFinidingMethod;
            var breadCrumbLevels = shoppingActionsJS.breadCrumb.split(":");
            if (breadCrumbLevels.length > 0 && breadCrumbLevels[0] == breadCrumbLevels[1] && breadCrumbLevels[1] == breadCrumbLevels[2]) {
                productFinidingMethod = breadCrumbLevels[0];
            } else {
                productFinidingMethod = shoppingActionsJS.breadCrumb.replace(/:/g, '/');
            }
            console.log(serviceResponse);
            //DBI-9358
            if(typeof(utag) != "undefined" && typeof utag.view != "undefined") {
                utag.link({
                    customer_city: utag.data.customer_city,
                    customer_country: utag.data.customer_country,
                    customer_email_address: utag.data.customer_email_address,
                    customer_id: utag.data.customer_id,
                    customer_state: utag.data.customer_state,
                    customer_zip: utag.data.customer_zip,
                    event_type: 'AddToCart',
                    logged_in_state: utag.data.logged_in_state,
                    product_availability: utag.data.product_availability,
                    product_avg_review_rating: utag.data.product_avg_review_rating,
                    product_base_price: utag.data.product_base_price,
                    product_brand: utag.data.product_brand,
                    product_category_level1: utag.data.product_category_level1,
                    product_category_level2: utag.data.product_category_level2,
                    product_category_level3: utag.data.product_category_level3,
                    product_finding_method: utag.data.product_finding_method,
                    product_is_quick_ship: [shoppingActionsJS.quickShipFlag],
                    product_isSpecialOrder: specOrderObj,
                    product_name: utag.data.product_name,
                    product_quantity: qtyObj,
                    product_review_count: utag.data.product_review_count,
                    product_sale_price: utag.data.product_sale_price,
                    product_style: utag.data.product_style,
                    product_type: typeObj,
                    product_wsc_category: utag.data.product_wsc_category,
                    site_type: utag.data.site_type,
                    visitor_device: utag.data.visitor_device,
                    visitor_selected_country: utag.data.visitor_selected_country,
                    product_finding_method: [productFinidingMethod]
                });
            }
            dojo.publish("CMAddToCart");

        }
     /**
     * display an error message
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
        ,failureHandler: function(serviceResponse) {

            if (serviceResponse.errorMessage) {
                 if(serviceResponse.errorMessageKey == "_ERR_NO_ELIGIBLE_TRADING"){
                     MessageHelper.displayErrorMessage(storeNLS["ERROR_CONTRACT_EXPIRED_GOTO_ORDER"]);
                 } else if (serviceResponse.errorMessageKey == "_ERR_RETRIEVE_PRICE") {
                    var tempString = storeNLS["GENERICERR_MAINTEXT"];
                    tempString = dojo.string.substitute(tempString,{0:storeNLS["GENERICERR_CONTACT_US"]});
                     MessageHelper.displayErrorMessage(tempString);
                 } else {
                     MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
                 }
            }
            else {
                 if (serviceResponse.errorMessageKey) {
                    MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
                 }
            }
            cursor_clear();
        }

    });
    
    
    

    /**
     * Adds a pre-defined dynamic kit to a shopping cart in Ajax mode. A message is displayed after
     * the service call.
     * @constructor
     */
    wc.service.declare({
        id: "AddPreConfigurationToCart",
        actionId: "AddOrderItem",
        url: getAbsoluteURL() + "AjaxOrderChangeServiceAddPreConfigurationToCart",
        formId: ""

     /**
     * display a success message
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */

        ,successHandler: function(serviceResponse) {
            MessageHelper.hideAndClearMessage();
            cursor_clear();
            if(shoppingActionsJS){

                var attributes = document.getElementsByName("attrValue");

                var singleSKU = true;

                for(var i=0; i<attributes.length; i++){
                    if (attributes[i].options.length > 1)
                    {
                        singleSKU = false;
                    }
                }

                if (!singleSKU)
                {
                    shoppingActionsJS.selectedAttributes = new Object();
                    for(var i=0; i<attributes.length; i++){
                        if(attributes[i] != null){
                            attributes[i].value = "";
                            attributes[i].onchange();
                        }
                    }
                }
            }
            if(typeof(ShipmodeSelectionExtJS)!= null && typeof(ShipmodeSelectionExtJS)!='undefined'){
                ShipmodeSelectionExtJS.setOrderItemId(serviceResponse.orderItemId[0]);
            }
        }
     /**
     * display an error message
     * @param (object) serviceResponse The service response object, which is the
     * JSON object returned by the service invocation
     */
        ,failureHandler: function(serviceResponse) {

            if (serviceResponse.errorMessage) {
                 if(serviceResponse.errorMessageKey == "_ERR_NO_ELIGIBLE_TRADING"){
                     MessageHelper.displayErrorMessage(storeNLS["ERROR_CONTRACT_EXPIRED_GOTO_ORDER"]);
                 } else if (serviceResponse.errorMessageKey == "_ERR_RETRIEVE_PRICE") {
                    var tempString = storeNLS["GENERICERR_MAINTEXT"];
                    tempString = dojo.string.substitute(tempString,{0:storeNLS["GENERICERR_CONTACT_US"]});
                     MessageHelper.displayErrorMessage(tempString);
                 } else {
                     MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
                 }
            }
            else {
                 if (serviceResponse.errorMessageKey) {
                    MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
                 }
            }
            cursor_clear();
        }

    });