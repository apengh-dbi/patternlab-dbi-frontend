//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

shoppingActionsJS={

    /** The language ID currently in use **/
    langId: "-1",

    /** The store ID currently in use **/
    storeId: "",

    /** The catalog ID currently in use **/
    catalogId: "",

    /** Holds the current user type such as guest or registered user. Allowed values are 'G' for guest and 'R' for registered.**/
    userType:"",

    /** Holds the productIds.**/
    productId:"",

    /** A boolean used in a variety of the add to cart methods to tell whether or not the base item was added to the cart. **/
    baseItemAddedToCart:false,

    inventoryCheck: false,

    /** An array of entitled items which is used in various methods throughout ShoppingActions.js **/
    entitledItems:[],

    /** a JSON object that holds attributes of an entitled item **/
    entitledItemJsonObject: null,

    /** A map of attribute name value pairs for the currently selected attribute values **/
    selectedAttributesList:new Object(),

    /** A variable used to form the url dynamically for the more info link in the Quickinfo popup */
    moreInfoUrl :"",

    /**
    * A boolean used to to determine is it from a Qick info popup or not.
    **/
    isPopup : false,

    /**
    * A boolean used to to determine whether or not to diplay the price range when the catEntry is selected.
    **/
    displayPriceRange : true,

    /**
    * This array holds the json object retured from the service, holding the price information of the catEntry.
    **/
    itemPriceJsonOject : [],

    /**
    * stores all name and value of all swatches
    * this is a 2 dimension array and each record i contains the following information:
    * allSwatchesArray[i][0] - attribute name of the swatch
    * allSwatchesArray[i][1] - attribute value of the swatch
    * allSwatchesArray[i][2] - image1 of swatch (image to use for enabled state)
    * allSwatchesArray[i][3] - image2 of swatch (image to use for disabled state)
    * allSwatchesArray[i][4] - onclick action of the swatch when enabled
    **/
    allSwatchesArrayList : new Object(),

    /**
    * Holds the ID of the image used for swatch
    **/
    skuImageId:"",

    /**
     * The prefix of the cookie key that is used to store item Ids.
     */
    cookieKeyPrefix: "CompareItems_",

    /**
     * The delimiter used to separate item Ids in the cookie.
     */
    cookieDelimiter: ";",

    shipInternationalAttrValue : false,

    cookieKeydbIntlCou : "",
    /**
     * The maximum number of items allowed in the compare zone.
     */
    maxNumberProductsAllowedToCompare: 4,

    /**
     * The minimum number of items allowed in the compare zone.
     */
    minNumberProductsAllowedToCompare: 2,

    /**
     * Id of the base catalog entry.
     */
    baseCatalogEntryId: 0,

    /**
     * Id of the item catalog entry.
     */
    itemCatentryId:0,

    /**
     * Id of the addon item catalog entry.
     */
    itemAddOnCatentryId:0,

    /**
     * An map which holds the attributes of a set of products
     */
    selectedProducts: new Object(),

    /**
     * An array to keep the quantity of the products in a list (bundle)
     */
    productList: new Object(),

    /**
     * stores the currency symbol
     */
    currencySymbol: "",

    /**
     * stores the compare return page name
     */
    compareReturnName: "",
    /**
     * stores the search term
     */
    searchTerm: "",

    /**
     * stores the PDP breadcrumb
     */
    breadCrumb: "",

    /**
     * stores the PDP WSC product categoryId needed for Tealium tags.
     */
    prodWscCategoryId: "",


    tabSelected: "0",

    quickShippableBrand: "false",

     /**
       * Single unit Order line Switch
       */
    suolSwitch: "0",
    quickShipFlag: "no",
    /**
     * OrderItemId to be deleted after adding a new order item in case of editing an existing order item.
     */
    orderItemId2Delete: "-1",
    /**
     * Flag to indicate an order item will be edited.
     */
    editAction: "false",

    getParameterByName:function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    checkEditParams:function(){
        var orderItemId2DeleteParam = shoppingActionsJS.getParameterByName('orderItemId');
        var editActionParam = shoppingActionsJS.getParameterByName('edit');
        if(editActionParam == 'true' && orderItemId2DeleteParam != ''){
            this.orderItemId2Delete = orderItemId2DeleteParam;
            this.editAction = editActionParam;
        }
    },
    setCompareReturnName:function(compareReturnName){
        this.compareReturnName = compareReturnName;
    },

    setSearchTerm:function(searchTerm){
        this.searchTerm = searchTerm;
    },

    setCommonParameters:function(langId,storeId,catalogId,userType,currencySymbol){
        this.langId = langId;
        this.storeId = storeId;
        this.catalogId = catalogId;
        this.userType = userType;
        this.currencySymbol = currencySymbol;
    },

    setEntitledItems : function(entitledItemArray){
        this.entitledItems = entitledItemArray;
    },

    getCatalogEntryId : function(entitledItemId) {
      var attributeArray = [];
      var selectedAttributes = this.selectedAttributesList[entitledItemId];
      var catalogEntryId = null;

      if((this.entitledItems == null) || (this.entitledItems.length == 0)) {
        this.setEntitledItems(JSON.parse(dojo.byId(entitledItemId).innerHTML, true));
      }

      if(this.entitledItems.length > 1) {
        for(attribute in selectedAttributes){
          attributeArray.push(attribute + "_" + selectedAttributes[attribute]);
        }
        catalogEntryId = this.resolveSKU(attributeArray);
      } else if(this.entitledItems.length > 0) {
        catalogEntryId = this.entitledItems[0].catentry_id;
      }

      return catalogEntryId;
    },

    /**
    * getCatalogEntryIdforProduct Returns the catalog entry ID for a catalog entry that has the same attribute values as a specified product's selected attributes as passed in via the selectedAttributes parameter.
    *
    * @param {String[]} selectedAttributes The array of selected attributes upon which to resolve the SKU.
    *
    * @return {String} catalog entry ID of the SKU.
    *
    **/
    getCatalogEntryIdforProduct : function(selectedAttributes){
        var attributeArray = [];
        for(attribute in selectedAttributes){
            attributeArray.push(attribute + "_" + selectedAttributes[attribute]);
        }
        return this.resolveSKU(attributeArray);
    },

    /**
     * retrieves the entitledItemJsonObject
     */
    getEntitledItemJsonObject: function () {
        return this.entitledItemJsonObject;
    },

    /**
    * resolveSKU Resolves a SKU using an array of defining attributes.
    *
    * @param {String[]} attributeArray An array of defining attributes upon which to resolve a SKU.
    *
    * @return {String} catentry_id The catalog entry ID of the SKU.
    *
    **/
    resolveSKU : function(attributeArray){

        // console.debug is not supported by all browsers
        // console.debug("Resolving SKU >> " + attributeArray +">>"+ this.entitledItems);
        var catentry_id = "";
        var attributeArrayCount = attributeArray.length;
        // if entitledItems is not loaded, load the same.
        if((this.entitledItems == null || this.entitledItems.length == 0) && attributeArrayCount == 0){
             //the json object for entitled items are already in the HTML.
            var entitledItemJSON = eval('('+ dojo.byId("entitledItem_"+this.productId).innerHTML +')');
            this.setEntitledItems(entitledItemJSON);
        }

        // if there is only one item, no need to check the attributes to resolve the sku
        if(this.entitledItems.length == 1){
            return this.entitledItems[0].catentry_id;
        }
        for(x = 0; x < this.entitledItems.length; x++){
            var catentry_id = this.entitledItems[x].catentry_id;
            var Attributes = this.entitledItems[x].Attributes;
            var attributeCount = 0;
            for(index in Attributes){
                attributeCount ++;
            }

            // Handle special case where a catalog entry has one sku with no attributes
            if (attributeArrayCount == 0 && attributeCount == 0){
                return catentry_id;
            }
            if(attributeCount != 0 && attributeArrayCount >= attributeCount){
                var matchedAttributeCount = 0;

                for(attributeName in attributeArray){
                    var attributeValue = attributeArray[attributeName];
                    if(attributeValue in Attributes){
                        matchedAttributeCount ++;
                    }
                }

                if(attributeCount == matchedAttributeCount){
                    // console.debug("CatEntryId:" + catentry_id + " for Attribute: " + attributeArray);
                    return catentry_id;
                }
            }
        }
        return null;
    },

    /**
    * setSelectedAttribute Sets the selected attribute value for a particular attribute not in reference to any catalog entry.
    *             One place this function is used is on CachedProductOnlyDisplay.jsp where there is a drop down box of attributes.
    *             When an attribute is selected from that drop down this method is called to update the selected value for that attribute.
    *
    * @param {String} selectedAttributeName The name of the attribute.
    * @param {String} selectedAttributeValue The value of the selected attribute.
    * @param {String} entitledItemId The element id where the json object of the sku is stored
    * @param {String} skuImageId This is optional. The element id of the product image - image element id is different in product page and category list view. Product page need not pass it because it is set separately
    * @param {String} imageField This is optional. The json field from which image should be picked. Pass value if a different size image need to be picked
    *
    **/
    setSelectedAttribute : function(selectedAttributeName , selectedAttributeValue, entitledItemId, skuImageId, imageField){
        // console.debug(selectedAttributeName +" : "+ selectedAttributeValue);
        var selectedAttributes = this.selectedAttributesList[entitledItemId];
        if(selectedAttributes == null){
            selectedAttributes = new Object();
        }
        selectedAttributes[selectedAttributeName] = selectedAttributeValue;
        this.moreInfoUrl=this.moreInfoUrl+'&'+selectedAttributeName+'='+selectedAttributeValue;
        this.selectedAttributesList[entitledItemId] = selectedAttributes;
        //this.changeProdImage(entitledItemId, selectedAttributeName, selectedAttributeValue, skuImageId, imageField);
        if (dojo.byId(entitledItemId)!=null) {
            //the json object for entitled items are already in the HTML.
             entitledItemJSON = eval('('+ dojo.byId(entitledItemId).innerHTML +')');
        }
        this.setEntitledItems(entitledItemJSON);
    },

    /**
    * setSelectedAttributeOfProduct Sets the selected attribute value for an attribute of a specified product.
    *                This function is used to set the assigned value of defining attributes to specific
    *                products which will be stored in the selectedProducts map.
    *
    * @param {String} productId The catalog entry ID of the catalog entry to use.
    * @param {String} selectedAttributeName The name of the attribute.
    * @param {String} selectedAttributeValue The value of the selected attribute.
    * @param {boolean} true, if it is single SKU
    *
    **/
    setSelectedAttributeOfProduct : function(productId,selectedAttributeName,selectedAttributeValue, isSingleSKU){

        var selectedAttributesForProduct = null;

        if(this.selectedProducts[productId]){
            selectedAttributesForProduct = this.selectedProducts[productId];
        } else {
            selectedAttributesForProduct = new Object();
        }

        // add only if attribute has some name. value can be empty
        if(null != selectedAttributeName && '' != selectedAttributeName){
            selectedAttributesForProduct[selectedAttributeName] = selectedAttributeValue;
        }
        this.selectedProducts[productId] = selectedAttributesForProduct;

        //the json object for entitled items are already in the HTML.
        var entitledItemJSON = eval('('+ dojo.byId("entitledItem_"+productId).innerHTML +')');

        this.setEntitledItems(entitledItemJSON);

        var catalogEntryId = this.getCatalogEntryIdforProduct(selectedAttributesForProduct);

        if(catalogEntryId == null) {
            catalogEntryId = 0;
        } else {
            // pass true to display price range
            this.changePrice("entitledItem_"+productId, false, true, productId);
        }
        var productDetails = null;
        if(this.productList[productId]){
            productDetails = this.productList[productId];
        } else {
            productDetails = new Object();
            this.productList[productId] = productDetails;
            productDetails.baseItemId = productId;
        }

        productDetails.id = catalogEntryId;
        if(productDetails.quantity){
            dojo.publish('quantityChanged', [dojo.toJson(productDetails)]);
        }

        if(!isSingleSKU){
            // publish the attribute change event
            dojo.publish('attributesChanged_'+productId, [dojo.toJson(selectedAttributesForProduct)]);
        }
    },

    /**
    * Add2ShopCartAjax This function is used to add a catalog entry to the shopping cart using an AJAX call. This will resolve the catentryId using entitledItemId and adds the item to the cart.
    *        This function will resolve the SKU based on the entitledItemId passed in and call {@link fastFinderJS.AddItem2ShopCartAjax}.
    * @param {String} entitledItemId A DIV containing a JSON object which holds information about a catalog entry. You can reference CachedProductOnlyDisplay.jsp to see how that div is constructed.
    * @param {int} quantity The quantity of the item to add to the cart.
    * @param {String} isPopup If the value is true, then this implies that the function was called from a quick info pop-up.
    * @param {Object} customParams - Any additional parameters that needs to be passed during service invocation.
    *
    **/
    Add2ShopCartAjax : function(entitledItemId,quantity,isPopup,customParams)
    {
        var entitledItemJSON;

        if (dojo.byId(entitledItemId)!=null) {
            //the json object for entitled items are already in the HTML.
             entitledItemJSON = eval('('+ dojo.byId(entitledItemId).innerHTML +')');
        }else{
            //if dojo.byId(entitledItemId) is null, that means there's no <div> in the HTML that contains the JSON object.
            //in this case, it must have been set in catalogentryThumbnailDisplay.js when the quick info
            entitledItemJSON = this.getEntitledItemJsonObject();
        }
        this.setEntitledItems(entitledItemJSON);
        var catalogEntryId = this.getCatalogEntryId(entitledItemId);

        if(catalogEntryId!=null){
            this.AddItem2ShopCartAjax(catalogEntryId , quantity,customParams);
            this.baseItemAddedToCart=true;
            if(dijit.byId('second_level_category_popup') != null){
                hidePopup('second_level_category_popup');
            }
        }
        else if (isPopup == true){
            dojo.byId('second_level_category_popup').style.zIndex = '1';
            MessageHelper.formErrorHandleClient('addToCartLinkAjax', storeNLS['ERR_RESOLVING_SKU']);
        } else{
            MessageHelper.displayErrorMessage(storeNLS['ERR_RESOLVING_SKU']);
            this.baseItemAddedToCart=false;
        }
    },

    AddItem2ShopCartAjax : function(entitledItemId, productId, quantity, pCatEntryIdentifier, pAttrNames, pAttrValues, skuForPersonalizedProduct, customParams)
    {
        //If item is not buyable online it shouldn't by added to cart
        var channelAvailability = this.checkForQuantityChange(entitledItemId).toUpperCase();
        if (channelAvailability == 'BUYABLEINSTORE' || channelAvailability == 'NOTBUYABLEINSTOREORONLINE' ) {
            dojo.addClass("channelAvailability", "attr-missing-msg");
            return;
        }


        var params = [];
        params.storeId    = this.storeId;
        params.catalogId  = this.catalogId;
        params.langId    = this.langId;
        params.orderId    = ".";
        params.calculationUsage = "-1,-2,-5,-6,-7";
        params.inventoryValidation = "true";
        params.orderItemId2Delete = this.orderItemId2Delete;
        params.editAction = this.editAction;
        params.quickShipAvailable = "no";

        params.jSessionId = cookies["JSESSIONID"];
        params.browserTypeAndVersion = window.browserTypeAndVersion;
        params.deviceType = cookies["device_type"];
        params.ipAddress = window.clientRemoteIP;
        params.userAgent = window.userAgent;

        var ajaxShopCartService = "AddOrderItem";
        this.productId = productId;
        // Check for personalization
        var orderWithoutPersn = document.getElementById("orderWithoutPersn");
        var catalogEntryId = this.getCatalogEntryId(entitledItemId);

        if(null != dojo.byId("quickShippableSkus") && (dojo.byId('quickShipTab').style.display != 'none') && this.tabSelected == "1"){
            var quickShippableSkus = dojo.byId("quickShippableSkus").value;
            if(quickShippableSkus.indexOf(catalogEntryId) != -1) {
                params.quickShipAvailable = "yes";
            }
        }

        this.quickShipFlag = params.quickShipAvailable;

        var persnIndiv = document.getElementById("persnIndiv");
        if(orderWithoutPersn!=null && !orderWithoutPersn.checked) {
            if(persnIndiv !=null && persnIndiv !=undefined && persnIndiv.checked){

                if(quantity > 10){
                    MessageHelper.displayErrorMessage(MessageHelper.messages['PDP_PERSONALIZED_MAX_LINES']);
                    return;
                }


                var individualPersonalizedItemsCount =   document.getElementById('individualPersonalizedItemsCount').value;
                if(individualPersonalizedItemsCount != ""){

                    individualPersonalizedItemsCount = parseInt(individualPersonalizedItemsCount);
                }
                else{
                    individualPersonalizedItemsCount = 0;
                }
                if(parseInt(quantity) > individualPersonalizedItemsCount + 1){
                    var pAttrNames = document.getElementById('pAttrNames').value;
                    this.personalizeIndividualItems(pAttrNames,productId, 'addToCartClicked');
                    MessageHelper.displayErrorMessage(MessageHelper.messages['PDP_PERSONALIZED_ADDITIONAL_LINES']);
                    return;
                }

            }
        }

        shoppingActionsJS.itemCatentryId = catalogEntryId;
        var definingAttributesExistVar = dojo.byId("product_defining_attr").value;
        if(catalogEntryId == null && orderWithoutPersn == null){
            //If SKU is not resolved and Add2Cart is clicked, show user appropriate message.
            this.handleAdd2CartError(entitledItemId);
            this.baseItemAddedToCart=false;
            return;
        }
        else if (orderWithoutPersn != null) {
            // To handle Personalized Products which has defining attributes
            if(catalogEntryId == null && definingAttributesExistVar!= null && definingAttributesExistVar.length > 0){
                 this.handleAdd2CartError(entitledItemId);
                 this.baseItemAddedToCart=false;
                 return;
            }
            else if(catalogEntryId == null) {
                catalogEntryId = skuForPersonalizedProduct;
                shoppingActionsJS.itemCatentryId = catalogEntryId;
            }
        }





        // do the quantity and inventory validation, then set the params
        if(quantity != null && quantity != "undefined") {
            if(!isPositiveInteger(quantity)){
                MessageHelper.displayErrorMessage(storeNLS['QUANTITY_INPUT_ERROR']);
                return;
            }
            //DAVIDS-1997: allow add to cart with no inventory for SO skus.
          if (this.checkInvRequired(catalogEntryId)) {
                checkInventoryAvailability(catalogEntryId,productId,quantity,'');
            }else{
                this.inventoryCheck = true;
            }
        }
        shoppingActionsJS.itemAddOnCatentryId = 0;
        if(document.getElementById('totalAddOns') != null){
        var totalAddOns = document.getElementById('totalAddOns').value;
            if (totalAddOns != null && totalAddOns != "undefined") {
                var skuIdArray = [];
                var skuQtyArray = [];
                for(i = 0; i < totalAddOns; i++) {
                    var nodId = i+1;
                    var selectId = document.getElementById("addOnCatEntry_"+nodId);
                    var skuId = selectId.options[selectId.selectedIndex].value;
                    shoppingActionsJS.itemAddOnCatentryId = skuId;
                    var skuQty = document.getElementById('addOnProductQty_'+nodId).value;
                    skuIdArray[i] = skuId;
                    skuQtyArray[i]= skuQty;

                    if(!isNonNegativeInteger(trim(skuQty))){
                        MessageHelper.displayErrorMessage(storeNLS['QUANTITY_INPUT_ERROR']);
                        return;
                    }
                    if(this.inventoryCheck && trim(skuQty) !='0' ){
                            checkInventoryAvailability(trim(skuId),trim(skuId),trim(skuQty), nodId);
                    }
                }
            }
        }


        cursor_wait();

        var proceedAddToCart = true;
        var attrNamesArray = pAttrNames.split(/,/);
        var attrValuesArray = pAttrValues.split(/,/);
        // If the 'Order without Personalization' is not checked, validate for input in personalization fields
        // Also check for minimum quantity required for Personalization Validation

       var persnIndivVal = false;
       if(persnIndiv){
        if(persnIndiv.checked){
            persnIndivVal = true;
        }
       }

        if(orderWithoutPersn!=null ){
            if(!persnIndivVal){
                if(!orderWithoutPersn.checked && this.checkPersonalizationOptions(attrNamesArray)){
                        proceedAddToCart = false;
                }
            }
            if(!orderWithoutPersn.checked){
                if(this.minimumQuantityReqdForPersonalization(quantity)) {
                    proceedAddToCart = false;
                }
           }
        }
        var persnIndiv = document.getElementById("persnIndiv");
        var nameArray = pCatEntryIdentifier.split(/,/);
        if(this.inventoryCheck && proceedAddToCart){



             if(persnIndiv !=null && persnIndiv.checked && !orderWithoutPersn.checked ) {

                 var count = 0;
                 for(var j=0; j<quantity; j++){

                     params["catEntryId_"+count] = trim(catalogEntryId);
                     params["quantity_"+count] = 1;

                     count = count + 1;


                     if(nameArray != null && nameArray != "undefined" && nameArray.length >= 1 ) {
                        var attrValues = "";
                         for(var i=0; i<nameArray.length; i++){

                             params["catEntryId_" + (count)] = trim(nameArray[i]);
                             count = count + 1;


                             if((attrNamesArray[i] != null && attrNamesArray[i] != "undefined" && attrNamesArray[i].length > 0 )){
                                 attrNamesArray[i] = trim(attrNamesArray[i]);

                                 var attrValue;
                                 var attrId;
                                 if(j == 0){
                                      attrValue = document.getElementById(attrNamesArray[i]).value;
                                      attrId = attrNamesArray[i];
                                 }else{
                                     attrValue = document.getElementById(attrNamesArray[i]+"_line"+j).value;
                                     attrId = attrNamesArray[i]+"_line"+j;
                                 }

                                 if(attrValue!=null && attrValue == ""){
                                     MessageHelper.formErrorHandleClient(attrId, storeNLS['PERSONALIZATION_INPUT_ERROR']);
                                     return true;
                                 }

                                 nameArray[i] = trim(nameArray[i]);
                                 params["personalizationAttributes"+nameArray[i]] = nameArray[i] + "_" + attrNamesArray[i] + "_" + attrValue;

                                 var seprator = "";
                                 if(attrValues.length > 0){
                                     seprator = "_";
                                 }
                                 else{
                                     seprator = '';
                                 }
                                 attrValues  = attrValues + seprator + attrValue;
                             }
                           }
                         params["attrValuesArray_"+j] = attrValues;
                         attrValues = '';

                       }

                 }
             }
             else{
             params["catEntryId_0"] = trim(catalogEntryId);
             params["quantity_0"] = quantity;

             }

             params['e4xCountry'] = e4x_country;
             params['breadCrumb'] = shoppingActionsJS.breadCrumb;
             params['prodWscCategoryId'] = shoppingActionsJS.prodWscCategoryId;



        if(orderWithoutPersn!=null && !orderWithoutPersn.checked) {


            if(persnIndiv !=null && persnIndiv.checked && !orderWithoutPersn.checked) {

                params["persnIndiv"] = "true";
            }
            else{

                if(nameArray != null && nameArray != "undefined" && nameArray.length >= 1 ) {
                    // Check if the personalization has valid values
                    for(var i=0; i<nameArray.length; i++){

                        params["catEntryId_" + (i+1)] = trim(nameArray[i]);
                        //params["correlationGroup_" + (i+1)]  = correlationGroup[i];

                        if((attrNamesArray[i] != null && attrNamesArray[i] != "undefined" && attrNamesArray[i].length > 0 )){
                            attrNamesArray[i] = trim(attrNamesArray[i]);
                            var attrValue = document.getElementById(attrNamesArray[i]).value;
                            nameArray[i] = trim(nameArray[i]);
                            params["personalizationAttributes"+nameArray[i]] = nameArray[i] + "_" + attrNamesArray[i] + "_" + attrValue;
                        }
                      }

                  }
            }


        }


        if (totalAddOns != null && totalAddOns != "undefined") {
            var counter = 1;
            for(i = 0; i < totalAddOns; i++) {
                if(trim(skuQtyArray[i]) != '0'){
                    params["catEntryId_" + (counter)] = trim(skuIdArray[i]);
                    params["quantity_" + (counter)] = trim(skuQtyArray[i]);
                    counter = counter + 1;
                }
            }
        }




       //Pass any other customParams set by other add on features
        if(customParams != null && customParams != 'undefined'){
            for(i in customParams){
                params[i] = customParams[i];
            }

            if(customParams['catalogEntryType'] == 'dynamicKit' ){
                ajaxShopCartService = "AddPreConfigurationToCart";
            }

        }

        var contractIdElements = document.getElementsByName('contractSelectForm_contractId');
        if (contractIdElements != null && contractIdElements != "undefined") {
            for (i=0; i<contractIdElements.length; i++) {
                if (contractIdElements[i].checked) {
                    params.contractId = contractIdElements[i].value;
                    break;
                }
            }
        }
        //For Handling multiple clicks
        if(!submitRequest()){
            return;
        }
        cursor_wait();

        wc.service.invoke(ajaxShopCartService, params);
        this.baseItemAddedToCart=true;

        if(document.getElementById("headerShopCartLink")&&document.getElementById("headerShopCartLink").style.display != "none")
        {
            document.getElementById("headerShopCart").focus();
        }
        else
        {
            if(document.getElementById("headerShopCart1")){
                document.getElementById("headerShopCart1").focus();
            }
        }
        }
    },
    /**
     * This function handles the case when Add2Cart is clicked before selecting all the defining
     * attributes of a product.
     */
    handleAdd2CartError : function (entitledItemId) {
        var attributeArray = [];
        var selectedAttributes = this.selectedAttributesList[entitledItemId];

        //IE8 and lower versions don't support Object.keys
        if (!Object.keys) {
            // console.debug('IE8 handling started..');
            Object.keys = function(obj) {
                var keys = [];

                for (var i in obj) {
                  if (obj.hasOwnProperty(i)) {
                    keys.push(i);
                  }
                }
                return keys;
            };
        }
        var selectedAttributesList = null;
        if (selectedAttributes != undefined) {
            selectedAttributesList = Object.keys(selectedAttributes);
        }
        //Product defining attributes e.g. Color_Size
        var productDefiningAttrList = dojo.byId("product_defining_attr").value;
        var productDefiningAttrArray = productDefiningAttrList.split("_");

        var attributesRequiredToSelect;
        var lookup = {};

        for (var j in selectedAttributesList) {
            lookup[selectedAttributesList[j]] = selectedAttributesList[j];
        }

        for (var i in productDefiningAttrArray) {
            if (typeof lookup[productDefiningAttrArray[i]] != 'undefined'  || productDefiningAttrArray[i] instanceof Function ) {

            } else {
                // console.debug("Missing defining attribute: " + productDefiningAttrArray[i]);
                if (attributesRequiredToSelect == undefined) {
                    attributesRequiredToSelect = productDefiningAttrArray[i].toUpperCase().replace(' ', '_');
                } else {
                    attributesRequiredToSelect = attributesRequiredToSelect + '_' + productDefiningAttrArray[i].toUpperCase().replace(' ', '_');
                }
            }
        }
        //If attributes required to select exists, display the error message.
        if (attributesRequiredToSelect != undefined) {
            this.displayAdd2CartError(attributesRequiredToSelect);
        }
    },
    /**
     * This message displays the error message to show missing defining attribute selection.
     */
    displayAdd2CartError: function (attributesRequiredToSelect) {
        var errorMessageKey = 'ERR_MISSING_' + attributesRequiredToSelect,
        errorMsg = MessageHelper.messages[errorMessageKey],
        msgArea = dojo.byId("defining_attr_missing_msg_area");
        msgArea.innerHTML = errorMsg;
        msgArea.style.display = 'inline-block';
    },
    /**
    * AddBundle2ShopCartAjax This function is used to add a bundle to the shopping cart.
    **/
    AddBundle2ShopCartAjax : function(){
        var ajaxShopCartService = "AddOrderItem";
        var params = [];

        params.storeId    = this.storeId;
        params.catalogId  = this.catalogId;
        params.langId    = this.langId;
        params.orderId    = ".";
        params.calculationUsage = "-1,-2,-5,-6,-7";
        params.inventoryValidation = "true";
        params.orderItemId2Delete = this.orderItemId2Delete;
        params.editAction = this.editAction;


        var idx = 1;
        for(productId in this.productList){
            var productDetails = this.productList[productId];
            if(productDetails.id == 0){
                MessageHelper.displayErrorMessage(storeNLS['ERR_RESOLVING_SKU']);
                return;
            }
            var quantity = dojo.number.parse(productDetails.quantity);
            if(isNaN(quantity) || quantity <= 0){
                MessageHelper.displayErrorMessage(storeNLS['QUANTITY_INPUT_ERROR']);
                return;
            }
            params["catEntryId_" + idx] = productDetails.id;
            params["quantity_" + idx++] = quantity;
            this.baseItemAddedToCart=true;
        }

        //For Handling multiple clicks
        if(!submitRequest()){
            return;
        }
        cursor_wait();
        wc.service.invoke(ajaxShopCartService, params);

    },

    /* SwatchCode start */

    /**
    *setSKUImageId Sets the ID of the image to use for swatch.
    **/
    setSKUImageId:function(skuImageId){
        this.skuImageId = skuImageId;
    },

     /**
         *setSKUImageId Sets the ID of the image to use for swatch.
         **/
         setSUOLSwitch:function(suolSwitch){
             this.suolSwitch = suolSwitch;
     },

    /**
    * getImageForSKU Returns the full image of the catalog entry with the selected attributes as specified in the {@link fastFinderJS.selectedAttributes} value.
    *          This method uses resolveImageForSKU to find the SKU image with the selected attributes values.
    *
    * @param {String} imageField, the field name from which the image should be picked
    * @return {String} path to the SKU image.
    *
    *
    **/
    getImageForSKU : function(entitledItemId, imageField){
        var attributeArray = [];
        var selectedAttributes = this.selectedAttributesList[entitledItemId];
        for(attribute in selectedAttributes){
            attributeArray.push(attribute + "_" + selectedAttributes[attribute]);
        }
        return this.resolveImageForSKU(attributeArray, imageField);
    },

    /**
    * resolveImageForSKU Resolves image of a SKU using an array of defining attributes.
    *
    * @param {String[]} attributeArray An array of defining attributes upon which to resolve a SKU.
    * @param {String} imageField, the field name from which the image should be picked
    *
    * @return {String} imagePath The location of SKU image.
    *
    **/
    resolveImageForSKU : function(attributeArray, imageField){

        // console.debug("Resolving SKU >> " + attributeArray +">>"+ this.entitledItems);
        var imagePath = "";
        var attributeArrayCount = attributeArray.length;

        for(x in this.entitledItems){
            if(null != imageField){
                var imagePath = this.entitledItems[x][imageField];
            } else {
            var imagePath = this.entitledItems[x].ItemImage467;
            }

            var Attributes = this.entitledItems[x].Attributes;
            var attributeCount = 0;
            for(index in Attributes){
                attributeCount ++;
            }

            // Handle special case where a catalog entry has one sku with no attributes
            if (attributeArrayCount == 0 && attributeCount == 0){
                return imagePath;
            }
            if(attributeCount != 0 && attributeArrayCount >= attributeCount){
                var matchedAttributeCount = 0;

                for(attributeName in attributeArray){
                    var attributeValue = attributeArray[attributeName];
                    if(attributeValue in Attributes){
                        matchedAttributeCount ++;
                    }
                }

                if(attributeCount == matchedAttributeCount){
                    // console.debug("ItemImage:" + imagePath + " for Attribute: " + attributeArray);
                    var imageArray = [];
                    imageArray.push(imagePath);
                    imageArray.push(this.entitledItems[x].ItemThumbnailImage);
                    if(this.entitledItems[x].ItemAngleThumbnail != null && this.entitledItems[x].ItemAngleThumbnail != undefined){
                        imageArray.push(this.entitledItems[x].ItemAngleThumbnail);
                        imageArray.push(this.entitledItems[x].ItemAngleFullImage);
                    }
                    return imageArray;
                }
            }
        }
        return null;
    },


    /**
    * changeViewImages Updates the angle views of the SKU.
    *
    * @param {String[]} itemAngleThumbnail An array of SKU view thumbnails.
    * @param {String[]} itemAngleFullImage An array of SKU view full images.
    **/
    changeViewImages : function(itemAngleThumbnail, itemAngleFullImage){
        var imageCount = 0;
        for (x in itemAngleThumbnail) {
            var prodAngleCount = imageCount;
            imageCount++;
            if(null != dojo.byId("WC_CachedProductOnlyDisplay_images_1_" + imageCount)){
                dojo.byId("WC_CachedProductOnlyDisplay_images_1_" + imageCount).src = itemAngleThumbnail[x];
            }
            if(null != dojo.byId("WC_CachedProductOnlyDisplay_links_1_" + imageCount)){
                dojo.byId("WC_CachedProductOnlyDisplay_links_1_" + imageCount).href =
                    "JavaScript:changeThumbNail('productAngleLi" + prodAngleCount + "','" + itemAngleFullImage[x] + "');";
            }

            if(null != dojo.byId("productAngleLi" + prodAngleCount) && dojo.byId("productAngleLi" + prodAngleCount).className == "selected"){
                changeThumbNail("productAngleLi" + prodAngleCount, itemAngleFullImage[x]);
            }
        }
    },


    /**
    * updates the product image from the PDP page to use the selected SKU image
    * @param String swatchAttrName the newly selection attribute name
    * @param String swatchAttrValue the newly selection attribute value
    * @param {String} imageField, the field name from which the image should be picked
    **/
    changeProdImage: function(entitledItemId, swatchAttrName, swatchAttrValue, skuImageId, imageField){
        if (dojo.byId(entitledItemId)!=null) {
            //the json object for entitled items are already in the HTML.
             entitledItemJSON = eval('('+ dojo.byId(entitledItemId).innerHTML +')');
        }

        this.setEntitledItems(entitledItemJSON);

        var skuImage = null;
        var imageArr = shoppingActionsJS.getImageForSKU(entitledItemId, imageField);
        if(imageArr != null){
            skuImage = imageArr[0];
        }

        if(skuImageId != undefined){
            this.setSKUImageId(skuImageId);
        }

        if(skuImage != null){
            if(dojo.byId(this.skuImageId) != null){
                document.getElementById(this.skuImageId).src = skuImage;
                var itemAngleThumbnail = imageArr[2];
                var itemAngleFullImage = imageArr[3];
                if(itemAngleThumbnail != null && itemAngleThumbnail != undefined){
                    shoppingActionsJS.changeViewImages(itemAngleThumbnail, itemAngleFullImage);
                }
            }
        } else {
            var imageFound = false;
            for (x in this.entitledItems) {
                var Attributes = this.entitledItems[x].Attributes;
                if(null != imageField){
                    var itemImage = this.entitledItems[x][imageField];
                } else {
                var itemImage = this.entitledItems[x].ItemImage467;
                }


                var itemAngleThumbnail = this.entitledItems[x].ItemAngleThumbnail;
                var itemAngleFullImage = this.entitledItems[x].ItemAngleFullImage;

                for(y in Attributes){
                    var index = y.indexOf("_");
                    var entitledSwatchName = y.substring(0, index);
                    var entitledSwatchValue = y.substring(index+1);

                    if (entitledSwatchName == swatchAttrName && entitledSwatchValue == swatchAttrValue) {
                        // set sku image only if the img element is present
                        if(null != dojo.byId(this.skuImageId)){
                            dojo.byId(this.skuImageId).src = itemImage;
                        }
                        if(itemAngleThumbnail != null && itemAngleThumbnail != undefined){
                            shoppingActionsJS.changeViewImages(itemAngleThumbnail, itemAngleFullImage);
                        }
                        imageFound = true;
                        break;
                    }
                }

                if(imageFound){
                    break;
                }
            }
        }
    },

    /**
    * Updates the swatches selections on list view.
    * Sets up the swatches array and sku images, then selects a default swatch value.
    **/
    updateSwatchListView: function(){
            var swatchArray = dojo.query("a[id^='swatch_array_']");
            for(var i = 0; i<swatchArray.length; i++){
                var swatchArrayElement = swatchArray[i];
                eval(dojo.attr(swatchArrayElement,"href"));
            }

            var swatchSkuImage = dojo.query("a[id^='swatch_setSkuImage_']");
            for(var i = 0; i<swatchSkuImage.length; i++){
                var swatchSkuImageElement = swatchSkuImage[i];
                eval(dojo.attr(swatchSkuImageElement,"href"));
            }

            var swatchDefault = dojo.query("a[id^='swatch_selectDefault_']");
            for(var i = 0; i<swatchDefault.length; i++){
                var swatchDefaultElement = swatchDefault[i];
                eval(dojo.attr(swatchDefaultElement,"href"));
            }
    },

    /**
    * Handles the case when a swatch is selected. Set the border of the selected swatch.
    * @param {String} selectedAttributeName The name of the selected swatch attribute.
    * @param {String} selectedAttributeValue The value of the selected swatch attribute.
    * @param {String} entitledItemId The ID of the SKU
    * @param {String} doNotDisable The name of the swatch attribute that should never be disabled.
    * @param {String} imageField, the field name from which the image should be picked
    * @return boolean Whether the swatch is available for selection
    **/
    selectSwatch: function(selectedAttributeName, selectedAttributeValue, entitledItemId, doNotDisable, skuImageId, imageField, swatchId, numberOfPriceBreaks) {
        var idPrefix = (selectedAttributeName == "Accent Color") ? "ac_swatch_link_" : "swatch_link_";
        var swatchLinkElement = dojo.byId("".concat(idPrefix,entitledItemId,"_",selectedAttributeValue));
        var selAttrVal = null;
        var dojoLabelElem = null;
        var hasInvForAttr = true;

        if(dojo.hasClass(swatchLinkElement, "color_swatch_disabled")){
            return;
        }

        this.enableSwatches(entitledItemId, selectedAttributeName);

        this.hideAndClearPDPErrorMessages();

        var selectedAttributes = this.selectedAttributesList[entitledItemId];

        var msgArea = dojo.byId('quickShipProductAvail');
            msgArea.style.display = 'none';

        for (attribute in selectedAttributes) {
            idPrefix = (attribute == "Accent Color") ? "ac_swatch_link_" : "swatch_link_";
            selAttrVal = selectedAttributes[attribute];
            swatchLinkElement = dojo.byId("".concat(idPrefix,entitledItemId,"_",selAttrVal));

            if((selectedAttributeName == doNotDisable) && (attribute != selectedAttributeName) && selectedAttributes[attribute]) {
              hasInvForAttr = this.checkForOutOfStockCombination(selectedAttributeValue, selectedAttributes[attribute]);
              if(!hasInvForAttr) {
                dojoLabelElem = dojo.byId("swatch_selection_".concat(entitledItemId,"_",attribute));
              }
            }

            if (swatchLinkElement != null) {
              if ((attribute == selectedAttributeName) || !hasInvForAttr) {
                // case when the selected swatch is already selected with a value, if the value is different than
                // what's being selected, reset other swatches and deselect the previous value and update selection
                if (selAttrVal != selectedAttributeValue) {
                    // deselect previous value and update swatch selection
                    swatchLinkElement.className = "color_swatch";
                    //swatchElement.src = swatchElement.src.replace("_disabled.png","_enabled.png");

                    //change the title text of the swatch link
                    swatchLinkElement.title = swatchLinkElement.alt;
                }
              }
              swatchLinkElement.setAttribute("aria-checked", "false");
            }
        }

        if (dojoLabelElem != null) {
          selectedAttributes = new Object();
          selectedAttributes[selectedAttributeName] = selectedAttributeValue;
          this.selectedAttributesList[entitledItemId] = selectedAttributes;
          dojoLabelElem.innerHTML = "";
          dojoLabelElem = dojo.byId("channelAvailability");
          if (dojoLabelElem != null) {
            dojoLabelElem.innerHTML = "";
          }
          dojoLabelElem = dojo.byId("InternationalShippingAvailable");
          if (dojoLabelElem != null) {
            dojoLabelElem.innerHTML = "";
          }
        }
        this.makeSwatchSelection(selectedAttributeName, selectedAttributeValue, entitledItemId, doNotDisable, skuImageId, imageField, swatchId, numberOfPriceBreaks);
    },

    /**
    * Make swatch selection - add to selectedAttribute, select image, and update other swatches and SKU image based on current selection.
    * @param {String} swatchAttrName The name of the selected swatch attribute.
    * @param {String} swatchAttrValue The value of the selected swatch attribute.
    * @param {String} entitledItemId The ID of the SKU.
    * @param {String} doNotDisable The name of the swatch attribute that should never be disabled.
    * @param {String} skuImageId This is optional. The element id of the product image - image element id is different in product page and category list view. Product page need not pass it because it is set separately
    * @param {String} imageField This is optional. The json field from which image should be picked. Pass value if a different size image need to be picked
    **/
    makeSwatchSelection: function(swatchAttrName, swatchAttrValue, entitledItemId, doNotDisable, skuImageId, imageField, swatchId, numberOfPriceBreaks) {
        // setSelectedAttribute internally calls changeProdImage method to change product image.
        this.setSelectedAttribute(swatchAttrName, swatchAttrValue, entitledItemId, skuImageId, imageField);
        var idPrefix = (swatchAttrName == "Accent Color") ? "ac_swatch_link_" : "swatch_link_";
        var swatchElement = dojo.byId("".concat(idPrefix,entitledItemId,"_",swatchAttrValue));
        swatchElement.className = "color_swatch_selected";
        swatchElement.setAttribute("aria-checked", "true");

        if( (swatchId != '' ||swatchId != undefined) && (swatchAttrName == "color" || swatchAttrName == "Color" || swatchAttrName == "COLOR")){
          swatchElement = dojo.byId("swatch_selection_".concat(entitledItemId,"_",swatchAttrName,"_",swatchId));
            if (swatchElement.style.display == "none") {
              swatchElement.style.display = "inline";
            }
            swatchElement.innerHTML = swatchAttrValue;

            for(var i=1; i<=numberOfPriceBreaks; i++) {
                if(i != swatchId){
                    document.getElementById("swatch_selection_" + entitledItemId + "_" + swatchAttrName + "_" + i).innerHTML = 'unselected';
                }
            }
        }
        else{
          swatchElement = dojo.byId("swatch_selection_".concat(entitledItemId,"_",swatchAttrName));
            if (swatchElement.style.display == "none") {
              swatchElement.style.display = "inline";
            }
            swatchElement.innerHTML = swatchAttrValue;
        }
        this.updateSwatchImages(swatchAttrName, entitledItemId, doNotDisable,imageField);
    },

    /**
    * Constructs record and add to this.allSwatchesArray.
    * @param {String} swatchName The name of the swatch attribute.
    * @param {String} swatchValue The value of the swatch attribute.
    * @param {String} swatchImg1 The path to the swatch image.
    **/
    addToAllSwatchsArray: function(swatchName, swatchValue, entitledItemId) {
        var swatchList = this.allSwatchesArrayList[entitledItemId];
        var idPrefix = (swatchName == "Accent Color") ? "ac_swatch_link_" : "swatch_link_";
        if(swatchList == null){
            swatchList = new Array();;
        }
        if (!this.existInAllSwatchsArray(swatchName, swatchValue, swatchList)) {
            var swatchRecord = new Array();
            swatchRecord[0] = swatchName;
            swatchRecord[1] = swatchValue;
            swatchRecord[4] = dojo.byId("".concat(idPrefix,entitledItemId,"_",swatchValue)).onclick;
            swatchList.push(swatchRecord);
            this.allSwatchesArrayList[entitledItemId] = swatchList;
        }
    },

    /**
    * Checks if a swatch is already exist in this.allSwatchesArray.
    * @param {String} swatchName The name of the swatch attribute.
    * @param {String} swatchValue The value of the swatch attribute.
    * @return boolean Value indicating whether or not the specified swatch name and value exists in the allSwatchesArray.
    */
    existInAllSwatchsArray: function(swatchName, swatchValue, swatchList) {
        for(var i=0; i<swatchList.length; i++) {
            var attrName = swatchList[i][0];
            var attrValue = swatchList[i][1];
            if (attrName == swatchName && attrValue == swatchValue) {
                return true;
            }
        }
        return false;
    },

    /**
    * Check the entitledItems array and pre-select the first entited SKU as the default swatch selection.
    * @param {String} entitledItemId The ID of the SKU.
    * @param {String} doNotDisable The name of the swatch attribute that should never be disabled.
    **/
    makeDefaultSwatchSelection: function(entitledItemId, doNotDisable) {
        if (this.entitledItems.length == 0) {
            if (dojo.byId(entitledItemId)!=null) {
                 entitledItemJSON = eval('('+ dojo.byId(entitledItemId).innerHTML +')');
            }
            this.setEntitledItems(entitledItemJSON);
        }

        // need to make selection for every single swatch
        for(x in this.entitledItems){
            var Attributes = this.entitledItems[x].Attributes;
            for(y in Attributes){
                var index = y.indexOf("_");
                var swatchName = y.substring(0, index);
                var swatchValue = y.substring(index+1);
                this.makeSwatchSelection(swatchName, swatchValue, entitledItemId, doNotDisable,imageField);
            }
            break;
        }
    },
    /**
     * We gray out a defining attribute option if inventory is not available. This method enables all the swatches
     * again. Afterwards, makeSwatchSelection function does the enabling and disabling of attributes.
     */
    enableSwatches : function(entitledItemId, selectedAttributeName) {
        var swatchList = this.allSwatchesArrayList[entitledItemId];
        var idPrefix = (selectedAttributeName == "Accent Color") ? "ac_swatch_link_" : "swatch_link_";
        for(var i=0; i<swatchList.length; i++) {
            var attrName = swatchList[i][0];
            if (attrName != selectedAttributeName) {
                var attrValue = swatchList[i][1];
                var elementToReset = dojo.byId("".concat(idPrefix,entitledItemId,"_",attrValue));
                if (elementToReset) {
                    elementToReset.setAttribute("aria-disabled", "false");
                    // elementToReset.className = "color_swatch";
                    dojo.removeClass(elementToReset, "color_swatch_disabled");
                }
            }
         }
    },
    /**
    * Update swatch images - this is called after selection of a swatch is made, and this function checks for
    * entitlement and disable swatches that are not available
    * @param selectedAttrName The attribute that is selected
    * @param {String} entitledItemId The ID of the SKU.
    * @param {String} doNotDisable The name of the swatch attribute that should never be disabled.
    **/
    updateSwatchImages: function(selectedAttrName, entitledItemId, doNotDisable,imageField) {
        var swatchToUpdate = new Array();
        var selectedAttributes = this.selectedAttributesList[entitledItemId];
        var selectedAttrValue = selectedAttributes[selectedAttrName];
        var swatchList = this.allSwatchesArrayList[entitledItemId];



        var sizeHeading = dojo.byId("product_size_swatch_heading");
        if( sizeHeading != null && swatchList.length != 1){
            sizeHeading.style.display = "block";
        }

        // finds out which swatch needs to be updated, add to swatchToUpdate array
        for(var i=0; i<swatchList.length; i++) {
            var attrName = swatchList[i][0];
            var attrValue = swatchList[i][1];
            var attrImg1 = swatchList[i][2];
            var attrImg2 = swatchList[i][3];
            var attrOnclick = swatchList[i][4];

            if (attrName != doNotDisable && attrName != selectedAttrName) {
                var swatchRecord = new Array();
                swatchRecord[0] = attrName;
                swatchRecord[1] = attrValue;
                swatchRecord[2] = attrImg1;
                swatchRecord[4] = attrOnclick;
                swatchRecord[5] = false;
                swatchToUpdate.push(swatchRecord);
            }
        }

        // finds out which swatch is entitled, if it is, image should be set to enabled
        // go through entitledItems array and find out swatches that are entitled
        for (x in this.entitledItems) {
            var Attributes = this.entitledItems[x].Attributes;

            for(y in Attributes){
                var index = y.indexOf("_");
                var entitledSwatchName = y.substring(0, index);
                var entitledSwatchValue = y.substring(index+1);

                //the current entitled item has the selected attribute value
                if (entitledSwatchName == selectedAttrName && entitledSwatchValue == selectedAttrValue) {
                    //go through the other attributes that are available to the selected attribute
                    //exclude the one that is selected
                    for (z in Attributes) {
                        var index2 = z.indexOf("_");
                        var entitledSwatchName2 = z.substring(0, index2);
                        var entitledSwatchValue2 = z.substring(index2+1);

                        if(y != z){ //only check the attributes that are not the one selected
                            for (i in swatchToUpdate) {
                                var swatchToUpdateName = swatchToUpdate[i][0];
                                var swatchToUpdateValue = swatchToUpdate[i][1];

                                if (entitledSwatchName2 == swatchToUpdateName && entitledSwatchValue2 == swatchToUpdateValue) {
                                    swatchToUpdate[i][5] = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Now go through swatchToUpdate array, and update swatch images
        var disabledAttributes = [];
        for (i=0; i<swatchToUpdate.length; i++) {
          var swatchToUpdateName = swatchToUpdate[i][0];
          var swatchToUpdateValue = swatchToUpdate[i][1];
          var swatchToUpdateOnclick = swatchToUpdate[i][4];
          var swatchToUpdateEnabled = swatchToUpdate[i][5];
          var idPrefix = (swatchToUpdateName == "Accent Color") ? "ac_swatch_link_" : "swatch_link_";
          var swatchLinkElement = dojo.byId("".concat(idPrefix,entitledItemId,"_",swatchToUpdateValue));
            //If swatchToUpdate is enabled, additionally check for OOS items.
            if (swatchToUpdateEnabled) {
                swatchToUpdateEnabled = this.checkForOutOfStockCombination(selectedAttrValue, swatchToUpdateValue);
            }

            if (swatchToUpdateEnabled) {
                if(swatchLinkElement.className != "color_swatch_selected"){
                    swatchLinkElement.className = "color_swatch";
                    if(this.quickShippableBrand == 'true'){
                        //swatchLinkElement.parentElement.style.display = "";
                        dojo.removeClass(swatchLinkElement, "color_swatch_disabled");
                    }
                    //swatchElement.src = swatchElement.src.replace("_disabled.png","_enabled.png");

                    //change the title text of the swatch link
                    swatchLinkElement.title = swatchLinkElement.title;
                }
                swatchLinkElement.setAttribute("aria-disabled", "false");
                swatchLinkElement.onclick = swatchToUpdateOnclick;
            } else {
                if(swatchToUpdateName != doNotDisable){
                    //swatchLinkElement.className = "color_swatch_disabled";
                    if(this.quickShippableBrand == 'true'){
                        //swatchLinkElement.parentElement.style.display = "none";
                        swatchLinkElement.className = "color_swatch_disabled";
                    }
                    //else if(this.tabSelected == "0" && this.quickShippableBrand == 'true'){
                        //swatchLinkElement.parentElement.style.display = "";
                        //swatchLinkElement.className = "color_swatch_disabled";

                        //swatchLinkElement.parentElement.style.display = "none";
                //    }
                else{
                        swatchLinkElement.parentElement.style.display = "";
                        swatchLinkElement.className = "color_swatch_disabled";
                    }
                    swatchLinkElement.onclick = null;
                    //swatchElement.src = swatchElement.src.replace("_enabled.png","_disabled.png");

                    //change the title text of the swatch link
                    var titleText = storeNLS["INV_ATTR_UNAVAILABLE"];
                    //swatchLinkElement.title = dojo.string.substitute(titleText,{0: swatchElement.alt});

                    swatchLinkElement.setAttribute("aria-disabled", "true");

                    //The previously selected attribute is now unavailable for the new selection
                    //Need to switch the selection to an available value
                    if(selectedAttributes[swatchToUpdateName] == swatchToUpdateValue){
                        disabledAttributes.push(swatchToUpdate[i]);
                    }
                }
            }
        }

        //If there were any previously selected attributes that are now unavailable
        //Find another available value for that attribute and update other attributes according to the new selection
        for(i in disabledAttributes){
            var disabledAttributeName = disabledAttributes[i][0];
            var disabledAttributeValue = disabledAttributes[i][1];

            for (i in swatchToUpdate) {
                var swatchToUpdateName = swatchToUpdate[i][0];
                var swatchToUpdateValue = swatchToUpdate[i][1];
                var swatchToUpdateEnabled = swatchToUpdate[i][5];

                if(swatchToUpdateName == disabledAttributeName && swatchToUpdateValue != disabledAttributeValue && swatchToUpdateEnabled){
                        this.makeSwatchSelection(swatchToUpdateName, swatchToUpdateValue, entitledItemId, doNotDisable,imageField);
                    break;
                }
            }
        }
    },
    /* SwatchCode end */
    /**
     * This function checks if selecting a defining attributes results a possible combination
     * resolving to an Out of Stock SKU.
     */
    checkForOutOfStockCombination : function(selectedAttr, attrToCheck) {

      var isValidCookie = ((this.cookieKeydbIntlCou === undefined) || (this.cookieKeydbIntlCou=="") || (this.cookieKeydbIntlCou=='US'))
      var isNotOOS = false;
      if(this.tabSelected == "1"){
          var attrSetQuickShip = dojo.byId('attrSetQuickShip').value;
          var skuAttrArray = attrSetQuickShip.split(':');

          var skuAvail = "".concat(selectedAttr,",",attrToCheck,",Available,true");
          var skuUnavail = "".concat(selectedAttr,",",attrToCheck,",Unavailable,true");
          if(skuAttrArray.indexOf(skuUnavail) < 0) {
              if((skuAttrArray.indexOf(skuAvail) > -1)) {
                isNotOOS = true;
              }
            }
      }
      else{
          var attrSet = dojo.byId('attrSet').value;
          var skuAttrArray = attrSet.split(':');

          var skuAvail = "".concat(selectedAttr,",",attrToCheck,",Available,false");
          var skuAvailSO = "".concat(selectedAttr,",",attrToCheck,",Available,true");
          var skuUnavail = "".concat(selectedAttr,",",attrToCheck,",Unavailable,false");
          var skuUnavailSO = "".concat(selectedAttr,",",attrToCheck,",Unavailable,true");

          if(skuAttrArray.indexOf(skuUnavail) < 0) {
              if((skuAttrArray.indexOf(skuAvail) > -1) || (skuAttrArray.indexOf(skuAvailSO) > -1)) {
                isNotOOS = true;
              } else if((skuAttrArray.indexOf(skuUnavailSO) > -1) && isValidCookie) {
                isNotOOS = true;
              }
            }
      }






      return isNotOOS;
    },
    /**
         * This function checks for SKU level SO/CTO/BSR attribute.
         */
        checkForQuantityChange : function(entitledItemId) {
            var catalogEntryId = this.getCatalogEntryId(entitledItemId);
            var channelAvailability = "";
            var quickShipSku = "";
            var quickShipSku = "";
            var isSpecialOrderable = null;
            var inventoryQty = null;
            var soType = '', soDeliveryDate = '';
            if (catalogEntryId != null) {
                for(var x = 0; x < this.entitledItems.length; x++) {
                    var catentry_id = this.entitledItems[x].catentry_id;
                    if(catentry_id == catalogEntryId) {
                        channelAvailability = this.entitledItems[x].channelAvailability;
                        var channelAvailable = channelAvailability.toUpperCase();

                        quickShipSku= this.entitledItems[x].quickShipAvailable;

                        var msgArea = dojo.byId('quickShipProductAvail');
                        var productHasColorAndSize = dojo.byId('productHasColorAndSize').value;
                        var InternationalShippingAvailable = dojo.byId('InternationalShippingAvailable');

                        if(quickShipSku && productHasColorAndSize == "true"){

                            inventoryQty = this.entitledItems[x].inventoryQuantity;
                            var qty = parseInt(inventoryQty);
                            var intvQty = isNaN(qty) ? 0 : qty;

                            isSpecialOrderable = this.entitledItems[x].isSpecialOrderable;

                            if ((null != isSpecialOrderable && 'true' == isSpecialOrderable && intvQty <= 0) || ('BUYABLEINSTORE' == channelAvailable || 'NOTBUYABLEINSTOREORONLINE' == channelAvailable)) {
                                msgArea.style.display = 'none';
                            }
                            else {
                                msgArea.style.display = '';
                            }
                        }
                        else{
                            msgArea.style.display = 'none';
                        }

                        if('BUYABLEINSTORE' == channelAvailable || 'NOTBUYABLEINSTOREORONLINE' == channelAvailable){
                          InternationalShippingAvailable.style.display = 'none';
                        }
                        else{
                          InternationalShippingAvailable.style.display = '';
                        }

                        if (null != this.suolSwitch && "1" == this.suolSwitch) {
                            inventoryQty = this.entitledItems[x].inventoryQuantity;
                            isSpecialOrderable = this.entitledItems[x].isSpecialOrderable;
                            if (null != isSpecialOrderable && 'true' == isSpecialOrderable) {
                                soType = this.entitledItems[x].soType;
                                soDeliveryDate = this.entitledItems[x].soDeliveryDate;
                            }
                        }
                    }
                }
                dojo.byId("soMessage").innerHTML = "";
                dojo.byId("ctoBsrMessage").innerHTML = "";
                dojo.byId("ctoBsrTitle").innerHTML = "";
                dojo.byId("InternationalShippingAvailable").innerHTML = "";
                dojo.byId("soDeliveryDate").innerHTML = '';
                if (null != isSpecialOrderable && 'true' == isSpecialOrderable) {
                  this.handleSpecialOrderSKU(entitledItemId, isSpecialOrderable, inventoryQty,  soType, soDeliveryDate);
                } else {
                  this.handleInternationalShippingAvailableForSku();
                }
            }
            return channelAvailability;
    },
    /**
       * This function checks for SKU level channelAvailibility attribute.
       */
        checkForChannelAvailability : function(entitledItemId) {
            var entitledItemJSON = eval('('+ dojo.byId(entitledItemId).innerHTML +')');
            var catalogEntryId = this.getCatalogEntryId(entitledItemId);

            if (catalogEntryId != null) {

                channelAvailability = this.checkForQuantityChange(entitledItemId).toUpperCase();

                this.handleChannelAvailabilityForSku(channelAvailability);

                this.handleMakeAnAppointment(channelAvailability);
                //if ((channelAvailability != 'BUYABLEINSTORE') && (channelAvailability != 'NOTBUYABLEINSTOREORONLINE')) {
                  this.handleAdd2Cart(channelAvailability);
                //}
            }
    },
    /**
     * This function hides and clear all PDP error messages if any.
     */
    hideAndClearPDPErrorMessages: function() {
        MessageHelper.hideAndClearMessage();
        var msgArea = dojo.byId('defining_attr_missing_msg_area');
        msgArea.style.display = 'none';
        msgArea.innerHTML = '';
        dojo.removeClass("channelAvailability" , "attr-missing-msg");
    },
    /**
     * This functions shows SKU availability message when an SKU is resolved.
     */
    handleChannelAvailabilityForSku: function (channelAvailability) {
        var availabilityNLSMsg;
        channelAvailability =channelAvailability.toUpperCase();

        if (channelAvailability == 'BUYABLEINSTORE') {
            availabilityNLSMsg = MessageHelper.messages['ITEM_AVAIL_IN_STORE'];

        } else if (channelAvailability == 'BUYABLEONLINE') {
            availabilityNLSMsg = MessageHelper.messages['ITEM_AVAIL_ONLINE'];

        } else if (channelAvailability == 'BUYABLEINSTOREANDONLINE') {
            availabilityNLSMsg = MessageHelper.messages['ITEM_AVAIL_IN_BOTH'];

        } else if (channelAvailability == 'NOTBUYABLEINSTOREORONLINE') {
            availabilityNLSMsg = MessageHelper.messages['ITEM_NOT_AVAIL'];
        }

        if (trim(channelAvailability) != '' ) {
            dojo.byId("channelAvailability").innerHTML = availabilityNLSMsg;
        }
    },
    /**
     * handle International Shipping Available message.
     */
    handleInternationalShippingAvailableForSku: function() {
         var InternationalavailabilityNLSMsg = "";
         if (this.shipInternationalAttrValue == "true") {
             InternationalavailabilityNLSMsg = " - " + MessageHelper.messages['ITEM_AVAIL_INTERNATIONAL'];
         }
         dojo.byId("InternationalShippingAvailable").innerHTML = InternationalavailabilityNLSMsg;
    },
        /**
         * This functions shows SKU availability message when an SKU is resolved.
         */
        handleSpecialOrderSKU: function (entitledItemId, isSpecialOrderable, inventoryQty, soType, soDeliveryDate) {
            var soAvailabilityMsg = "";
            var ctoBsrMsg = "";
            var ctoBsrTitle = "";
            var isShipInternational = false;
            if (this.cookieKeydbIntlCou != null && this.cookieKeydbIntlCou.length > 0 && this.cookieKeydbIntlCou != "US") {
                isShipInternational = true;
            }
            this.handleInternationalShippingAvailableForSku();
            if (null != this.suolSwitch && "1" == this.suolSwitch && null != isSpecialOrderable && 'true' == isSpecialOrderable && !isShipInternational) {
                var selectedQuantity =  parseInt(document.getElementById("quantity_" + entitledItemId.split("_")[1]).value);
                var qty = parseInt(inventoryQty);
                var intvQty = isNaN(qty) ? 0 : qty;
                soType = soType.toUpperCase();

                if ('CTO' == soType || 'BSR' == soType) {
                    if(intvQty < selectedQuantity) { // adding if check to  display cto-bsr message if inventory not exists for the selected qty.
                         soAvailabilityMsg = MessageHelper.messages['CTO_BSR_MSG'];
                         ctoBsrTitle = MessageHelper.messages['CTO_BSR_NON_RETUNABLE_LABLE'];
                         ctoBsrMsg = MessageHelper.messages['CTO_BSR_NON_RETUNABLE_MSG'];
                         dojo.byId("InternationalShippingAvailable").innerHTML = "";
                    }
                } else if ('SO' == soType) {
                    if (intvQty <= 0) {
                        soAvailabilityMsg = MessageHelper.messages['SO_MSG_EQTY'];
                        dojo.byId("InternationalShippingAvailable").innerHTML = "";
                    } else if(intvQty < selectedQuantity) {
                         soAvailabilityMsg = MessageHelper.messages['SO_MSG_PQTY'];
                         dojo.byId("InternationalShippingAvailable").innerHTML = "";
                    }
                }
            }

            dojo.byId("soMessage").innerHTML = soAvailabilityMsg;
            dojo.byId("ctoBsrMessage").innerHTML = ctoBsrMsg;
            dojo.byId("ctoBsrTitle").innerHTML = ctoBsrTitle;
            if ('' != soAvailabilityMsg) {
                var params = new Array();
                params[0] = soDeliveryDate;
                dojo.byId("soDeliveryDate").innerHTML = MessageHelper.formatMessage('PD_SO_DATE_LBL' , params);
            } else {
                dojo.byId("soDeliveryDate").innerHTML = '';
            }
    },
    /**
     * This function handles Make an appointment button enable/disable when an SKU is resolved.
     */
    handleMakeAnAppointment: function(channelAvailability) {
        //channelAvailability =channelAvailability.toUpperCase();
       // if (channelAvailability == 'BUYABLEINSTORE' || channelAvailability == 'BUYABLEINSTOREANDONLINE' ) {
            dojo.byId("makeApptBtn").style.opacity = "1";
            dojo.byId("makeApptBtn").style.filter='alpha(opacity=100)';
            dojo.byId("makeApptBtn").removeAttribute("disabled");

       // } else {
       //     dojo.byId("makeApptBtn").style.opacity = "0.3";
       //     dojo.byId("makeApptBtn").style.filter='alpha(opacity=30)';
       //     dojo.byId("makeApptBtn").setAttribute("disabled","true");
       // }
    },
    /**
     * This function enables/disables add2cart button when an SKU is resolved.
     */
    handleAdd2Cart: function(channelAvailability) {
        channelAvailability =channelAvailability.toUpperCase();
        if (channelAvailability == 'BUYABLEINSTORE') {
            //we need to display error message when available only in store , so we are not disabling the button
            this.enableOrDisableAddtoCartBtn(false);
            dojo.addClass("add2CartBtn","disabled");
            dojo.byId("add2CartBtn").style.cursor = 'default';

        } else if(channelAvailability == 'NOTBUYABLEINSTOREORONLINE'){
            this.enableOrDisableAddtoCartBtn(true);
            dojo.byId("add2CartBtn").style.cursor = 'default';

        }else {

            if(this.isShipInternationalCheck(this.shipInternationalAttrValue, this.cookieKeydbIntlCou)){
                this.enableOrDisableAddtoCartBtn(false);
                dojo.byId("add2CartBtn").style.cursor = 'pointer';

            } else {
                this.enableOrDisableAddtoCartBtn(true);
            }
        }
    },
    /**
    * This function is used to change the price displayed in the Product Display Page on change of  a attribute of the product using an AJAX call.
    * This function will resolve the catentryId using entitledItemId and displays the price of the catentryId.
    *
    * @param {Object} entitledItemId A DIV containing a JSON object which holds information about a catalog entry. You can reference CachedProductOnlyDisplay.jsp to see how that div is constructed.
    * @param {Boolean} isPopup If the value is true, then this implies that the function was called from a quick info pop-up.
    * @param {Boolean} displayPriceRange If the value is true, then display the price range. If it is false then donot display the price range.
    *
    **/
    changePrice : function(entitledItemId,isPopup,displayPriceRange,productId){
        this.displayPriceRange = displayPriceRange;
        this.isPopup = isPopup;
        var entitledItemJSON;

        if (dojo.byId(entitledItemId)!=null && !this.isPopup) {
            //the json object for entitled items are already in the HTML.
             entitledItemJSON = eval('('+ dojo.byId(entitledItemId).innerHTML +')');
        }else{
            //if dojo.byId(entitledItemId) is null, that means there's no <div> in the HTML that contains the JSON object.
            //in this case, it must have been set in catalogentryThumbnailDisplay.js when the quick info
            entitledItemJSON = this.getEntitledItemJsonObject();
        }
        var catalogEntryId = null;
        this.setEntitledItems(entitledItemJSON);

        if(this.selectedProducts[productId]){
            var catalogEntryId = this.getCatalogEntryIdforProduct(this.selectedProducts[productId]);
        } else {
            var catalogEntryId = this.getCatalogEntryId(entitledItemId);
        }

        if(catalogEntryId!=null){
            //check if the json object is already present for the catEntry.
            if(this.itemPriceJsonOject[catalogEntryId] != null && this.itemPriceJsonOject[catalogEntryId] != 'undefined'){
                this.displayPrice(this.itemPriceJsonOject[catalogEntryId].catalogEntry,productId);
                // console.debug("ShoppingActions.changePrice: using stored json object.");
            }
            //if json object is not present, call the service to get the details.
            else{
                var parameters = {};
                parameters.storeId = this.storeId;
                parameters.langId= this.langId;
                parameters.catalogId= this.catalogId;
                parameters.catalogEntryId= catalogEntryId;
                parameters.productId = productId;

                dojo.xhrPost({
                    url: getAbsoluteURL() + "GetCatalogEntryDetailsByIDView",
                    handleAs: "json-comment-filtered",
                    content: parameters,
                    service: this,
                    load: shoppingActionsJS.displayPriceServiceResponse,
                    error: function(errObj,ioArgs) {
                        // console.debug("ShoppingActions.changePrice: Unexpected error occurred during an xhrPost request.");
                    }
                });
            }
        }
        else{
            // console.debug("ShoppingActions.changePrice: all attributes are not selected.");
        }

    },

    /**
     * Displays price of the catEntry selected with the JSON objrct returned from the server.
     *
     * @param {object} serviceRepsonse The JSON response from the service.
     * @param {object} ioArgs The arguments from the service call.
     */
     displayPriceServiceResponse : function(serviceResponse, ioArgs){
        var productId = ioArgs['args'].content['productId'];
        //stores the json object, so that the service is not called when same catEntry is selected.
        shoppingActionsJS.itemPriceJsonOject[serviceResponse.catalogEntry.catalogEntryIdentifier.uniqueID] = serviceResponse;

        shoppingActionsJS.displayPrice(serviceResponse.catalogEntry,productId);
     },

     /**
     * Displays price of the attribute selected with the JSON oject.
     *
     * @param {object} catEntry The JSON object with catalog entry details.
     */
     displayPrice : function(catEntry,productId){

        var tempString;
        var popup = shoppingActionsJS.isPopup;

        if(popup == true){
            document.getElementById('productPrice').innerHTML = catEntry.offerPrice;
            document.getElementById('productName').innerHTML = catEntry.description[0].name;
            document.getElementById('productSKUValue').innerHTML = catEntry.catalogEntryIdentifier.externalIdentifier.partNumber;
        }

        if(popup == false){
            var innerHTML = "";
            var listPrice = dojo.currency.parse(catEntry.listPrice,{symbol: this.currencySymbol});
            var offerPrice = dojo.currency.parse(catEntry.offerPrice,{symbol: this.currencySymbol});

            this.setPriceInProductList(productId,offerPrice);

            if(!catEntry.listPriced || listPrice <= offerPrice){
                innerHTML = "<span id='offerPrice_" + catEntry.catalogEntryIdentifier.uniqueID + "' class='price'>" + catEntry.offerPrice + "</span>";
            }
            else{
                innerHTML = "<span id='offerPrice_" + catEntry.catalogEntryIdentifier.uniqueID + "' class='price'> Now " + catEntry.offerPrice + "</span>"+"<br/>" +
                            "<span id='listPrice_" + catEntry.catalogEntryIdentifier.uniqueID + "' class='old_price'>" + catEntry.listPrice + "</span>";
            }
            document.getElementById('price_display_'+productId).innerHTML = innerHTML;

            innerHTML = "";
            if(shoppingActionsJS.displayPriceRange == true){
                for(var i in catEntry.priceRange){
                    if(catEntry.priceRange[i].endingNumberOfUnits == catEntry.priceRange[i].startingNumberOfUnits){
                        tempString = storeNLS['PQ_PRICE_X'];
                        innerHTML = innerHTML + "<p>" + dojo.string.substitute(tempString,{0: catEntry.priceRange[i].startingNumberOfUnits});
                    }
                    else if(catEntry.priceRange[i].endingNumberOfUnits != 'null'){
                        tempString = storeNLS['PQ_PRICE_X_TO_Y'];
                        innerHTML = innerHTML + "<p>" + dojo.string.substitute(tempString,{0: catEntry.priceRange[i].startingNumberOfUnits,
                            1: catEntry.priceRange[i].endingNumberOfUnits});
                    }
                    else{
                        tempString = storeNLS['PQ_PRICE_X_OR_MORE'];
                        innerHTML = innerHTML + "<p>" + dojo.string.substitute(tempString,{0: catEntry.priceRange[i].startingNumberOfUnits});
                    }
                    innerHTML = innerHTML + " <span class='price'>" + catEntry.priceRange[i].localizedPrice + "</span></p>";
                }
            }
            // Append productId so that element is unique in bundle page, where there can be multiple components
            var quantityDiscount = dojo.byId("productLevelPriceRange_"+productId);
            var itemQuantityDiscount = dojo.byId("itemLevelPriceRange_"+productId);

            // if product level price exists and no section to update item level price
            if(null != quantityDiscount && null == itemQuantityDiscount){
                dojo.style(quantityDiscount, "display", ""); //display product level price range
            }
            // if item level price range is present
            else if("" != innerHTML && null != itemQuantityDiscount){
                innerHTML = storeNLS['PQ_PURCHASE'] + innerHTML;
                itemQuantityDiscount.innerHTML = innerHTML;
                dojo.style(itemQuantityDiscount, "display", "");
                // hide the product level price range
                if(null != quantityDiscount){
                    dojo.style(quantityDiscount, "display", "none");
                }
            }
            // if item level price range is not present
            else if("" == innerHTML){
                if(null != itemQuantityDiscount){
                    dojo.style(itemQuantityDiscount, "display", "none"); //hide item level price range
                }
                if(null != quantityDiscount){
                    dojo.style(quantityDiscount, "display", ""); //display product level price range
                }
            }

            /*
             * If the product name is a link, do not replace the link only replace the text in the link.
             * Otherwise, replace the whole text
             */
            var productNameLink = dojo.query('#product_name_' + productId + ' > a');
            if(productNameLink.length == 1){
                productNameLink[0].innerHTML = catEntry.description[0].name;
            } else if(dojo.byId('product_name_'+productId)) {
                dojo.byId('product_name_'+productId).innerHTML = catEntry.description[0].name;
            }
            if(document.getElementById('product_shortdescription_'+productId)) {
                document.getElementById('product_shortdescription_'+productId).innerHTML = catEntry.description[0].shortDescription;
            }
            if(document.getElementById('product_SKU_'+productId)){
                document.getElementById('product_SKU_'+productId).innerHTML = storeNLS['SKU'] + " " + catEntry.catalogEntryIdentifier.externalIdentifier.partNumber;
            }
        }
     },

    /**
     *This method will show the WC Dialog popup
     *@param{String} widgetId The ID of the WC Dialog which should be shown
     */
    showWCDialogPopup : function(widgetId){
        var popup = dijit.byId(widgetId); //Change the id of the popup div if it is changed in the html
        if(popup != null){
            popup.closeButtonNode.style.display='none';
            popup.show();
        }
        else {
            // console.debug(widgetId +" does not exist");
        }
    },

    /**
     * To notify the change in attribute to other components that is subscribed to attributesChanged event.
     */
    notifyAttributeChange: function(catalogEntryID, entitledItemId){
        this.baseCatalogEntryId = catalogEntryID;
        var selectedAttributes = this.selectedAttributesList["entitledItem_" + catalogEntryID];
        dojo.publish('attributesChanged_'+catalogEntryID, [dojo.toJson(selectedAttributes)]);
        dojo.publish('attributesChanged', [dojo.toJson(selectedAttributes)]);
    },

    /**
     * To notify the change in quantity to other components that is subscribed to attributesChanged event.
     */
    notifyQuantityChange: function(quantity, entitledItemId){
        if (null != this.suolSwitch && "1" == this.suolSwitch) {
            this.checkForQuantityChange(entitledItemId);
        }
        this.updateQtyPricing(entitledItemId, quantity);
        dojo.publish('quantityChanged', [quantity]);
    },
    /**
     * This method displays the price * qty next to qty box.
     */
    updateQtyPricing: function(entitledItemId, quantity){
        var hasTierPricing = document.getElementById("hasTierPricing").value;
        if(hasTierPricing == "true"){
            if((quantity != 0 && quantity != '') ){
                var catalogEntryId = document.getElementById("productIdForQtyPrices").value;
                var volumePriceArray =e4xVolumePriceArray[catalogEntryId];
                volumePriceArray.sort(function(a,b) { return parseFloat(a.volume) - parseFloat(b.volume) } );

                if (typeof(volumePriceArray) != "undefined" && volumePriceArray.length > 0){
                    for(i=0;i<volumePriceArray.length;i++){
                        var volumePricelist =volumePriceArray[i];
                        var nextVolumePricelist =volumePriceArray[i+1];
                        var price = volumePricelist.value;
                        var volume = volumePricelist.volume ;
                        var maxLen = volumePriceArray.length - 1;

                        if((i == maxLen) || (quantity >= volume && quantity < nextVolumePricelist.volume)){
                            var finalPrice = quantity * price;
                            if(shoppingActionsJS.shipInternationalAttrValue == "true"){
                                var e4x_currency = cookies['db_intl_cur'];
                                if(e4x_currency == null || e4x_currency == 'undefined') e4x_currency = 'USD';
                                var localE4xCurrency = "USD";
                                if(e4x_currency!=""){
                                    localE4xCurrency = e4x_currency;
                                }
                            }
                            else{
                                var localE4xCurrency = "USD";
                            }

                            var currSymbol = "$";
                            if(localE4xCurrency == "USD" || localE4xCurrency ==""){currSymbol = "$";}else{currSymbol = localE4xCurrency+"&nbsp;";}
                                setElementHTML("quantity_price", currSymbol+"&nbsp;"+ getE4XPrice(localE4xCurrency,finalPrice));
                            break;
                        }
                    }
                }
                volumePriceArrayTemp = [];
            }
            else{
                setElementHTML("quantity_price", '');
            }
        }
        else{
            var numberOfSKUs = document.getElementById("numberOfSKUs").value;
            if(numberOfSKUs!= null && numberOfSKUs <= 1){
                var catalogEntryId = document.getElementById("productIdForQtyPrices").value;
            }
            else{
                var catalogEntryId = this.getCatalogEntryId(entitledItemId);
            }
            if(catalogEntryId!=null){
                if(quantity == '' || quantity == undefined){
                    var catId = entitledItemId.replace("entitledItem_" , "");
                    if(quantity = document.getElementById("quantity_"+catId)){
                        quantity = document.getElementById("quantity_"+catId).value;
                    }
                }
                if((quantity != 0 && quantity != '') ){

                    if(numberOfSKUs!= null && numberOfSKUs > 0){
                        var cId = catalogEntryId;
                    }else{
                        var cId = this.getCatalogEntryId(entitledItemId);
                    }
                    var pStr="";
                    if(cId != null){
                        pStr = getE4xProductPrice(cId );
                    }
                    var priceNum = Number(pStr.replace(/[^0-9\.]+/g,""));
                    priceNum = priceNum.toFixed(2);
                    var newprice = priceNum * quantity;
                    newprice = newprice.toFixed(2);
                    var currencyCode = pStr.replace(priceNum , "");
                    var finalPrice = currencyCode.concat(newprice );
                    setElementHTML("quantity_price", finalPrice);
                }
                else{
                    setElementHTML("quantity_price", '');
                }
            }
            else{
                setElementHTML("quantity_price", '');
            }
        }
    },

    /**
     * Initializes the compare check box for all the products added to compare.
     */
    initCompare: function(fromPage){
        if(fromPage == 'compare'){
            this.checkForCompare();
        } else {
            var cookieKey = this.cookieKeyPrefix + this.storeId;
            var newCookieValue = "";
            dojo.cookie(cookieKey, newCookieValue, {path:'/'});
        }

    },

    /**
     * Change the compare box status to checked or unchecked
     * @param{String} cbox The ID of the compare check box of the given catentry identifier.
     * @param{String} catEntryIdentifier The catalog entry identifer to current product.
     */
    changeCompareBox: function(cbox,catEntryIdentifier) {
        box = document.getElementById(cbox);
        box.checked = !box.checked;
        this.addOrRemoveFromCompare(catEntryIdentifier,box.checked, box);
    },

    /**
     * Adds or removes the product from the compare depending on the compare box checked or unchecked.
     * @param{String} catEntryIdentifier The catalog entry identifer to current product.
     * @param{boolean} checked True if the checkbox is checked or False
     */
    addOrRemoveFromCompare: function(catEntryIdentifier, checked, box){
        //box = eval(cbox);
        if(checked){
            this.addToCompare(catEntryIdentifier);
            //add class to parent
            box.parentNode.setAttribute("class", "compare_target selected");
        }
        else{
            this.removeFromCompare(catEntryIdentifier);
            //remove class from parent
            box.parentNode.setAttribute("class", "compare_target");
        }
    },

    /**
     * Throw error message if no items selected for Compare :: PNX -- 1329
     *
     */
    compareErrorMessage : function () {
        MessageHelper.displayErrorMessage(storeNLS['COMPARE_ITEMS_ERROR']);
    },
    /**
     * Adds the product to the compare cookie.
     * @param{String} catEntryIdentifier The catalog entry identifer to current product.
     */
    addToCompare:function(catEntryIdentifier){

        var cookieKey = this.cookieKeyPrefix + this.storeId;
        var cookieValue = dojo.cookie(cookieKey);

        if(cookieValue != null){
            if(cookieValue.indexOf(catEntryIdentifier) != -1 || catEntryIdentifier == null){
                MessageHelper.displayErrorMessage(storeNLS["COMPARE_ITEM_EXISTS"]);
                return;
            }
        }

        var currentNumberOfItemsInCompare = 0;
        if(cookieValue != null && cookieValue != ""){
            currentNumberOfItemsInCompare = cookieValue.split(this.cookieDelimiter).length;
        }

        if (currentNumberOfItemsInCompare < parseInt(this.maxNumberProductsAllowedToCompare)) {
            var newCookieValue = "";
            if(cookieValue == null || cookieValue == ""){
                newCookieValue = catEntryIdentifier;
            }
            else{
                newCookieValue = cookieValue + this.cookieDelimiter + catEntryIdentifier;
            }
            dojo.cookie(cookieKey, newCookieValue, {path:'/'});
            document.getElementById("compare_"+catEntryIdentifier).style.visibility = "visible";
            // console.debug("Product added to cookie");
            currentNumberOfItemsInCompare = currentNumberOfItemsInCompare + 1;
            if(currentNumberOfItemsInCompare == this.minNumberProductsAllowedToCompare){
                var compareButtonDisabled = document.getElementById("compare_button_disabled");
                var compareButtonEnabled = document.getElementById("compare_button_enabled");
                //compareButton.className = "button_primary";
                //compareButton.onclick = function()
                //{
                //  setCurrentId('compare_button');
                //  shoppingActionsJS.compareProducts();
                //}
                compareButtonDisabled.style.display = "none";
                compareButtonEnabled.style.display = "block";
                dojo.removeClass("compare_button", "disabled");
            }
        } else {
            //MessageHelper.displayErrorMessage(storeNLS["COMPATE_MAX_ITEMS"]);
            this.showWCDialogPopup('widget_product_comparison_popup');
            document.getElementById("comparebox_"+catEntryIdentifier).checked = false;
            // console.debug("You can only compare up to 4 products");
        }
    },

    /**
     * Removes the product from the compare cookie.
     * @param{String} catEntryIdentifier The catalog entry identifer to current product.
     */
    removeFromCompare: function(catEntryIdentifier){
        var cookieKey = this.cookieKeyPrefix + this.storeId;
        var cookieValue = dojo.cookie(cookieKey);
        var currentNumberOfItemsInCompare = 0;
        if(cookieValue != null){
            if(dojo.trim(cookieValue) == ""){
                dojo.cookie(cookieKey, null, {expires: -1});
            }else{
                var cookieArray = cookieValue.split(this.cookieDelimiter);
                var newCookieValue = "";
                for(index in cookieArray){
                    if(cookieArray[index] != catEntryIdentifier){
                        if(newCookieValue == ""){
                            newCookieValue = cookieArray[index];
                        }else{
                            newCookieValue = newCookieValue + this.cookieDelimiter + cookieArray[index];
                        }
                    }
                }
                dojo.cookie(cookieKey, newCookieValue, {path:'/'});
                currentNumberOfItemsInCompare = newCookieValue.split(this.cookieDelimiter).length;
            }
            document.getElementById("compare_"+catEntryIdentifier).removeAttribute("style");
            // console.debug("Product removed from cookie");
            if(currentNumberOfItemsInCompare < this.minNumberProductsAllowedToCompare){
                var compareButtonDisabled = document.getElementById("compare_button_disabled");
                var compareButtonEnabled = document.getElementById("compare_button_enabled");
                //compareButton.className = "button_secondary";
                //compareButton.removeAttribute("onclick");
                compareButtonDisabled.style.display = "inline";
                compareButtonEnabled.style.display = "none";
                dojo.addClass("compare_button", "disabled");
            }
        }
    },

    /**
     * Re-directs the browser to the CompareProductsDisplay page to compare products side-by-side.
     */
    compareProducts:function(categoryIds){
        var url = "CompareProductsDisplayView?storeId=" + this.storeId + "&catalogId=" + this.catalogId + "&langId=" + this.langId + "&compareReturnName=" + this.compareReturnName + "&searchTerm="+ this.searchTerm;

        if('' != categoryIds.top_category){
            url = url + "&top_category=" + categoryIds.top_category;
        }
        if('' != categoryIds.parent_category_rn){
            url = url + "&parent_category_rn=" + categoryIds.parent_category_rn;
        }
        if('' != categoryIds.categoryId){
            url = url + "&categoryId=" + categoryIds.categoryId;
        }

        var cookieKey = this.cookieKeyPrefix + this.storeId;
        var cookieValue = dojo.cookie(cookieKey);
        if(cookieValue != null && dojo.trim(cookieValue) != ""){
            url = url + "&catentryId=" + cookieValue;
        }
        var returnUrl = location.href;
        if(returnUrl.indexOf("?") == -1){
            returnUrl = returnUrl + "?fromPage=compare";
        } else if(returnUrl.indexOf("fromPage=compare") == -1){
            returnUrl = returnUrl + "&fromPage=compare";
        }
        url = url + "&returnUrl=" + encodeURIComponent(returnUrl);
        location.href = getAbsoluteURL() + url;
    },

    /**
     * Sets the quantity of a product in the list (bundle)
     *
     * @param {String} catalogEntryType, type of catalogEntry (item/product/bundle/package)
     * @param {int} catalogEntryId The catalog entry identifer to current product.
     * @param {int} quantity The quantity of current product.
     * @param {float} price The price of current product.
     */
    setProductQuantity: function(catalogEntryType, catalogEntryId, quantity, price){
        var productDetails = null;
        if(this.productList[catalogEntryId]){
            productDetails = this.productList[catalogEntryId];
        } else {
            productDetails = new Object();
            this.productList[catalogEntryId] = productDetails;
            productDetails.baseItemId = catalogEntryId;
            if("item" == catalogEntryType){
                productDetails.id = catalogEntryId;
            } else {
                productDetails.id = 0;
            }
        }
        productDetails.quantity = quantity;
        dojo.publish('quantityChanged', [dojo.toJson(productDetails)]);

        productDetails.price = dojo.number.parse(price);
    },

    /**
     * Sets the quantity of a product in the list (bundle)
     *
     * @param {int} catalogEntryId The catalog entry identifer to current product.
     * @param {int} quantity The quantity of current product.
     */
    quantityChanged: function(catalogEntryId, quantity){
        if(this.productList[catalogEntryId]){
            var productDetails = this.productList[catalogEntryId];
            productDetails.quantity = dojo.trim(quantity);
            dojo.publish('quantityChanged', [dojo.toJson(productDetails)]);
        }
    },

    /**
     * Sets the price of a product in the list (bundle)
     *
     * @param {int} catalogEntryId The catalog entry identifer to current product.
     * @param {int} price The price of current product.
     */
    setPriceInProductList: function(catalogEntryId, price){
        var productDetails = this.productList[catalogEntryId];
        if(productDetails){
            productDetails.price = price;
        }
    },

    /**
     * Select bundle item swatch
     *
     * @param {int} catalogEntryId The catalog entry identifer to current product.
     * @param {String} swatchName
     * @param {String} swatchValue
     * @param {String} doNotDisable, the first swatch, that should not be disabled
     */
     selectBundleItemSwatch: function(catalogEntryId, swatchName, swatchValue, doNotDisable){
        if(dojo.hasClass("swatch_link_" + catalogEntryId + "_" + swatchName + "_" + swatchValue, "color_swatch_disabled")){
            return false;
        }
        if (dojo.byId("entitledItem_"+catalogEntryId)!=null) {
            var entitledItemJSON;
            var currentSwatchkey = swatchName + "_" +swatchValue;
            //the json object for entitled items are already in the HTML.
            entitledItemJSON = dojo.fromJson(dojo.byId("entitledItem_"+catalogEntryId).innerHTML);
            var validSwatchArr = new Array();
            for(idx in entitledItemJSON){
                var validItem = false;
                var entitledItem = entitledItemJSON[idx];
                for(attribute in entitledItem.Attributes){

                    if(currentSwatchkey == attribute){
                        validItem = true;
                        break;
                    }
                }
                if(validItem){
                    for(attribute in entitledItem.Attributes){
                        var currentSwatch = attribute.substring(0, attribute.lastIndexOf("_"));
                        if(currentSwatch != doNotDisable && currentSwatch != swatchName){
                            validSwatchArr.push(attribute);
                        }
                    }
                }
            }

            var swatchesDisabled = new Array();
            var selectedSwatches = new Array();
            for(idx in entitledItemJSON){
                var entitledItem = entitledItemJSON[idx];
                for(attribute in entitledItem.Attributes){
                    var currentSwatch = attribute.substring(0, attribute.lastIndexOf("_"));
                    if(currentSwatch != doNotDisable && currentSwatch != swatchName){
                        var swatchLinkId = "swatch_link_" + catalogEntryId +"_" + attribute;
                        if(dojo.indexOf(validSwatchArr, attribute) > -1){
                            if(!dojo.hasClass(swatchId,"color_swatch_selected")){
                                dojo.byId(swatchLinkId).className = "color_swatch";
                                dojo.byId(swatchLinkId).src = dojo.byId(swatchLinkId).src.replace("_disabled.png","_enabled.png");

                                //change the title text of the swatch link
                                dojo.byId(swatchLinkId).title = dojo.byId(swatchId).alt;
                                document.getElementById(swatchLinkId).setAttribute("aria-disabled", "false");
                            }
                        } else if(dojo.indexOf(swatchesDisabled, attribute) == -1){
                            swatchesDisabled.push(attribute);
                            if(dojo.hasClass(swatchLinkId,"color_swatch_selected")){
                                selectedSwatches.push(swatchLinkId);
                            }
                            dojo.byId(swatchLinkId).className = "color_swatch_disabled";
                            dojo.byId(swatchLinkId).src = dojo.byId(swatchLinkId).src.replace("_enabled.png","_disabled.png");

                            //change the title text of the swatch link
                            var titleText = storeNLS["INV_ATTR_UNAVAILABLE"];
                            var altText = dojo.byId(swatchLinkId).alt;

                            dojo.byId(swatchLinkId).title = dojo.string.substitute(titleText,{0: altText});
                            document.getElementById(swatchLinkId).setAttribute("aria-disabled", "true");
                        }
                    }
                    if (document.getElementById("swatch_link_" + catalogEntryId +"_" + attribute) != null) {
                        document.getElementById("swatch_link_" + catalogEntryId +"_" + attribute).setAttribute("aria-checked", "false");
                    }
                }
            }

            for(idx in selectedSwatches){
                var selectedSwatch = selectedSwatches[idx];
                var idSelector = selectedSwatch.substring(0, selectedSwatch.lastIndexOf("_"));
                var swatchSelected = false;
                dojo.query("[id^='" + idSelector + "']").forEach(function(node, index, arr){
                    if(!swatchSelected && dojo.hasClass(node,"color_swatch")){
                        var values = node.id.split("_");
                        shoppingActionsJS.selectBundleItemSwatch(values[1],values[2],values[3], doNotDisable);
                        shoppingActionsJS.setSelectedAttributeOfProduct(values[1],values[2],values[3],false);
                        swatchSelected = true;
                    }
                });
            }
        }

        if (dojo.byId("swatch_selection_" + catalogEntryId + "_" + swatchName).style.display == "none") {
            dojo.byId("swatch_selection_" + catalogEntryId + "_" + swatchName).style.display = "inline";
        }
        dojo.byId("swatch_selection_" + catalogEntryId + "_" + swatchName).innerHTML = swatchValue;

        var swatchItemLink = "swatch_link_" + catalogEntryId + "_" + swatchName + "_";

        dojo.query("img[id^='" + swatchItemLink + "']").forEach(function(node, index, arr){
            if(dojo.hasClass(node, "color_swatch_disabled")){
                dojo.removeClass(node, "color_swatch")
            } else {
                dojo.addClass(node, "color_swatch");
            }
            dojo.removeClass(node, "color_swatch_selected");
        });

        dojo.byId(swatchItemLink + swatchValue).className = "color_swatch_selected";
        document.getElementById(swatchItemLink + swatchValue).setAttribute("aria-checked", "true");

        this.setSelectedAttributeOfProduct(catalogEntryId, swatchName, swatchValue,false);
        // select image
        this.changeBundleItemImage(catalogEntryId, swatchName, swatchValue, "productThumbNailImage_" + catalogEntryId);

    },

    changeBundleItemImage: function(catalogEntryId, swatchAttrName, swatchAttrValue, skuImageId){
        var entitledItemId = "entitledItem_" + catalogEntryId;
        if (dojo.byId(entitledItemId)!=null) {
            //the json object for entitled items are already in the HTML.
             entitledItemJSON = eval('('+ dojo.byId(entitledItemId).innerHTML +')');
        }

        this.setEntitledItems(entitledItemJSON);

        var skuImage = null;
        var imageArr = shoppingActionsJS.getImageForBundleItem(catalogEntryId);
        if(imageArr != null){
            skuImage = imageArr[1];
        }

        if(skuImageId != undefined){
            this.setSKUImageId(skuImageId);
        }

        if(skuImage != null){
            if(dojo.byId(this.skuImageId) != null){
                document.getElementById(this.skuImageId).src = skuImage;
            }
        } else {
            var imageFound = false;
            for (x in this.entitledItems) {
                var Attributes = this.entitledItems[x].Attributes;
                var itemImage = this.entitledItems[x].ItemThumbnailImage;

                for(y in Attributes){
                    var index = y.indexOf("_");
                    var entitledSwatchName = y.substring(0, index);
                    var entitledSwatchValue = y.substring(index+1);

                    if (entitledSwatchName == swatchAttrName && entitledSwatchValue == swatchAttrValue) {
                        document.getElementById(this.skuImageId).src = itemImage;
                        imageFound = true;
                        break;
                    }
                }

                if(imageFound){
                    break;
                }
            }
        }
    },

    getImageForBundleItem: function(entitledItemId){
        var attributeArray = [];
        var selectedAttributes = this.selectedProducts[entitledItemId];
        for(attribute in selectedAttributes){
            attributeArray.push(attribute + "_" + selectedAttributes[attribute]);
        }
        return this.resolveImageForSKU(attributeArray);
    },

    personalizeIndividualItems: function(customParams, catalogEntryID, event){
        var quanitity = document.getElementById('quantity_'+catalogEntryID).value;
        var persnIndiv = document.getElementById("persnIndiv");
        var individualPersonalizedItemsCount = document.getElementById('individualPersonalizedItemsCount').value;
        if(quanitity != undefined && quanitity > 10){
            //quanitity = 10;
            if(persnIndiv !=null && persnIndiv !=undefined && persnIndiv.checked){
                MessageHelper.displayErrorMessage(MessageHelper.messages['PDP_PERSONALIZED_MAX_LINES']);
                return;
            }
        }
        if(event != undefined && event == 'addToCartClicked'){
            quanitity = quanitity - individualPersonalizedItemsCount;
        }
        var MyDiv2 = document.getElementById('individualPersonalizedItems');

        if(persnIndiv !=null && persnIndiv !=undefined && persnIndiv.checked){
        var customParams = customParams.split(/,/);
        for(idx=1; idx<quanitity; idx++){

            if(individualPersonalizedItemsCount != ""){

                individualPersonalizedItemsCount = parseInt(individualPersonalizedItemsCount);
            }
            else{
                individualPersonalizedItemsCount = 0;
            }

            var updateCount = individualPersonalizedItemsCount + parseInt(idx);

             for(i = 1; i <= customParams.length; i++) {



                 var divId = 'personalizedAttr_'+i;
                 var MyDiv1 = document.getElementById(divId);



                 var newNode = document.createElement('personalizedAttrDynamicDiv_'+updateCount);



                 newNode.innerHTML = MyDiv1.innerHTML;






                 var elm = {};
                 var elms = newNode.getElementsByTagName("*");
                 for (var j = 0; j < elms.length; j++) {
                     var elementname = elms[j].name;
                     if (elementname != undefined && elementname.indexOf("attrValue") > -1) {

                         if(event != undefined && event == 'addToCartClicked'){
                             elms[j].id = elms[j].id+"_line"+updateCount;
                         }
                         else{
                             elms[j].id = elms[j].id+"_line"+idx;
                         }
                     }
                 }
                 if(i == 1){
                     var span = document.createElement('span')
                     span.innerHTML = 'Personalization'
                         MyDiv2.appendChild(span);
                 }
                 MyDiv2.appendChild( newNode );
             }


             document.getElementById('individualPersonalizedItemsCount').value= updateCount;
            }
        }
        else{
            MyDiv2.innerHTML = "";
            document.getElementById('individualPersonalizedItemsCount').value='';
        }
    },

    adjustpersonalizeIndividualItems: function(newQty, catalogEntryID){

        var indvItems = document.getElementById('individualPersonalizedItemsCount').value;

        if(indvItems >= newQty){
            var newNode = document.getElementById('individualPersonalizedItems');
            var elm = {};
            var elms = newNode.getElementsByTagName("*");
            for (var j = 0; j < elms.length; j++) {
                var elementname = elms[j].id;
                if (elementname != undefined && elementname.indexOf("personalizedAttrDynamicDiv") > -1) {


                    elementname.remove();

                }
            }
        }
    },

    togglePersonalizedItems: function(customParams){
        var orderWithoutPersn = document.getElementById("orderWithoutPersn");
        var persnIndiv = document.getElementById("persnIndiv");
        var MyDiv2 = document.getElementById('individualPersonalizedItems');
        var elm = {};
        var elms = MyDiv2.getElementsByTagName("*");
        for (var j = 0; j < elms.length; j++) {
            var elementname = elms[j].name;
            if (elementname != undefined && elementname.indexOf("attrValue") > -1) {
                if(orderWithoutPersn.checked){
                    elms[j].disabled = true;
                }else{
                    elms[j].disabled = false;
                }
            }
        }

        if(persnIndiv!= null){
            if(orderWithoutPersn.checked){
                document.getElementById("persnIndiv").disabled= true;
            }
            else{
                document.getElementById("persnIndiv").disabled= false;
            }
        }
        var customParams = customParams.split(/,/);
           for(i = 0; i < customParams.length; i++) {
               var catentID = customParams[i].trim();

                   if(orderWithoutPersn.checked){
                       document.getElementById(catentID).disabled = true;
                   }else{
                       document.getElementById(catentID).disabled = false;
                   }
           }
   },

    /**
     * Check if any product is already selected for compare in other pages and select them
     */
    checkForCompare: function() {
        var cookieKey = this.cookieKeyPrefix + this.storeId;
        var cookieValue = dojo.cookie(cookieKey);
        var currentNumberOfItemsInCompare = 0;

        if(cookieValue != null) {
            var catEntryIds = cookieValue.split(this.cookieDelimiter);

            for(idx=0; idx<catEntryIds.length; idx++){
                var catEntryIdentifier = catEntryIds[idx];

                if(null != dojo.byId("compare_"+catEntryIdentifier)){
                    dojo.byId("comparebox_"+catEntryIdentifier).checked = true;
                    dojo.byId("compare_"+catEntryIdentifier).style.visibility = "visible";
                }

                if(++currentNumberOfItemsInCompare == this.minNumberProductsAllowedToCompare){
                    var compareButtonDisabled = dojo.byId("compare_button_disabled");
                    var compareButtonEnabled = dojo.byId("compare_button_enabled");
                    compareButtonDisabled.style.display = "none";
                    compareButtonEnabled.style.display = "block";
                    dojo.removeClass("compare_button", "disabled");
                }
            }
        }
    },

    /**
    * replaceItemAjaxHelper This function is used to replace an item in the cart. This will be called from the {@link fastFinderJS.ReplaceItemAjax} method.
    *
    * @param {String} catalogEntryId The catalog entry of the item to replace to the cart.
    * @param {int} qty The quantity of the item to add.
    * @param {String} removeOrderItemId The order item ID of the catalog entry to remove from the cart.
    * @param {String} addressId The address ID of the order item.
    * @param {String} shipModeId The shipModeId of the order item.
    * @param {String} physicalStoreId The physicalStoreId of the order item.
    *
    **/
    replaceItemAjaxHelper : function(catalogEntryId,qty,removeOrderItemId,addressId,shipModeId,physicalStoreId){

        var params = [];
        params.storeId = WCParamJS.storeId;
        params.catalogId = WCParamJS.catalogId;
        params.langId = WCParamJS.langId;
        params.orderItemId  = removeOrderItemId;
        params.orderId = ".";
        if(CheckoutHelperJS.shoppingCartPage){
            params.calculationUsage = "-1,-2,-5,-6,-7";
        }else{
            params.calculationUsage = "-1,-2,-3,-4,-5,-6,-7";
        }

        var params2 = [];
        params2.storeId = WCParamJS.storeId;
        params2.catalogId = WCParamJS.catalogId;
        params2.langId = WCParamJS.langId;
        params2.catEntryId  = catalogEntryId;
        params2.quantity = qty;
        params2.orderId = ".";
        if(CheckoutHelperJS.shoppingCartPage){
            params2.calculationUsage = "-1,-2,-5,-6,-7";
        }else{
            params2.calculationUsage = "-1,-2,-3,-4,-5,-6,-7";
        }

        var params3 = [];
        params3.storeId = WCParamJS.storeId;
        params3.catalogId = WCParamJS.catalogId;
        params3.langId = WCParamJS.langId;
        params3.orderId = ".";
        if(CheckoutHelperJS.shoppingCartPage){
            params3.calculationUsage = "-1,-2,-5,-6,-7";
        }else{
            params3.calculationUsage = "-1,-2,-3,-4,-5,-6,-7";
        }

        var shipInfoUpdateNeeded = false;
        var orderItemReqd = true;
        if(addressId != null && addressId != ""){
            params3.addressId = addressId;
        }
        if(shipModeId != null && shipModeId != ""){
            params3.shipModeId = shipModeId;
        }
        if(physicalStoreId != null && physicalStoreId != ""){
            params3.physicalStoreId = physicalStoreId;
            orderItemReqd = false;
        }
        if(params3.shipModeId != null && (params3.addressId != null || params3.physicalStoreId != null)){
            shipInfoUpdateNeeded = true;
        }

        if(orderItemReqd){
            params3.allocate="***";
            params3.backorder="***";
            params3.remerge="***";
            params3.check="*n";
        }

        //Delcare service for deleting item...
        wc.service.declare({
            id: "AjaxReplaceItem",
            actionId: "AjaxReplaceItem",
            url: "AjaxOrderChangeServiceItemDelete",
            formId: ""

            ,successHandler: function(serviceResponse) {
                //Now add the new item to cart..
                if(!shipInfoUpdateNeeded){
                    //We dont plan to update addressId and shipMOdeId..so call AjaxAddOrderItem..
                    wc.service.invoke("AjaxAddOrderItem", params2);
                }
                else{
                    //We need to update the adderessId and shipModeId..so call our temp service to add..
                    wc.service.invoke("AjaxAddOrderItemTemp", params2);
                }
            }

            ,failureHandler: function(serviceResponse) {
                if (serviceResponse.errorMessage) {
                             MessageHelper.displayErrorMessage(serviceResponse.errorMessage);
                      } else {
                             if (serviceResponse.errorMessageKey) {
                                    MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
                             }
                      }
                      cursor_clear();
            }

        });

        //Delcare service for adding item..
        wc.service.declare({
            id: "AjaxAddOrderItemTemp",
            actionId: "AjaxAddOrderItemTemp",
            url: "AjaxOrderChangeServiceItemAdd",
            formId: ""

            ,successHandler: function(serviceResponse) {
                if(orderItemReqd){
                    // setting the newly created orderItemId
                    params3.orderItemId = serviceResponse.orderItemId[0];
                }

                MessageHelper.displayStatusMessage(MessageHelper.messages["SHOPCART_ADDED"]);

                //Now item is added.. call update to set addressId and shipModeId...
                wc.service.invoke("OrderItemAddressShipMethodUpdate", params3);
            }

            ,failureHandler: function(serviceResponse) {
                MessageHelper.displayErrorMessage(serviceResponse.errorMessageKey);
            }
        });

        //For Handling multiple clicks
        if(!submitRequest()){
            return;
        }
        cursor_wait();
        wc.service.invoke("AjaxReplaceItem",params);
    },

    /**
    * customizeDynamicKit This function is used to call the configurator page for a dynamic kit.
    * @param {String} catEntryIdentifier A catalog entry ID of the item to add to the cart.
    * @param {int} quantity A quantity of the item to add to the cart.
    * @param {Object} customParams - Any additional parameters that needs to be passed to the configurator page.
    *
    **/
    customizeDynamicKit : function(catEntryIdentifier, quantity, customParams)
    {
        var params = [];
        params.storeId    = this.storeId;
        params.catalogId  = this.catalogId;
        params.langId    = this.langId;
        params.catEntryId  = catEntryIdentifier;
        params.quantity    = quantity;

        if(!isPositiveInteger(quantity)){
            MessageHelper.displayErrorMessage(storeNLS['QUANTITY_INPUT_ERROR']);
            return;
        }

        var contractIdElements = document.getElementsByName('contractSelectForm_contractId');
        if (contractIdElements != null && contractIdElements != "undefined") {
            for (i=0; i<contractIdElements.length; i++) {
                if (contractIdElements[i].checked) {
                    params.contractId = contractIdElements[i].value;
                    break;
                }
            }
        }

        //Pass any other customParams set by other add on features
        if(customParams != null && customParams != 'undefined'){
            for(i in customParams){
                params[i] = customParams[i];
            }
        }

        //For Handling multiple clicks
        if(!submitRequest()){
            return;
        }
        cursor_wait();

        var configureURL = "ConfigureView";
        var i =0;
        for(param in params){
            configureURL += ((i++ == 0)? "?" : "&") + param + "=" + params[param];
        }
        document.location.href = getAbsoluteURL() + configureURL;
    },

 // This function is to check if there are values selected or input for Personalization Products
 // for a given pAttrName, pAttrValue

  checkPersonalizationOptions : function(pAttrNameArray){
             for (var f = 0; f < pAttrNameArray.length; f++) {
              pAttrNameArray[f] = trim(pAttrNameArray[f]);
               var pAttrValue = document.getElementById(pAttrNameArray[f]).value;
               if(pAttrValue!=null && pAttrValue == ""){
                     MessageHelper.displayErrorMessage(storeNLS['PERSONALIZATION_INPUT_ERROR']);
                     return true;
               }
        }
 },

 minimumQuantityReqdForPersonalization : function(quantity){
     var pQty = document.getElementById("minp13nqty").value;
     if(quantity/1 < pQty/1){
         MessageHelper.displayErrorMessage(storeNLS['PERSONALIZATION_MIN_QTY_INPUT_ERROR']);
         return true;
     }
 },

 enableOrDisableAddtoCartBtn : function(flag){
    var a2cBtn = dojo.byId("add2CartBtn");

    if(typeof a2cBtn !== 'undefined' && a2cBtn !== null){
         if(flag){
             a2cBtn.setAttribute("disabled","true");
             dojo.addClass("add2CartBtn","disabled");//required for IE8
         }
         else {
             a2cBtn.removeAttribute("disabled");
             dojo.removeClass("add2CartBtn", "disabled");
         }
     }
 },

 isShipInternationalCheck : function(shipInternationalFlag, cookieValue){
     if(shipInternationalFlag != "true" && (cookieValue != null && cookieValue.length > 0 && cookieValue != "US")){
         return false;
     }
     else {
         return true;
     }

 },
 checkInvRequired : function(catentryId){
     //check value of SpecialOrderable:  0 is no,  1 is specialorderable
     var so_switch = dojo.byId('so_switch').innerHTML;
     if(so_switch == "1") {
         var soSkusStr = dojo.byId('specialOrderableSkus').value;
         var soItems = soSkusStr.split(',');
         for (i=0; i<soItems.length; i++) {
             var catId = soItems[i];
             if(catId === catentryId) {
                 // sku is an SO item, skip inv check.
                 return false;
             }
         }
         // sku is not special orderable so return true for invcheck.
        return true;
     }
     else{
         // Special Order Switch is disabled so continue with invCheck =true.
         return true;
     }
 },

 setProductDisplayPrice : function(Id) {
     var id = "#"+Id;
     var dataDivId = "#product_price_"+Id+"_data";
     var commonPricePrefixTextDiv = "#commonPricePrefixText";
     var empty = function (aVar) {
         return aVar == undefined || aVar ==null || aVar == "";
      }

       var minOP;
       var maxOP;
       var minRP;
       var maxRP;
       var minTP;
       var maxTP;
       var minRP_Without_OP;
       var maxRP_Without_OP;
       var currCode;
       var currencyCookie = e4x_currency;
       var isShipIntl = dojo.query(dataDivId + " #isShipInternational").val();
       var priceHTML = "";
       var priceNotAvailable = dojo.query(commonPricePrefixTextDiv + " #priceNotAvailable").text();
       var rangeSeparator = dojo.query(commonPricePrefixTextDiv + " #rangeSeparator").text();

       var minRP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #minregularprice").val());
       var maxRP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #maxregularprice").val());
       var minOP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #minofferprice").val());
       var maxOP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #maxofferprice").val());
       var minTP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #mintierprice").val());

       var minRP_Without_OP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #minRegWithoutOfferPrice").val());
       var maxRP_Without_OP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #maxRegWithoutOfferPrice").val());

       //format all prices on the server for caching
       var minRP_USD = dojo.query(dataDivId + " #minRP_USD").val();
       var maxRP_USD = dojo.query(dataDivId + " #maxRP_USD").val();

       var minOP_USD = dojo.query(dataDivId + " #minOP_USD").val();
       var maxOP_USD = dojo.query(dataDivId + " #maxOP_USD").val();

       var minTP_USD = dojo.query(dataDivId + " #minTP_USD").val();
       var maxTP_USD = dojo.query(dataDivId + " #maxTP_USD").val();

       var minRP_Without_OP_USD = dojo.query(dataDivId + " #minRegWithoutOfferPrice_USD").val();
       var maxRP_Without_OP_USD = dojo.query(dataDivId + " #maxRegWithoutOfferPrice_USD").val();

       var noOfSkusNotOnSale = !empty(parseInt(dojo.query(dataDivId + " #noOfSkusNotOnSale").val())) ? parseInt(dojo.query(dataDivId + " #noOfSkusNotOnSale").val()) : 0;

       var isProductOnSale = (empty(minOP_USD) && empty(maxOP_USD)) ? false : true;

       var standardPrice_Class = "pd_standardprice";
       var originalPrice_Class = "pd_origprice";
       var offerPriceClass = "pd_saleprice";
       var priceDisplayId = "offerPrice_"+Id;

       //on client side, determine what currency to show based on currencyCookie and isShipIntl flag
       if(isShipIntl != undefined && isShipIntl != null && isShipIntl == 1 && currencyCookie != undefined && currencyCookie != '' && currencyCookie != 'USD') {
           var currSymbol = currencyCookie + '&nbsp;';
             if(!empty(minOP_USD)) {
                minOP = currSymbol + getE4XPrice(currencyCookie,minOP_USD_unformatted);
             } else {
                minOP = '';
             }
             if(!empty(maxOP_USD)) {
                maxOP = currSymbol + getE4XPrice(currencyCookie,maxOP_USD_unformatted);
             } else {
                maxOP = '';
             }
             if(!empty(minRP_USD)) {
                minRP = currSymbol + getE4XPrice(currencyCookie,minRP_USD_unformatted);
             } else {
                minRP = '';
             }
             if(!empty(maxRP_USD)) {
                maxRP = currSymbol + getE4XPrice(currencyCookie,maxRP_USD_unformatted);
             } else {
                maxRP = '';
             }
             if(!empty(minTP_USD)) {
                minTP = currSymbol + getE4XPrice(currencyCookie,minTP_USD_unformatted);
             } else {
                minTP = '';
             }
             if(!empty(minRP_Without_OP_USD)) {
                minRP_Without_OP = currSymbol + getE4XPrice(currencyCookie,minRP_Without_OP_USD_unformatted);
             } else {
                minRP_Without_OP = '';
             }
             if(!empty(maxRP_Without_OP_USD)) {
                maxRP_Without_OP = currSymbol + getE4XPrice(currencyCookie,maxRP_Without_OP_USD_unformatted);
             } else {
                maxRP_Without_OP = '';
             }
       } else {
             minOP = minOP_USD;
             maxOP = maxOP_USD;
             minRP = minRP_USD;
             maxRP = maxRP_USD;
             minTP = minTP_USD;
             minRP_Without_OP = minRP_Without_OP_USD;
             maxRP_Without_OP = maxRP_Without_OP_USD;
       }

       //If there is no regular or offer price or tierprice, display "Price not available"
       if (empty(minOP) && empty(maxOP) && empty(minRP) && empty(maxRP) && empty(minTP) && empty(maxTP) ) {
         priceHTML = "<span class='"+standardPrice_Class+"'>" + priceNotAvailable + "</span>";
       }

       //If there is tier price, display the mintierprice with "As low as" in front of it
       if (!empty(minTP)) {
          priceHTML = dojo.query(commonPricePrefixTextDiv + " #searchLowPrice").text()+ " " + minTP;
       }

       if(!isProductOnSale){
           // If there is only regular price and the product is not on sale, display the regular price.
           if (!empty(minRP) && !empty(maxRP) ){
             priceHTML = "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + minRP + "</span>";
           }

           //If there is range of regular price and the product is not on sale, display the regular price range.
           if (!empty(minRP) && !empty(maxRP) && minRP_USD_unformatted != maxRP_USD_unformatted){
               priceHTML = "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>"  + priceHTML + rangeSeparator + maxRP + "</span>";
           }
       }else{
            //As the product is on sale We need to have list price striked off. When we have list price range for skus.
            if (!empty(minRP) && !empty(maxRP) && !empty(maxRP_USD_unformatted) && !empty(minRP_USD_unformatted) && maxRP_USD_unformatted > minRP_USD_unformatted) {
               priceHTML = "<span class='"+originalPrice_Class+"'>" + minRP + rangeSeparator + maxRP + "</span></br>";
             }
            //As the product is on sale We need to have list price striked off. When we have same listprice for all skus.
            if(!empty(minRP) && !empty(maxRP) && !empty(maxRP_USD_unformatted) && !empty(minRP_USD_unformatted) && maxRP_USD_unformatted == minRP_USD_unformatted){
                priceHTML = "<span  class='"+originalPrice_Class+"'>" + minRP + "</span>&nbsp;" ;
            }

            //Partial Sale (Some skus are not on sale)
            if(noOfSkusNotOnSale > 0){
                if(!empty(minRP_Without_OP_USD_unformatted) && !empty(maxRP_Without_OP_USD_unformatted) && !empty(minOP_USD_unformatted) && !empty(maxOP_USD_unformatted)){
                    //
                    if((minRP_Without_OP_USD_unformatted < minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted < maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + minRP_Without_OP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>";
                    }
                    //
                    if((minRP_Without_OP_USD_unformatted < minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted > maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + minRP_Without_OP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + maxRP_Without_OP + "</span>";
                    }
                    //
                    if((minRP_Without_OP_USD_unformatted < minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted == maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + minRP_Without_OP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>";
                    }

                    //
                    if((minRP_Without_OP_USD_unformatted > minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted < maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>";
                    }
                    //
                    if((minRP_Without_OP_USD_unformatted > minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted > maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + maxRP_Without_OP + "</span>";
                    }
                    //
                    if((minRP_Without_OP_USD_unformatted > minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted == maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>";
                    }

                    //
                    if((minRP_Without_OP_USD_unformatted == minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted < maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>";
                    }
                    //
                    if((minRP_Without_OP_USD_unformatted == minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted > maxOP_USD_unformatted)){
                        priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + maxRP_Without_OP + "</span>";
                    }
                    //
                    if((minRP_Without_OP_USD_unformatted == minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted == maxOP_USD_unformatted)){
                        if(minOP_USD_unformatted == maxOP_USD_unformatted){
                            priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>";
                        }else{
                            priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>";
                        }
                    }
                }
            }else{
                //All the skus are on sale and has a range.
                if (!empty(maxOP_USD_unformatted) && !empty(minOP_USD_unformatted) && maxOP_USD_unformatted > minOP_USD_unformatted) {
                    priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + rangeSeparator + maxOP + "</span>";
                }
                //All the skus are on sale and has same sale price.
                if(!empty(maxOP_USD_unformatted) && !empty(minOP_USD_unformatted) && maxOP_USD_unformatted == minOP_USD_unformatted){
                    priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>";
                }
            }
       }


       document.getElementById(priceDisplayId).innerHTML = priceHTML;

 }

};

function checkIsNumber(currentBox, evt)
{
    if (!isNumberKey(evt))
    {
        return false;
    }
    else
        return evt;
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function printMe()
{
    window.print();
    return false;
}
//Utill functions to get the volume price information
function writeVolumeList(elementId, currencyValue,currencySymbol,catentryId){
    var volumePriceArray = e4xVolumePriceArray[catentryId];
    volumePriceArray.sort(function(a,b) { return parseFloat(a.volume) - parseFloat(b.volume) } );
    if (typeof(volumePriceArray) != "undefined" && volumePriceArray.length > 0){
        var volumeListTable = [];
        volumeListTable.push("<div class='ex-button'>Buy More and Save<span class='btn-expand-ctrl'></span></div>");
        volumeListTable.push("<table class='ex-panel'>");
        var objElement = document.getElementById(elementId);
        for(i=0;i<volumePriceArray.length;i++){
            var volumePricelist =volumePriceArray[i];
            volumeListTable.push("<tr><td>" + currSymbol+"&nbsp;"+ getE4XPrice(currencyValue,volumePricelist.value) +"</td>");
            volumeListTable.push("<td>&ndash; " +volumePricelist.volume + "+ </td>");
            volumeListTable.push("</tr>");
        }
        volumeListTable.push("</table>");
        objElement.innerHTML = volumeListTable.join('');
    }
}
function setPrice(pStr,entitledItemId,productId) {
    if (pStr === undefined) pStr='';
    var empty = function (aVar) {
        return aVar == undefined || aVar ==null || aVar == "";
     }

    if (pStr=='') {
        //var key = getSelectedAttKey();
        //if (checkAttKey(key)) {
            //var cId = getCidByKey(key);

            var cId = shoppingActionsJS.getCatalogEntryId(entitledItemId);

            if (productPriceArray[cId]) {
                if (productPriceArray[cId].usdMinPrice == productPriceArray[cId].usdMaxPrice) {
                    pStr = getE4xProductPrice(cId);
                } else {
                    pStr = getComboSalePriceDisplay(getE4xMaxProductPrice(cId), getE4xProductPrice(cId), cId);
                }
            }
        //}
    }
    if(!empty(cId)){
        if (pStr!='') {
            setElementHTML("offerPrice_"+productId, pStr);
        }else{setDefaultPrice();
        }
    }

}
function getSalePriceDisplay(tStr, isSale) {
    var oStr = tStr;
    if (isSale) oStr = '<span class="pd_saleprice">' + tStr + '</span>';
    return oStr;
}

function getComboSalePriceDisplay(oPrice, sPrice, cId) {
    var oStr = '';
    var isSale = (oPrice != sPrice);
    if (isSale) {
        //oStr = '<span class="pd_origprice">'+oPrice+'</span>-&nbsp;' + getSalePriceDisplay(sPrice, isSale);
        oStr = "<span id='listPrice_" + cId + "' class='pd_origprice'>" + oPrice + "</span>"+"<span id='offerPrice_" + cId + "' class='pd_saleprice'> Now " + sPrice + "</span>";
    } else {
        oStr = sPrice;
    }
    return oStr;
}
function getE4XMaxPrice(currency,catentryId){
    var productPricelist = productPriceArray[catentryId];
    var currSymbol = "$";
    if(currency == "USD" || currency ==""){ currSymbol = "$";}else{currSymbol = currency+"&nbsp;";}
    var result ="";
    result = getE4XPrice(currency,productPricelist.usdMaxPrice);
    return currSymbol + roundNumber(result,2);
}
function getE4XPrice(currency,value){
    if(currency == "" || currency == "USD") return roundNumber(value/1,2);  return roundNumber(value/1 * e4xFxRates[currency],2);
}
function getE4XMinPrice(currency,catentryId){
    var productPricelist = productPriceArray[catentryId];
    var currSymbol = "$";
    if(currency == "USD" || currency ==""){currSymbol = "$";}else{currSymbol = currency+"&nbsp;";}
    var result ="";
    result = getE4XPrice(currency,productPricelist.usdMinPrice);
    return currSymbol + roundNumber(result,2);
}
function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
  var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);  newnumber = newnumber.toFixed(2);
  return newnumber; // Output the result
}
function getE4xProductPrice(itemId){return productPriceArray[itemId]!=null ?  getE4XMinPrice(localE4xCurrency,itemId) :  null;}
function getE4xMaxProductPrice(itemId){return productPriceArray[itemId]!=null ?  getE4XMaxPrice(localE4xCurrency,itemId) :  null;}
function getE4xMaxUSDProductPrice(itemId){
    var prItem = productPriceArray[itemId];var result ="";
    if(prItem!=null) result = prItem.usdMaxPrice
    return result;
}
function getE4xMinUSDProductPrice(itemId){
    var prItem = productPriceArray[itemId];var result ="";
    if(prItem!=null) result = prItem.usdMinPrice
    return result;
}

function addAddOnRow(addOnIds, addOnvals, firstColorCode, inventoryStatus){

     var addOnIdsArray = addOnIds.split(/,/);
     var addOnvalsArray = addOnvals.split(/,/);
     var inventoryStatusArray = inventoryStatus.split(/,/);

    var totalAddOns = document.getElementById('totalAddOns').value;
    var newVal = parseInt(totalAddOns) + 1;

    var newDiv=document.createElement('div');
    newDiv.setAttribute("id", "addOnProduct_"+newVal);
    var html = '<div id="AddOnColor_'+newVal+'" style="background-color:#'+firstColorCode+'" class="swatch_item">&nbsp; ';
    html +='</div>';
    html += ' <select id="addOnCatEntry_'+newVal+ '" name="addOnCatEntry_'+newVal+ '" class="add-on-select" onchange="changeAddOnRowColor(addOnCatEntry_'+newVal+ ');" >';
    for(i = 0; i < addOnIdsArray.length; i++) {
        if(trim(inventoryStatusArray[i]) == 'Available'){
            html += "<option value='"+addOnIdsArray[i]+"'>"+addOnvalsArray[i]+"</option>";
        }else{
            html += "<option disabled=disabled value='"+addOnIdsArray[i]+"'>"+addOnvalsArray[i]+"</option>";
        }
    }
    html += '</select>';
    var input = '<span class="attri-heading"> Quantity:</span> <input type="text" id="addOnProductQty_'+newVal+ '" name="addOnProductQty_'+newVal+ '" value="1" maxlength="4" type="text" class="qty" value="1" onkeypress="return checkIsNumber(this, event);"/>';
    html +=input;
    var channelLable = '&nbsp;<div id="addOnChannelAvailability_'+newVal+'" class="inventory-flag"></div>';

    html +=channelLable;

    newDiv.innerHTML= html;
    document.getElementById('addOnProducts').appendChild(newDiv);
    document.getElementById('totalAddOns').value = newVal;

    handleChannelAvailabilityForAddOnSku(addOnIdsArray[0],newVal);
}

function changeAddOnRowColor(obj){
    var selectId = document.getElementById(obj.id);
    var skuId = selectId.options[selectId.selectedIndex].value;
    var colorcodeList = document.getElementById('addOncatEntryColorCodeListField').value;
    var colorcodeListArray = colorcodeList.split(/,/);
    for(i = 0; i < colorcodeListArray.length; i++) {
        if (colorcodeListArray[i].indexOf(skuId) !=-1){
            var id = (obj.id).split("_");
            var color = (colorcodeListArray[i]).split("_");
            if(color[1] != '')
                {
                 document.getElementById("AddOnColor_"+id[1]).style.backgroundColor="#"+color[1];
                }
            else{
                document.getElementById("AddOnColor_"+id[1]).style.backgroundColor="";
            }
        }
    }
    handleChannelAvailabilityForAddOnSku(color[0],id[1]);

}

/**
 * This functions shows SKU availability message when an SKU is resolved.
 */
 function handleChannelAvailabilityForAddOnSku(skuId, rowNum) {

    var channelAvailabilityList = document.getElementById('addOncatEntryChannelAvailability').value;
    var channelAvailabilityListArray = channelAvailabilityList.split(/,/);

    var channelAvailabilityTemp = '';


    for(i = 0; i < channelAvailabilityListArray.length; i++) {
        if (channelAvailabilityListArray[i].indexOf(skuId) !=-1){
            //var id = (channelAvailabilityListArray[i]).split("_");
            channelAvailabilityTemp = (channelAvailabilityListArray[i]).split("_");


        }
    }

    var channelAvailability = channelAvailabilityTemp[1];
    channelAvailability =channelAvailability.toUpperCase();
    if (channelAvailability == 'BUYABLEINSTORE') {
        availabilityNLSMsg = MessageHelper.messages['ITEM_AVAIL_IN_STORE'];

    } else if (channelAvailability == 'BUYABLEONLINE') {
        availabilityNLSMsg = MessageHelper.messages['ITEM_AVAIL_ONLINE'];

    } else if (channelAvailability == 'BUYABLEINSTOREANDONLINE') {
        availabilityNLSMsg = MessageHelper.messages['ITEM_AVAIL_IN_BOTH'];

    } else if (channelAvailability == 'NOTBUYABLEINSTOREORONLINE') {
        availabilityNLSMsg = MessageHelper.messages['ITEM_NOT_AVAIL'];
    }

    if ( channelAvailability != undefined && channelAvailability != '' ) {
        dojo.byId("addOnChannelAvailability_"+rowNum).innerHTML = availabilityNLSMsg;
    }
    else{
        dojo.byId("addOnChannelAvailability_"+rowNum).innerHTML = "&nbsp;";
    }

}
 function colorizeProductImage(ele,p13nAttributeId, color, hexColor){
     document.getElementById('colorizeSwatch').innerHTML = ' : ' + color;
     //document.getElementById('colorizeSwatch').style.display = 'block';
     document.getElementById(p13nAttributeId).value = color;
     dojo.query("li > a",ele.parentNode.parentNode).forEach(function(node, index, arr){
         dojo.removeClass(node, "color_swatch_selected")
        });
     dojo.addClass(ele, "color_swatch_selected")
     ele.setAttribute("aria-disabled", "true");
     db.prodImg.mainColorName = color;
     db.s7.setAsset({"hexColor": hexColor})
 }