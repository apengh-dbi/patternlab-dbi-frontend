//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2011, 2014 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

// Declare context and refresh controller which are used in pagination controls of SearchBasedNavigationDisplay -- both products and articles+videos

wc.render.declareContext("searchBasedNavigation_context", {"contentBeginIndex":"0", "productBeginIndex":"0", "beginIndex":"0", "orderBy":"", "facetId":"", "pageView":"", "resultType":"products", "orderByContent":"", "searchTerm":"", "facet":"", "facetLimit":"", "minPrice":"", "maxPrice":"", "pageSize":"", "facetValues":""}, "");

var categoryPageParams={
         /** The language ID currently in use **/
        langId: "-1",

        /** The store ID currently in use **/
        storeId: "",

        /** The catalog ID currently in use **/
        catalogId: "",

        /** The category ID currently in use **/
        categoryId: "",

        /** Initial page load **/
        initialPageLoad: true

}
// Declare context and refresh controller which are used in pagination controls of SearchBasedNavigationDisplay to display content results (Products).
var searchBasedNavigation_controller_initProperties = {
    id: "searchBasedNavigation_controller",
    renderContext: wc.render.getContextById("searchBasedNavigation_context"),
    url: "",
    formId: ""

,renderContextChangedHandler: function(message, widget) {
    console.log("renderContextChangedHandler");
    var controller = this;
    var renderContext = this.renderContext;
    var resultType = renderContext.properties["resultType"];

    var sizesSelected = "";
    var sizeValue;
    var sizesCount = 0;
    var facets = renderContext.properties["facet"];
    for(var i = 0; i < facets.length ; i++){
        if (facets[i].startsWith("ads_f1_ntk_cs")) {
            var currentFacetValue = decodeURIComponent(facets[i]).split(":")[1].trim().replace(/"/g, "").replace(/\+/g," ")
             , colorIndex = SearchBasedNavigationDisplayJS.filterAppliedColors.indexOf(currentFacetValue);
             if (colorIndex == -1) {
                 SearchBasedNavigationDisplayJS.filterAppliedColors.push(currentFacetValue);
             }
        } else if (facets[i].startsWith("childSizeFamilySize") || facets[i].startsWith("ads_f2_ntk_cs")) {
            var currentFacetValue = $("[value='"+facets[i]+"']");
            sizeValue = $("#facetLabel_"+currentFacetValue[0].id).text();
            if(sizesCount == 0){
                sizesSelected = sizeValue;
                sizesCount++;
            }else {
                sizesSelected += ":" + sizeValue;
                sizesCount++;
             }
        } else if (facets[i].startsWith("childSizeFamilySize") || facets[i].startsWith("ads_f2_ntk_cs")) {
            var currentFacetValue = $("[value='"+facets[i]+"']");
            sizeValue = $("#facetLabel_"+currentFacetValue[0].id).text();
            if(sizesCount == 0){
                sizesSelected = sizeValue;
                sizesCount++;
            }else {
                sizesSelected += ":" + sizeValue;
                sizesCount++;
            }
        }
    }
    var colorsSelected = "";
    for(var k = 0;k < SearchBasedNavigationDisplayJS.filterAppliedColors.length; k++) {
        if(k == 0){
            colorsSelected = SearchBasedNavigationDisplayJS.filterAppliedColors[k];
        }else {
            colorsSelected += ":" + SearchBasedNavigationDisplayJS.filterAppliedColors[k];
        }
    }

    renderContext.properties["sizesSelected"] = sizesSelected;
    renderContext.properties["colorsSelected"] = colorsSelected;
    renderContext.properties["refreshProductsList"] = (renderContext.properties["refreshProductsList"] == "false") ? "false" : "true";

    if(resultType == "products" || resultType == "both"){
        renderContext.properties["beginIndex"] = renderContext.properties["productBeginIndex"];
        widget.refresh(renderContext.properties);
    }

}

,postRefreshHandler: function(widget) {
    if(this.isExternalURL){
        this.isExternalURL = false;
    }
    console.log("postRefreshHandler");
    var facetCounts = byId("facetCounts" + widget.objectId);

    listingCurrentLayout.switch(true);
    listingSidebarStatus.switchSectionStatusModal();
    fixedOnScroll.init();
    scrollToHead.init();
    tooltips.init();
    if(facetCounts != null) {
        var scripts = facetCounts.getElementsByTagName("script");
        var j = scripts.length;
        for (var i = 0; i < j; i++){
            var newScript = document.createElement('script');
            newScript.type = "text/javascript";
            newScript.text = scripts[i].text;
            facetCounts.appendChild(newScript);
        }
        SearchBasedNavigationDisplayJS.resetFacetCounts();

        //uncomment this if you want tohide zero facet values and the facet itself
        //SearchBasedNavigationDisplayJS.removeZeroFacetValues();
        SearchBasedNavigationDisplayJS.validatePriceInput();
    }
    updateFacetCounts();
    var resultType = widget.controller.renderContext.properties["resultType"];
    this.facetValuesArray = widget.controller.renderContext.properties["facetValues"];

    if(resultType == "products" || resultType == "both"){
        var currentIdValue = currentId;
        if(widget.controller.renderContext.properties["refreshProductsList"] != "false") {
            cursor_clear();
        } else {
            requestSubmitted = false;
        }
        SearchBasedNavigationDisplayJS.initControlsOnPage(widget.objectId, widget.controller.renderContext.properties);
        shoppingActionsJS.updateSwatchListView();
        shoppingActionsJS.checkForCompare();
        var gridViewLinkId = "WC_SearchBasedNavigationResults_pagination_link_grid_categoryResults";
        var listViewLinkId = "WC_SearchBasedNavigationResults_pagination_link_list_categoryResults";
        var selectedFacet = dojo.query("#filter_" + currentIdValue +" > a")[0];
        var deSelectedFacet = dojo.query("li[id^='facet_" + currentIdValue + "']" + " .facetbutton")[0];
        if(currentIdValue != ''){
            if (selectedFacet != null && selectedFacet != 'undefined'){
                selectedFacet.focus();
            }else if(currentIdValue == "orderBy"){
                byId("orderBy"+widget.objectId).focus();
            }
            else{
                if((currentIdValue == gridViewLinkId || currentIdValue != listViewLinkId) && byId(listViewLinkId)){
                    byId(listViewLinkId).focus();
                }
                if((currentIdValue == listViewLinkId || currentIdValue != gridViewLinkId) && byId(gridViewLinkId)){
                    byId(gridViewLinkId).focus();
                }
            }
        }
    }

    var pagesList = document.getElementById("pages_list_id");
    if (pagesList != null && !isAndroid() && !isIOS()) {
        dojo.addClass(pagesList, "desktop");
    }

    var parents = dojo.query(".grid_product_price");

    for(var i = 0 ; i < parents.length ; i++){
        SearchBasedNavigationDisplayJS.setProductPrices(parents[i].id);
    }
    dojo.publish("CMPageRefreshEvent");
    categoryPageParams.initialPageLoad = false;
    try{SearchBasedNavigationDisplayJS.filterClickEvent();}catch(e){}
    dojo.publish("facetRefreshEvent");
    try{SearchBasedNavigationDisplayJS.UpdateSelectAllState();}catch(e){}
    try{plpFilterEvent();}catch(e){}
    try{callTrackCategory();}catch(e){}
    $(document).trigger("afterSelectingColorFamily");
    $(document).trigger("selectDefaultDynaCatColor");
    if(SearchBasedNavigationDisplayJS.isDynamicCategory == true){
        SearchBasedNavigationDisplayJS.isDynamicCategory = false;
    }
}

};

// Declare context and refresh controller which are used in pagination controls of SearchBasedNavigationDisplay to display content results (Articles and videos).
wc.render.declareRefreshController({
    id: "searchBasedNavigation_content_controller",
    renderContext: wc.render.getContextById("searchBasedNavigation_context"),
    url: "",
    formId: ""

,renderContextChangedHandler: function(message, widget) {
    var controller = this;
    var renderContext = this.renderContext;
    var resultType = renderContext.properties["resultType"];
    if(resultType == "content" || resultType == "both"){
        renderContext.properties["beginIndex"] = renderContext.properties["contentBeginIndex"];
        widget.refresh(renderContext.properties);
    }
}


,postRefreshHandler: function(widget) {
    var resultType = widget.controller.renderContext.properties["resultType"];
    if(resultType == "content" || resultType == "both"){
            var currentIdValue = currentId;
            cursor_clear();
            SearchBasedNavigationDisplayJS.initControlsOnPage(widget.objectId, widget.controller.renderContext.properties);
            shoppingActionsJS.initCompare();
            if(currentIdValue == "orderByContent"){
                byId("orderByContent").focus();
            }
        }
        dojo.publish("CMPageRefreshEvent");
    }
});



if(typeof(SearchBasedNavigationDisplayJS) == "undefined" || SearchBasedNavigationDisplayJS == null || !SearchBasedNavigationDisplayJS){

    SearchBasedNavigationDisplayJS = {
        /**
         * This variable is an array to contain all of the facet ID's generated from the initial search query.  This array will be the master list when applying facet filters.
         */
        contextValueSeparator: "&",
        contextKeySeparator: ":",
        widgetId: "",
        facetIdsArray: new Array,
        facetValuesArray: new Array,
        filterAppliedColors: new Array,
        appliedFacetIdsArray: new Array,
        facetIdsParentArray: new Array,
        uniqueParentArray: new Array,
        noOfFilters : 0,
        changeNoOfFilters : true,
        isPriceFacet: false,
        topLevelCategoryName: "",
        filterClickEvent_priceSliderClick: false,
        filterClickEvent_minPrice: "",
        filterClickEvent_maxPrice: "",
        facetAction: "",
        currentFilterId: "",
        currentFilterType: "",
        selectedSize:"",
        isExternalURL:false,
        init:function(widgetSuffix,searchResultUrl, widgetProperties){
            wc.render.getRefreshControllerById('searchBasedNavigation_controller'+widgetSuffix).url = searchResultUrl;
            var lang = require("dojo/_base/lang");
            var widgetInitProperties = {};
            lang.mixin(widgetInitProperties, WCParamJS, widgetProperties);
            this.initControlsOnPage(widgetSuffix, widgetInitProperties);
            this.updateContextProperties("searchBasedNavigation_context", widgetInitProperties);
            var query = searchResultUrl.substring(searchResultUrl.indexOf("?") + 1, searchResultUrl.length);
            var queryObject = dojo.queryToObject(query);
            //Load Category Page Params
            categoryPageParams.storeId = queryObject.storeId;
            categoryPageParams.langId = queryObject.langId;
            categoryPageParams.categoryId = queryObject.categoryId;
            categoryPageParams.catalogId = queryObject.catalogId;
            categoryPageParams.initialPageLoad = true;
            //console.log("searchResultUrl= " + searchResultUrl);
//			var currentContextProperties = wc.render.getContextById('searchBasedNavigation_context').properties;
            this.facetValuesArray = widgetProperties['facetValues'].split(",");
        },

        initConstants:function(removeCaption, moreMsg, lessMsg, currencySymbol, selectedSize) {
            this.removeCaption = removeCaption;
            this.moreMsg = moreMsg;
            this.lessMsg = lessMsg;
            this.currencySymbol = currencySymbol;
            this.selectedSize = selectedSize;
        },

        initControlsOnPage:function(widgetSuffix,properties){
            //Set state of sort by select box..
            var selectBox = dojo.byId("orderBy"+widgetSuffix);
            if(selectBox != null && selectBox != 'undefined' && properties['orderBy'] != ''){
                dojo.byId("orderBy"+widgetSuffix).value = properties['orderBy'];
            }

            selectBox = dojo.byId("orderByContent");
            if(selectBox != null && selectBox != 'undefined' && properties['orderByContent'] != ''){
                dojo.byId("orderByContent").value = properties['orderByContent'];
            }
        },

        initContentUrl:function(contentUrl){
            wc.render.getRefreshControllerById('searchBasedNavigation_content_controller').url = contentUrl;
        },

        findContainer:function(el) {
        console.debug(el);
            while (el.parentNode) {
                el = el.parentNode;
                if (el.className == 'optionContainer') {
                    return el;
                }
            }
            return null;
        },

        resetFacetCounts:function() {
            for(var i = 0; i < this.facetIdsArray.length; i++) {
                var facetValue = byId("facet_count" + this.facetIdsArray[i]);
                var facetAcceValue = byId(this.facetIdsArray[i] + "_ACCE_Label_Count");
                if(facetValue != null) {
                    facetValue.innerHTML = 0;
                }
                if(facetAcceValue != null) {
                    facetAcceValue.innerHTML = 0;
                }
            }
        },
        removeZeroFacetValues:function() {
            var uniqueId = this.uniqueParentArray;
            var widget = this.widgetId;
            for(var i = 0; i < this.facetIdsArray.length; i++) {
                var facetId = "facet_" + this.facetIdsArray[i];
                var parentId = this.facetIdsParentArray[i];
                var facetValue = byId("facet_count" + this.facetIdsArray[i]);

                if(facetValue.innerHTML == '0') {
                    document.getElementById(facetId + widget).style.display = 'none';
                }
                else if(facetValue.innerHTML != '0') {
                    document.getElementById(facetId + widget).style.display = 'block';
                    uniqueId[parentId] = uniqueId[parentId] + 1;
                }
            }
            for(var key in uniqueId){
                if(uniqueId[key] == 0){
                    document.getElementById(key).style.display = 'none';
                    uniqueId[key] = 0;//reset the count
                }
                else if(uniqueId[key] != 0){
                    document.getElementById(key).style.display = 'block';
                    uniqueId[key] = 0;//reset the count
                }
            }

        },
        updatePriceSlider: function(min, max) {
            if(max - min > 1 ){
                $("#PRICE").css("display", "block");
                var selectedMinPrice = byId("low_price_value").value;
                var selectedMaxPrice = byId("high_price_value").value;
                var lowInput = $(".listing__price-range-low-input");
                var highInput = $(".listing__price-range-high-input");
                lowInput.attr("min", min);
                highInput.attr("max", max);
                lowInput.val(min);
                highInput.val(max);
                var highElement = $(".listing__price-range-high-select-value");
                var lowElement = $(".listing__price-range-low-select-value");
                if(selectedMinPrice != "" && selectedMaxPrice != "") {
                    highElement.innerHTML = "$" +selectedMinPrice;
                    lowElement.innerHTML = "$" +selectedMaxPrice;
                }else {
                    highElement.innerHTML = "$" + max;
                    lowElement.innerHTML  = "$" + min;
                }
                priceRange.destroy();
                priceRange.move();
            } else {
               // $("#PRICE").css("display", "none");
            }
        },
        updateFacetCount:function(id, count, value, label, image, contextPath, group, multiFacet) {
            var facetValue = byId("facet_count" + id);
            if(facetValue != null) {
                var checkbox = byId(id);
                var facetAcceValue = byId(id + "_ACCE_Label_Count");
                if(count > 0) {
                    // Reenable the facet link
                    checkbox.disabled = false;
                    var optionId = checkbox.id;
                    if(optionId.indexOf("_") != -1) {
                        optionId = optionId.split("_")[0];
                    }
                  if ( $("option[value="+optionId+"]").length > 0 ){
                      $("option[value="+optionId+"]").prop("disabled", checkbox.disabled);
                  }
                    if(facetValue != null) {
                        facetValue.innerHTML = count;
                    }
                    if(facetAcceValue != null) {
                        facetAcceValue.innerHTML = count;
                    }
                }
            }
            else if(count > 0) {
                // there is no limit to the number of facets shown, and the user has exposed the show all link
                if(byId("facet_" + id) == null) {
                    // this facet does not exist in the list.  Insert it.
                    var divContainer = dojo.query("[id^='section_list_" + group + "']")[0];
                    var grouping = dojo.query(" > ul.facetSelect", divContainer)[0];
                    if(grouping) {
                        this.facetIdsArray.push(id);
                        var newFacet = document.createElement("li");
                        newFacet.setAttribute("onclick","SearchBasedNavigationDisplayJS.triggerCheckBox(this)");
                        var facetClass = "";
                        var section = "";
                        if(!multiFacet) {
                            if(image != ""){
                            facetClass = "singleFacet";
                            }
                            // specify which facet group to collapse when multifacets are not enabled.
                            section = group;
                        }
                        if(image != "") {
                            facetClass = "singleFacet left";
                        }
                        newFacet.setAttribute("id", "facet_" + id);
                        newFacet.setAttribute("class", facetClass);
                        newFacet.setAttribute("data-additionalvalues", "More")
                        var facetLabel = "<label for='" + id + "' class='listing__checkbox-filter-label'>";
                        if(image != "") {
                            facetLabel = facetLabel + "<span class='swatch'><span class='outline'><span id='facetLabel_" + id + "'><img src='" + image + "' title='" + label + "' alt='" + label + "'/></span> <div class='facetCountContainer'>(<span id='facet_count" + id + "'>"+ count + "</span>)</div>";
                        }
                        else {
                            facetLabel = facetLabel + "<span class='outline listing__checkbox-filter-label-content'><span id='facetLabel_" + id + "' class='listing__checkbox-filter-title sans-xs'>" + label + "</span> (<span id='facet_count" + id +"' class='listing__checkbox-filter-count sans-xxs'>" + count + "</span>)</span>";
                        }
                        facetLabel = facetLabel + "</label>";
                        newFacet.innerHTML = "<input type='checkbox' class='listing__checkbox-filter-input' aria-labelledby='" + id + "_ACCE_Label' id='" + id + "' value='" + value + "' onclick='javascript: SearchBasedNavigationDisplayJS.toggleSearchFilter(this, \"" + id + "\")'/>" + facetLabel;
                        grouping.appendChild(newFacet);
                    }
                }
            }
        },

        triggerCheckBox:function(elem){
            var inputBox = elem.children[0];
            inputBox.click();
        },


        isValidNumber:function(n) {
            return !isNaN(parseFloat(n)) && isFinite(n) && n >= 0;
        },

        checkPriceInput:function(event) {
            if(this.validatePriceInput() && event.keyCode == 13) {
                this.appendFilterPriceRange();
                this.doSearchFilter();
            }else if(byId("low_price_input") != null && byId("high_price_input") != null){
                var lowPrice = byId("low_price_input").value;
                var highPrice = byId("high_price_input").value;
                if((!this.isValidNumber(lowPrice) || !this.isValidNumber(highPrice)) && event.keyCode == 13) {
                    MessageHelper.formErrorHandleClient("high_price_input", storeNLS['ERROR_FACET_PRICE_INVALID']);
                }
            }
            return false;
        },

        validatePriceInput:function() {
            if(byId("low_price_input") != null && byId("high_price_input") != null && byId("price_range_go") != null) {
                var low = byId("low_price_input").value;
                var high = byId("high_price_input").value;
                var go = byId("price_range_go");
                if(this.isValidNumber(low) && this.isValidNumber(high) && parseFloat(high) > parseFloat(low)) {
                    go.className = "go_button";
                    go.disabled = false;
                }
                else {
                    go.className = "go_button_disabled";
                    go.disabled = true;
                }
                return !go.disabled;
            }
            return false;
        },

        toggleShowMore:function(index, show) {
            var list = byId('more_' + index);
            var morelink = byId('morelink_' + index);
            if(list != null) {
                if(show) {
                    morelink.style.display = "none";
                    list.style.display = "inline-block";
                }
                else {
                    morelink.style.display = "inline-block";
                    list.style.display = "none";
                }
            }
        },

        toggleSearchFilterOnKeyDown:function(event, element, id) {
            if (event.keyCode == dojo.keys.ENTER) {
                element.checked = !element.checked;
                this.toggleSearchFilter(element, id);
            }
        },

        toggleSearchFilter:function(element, id) {
            if(element.checked) {
                this.appendFilterFacet(id);
            }
            else {
                this.removeFilterFacet(id);
            }

/*

            if(section != "") {
                byId('section_' + section).style.display = "none";
            }
*/
            this.doSearchFilter();
        },

        appendFilterPriceRange:function() {
            var el = byId("price_range_input");
            var section = this.findContainer(el);

            if(section) {
                byId(section.id).style.display = "none";
            }
            byId("clear_all_filter").style.display = "block";
            $(".listing__filters-applied").attr("data-filters-applied", true);


            var facetFilterList = byId("facetFilterList");
            // create facet filter list if it's not exist
            if (facetFilterList == null) {
                facetFilterList = document.createElement("ul");
                facetFilterList.setAttribute("id", "facetFilterList");
                facetFilterList.setAttribute("class", "facetSelectedCont");
                var facetFilterListWrapper = byId("facetFilterListWrapper");
                facetFilterListWrapper.appendChild(facetFilterList);
            }

            var filter = byId("pricefilter");
            if(filter == null) {
                filter = document.createElement("li");
                filter.setAttribute("id", "pricefilter");
                filter.setAttribute("class", "facetSelected listing__filters-applied-item");
                facetFilterList.appendChild(filter);
            }
          // SearchBasedNavigationDisplayJS.updatePriceSlider(parseInt(byId("low_price_input").value), parseInt(byId("high_price_input").value));
            var label = this.currencySymbol + byId("low_price_input").value + " - " + this.currencySymbol + byId("high_price_input").value;
            var facetValue = this.currencySymbol + byId("low_price_input").value + "-" + this.currencySymbol + byId("high_price_input").value;
            this.pushFacetValue(facetValue);
            this.pushFacetId("price");

            filter.innerHTML = "<a role='button' href='#' onclick='dojo.topic.publish(\"Facet_Remove\",\"" + facetValue + "\"); return false;' class='listing__filters-applied-item-link sans-xs'>" + "<div class='filter_option'><span class='listing__filters-applied-item-text'>" + label + "</span></div></div></a>";

            if (this.changeNoOfFilters) {
                this.noOfFilters += 1;
                this.changeNoOfFilters = false;
            }
            byId("clear_all_filter").style.display = "block";
            $(".listing__filters-applied").attr("data-filters-applied", true);

            if(this.validatePriceInput()) {
                // Promote the values from the input boxes to the internal inputs for use in the request.
                byId("low_price_value").value = byId("low_price_input").value;
                byId("high_price_value").value = byId("high_price_input").value;
            }
        },

        removeFilterPriceRange:function() {
            this.popFacetValue(this.currencySymbol + byId("low_price_input").value + "-" + this.currencySymbol + byId("high_price_input").value);
            if(byId("low_price_value") != null && byId("high_price_value") != null) {
                byId("low_price_value").value = "";
                byId("high_price_value").value = "";
            }
            var facetFilterList = byId("facetFilterList");
            var filter = byId("pricefilter");
            if(filter != null) {
                facetFilterList.removeChild(filter);
            }
            this.popFacetId("price");

            if(facetFilterList !=null &&  facetFilterList.childNodes.length == 0) {
                byId("clear_all_filter").style.display = "none";
                byId("facetFilterListWrapper").innerHTML = "";
                $(".listing__filters-applied").attr("data-filters-applied", false);
            }
            this.noOfFilters -= 1;
            this.changeNoOfFilters = true;

            var el = byId("price_range_input");
            var section = this.findContainer(el);
            if(section) {
                byId(section.id).style.display = "block";
            }
            var lowInput = $(".listing__price-range-low-input");
            var hightInput = $(".listing__price-range-high-input");
            lowInput.attr('value', lowInput.attr("min"));
            hightInput.attr('value', hightInput.attr("max"));
            priceRange.initBar();
            //SearchBasedNavigationDisplayJS.updatePriceSlider(parseInt(lowInput.attr('value')), parseInt(highInput.attr('value')));
            this.doSearchFilter();
        },
        appendFilterFacet:function(id, customClass) {

            if(SearchBasedNavigationDisplayJS.isDynamicCategory == true){return;}
            if(id.indexOf("_") != -1) {
                var splittedId = id.split("_")[0];
                $("[id^='"+splittedId+"_']").each(function(){
                    $(this).prop('checked',true);

                });

            }
            var facetFilterList = byId("facetFilterList");
            // create facet filter list if it's not exist
            if (facetFilterList == null) {
                facetFilterList = document.createElement("ul");
                facetFilterList.setAttribute("id", "facetFilterList");
                facetFilterList.setAttribute("class", "facetSelectedCont");
                var facetFilterListWrapper = byId("facetFilterListWrapper");
                facetFilterListWrapper.appendChild(facetFilterList);
            }

            var filter = byId("filter_" + id);
            if(!customClass){
                customClass = "";
            }
            // do not add it again if the user clicks repeatedly
            if(filter == null) {
                filter = document.createElement("li");
                filter.setAttribute("id", "filter_" + id);
                filter.setAttribute("class", "facetSelected listing__filters-applied-item " + customClass);
                var label = byId("facetLabel_" + id).innerHTML;
                var sizeLabel = "";
                if( byId("sizeLabel_" + id) != null) {
                    sizeLabel = byId("sizeLabel_" + id).innerHTML;
                }
                var colorSwatch ="";
                if( byId("facetColorSwatch_" + id) != null) {
                    colorSwatch = byId("facetColorSwatch_" + id).innerHTML;
                }
                if(colorSwatch != ""){
                    dojo.addClass(filter , "with-swatch");
                }
                var acceRemoveLabel = "<span class='spanacce' id='ACCE_Label_Remove'>"+ MessageHelper.messages['REMOVE']+ "</span>"
                escapedLabel = label;
                if(label.indexOf("'") != -1){
                    escapedLabel = label.replace("'", "\\&apos;");
                }else if(label.indexOf('"') != -1){
                    escapedLabel = label.replace('"', "\\&quot;");
                }

                filter.innerHTML = "<a role='button' href='#' onclick='javascript:setCurrentId(\"" + id + "\");dojo.topic.publish(\"Facet_Remove\", \"" + id + "\", \"" + escapedLabel + "\"); return false;' class='listing__filters-applied-item-link sans-xs'> " + "<div class='filter_option'>"+ colorSwatch+"<span class='listing__filters-applied-item-text'>" + sizeLabel +label + "</span>"+"</div></a>";
                this.noOfFilters += 1;
                facetFilterList.appendChild(filter);
            }

            dojo.addClass(byId("facetLabel_" + id).parentElement,"facetSelectedHighlight");

            var el = byId(id);
            var section = this.findContainer(el);
            if(section) {
                byId(section.id).style.display = "none";
            }
            byId("clear_all_filter").style.display = "block";
            $(".listing__filters-applied").attr("data-filters-applied", true);

            SearchBasedNavigationDisplayJS.pushFacetId(id);

        },

        removeFilterFacet:function(id) {
            if(id.indexOf("_") != -1) {
                var splittedId = id.split("_")[0];
                $("[id^='"+splittedId+"_']").each(function(){
                    $(this).prop("checked",false);
                });
            }

            var facetFilterList = byId("facetFilterList");
            var filter;
            if(id.indexOf("_") != -1) {
                filter = $("[id^='filter_"+splittedId+"_']")[0];
            } else {
                filter = byId("filter_" + id);
            }

            if(filter != null) {
                facetFilterList.removeChild(filter);
                byId(id).checked = false;
            }

            if(facetFilterList.childNodes.length == 0) {
                byId("clear_all_filter").style.display = "none";
                byId("facetFilterListWrapper").innerHTML = "";
                $(".listing__filters-applied").attr("data-filters-applied", false);
            }

            dojo.removeClass(byId("facetLabel_" + id).parentElement,"facetSelectedHighlight");

            this.noOfFilters -= 1;
            var el = byId(id);
            var section = this.findContainer(el);
            if(section) {
                byId(section.id).style.display = "block";
            }
           // this.doSearchFilter();

            this.popFacetId(id);
        },

        getEnabledProductFacets:function() {
            var facetForm = document.forms['productsFacets'] != null ? document.forms['productsFacets'] : document.forms['productsFacetsHorizontal'];
            var elementArray = facetForm.elements;

            var facetArray = new Array();
            var facetIds = new Array();
            if(_searchBasedNavigationFacetContext != 'undefined') {
                for(var i=0; i< _searchBasedNavigationFacetContext.length; i++) {
                    facetArray.push(_searchBasedNavigationFacetContext[i]);
                    //facetIds.push();
                }
            }
            var facetLimits = new Array();
            for (var i=0; i < elementArray.length; i++) {
                var element = elementArray[i];

                if(element.type != null && element.type.toUpperCase() == "CHECKBOX" && !$(element).hasClass("select_all") ) {
                    if(element.title == "MORE") {
                        // scan for "See More" facet enablement.
                        if(element.checked) {
                            facetLimits.push(element.value);
                        }
                    }
                    else {
                        // disable the checkbox while the search is being performed to prevent double clicks
                        element.disabled = true;

                        if(element.checked ) {
                            if(facetArray.indexOf(element.value) == -1) {
                                facetArray.push(element.value);
                            }
                            var id = element.id;
                            var push =true;
                            if(id.indexOf("_") != -1) {
                                id = id.split("_")[0];
                                if(facetIds.indexOf(id) != -1) {
                                    push = false;
                                }
                            }
                            if(push)
                                facetIds.push(id);
                        }
                    }
                }
            }
            var tempArr = new Array();
            for (var i = 0; i < facetIds.length; i++) {
                if (this.appliedFacetIdsArray.indexOf(facetIds[i]) > -1) {
                    tempArr[this.appliedFacetIdsArray.indexOf(facetIds[i])] = facetIds[i];
                }
            }
            if (this.appliedFacetIdsArray.indexOf("price") > -1) {
                tempArr[this.appliedFacetIdsArray.indexOf("price")] = "price";
            }
            facetIds = tempArr;

            $("#add_color_drop_down").find(".add_a_color_option").each(function(){
                $(this).prop("disabled", true);
            });
            // disable the price range button also
            if(byId("price_range_go") != null) {
                byId("price_range_go").disabled = true;
            }

            var results = new Array();
            results.push(facetArray);
            results.push(facetLimits);
            results.push(facetIds);
            return results;
        },

        doSearchFilter:function(refreshWidget, keepPagination) {
            if(!submitRequest()){
                return;
            }
            block_screen();

            var minPrice = "";
            var maxPrice = "";

            if(byId("low_price_value") != null && byId("high_price_value") != null) {
                minPrice = byId("low_price_value").value;
                maxPrice = byId("high_price_value").value;
            }
            if(minPrice == '' && maxPrice == '')
            {
                minPrice = window.initialMinPrice;
                maxPrice = window.initialMaxPrice;
            }
            this.filterClickEvent_minPrice = minPrice;
            this.filterClickEvent_maxPrice = maxPrice;

            var facetArray = this.getEnabledProductFacets();

            var refreshProductsList = (refreshWidget != undefined && refreshWidget == false) ? "false" : "true";
            if(!keepPagination){
                wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0", "facet": facetArray[0], "facetLimit": facetArray[1], "facetId": facetArray[2], "resultType":"products", "minPrice": minPrice, "maxPrice": maxPrice, "isPriceFacet": this.isPriceFacet, "facetValues":this.facetValuesArray, "refreshProductsList": refreshProductsList});
            }else{
                var pageURL = this.getModifiedURL();
                var indexOfProductBegin = pageURL.indexOf("productBeginIndex");
                var facetsString = pageURL.substr(indexOfProductBegin);
                var productBeginIndexProperty = facetsString.split(this.contextValueSeparator)[0];
                var productBeginIndex = productBeginIndexProperty.split(":")[1];
                wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": productBeginIndex, "facet": facetArray[0], "facetLimit": facetArray[1], "facetId": facetArray[2], "resultType":"products", "minPrice": minPrice, "maxPrice": maxPrice, "isPriceFacet": this.isPriceFacet, "facetValues":this.facetValuesArray, "refreshProductsList": refreshProductsList});
            }

            this.updateHistory();

            MessageHelper.hideAndClearMessage();

        },

        filterClickEvent:function() {
            if (this.facetAction == "Facet_Add"){
                var filterId = this.currentFilterId;
                var facetAction = this.currentFilterType
                var topLevelCategory = this.topLevelCategoryName;
                var minPrice = parseInt(this.filterClickEvent_minPrice);
                var maxPrice = parseInt(this.filterClickEvent_maxPrice);

                 if(this.filterClickEvent_priceSliderClick == true){
                     var manualCMSP = "Filter-_-"+topLevelCategory+"-_-Price|"+minPrice+"-"+maxPrice;
                     manualCMSP = manualCMSP.replace(/ /g,'');
                     utag.link({
                         "EventCategory": "PLP_Filter_Click_"+topLevelCategory,
                         "EventAction" :"Price",
                         "Facet_detail" : minPrice.toString(),
                         "EventPage": pageNameValue,
                         "Manual_cm_sp" : manualCMSP
                       });

                     utag.link({
                         "EventCategory": "PLP_Filter_Click_"+topLevelCategory,
                         "EventAction" :"Price",
                         "Facet_detail" : maxPrice.toString(),
                         "EventPage": pageNameValue,
                         "Manual_cm_sp" : manualCMSP
                       });

                 }else{

                     var facetDetail = document.getElementById('facetLabel_'+filterId).innerHTML;
                     var manualCMSP = "Filter-_-"+topLevelCategory+"-_-"+facetAction+" | "+facetDetail;
                     manualCMSP = manualCMSP.replace(/ /g,'');

                     utag.link({
                       "EventCategory": "PLP_Filter_Click_"+topLevelCategory,
                       "Facet_detail" : facetDetail,
                       "EventAction" : facetAction,
                       "EventPage": pageNameValue,
                       "Manual_cm_sp" : manualCMSP
                     });
                 }
            }
            this.facetAction = "";
            this.currentFilterId = "";
            this.filterClickEvent_priceSliderClick = false;
        },

        toggleShowMore:function(element, id) {
            var label = byId("showMoreLabel_" + id);
            var divContainer = dojo.query("[id^='section_list_" + id + "']")[0];
            var grouping = dojo.query(" > ul.facetSelect > li.after-more", divContainer);

            if(element.checked) {
                label.innerHTML = this.lessMsg;
                var group = dojo.query(" > ul.facetSelect", divContainer)[0];
                var clearFloat = dojo.query(" > div.clear_float", group)[0];
                if(clearFloat != undefined) {
                    group.removeChild(clearFloat);
                }
                if(grouping) {
                    for(var i = 0;i < grouping.length; i++) {
                        grouping[i].style.display="";
                    }
                }

            }
            else {
                if(grouping) {
                    for(var i = 0;i < grouping.length; i++) {
                        grouping[i].style.display="none";
                    }
                }
                label.innerHTML = this.moreMsg;


            }
        },


        clearAllFacets:function(execute) {
            this.noOfFilters = 0;
            this.changeNoOfFilters = true;
            this.facetValuesArray.length = 0;
            this.filterAppliedColors.length = 0;
            this.appliedFacetIdsArray.length = 0;
            byId("clear_all_filter").style.display = "none";
            byId("facetFilterListWrapper").innerHTML = "";
            $(".listing__filters-applied").attr("data-filters-applied", false);
            if(byId("low_price_value") != null && byId("high_price_value") != null) {
                byId("low_price_value").value = "";
                byId("high_price_value").value = "";
            }
            var lowInput = $(".listing__price-range-low-input");
            var hightInput = $(".listing__price-range-high-input");
            lowInput.attr('value', lowInput.attr("min"));
            hightInput.attr('value', hightInput.attr("max"));
            priceRange.initBar();
            //SearchBasedNavigationDisplayJS.updatePriceSlider(parseInt(lowInput.attr('value')), parseInt(highInput.attr('value')));

            var facetForm = document.forms['productsFacets'] != null ? document.forms['productsFacets'] : document.forms['productsFacetsHorizontal'];
            var elementArray = facetForm.elements;
            for (var i=0; i < elementArray.length; i++) {
                var element = elementArray[i];
                if(element.type != null && element.type.toUpperCase() == "CHECKBOX" && element.checked && element.title != "MORE" && !$(element).hasClass("select_all") ) {
                    element.checked = false;
                }
            }

            var elems = document.getElementsByTagName("*");
            for (var i=0; i < elems.length; i++) {
                // Reset all hidden facet sections (single selection facets are hidden after one facet is selected from that facet grouping)
                // and clear all selected facet highlights.
                var element = elems[i];
                if (element.id != null) {
                    if (element.id.indexOf("section_") == 0 && !(element.id.indexOf("section_list") == 0)) {
                        element.style.display = "block";
                    }
                    if (element.id.indexOf("facetLabel_") == 0) {
                        dojo.removeClass(element.parentElement,"facetSelectedHighlight");
                    }
                }
            }

            if(execute) {
                this.doSearchFilter();
            }
        },

        toggleSearchContentFilter:function() {
            if(!submitRequest()){
                return;
            }
            cursor_wait();

            var facetList = "";
            var facetForm = document.forms['contentsFacets'];
            var elementArray = facetForm.elements;
            for (var i=0; i < elementArray.length; i++) {
                var element = elementArray[i];
                if(element.type != null && element.type.toUpperCase() == "CHECKBOX" && element.checked && element.title != "MORE" && !$(element).hasClass("select_all")) {
                    facetList += element.value + ";";
                }
            }

            wc.render.updateContext('searchBasedNavigation_context', {"facet": facetList, "resultType":"content"});
            this.updateHistory();
            MessageHelper.hideAndClearMessage();
        },


        updateContextProperties:function(contextId, properties){
            //Set the properties in context object..
            for(key in properties){
                wc.render.getContextById(contextId).properties[key] = properties[key];
                console.debug(" key = "+key +" and value ="+wc.render.getContextById(contextId).properties[key]);
            }
        },

        showResultsPageForContent:function(data){

            var pageNumber = data['pageNumber'];
            var pageSize = data['pageSize'];
            pageNumber = dojo.number.parse(pageNumber);
            pageSize = dojo.number.parse(pageSize);

            setCurrentId(data["linkId"]);

            if(!submitRequest()){
                return;
            }

            var beginIndex = pageSize * ( pageNumber - 1 );
            cursor_wait();
            wc.render.updateContext('searchBasedNavigation_context', {"contentBeginIndex": beginIndex,"resultType":"content"});
            this.updateHistory();
            MessageHelper.hideAndClearMessage();
        },

        showResultsPage:function(data){

            var pageNumber = data['pageNumber'];
            var pageSize = data['pageSize'];
            pageNumber = dojo.number.parse(pageNumber);
            pageSize = dojo.number.parse(pageSize);

            setCurrentId(data["linkId"]);

            if(!submitRequest()){
                return;
            }

            console.debug(wc.render.getContextById('searchBasedNavigation_context').properties);
            var beginIndex = pageSize * ( pageNumber - 1 );
            block_screen();


            wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": beginIndex,"resultType":"products"});

            this.updateHistory();
            $('html,body').scrollTop(0);
            MessageHelper.hideAndClearMessage();
        },

        setRelTags:function(){
            var resultsInfo = $("#catalog_search_result_information");
            var resultsInfo_json = eval ("(" + resultsInfo.text() + ")");
            var pageNumber = resultsInfo_json.searchResult.currentPageNumber;
            var totalPages = resultsInfo_json.searchResult.totalPageNumber;
            var pageSize = resultsInfo_json.searchResult.pageSize;
            pageNumber = dojo.number.parse(pageNumber);
            totalPages = dojo.number.parse(totalPages);
            pageSize = dojo.number.parse(pageSize);
            var beginIndex = pageSize * ( pageNumber - 1 );

            $('link[rel="prev"]').remove();
            $('link[rel="next"]').remove();

            var pageURL = this.getModifiedURL();
            var indexOfProductBegin = pageURL.indexOf("productBeginIndex");
            var facetsString = pageURL.substr(indexOfProductBegin);
            var productBeginIndexProperty = facetsString.split(this.contextValueSeparator)[0];

            var prevPageURL = pageURL.replace(productBeginIndexProperty,"productBeginIndex:"+(beginIndex-pageSize));
            var nextPageURL = pageURL.replace(productBeginIndexProperty,"productBeginIndex:"+(beginIndex+pageSize));

            if (pageNumber > 1) {
                var fileref = document.createElement("link")
                fileref.setAttribute("rel", "prev");
                fileref.setAttribute("href", prevPageURL);

                if (typeof fileref != "undefined")
                    document.getElementsByTagName("head")[0].appendChild(fileref);
            }

            if (pageNumber < totalPages) {
                var fileref = document.createElement("link")
                fileref.setAttribute("rel", "next");
                fileref.setAttribute("href", nextPageURL);

                if (typeof fileref != "undefined")
                    document.getElementsByTagName("head")[0].appendChild(fileref);
            }

            /* START: Robots meta tag */
            var currentContextProperties = wc.render.getContextById('searchBasedNavigation_context').properties;
            var beginIndex = currentContextProperties["beginIndex"];
            var orderBy = currentContextProperties["orderBy"];
            var facetId = currentContextProperties["facetId"];
            if ((facetId != null && facetId.length > 0) || (orderBy != null && orderBy != 0) || (beginIndex != '' && beginIndex > 0)) {
                if($('meta[name="robots"]').length == 0) {
                    var robotsMetaTag = document.createElement("meta");
                    robotsMetaTag.setAttribute("name", "robots");
                    robotsMetaTag.setAttribute("content", "noindex, follow");
                    if (typeof robotsMetaTag != "undefined")
                        document.getElementsByTagName("head")[0].appendChild(robotsMetaTag);
                }
            } else if ($('meta[name="robots"]').length > 0 ) {
                 $('meta[name="robots"]').remove();
            }
            /* END: Robots meta tag */

        },

        toggleView:function(data){
            var pageView = data["pageView"];
            setCurrentId(data["linkId"]);
            if(!submitRequest()){
                return;
            }
            cursor_wait();
            console.debug("pageView = "+pageView+" controller = +searchBasedNavigation_controller");
            wc.render.updateContext('searchBasedNavigation_context', {"pageView": pageView,"resultType":"products", "enableSKUListView":data.enableSKUListView});
            this.updateHistory();
            MessageHelper.hideAndClearMessage();
        },
        toggleProductsSize: function (data) {
            wc.render.getContextById('searchBasedNavigation_context').properties.productSize = data;
            this.updateHistory();
        },

        toggleExpand:function(id) {
            var section_list = byId("section_list_" + id);
            var parentNode = byId(id).parentNode.parentNode.attributes['data-listing-filter-open'];
            var isListingFilterOpen = parentNode.value;
            if (isListingFilterOpen == 'true') {
                parentNode.value = false;
                section_list.setAttribute("aria-expanded", "false");
                section_list.style.display = "none";
            } else {
                parentNode.value = true;
                section_list.setAttribute("aria-expanded", "true");
                section_list.style.display = "block";
            }
        },
        toggleExpandFacet:function (id) {
            var parentNode = byId(id).attributes['data-listing-aside-section-open'];
            isFacetNavOpen = parentNode.value;
            if (isFacetNavOpen == 'true') {
                parentNode.value = false;
            } else {
                parentNode.value = true;
            }

        },

        setPageSize:function(newPageSize){
            if(!submitRequest()){
                return;
            }
            cursor_wait();
            console.debug("resultsPerPage = "+newPageSize+" controller = +searchBasedNavigation_controller");

            wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0","resultType":"products","pageSize":newPageSize});
            this.updateHistory();
            MessageHelper.hideAndClearMessage();
        },

        sortResults:function(orderBy){
            if(!submitRequest()){
                return;
            }
            block_screen();
            console.debug("orderBy = "+orderBy+" controller = +searchBasedNavigation_controller");
            //Reset beginIndex = 1

            wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0","orderBy":orderBy,"resultType":"products"});
            this.updateHistory();
            MessageHelper.hideAndClearMessage();
        },

        sortResults_content:function(orderBy){
            if(!submitRequest()){
                return;
            }
            cursor_wait();
            console.debug("orderBy = "+orderBy+" controller = +searchBasedNavigation_controller");
            //Reset beginIndex = 1
            wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0","orderByContent":orderBy,"resultType":"content"});
            this.updateHistory();
            MessageHelper.hideAndClearMessage();
        },

        swatchImageClicked:function(id) {
            // This is a workaround for IE's bug for non-clickable label images.
            var e = byId(id);
            if(!e.checked) {
                e.click();
            }
        },

        clone:function(masterObj) {
            if (null == masterObj || "object" != typeof masterObj) return masterObj;
            var clone = masterObj.constructor();
            for (var attr in masterObj) {
                if (masterObj.hasOwnProperty(attr)) clone[attr] = masterObj[attr];
            }
            return clone;
        },

        getContextPropertiesAsString:function(){

            var currentContextProperties = wc.render.getContextById('searchBasedNavigation_context').properties;
            var contextValues = "facet:" + currentContextProperties["facetId"] + this.contextValueSeparator;
            contextValues+= "productBeginIndex:" + currentContextProperties["beginIndex"] + this.contextValueSeparator;
            contextValues+= "orderBy:" + currentContextProperties["orderBy"] + this.contextValueSeparator;
            contextValues+= "pageView:" + currentContextProperties["pageView"] + this.contextValueSeparator;
            contextValues+= "minPrice:" + currentContextProperties["minPrice"] + this.contextValueSeparator;
            contextValues+= "maxPrice:" + currentContextProperties["maxPrice"] + this.contextValueSeparator;
            contextValues+= "pageSize:" + currentContextProperties["pageSize"] + this.contextValueSeparator;
            contextValues+= "productSize:" + currentContextProperties["productSize"] + this.contextValueSeparator;
            contextValues+= "isPriceFacet:" + currentContextProperties["isPriceFacet"] + this.contextValueSeparator;
            contextValues+= "facetValues:" + currentContextProperties["facetValues"] + this.contextValueSeparator;
            return contextValues;

        },

        getModifiedURL:function(){
            var contextValues = this.getContextPropertiesAsString();
            var http = location.protocol;
            var slashes = http.concat("//");
            var host = slashes.concat(window.location.hostname);
            modifiedURL = host + "/SearchDisplay?categoryId=" + categoryPageParams.categoryId + "&storeId=" + categoryPageParams.storeId + "&langId=" + categoryPageParams.langId + "#!&" + contextValues;
            return modifiedURL;
        },
        updateHistory:function() {
/*
            var contextValues = "";
            for(var i = 0; i < facetArray[2].length; i++) {
                console.debug(facetArray[2][i]);
                contextValues= contextValues + facetArray[2][i] + "|";
            }
            */
            if(SearchBasedNavigationDisplayJS.isDynamicCategory == true){return;}
            var contextValues = this.getContextPropertiesAsString();
            var yScroll=document.body.scrollTop;

            var currentURL = window.location.href;
            var modifiedURL="";
            var url="";


/*            if(currentURL != null && (currentURL.search(/browse_/i) > 0 || currentURL.search(/searchdisplay?/i) > 0)) {  */
            if(currentURL != null && ((this.topLevelCategoryName != "" && this.topLevelCategoryName.length > 1 ) || currentURL.search(/searchdisplay?/i) > 0)) {
                //DBI changes to support current SEO URLs
                modifiedURL = this.getModifiedURL();
                console.log("modifiedURL = " + modifiedURL);

                if(currentURL.search(/searchdisplay?/i) > 0) {
                    //Back button scenario
                    categoryPageParams.initialPageLoad = false;
                }

                if(!(categoryPageParams.initialPageLoad) || (currentURL.search(/searchdisplay?/i) > 0)) {
                    url = modifiedURL;
                    categoryPageParams.initialPageLoad = false;
                }
                else {
                    url = "#dbi";

                }

                if(history.pushState) {
                    if (location.hash == "") {
                        history.replaceState(null, null, url);
                    } else if(!this.isExternalURL){
                            history.pushState(null, null, url);
                    }
                    this.isExternalURL= false;
                }
                else {
                    window.location.hash = contextValues;
                }
                //Reset page load
                categoryPageParams.initialPageLoad = false;

            } else {
                //OOB Behavior
                if(history.pushState) {
                    if (location.hash == "") {
                        history.replaceState(null, null, "#" + contextValues);
                    } else {
                        history.pushState(null, null, "#" + contextValues);
                    }
                }
                else {
                    window.location.hash = contextValues;
                }
            }
            document.body.scrollTop=yScroll;
        },

        restoreHistoryContext:function() {
             this.isExternalURL = false;
            if(location.hash != null && location.hash != "" && location.hash != "#") {

                var contextValues = this.getContextPropertiesAsString();
                if( location.hash == "#"+contextValues || (categoryPageParams.initialPageLoad && location.hash == "#dbi")){
                    // Page is loaded and contextValues is same as the hash value in URL
                    // No need to make another Ajax call to update same content.
                    categoryPageParams.initialPageLoad = false;
                    $(document).trigger("selectDefaultDynaCatColor");
                    return;
                }
                this.isExternalURL = true;
                this.restoreHistoryContextFlag = true;
                this.clearAllFacets(false);

                var productBeginIndex = "";
                var orderBy = "";
                var pageView = "";
                var minPrice = "";
                var maxPrice = "";
                var pageSize = "";
                var productSize = "";
                var facetValues = "";
                var isPriceFacet = "";
                var ids = new Array();
                //noOfFilters = 0;

                var pairs = location.hash.substring(1).split(this.contextValueSeparator);
                for(var k = 0; k < pairs.length; k++) {
                    var pair = pairs[k].split(":");
                    if(pair[0] == "facet") {
                        ids = pair[1].split(",");

                        //console.debug("number of filters applied: "+this.noOfFilters);
                    }
                    else if(pair[0] == "productBeginIndex") {
                        productBeginIndex = pair[1];
                    }
                    else if(pair[0] == "facetValues") {
                        facetValues = pair[1];
                    }
                    else if(pair[0] == "orderBy") {
                        orderBy = pair[1];
                    }
                    else if(pair[0] == "pageView") {
                        pageView = pair[1];
                    }
                    else if(pair[0] == "minPrice" && byId("price_range_input")!= null) {
                        byId("low_price_input").value = pair[1];
                        byId("low_price_value").value = pair[1];
                        minPrice = pair[1];
                    }
                    else if(pair[0] == "maxPrice" && byId("price_range_input")!= null) {
                        byId("high_price_input").value = pair[1];
                        byId("high_price_value").value = pair[1];
                        maxPrice = pair[1];
                    }
                    else if(pair[0] == "pageSize") {
                        pageSize = pair[1];
                    } else if (pair[0] == "productSize") {
                        productSize = pair[1];
                    }else if(pair[0] == "isPriceFacet") {
                        isPriceFacet = pair[1];
                    }
                }
                if(!submitRequest()){
                    this.restoreHistoryContextFlag = false;
                    return;
                }
                cursor_wait();

                for(var i = 0; i < ids.length; i++) {

                    if( $("[id^='"+ids[i]+"_']").length > 0){
                        var  ele = $("[id^='"+ids[i]+"_']")[0];
                            ele.checked =true;
                            if($(ele).hasClass("listing__color-in-family-item-input")){
                                var link = $(".listing__select-color-family-wrapper [title="+$(ele).closest("div").attr("parentcolor")+"]");
                                (link && link.length > 0)?link[0].click():"";
                            }
                            if (ids[i] == "price") {
                                if(minPrice != "" && maxPrice != "") {
                                    this.appendFilterPriceRange();
                                    $(".listing__price-range-low-input").attr('value', parseInt(minPrice));
                                    $(".listing__price-range-high-input").attr('value', parseInt(maxPrice));
                                    priceRange.initBar();
                                }

                            } else{
                                SearchBasedNavigationDisplayJS.appendFilterFacet(ele.id, $(ele).hasClass("listing__color-in-family-item-input")?"colorFamilyBased":"");
                            }

                    } else {
                        var e = byId(ids[i]);
                        if (e) {
                            e.checked = true;
                            if($(e).hasClass("listing__color-in-family-item-input")){
                                var link = $(".listing__select-color-family-wrapper [title="+$(e).closest("div").attr("parentcolor")+"]");
                                (link && link.length > 0)?link[0].click():"";
                            }
                            this.appendFilterFacet(ids[i], $(e).hasClass("listing__color-in-family-item-input")?"colorFamilyBased":"");

                            //this.noOfFilters=this.noOfFilters+1;
                        }
                    }
                }

                if(productSize != "") {
                     if(productSize == "small") {
                         $(".listing__grid-select-smaller:visible").click();
                     } else if(productSize == "large") {
                         $(".listing__grid-select-larger:visible").click();
                     }
                }

                var facetArray = this.getEnabledProductFacets();
                var facets = facetArray[0]
                    , tempFacets = [];
                for(var i = 0; i < facets.length ; i++){
                    if(facets[i].startsWith("ads_f1_ntk_cs")){
                        var currentFacetValue = decodeURIComponent(facets[i]).split(":")[1].trim().replace(/"/g, "")
                            , colorIndex = tempFacets.indexOf(currentFacetValue);
                        if (colorIndex == -1) {
                            tempFacets.push(currentFacetValue);
                        }
                    }
                }

                var facetValuesArray = facetValues.split(",");
                for(var i = 0; i < facetValuesArray.length; i++){
                    if(jQuery.inArray(facetValuesArray[i], tempFacets) != -1){
                        var colorIndex = SearchBasedNavigationDisplayJS.filterAppliedColors.indexOf(facetValuesArray[i]);
                        if (colorIndex == -1) {
                            SearchBasedNavigationDisplayJS.filterAppliedColors.push(facetValuesArray[i]);
                        }
                    }
                }

                if(facetValuesArray.length == 0 || !facetValuesArray){
                    SearchBasedNavigationDisplayJS.filterAppliedColors = tempFacets;
                }


                wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": productBeginIndex, "orderBy": orderBy, "pageView": pageView, "facet": facetArray[0], "facetLimit": facetArray[1], "facetId": facetArray[2], "minPrice": minPrice, "maxPrice": maxPrice, "isPriceFacet": this.isPriceFacet, "pageSize": pageSize, "facetValues": facetValues} );
                this.updateHistory();
            }
            else {
                this.updateHistory();
                $(document).trigger("selectDefaultDynaCatColor");
            }
            this.restoreHistoryContextFlag = false;
        },
        getContrast50:function (hexcolor){
            var toHex = '0x'+hexcolor;
            $('.'+hexcolor).addClass((parseInt(hexcolor, 16) > 0xffffff/1.1) ? '':'dark');
        },
        removeAllColorsFromFilter : function(singleColor){
            var count = $("#facetFilterList .colorFamilyBased ").length;
            $("#facetFilterList .colorFamilyBased ").each(function( index ) {
                var id = $(this).attr("id").split("filter_")[1];
                SearchBasedNavigationDisplayJS.removeFilterFacet(id);
                var currentFacetValue = $(this).find(".listing__filters-applied-item-text").text();
                var colorIndex = SearchBasedNavigationDisplayJS.filterAppliedColors.indexOf(currentFacetValue);
                if (colorIndex > -1) {
                    SearchBasedNavigationDisplayJS.filterAppliedColors.splice(colorIndex, 1);
                }
            });
            /*if(count !=0 && !singleColor){
                this.doSearchFilter(false);
            }
*/
            return count;
        },
        afterSelectingColorFamily : function(event){
            var selectAllStr = "select_all_colors_" + event.data.str;
            var keyworkFlag = event.data.keyworkFlag;
            var childColorToFamily = document.getElementsByClassName("childColorToFamily " + event.data.str);
            var el = $(childColorToFamily).find("[data-for="+selectAllStr +"] input")[0];
            $(document).off("afterSelectingColorFamily");
            el.checked = true;
            SearchBasedNavigationDisplayJS.selectAllFamilyColors(el, !jQuery(".FAMILY_RADIO_"+event.data.str).prop('checked'), keyworkFlag);
            $(document).trigger("selectColorsFromkeyword");
        },
        showChildColorFamily:function (str, singleColor, keyworkFlag){
            var childColorToFamilies = document.getElementsByClassName("childColorToFamily");
            var i;
            for (i = 0; i < childColorToFamilies.length; i++) {
                childColorToFamilies[i].style.display = "none";
            }
            var childColorToFamily = document.getElementsByClassName("childColorToFamily " + str);
            childColorToFamily[0].style.display="block";


            if(this.restoreHistoryContextFlag || document.env_omniPlpImageColorization == "0"){
                return;
            }

            categoryPageParams.initialPageLoad = false;
            if(singleColor){
                this.removeAllColorsFromFilter(singleColor);
                return;
            }

            $(document).on("afterSelectingColorFamily", {str: ""+str, "keyworkFlag": keyworkFlag}, this.afterSelectingColorFamily);
            var removedCount = this.removeAllColorsFromFilter();
            $(document).trigger("afterSelectingColorFamily");
            return removedCount;
        } ,
        selectColorChild:function (item) {
            var id=item.value;
            if($("#"+id).length){
                id = $("#"+id)[0].id;
            }else{
                if ( $("[id^='"+id+"']").length !=0 ) {
                    var selectedKey = item.options[item.selectedIndex].getAttribute("key");
                    for(var i = 0 ; i < $("[id^='"+id+"']").length ; i++){
                        var text = $($("[id^='"+id+"']")[i]).closest("li").find(".listing__color-in-family-item-title").text();
                        if(text == selectedKey){
                            id = $("[id^='"+id+"']")[i].id;
                        }
                    }
                }
            }
            if (id != 0) {
               var input = $("input#" + id);
               var key = input.parent().closest('div').attr("parentColor");
                if (!input.attr('disabled')) {
                    this.showChildColorFamily(key, true);
                    document.getElementsByClassName("FAMILY_RADIO_"+key)[0].checked= true;
                    if(!document.getElementById(id).checked) {
                        input.click();
                        document.getElementById(id).checked = true;
                    }
                }
                $("#add_color_drop_down option:eq(0)").prop('selected', true);
            }
        },
        selectDefaultDynaCatColorChild:function () {
            if(SearchBasedNavigationDisplayJS.keyword){
                SearchBasedNavigationDisplayJS.isDynamicCategory = true;
                var keywordObject = eval("(" + SearchBasedNavigationDisplayJS.keyword + ')')
                    , colorFamily = keywordObject.colorFamily
                    , colors = keywordObject.colors
                    , input
                    , removedCount = 0;
                $(".listing__select-color-family-wrapper").closest(".listing__filter-wrapper").attr("data-listing-filter-open", true);
                SearchBasedNavigationDisplayJS.keyword = "";
                for(var i = 0; i < colorFamily.length ; i++){
                    jQuery(".FAMILY_RADIO_"+colorFamily[i]).prop("checked", true);
                    removedCount = SearchBasedNavigationDisplayJS.showChildColorFamily(colorFamily[i], null, true);
                }
                if(removedCount > 0){
                    $(document).on("selectColorsFromkeyword", {"colors": colors}, SearchBasedNavigationDisplayJS.selectColorsFromkeyword);
                }else {
                    SearchBasedNavigationDisplayJS.selectColorsFromkeyword(colors);
                    SearchBasedNavigationDisplayJS.doSearchFilter(undefined, true);
                }

           }
        },
        selectColorsFromkeyword: function(colors){
            var flag = false;
            if(colors.data && colors.data.colors){
                flag = true;
                colors = colors.data.colors;
                $(document).off("selectColorsFromkeyword");
            }
            for(var i = 0; i < colors.length; i++){
                input = $("[color-name='"+colors[i]+"']").closest("a").find("input");
                if ($(input).length > 0 && input) {
                    var isChecked = $(input).prop('checked');
                    var disabled = $(input).prop('disabled');
                    if(!isChecked && !disabled) {
                        var id = $(input).attr("id");
                        SearchBasedNavigationDisplayJS.appendFilterFacet(id);
                        var currentFacetValue = decodeURIComponent($(input).val()).split(":")[1].trim().replace(/"/g, "").replace(/\+/g," ")
                            , colorIndex = SearchBasedNavigationDisplayJS.filterAppliedColors.indexOf(currentFacetValue);
                        if (colorIndex == -1) {
                            SearchBasedNavigationDisplayJS.filterAppliedColors.push(currentFacetValue);
                        }
                        $(input).prop('checked', true);
                    }
               }
            }
            if(flag){
                SearchBasedNavigationDisplayJS.doSearchFilter(undefined, true);
            }
        },
        selectAllFamilyColors:function(el, familyChecked, keyworkFlag){
             if(familyChecked == undefined){
                 jQuery(".FAMILY_RADIO_"+el.getAttribute("id").split("select_all_colors_")[1]).prop("checked", el.checked)
             }
             if(el.checked && !familyChecked){
                 $(".childColorToFamily:visible li a input.listing__color-in-family-item-input").each(function( index ) {
                     var isChecked = $(this).prop('checked');
                     var disabled = $(this).prop('disabled');
                     if(disabled) {
                         $(this).attr("should-checked-when-enabled", "true");
                     }
                     if(!isChecked && !disabled) {
                         var id = $(this).attr("id");
                         SearchBasedNavigationDisplayJS.appendFilterFacet(id, "colorFamilyBased");
                         var currentFacetValue = decodeURIComponent($(this).val()).split(":")[1].trim().replace(/"/g, "").replace(/\+/g," ")
                             , colorIndex = SearchBasedNavigationDisplayJS.filterAppliedColors.indexOf(currentFacetValue);
                         if (colorIndex == -1) {
                             SearchBasedNavigationDisplayJS.filterAppliedColors.push(currentFacetValue);
                         }
                         $(this).prop('checked', true);
                     }
                 });
             } else {
                 $(".childColorToFamily:visible li a input.listing__color-in-family-item-input").each(function( index ) {
                      var isChecked = $(this).prop('checked');
                      if(isChecked) {
                          var id = $(this).attr("id");
                          SearchBasedNavigationDisplayJS.removeFilterFacet(id);
                          var currentFacetValue = decodeURIComponent($(this).val()).split(":")[1].trim().replace(/"/g, "").replace(/\+/g," ");
                          var colorIndex = SearchBasedNavigationDisplayJS.filterAppliedColors.indexOf(currentFacetValue);
                          if (colorIndex > -1) {
                              SearchBasedNavigationDisplayJS.filterAppliedColors.splice(colorIndex, 1);
                          }
                          $(this).prop('checked', false);

                      }
                      if ($(this).is("[should-checked-when-enabled]")) {
                          $(this).removeAttr("should-checked-when-enabled");
                      }
                  });
             }
             if(!keyworkFlag){
                 SearchBasedNavigationDisplayJS.doSearchFilter();
             }
        },
        selectAllFabricFamily:function (className, el) {
            if(el.checked){
                var parent = $(el).closest("#listing_fabric_childs");
                $(parent).find("."+className).each(function( index ) {
                    var isChecked = document.getElementsByClassName(className)[index].checked;
                    var disabled = document.getElementsByClassName(className)[index].disabled;
                    if(!isChecked && !disabled) {
                        var id = $(this).attr("id");
                        SearchBasedNavigationDisplayJS.appendFilterFacet(id);
                        document.getElementsByClassName(className)[index].checked = true;
                    }
                });
            } else {
                 var parent = $(el).closest("#listing_fabric_childs");
                 $(parent).find("."+className).each(function( index ) {
                     var isChecked = document.getElementsByClassName(className)[index].checked;
                     if(isChecked) {
                         var id = $(this).attr("id");
                         SearchBasedNavigationDisplayJS.removeFilterFacet(id);
                         document.getElementsByClassName(className)[index].checked = false;

                     }
                 });
            }
            SearchBasedNavigationDisplayJS.doSearchFilter();
        },
        selectRegularSize:function() {
            var regularElement = byId("size-type--"+this.selectedSize);
            if(regularElement != null) {
                regularElement.checked = true;
                this.showChildSizeFamily(this.selectedSize);
            }
        },
        UpdateSelectAllState:function () {
             var allChecked;
             var allDisabled;
            $(".FAMILY_CHILDS").each(function(index){
                allChecked = true;
                allDisabled = true;
                $(this).find(".FAMILY_A_CHILD").each(function (index1){
                    if(!$(this).is(":checked") && !$(this).is(":disabled")){
                        allChecked = false;
                    }
                    if(!$(this).is(":disabled")){
                        allDisabled = false;
                    }
                });
                $(this).find(".select_all").each(function(index1){
                      if(allChecked && !allDisabled) {
                          $(this).prop('checked', true);
                      } else {
                          $(this).prop('checked', false);
                      }
                      if(allDisabled) {
                          $(this).prop('disabled', true);
                      } else {
                          $(this).prop('disabled', false);
                      }
                      if(this.checked && this.id.startsWith("select_all_colors_") && jQuery(this).closest(".listing__color-in-family-wrapper").is(":visible")){
                          jQuery(".FAMILY_RADIO_"+this.getAttribute("id").split("select_all_colors_")[1]).prop("checked", this.checked);
                      }
                });
            });
        },
        showChildSizeFamily:function (sizeFamily){
            var childSizeToFamilies = document.getElementsByClassName("childSizeFamilySize");
            var i;
            for (i = 0; i < childSizeToFamilies.length; i++) {

                    childSizeToFamilies[i].style.display = "none";

            }
            var childSizeToFamily = document.getElementsByClassName("childSizeFamilySize " + sizeFamily);
            for (i = 0; i < childSizeToFamily.length; i++) {
                childSizeToFamily[i].style.display="inline-block";
            }
        },
        setProductPrices : function(Id) {
            var id = "#"+Id;
            var dataDivId = "#"+Id+"_data";
            var commonPricePrefixTextDiv = "#commonPricePrefixText";
            var empty = function (aVar) {
                return aVar == undefined || aVar ==null || aVar == "";
             }

              var minOP;
              var maxOP;
              var minRP;
              var maxRP;
              var minTP;
              var maxTP;
              var minRP_Without_OP;
              var maxRP_Without_OP;
              var currCode;
              var currencyCookie = e4x_currency;
              var isShipIntl = dojo.query(dataDivId + " #isShipInternational").val();
              var priceHTML = "";
              var priceNotAvailable = dojo.query(commonPricePrefixTextDiv + " #priceNotAvailable").text();
              var rangeSeparator = dojo.query(commonPricePrefixTextDiv + " #rangeSeparator").text();

              var minRP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #minregularprice").val());
              var maxRP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #maxregularprice").val());
              var minOP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #minofferprice").val());
              var maxOP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #maxofferprice").val());
              var minTP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #mintierprice").val());

              var minRP_Without_OP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #minRegWithoutOfferPrice").val());
              var maxRP_Without_OP_USD_unformatted = parseFloat(dojo.query(dataDivId + " #maxRegWithoutOfferPrice").val());

              //format all prices on the server for caching
              var minRP_USD = dojo.query(dataDivId + " #minRP_USD").val();
              var maxRP_USD = dojo.query(dataDivId + " #maxRP_USD").val();

              var minOP_USD = dojo.query(dataDivId + " #minOP_USD").val();
              var maxOP_USD = dojo.query(dataDivId + " #maxOP_USD").val();

              var minTP_USD = dojo.query(dataDivId + " #minTP_USD").val();
              var maxTP_USD = dojo.query(dataDivId + " #maxTP_USD").val();

              var minRP_Without_OP_USD = dojo.query(dataDivId + " #minRegWithoutOfferPrice_USD").val();
              var maxRP_Without_OP_USD = dojo.query(dataDivId + " #maxRegWithoutOfferPrice_USD").val();

              var noOfSkusNotOnSale = !empty(parseInt(dojo.query(dataDivId + " #noOfSkusNotOnSale").val())) ? parseInt(dojo.query(dataDivId + " #noOfSkusNotOnSale").val()) : 0;

              var isProductOnSale = (empty(minOP_USD) && empty(maxOP_USD)) ? false : true;

              var standardPrice_Class = "listing__grid-item-price-standard sans-xs";
              var originalPrice_Class = "pd_origprice listing__grid-item-price-standard sans-xs";
              var offerPriceClass = "listing__grid-item-price-marked-down sans-xs-bold";
              var priceDisplayId = Id+"_price";

              var standardPriceMessage = MessageHelper.messages['REGULAR_PRICE'];
              var standardPriceRangeMessage = MessageHelper.messages['PRICE_FROM_TO'];
              var orginalPriceRangeMessage = MessageHelper.messages['ORIGINAL_PRICE_FROM_TO'];
              var originalPriceMessage = MessageHelper.messages['ORIGINAL_PRICE'];
              var salePriceRangeMessage = MessageHelper.messages['SALE_PRICE_FROM_TO'];
              var salePriceMessage = MessageHelper.messages['SALE_PRICE'];
              var notAvaliableMessage = MessageHelper.messages['PRICE_NOT_AVAILABLE'];

              //on client side, determine what currency to show based on currencyCookie and isShipIntl flag
              if(isShipIntl != undefined && isShipIntl != null && isShipIntl == 1 && currencyCookie != undefined && currencyCookie != '' && currencyCookie != 'USD') {
                  var currSymbol = currencyCookie + '&nbsp;';
                    if(!empty(minOP_USD)) {
                       minOP = currSymbol + getE4XPrice(currencyCookie,minOP_USD_unformatted);
                    } else {
                       minOP = '';
                    }
                    if(!empty(maxOP_USD)) {
                       maxOP = currSymbol + getE4XPrice(currencyCookie,maxOP_USD_unformatted);
                    } else {
                       maxOP = '';
                    }
                    if(!empty(minRP_USD)) {
                       minRP = currSymbol + getE4XPrice(currencyCookie,minRP_USD_unformatted);
                    } else {
                       minRP = '';
                    }
                    if(!empty(maxRP_USD)) {
                       maxRP = currSymbol + getE4XPrice(currencyCookie,maxRP_USD_unformatted);
                    } else {
                       maxRP = '';
                    }
                    if(!empty(minTP_USD)) {
                       minTP = currSymbol + getE4XPrice(currencyCookie,minTP_USD_unformatted);
                    } else {
                       minTP = '';
                    }
                    if(!empty(minRP_Without_OP_USD)) {
                       minRP_Without_OP = currSymbol + getE4XPrice(currencyCookie,minRP_Without_OP_USD_unformatted);
                    } else {
                       minRP_Without_OP = '';
                    }
                    if(!empty(maxRP_Without_OP_USD)) {
                       maxRP_Without_OP = currSymbol + getE4XPrice(currencyCookie,maxRP_Without_OP_USD_unformatted);
                    } else {
                       maxRP_Without_OP = '';
                    }
              } else {
                    minOP = minOP_USD;
                    maxOP = maxOP_USD;
                    minRP = minRP_USD;
                    maxRP = maxRP_USD;
                    minTP = minTP_USD;
                    minRP_Without_OP = minRP_Without_OP_USD;
                    maxRP_Without_OP = maxRP_Without_OP_USD;
              }

              //If there is no regular or offer price or tierprice, display "Price not available"
              if (empty(minOP) && empty(maxOP) && empty(minRP) && empty(maxRP) && empty(minTP) && empty(maxTP) ) {
                priceHTML = "<span class='"+standardPrice_Class+"' tabindex='0' aria-label='"+notAvaliableMessage+"' role='text'>" + priceNotAvailable + "</span>";
              }

              //If there is tier price, display the mintierprice with "As low as" in front of it
              if (!empty(minTP)) {
                 priceHTML =  "<span tabindex='0'>" + "<span class='listing__grid-item-price-as-low-as sans-xs-bold'>"+ dojo.query(commonPricePrefixTextDiv + " #searchLowPrice").text()+"</span>"+ "<span class='listing__grid-item-price-standard sans-xs'>" + minTP + "</span>"+"</span>";
              }

              if(!isProductOnSale){
                  // If there is only regular price and the product is not on sale, display the regular price.
                  if (!empty(minRP) && !empty(maxRP) ){
                    priceHTML = "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"' tabindex='0' aria-label='"+ standardPriceMessage.replace('{0}',minRP)+"' role='text'>" + minRP + "</span>";
                  }

                  //If there is range of regular price and the product is not on sale, display the regular price range.
                  if (!empty(minRP) && !empty(maxRP) && minRP_USD_unformatted != maxRP_USD_unformatted){
                      priceHTML = "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"' tabindex='0' aria-label='"+ standardPriceRangeMessage.replace('{0}',minRP).replace('{1}',maxRP)+"' role='text'>"  + minRP + rangeSeparator + maxRP + "</span>";
                  }
              }else{
                   //As the product is on sale We need to have list price striked off. When we have list price range for skus.
                   if (!empty(minRP) && !empty(maxRP) && !empty(maxRP_USD_unformatted) && !empty(minRP_USD_unformatted) && maxRP_USD_unformatted > minRP_USD_unformatted) {
                      priceHTML = "<span class='"+originalPrice_Class+"' tabindex='0' aria-label='"+ orginalPriceRangeMessage.replace('{0}',minRP).replace('{1}',maxRP)+"' role='text'>" + minRP + rangeSeparator + maxRP + "</span>&nbsp;";
                    }
                   //As the product is on sale We need to have list price striked off. When we have same listprice for all skus.
                   if(!empty(minRP) && !empty(maxRP) && !empty(maxRP_USD_unformatted) && !empty(minRP_USD_unformatted) && maxRP_USD_unformatted == minRP_USD_unformatted){
                       priceHTML = "<span  class='"+originalPrice_Class+"' tabindex='0' aria-label='"+ originalPriceMessage.replace('{0}',minRP)+"' role='text'>" + minRP + "</span>&nbsp;" ;
                   }

                   //Partial Sale (Some skus are not on sale)
                   if(noOfSkusNotOnSale > 0){
                       if(!empty(minRP_Without_OP_USD_unformatted) && !empty(maxRP_Without_OP_USD_unformatted) && !empty(minOP_USD_unformatted) && !empty(maxOP_USD_unformatted)){
                           //
                           if((minRP_Without_OP_USD_unformatted < minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted < maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+standardPriceRangeMessage.replace('{0}',minRP_Without_OP).replace('{1}',maxOP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + minRP_Without_OP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP +  "</span>" + "</span>";
                           }
                           //
                           if((minRP_Without_OP_USD_unformatted < minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted > maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+standardPriceRangeMessage.replace('{0}',minRP_Without_OP).replace('{1}',maxRP_Without_OP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + minRP_Without_OP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + maxRP_Without_OP + "</span>" + "</span>";
                           }
                           //
                           if((minRP_Without_OP_USD_unformatted < minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted == maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+ standardPriceRangeMessage.replace('{0}',minRP_Without_OP).replace('{1}',maxOP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + minRP_Without_OP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>" + "</span>";
                           }

                           //
                           if((minRP_Without_OP_USD_unformatted > minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted < maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+ standardPriceRangeMessage.replace('{0}',minOP).replace('{1}',maxOP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>" + "</span>";
                           }
                           //
                           if((minRP_Without_OP_USD_unformatted > minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted > maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+ standardPriceRangeMessage.replace('{0}',minOP).replace('{1}',maxRP_Without_OP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + maxRP_Without_OP + "</span>" + "</span>";
                           }
                           //
                           if((minRP_Without_OP_USD_unformatted > minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted == maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+  standardPriceRangeMessage.replace('{0}',minOP).replace('{1}',maxOP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>" + "</span>";
                           }

                           //
                           if((minRP_Without_OP_USD_unformatted == minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted < maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+ standardPriceRangeMessage.replace('{0}',minOP).replace('{1}',maxOP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP + "</span>" + "</span>";
                           }
                           //
                           if((minRP_Without_OP_USD_unformatted == minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted > maxOP_USD_unformatted)){
                               priceHTML = priceHTML + "<span tabindex='0' aria-label='"+ standardPriceRangeMessage.replace('{0}',minOP).replace('{1}',maxRP_Without_OP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+standardPrice_Class+"'>" + maxRP_Without_OP + "</span>" + "</span>";
                           }
                           //
                           if((minRP_Without_OP_USD_unformatted == minOP_USD_unformatted) && (maxRP_Without_OP_USD_unformatted == maxOP_USD_unformatted)){
                               if(minOP_USD_unformatted == maxOP_USD_unformatted){
                                   priceHTML = priceHTML + "<span tabindex='0' aria-label='"+ standardPriceMessage.replace('{0}',minOP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span></span>";
                               }else{
                                   priceHTML = priceHTML + "<span tabindex='0' aria-label='"+standardPriceRangeMessage.replace('{0}',minOP).replace('{1}',maxOP)+"' role='text'>" + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + minOP + "</span>" + rangeSeparator + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"'>" + maxOP +"</span>" + "</span>";
                               }
                           }
                       }
                   }else{
                       //All the skus are on sale and has a range.
                       if (!empty(maxOP_USD_unformatted) && !empty(minOP_USD_unformatted) && maxOP_USD_unformatted > minOP_USD_unformatted) {
                           priceHTML = priceHTML +"<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"' tabindex='0' role ='text' aria-label='"+salePriceRangeMessage.replace('{0}',minOP).replace('{1}',maxOP)+"' >" + minOP + rangeSeparator + maxOP + "</span>";
                       }
                       //All the skus are on sale and has same sale price.
                       if(!empty(maxOP_USD_unformatted) && !empty(minOP_USD_unformatted) && maxOP_USD_unformatted == minOP_USD_unformatted){
                           priceHTML = priceHTML + "<span id='"+Id+"_priceSpan' class='"+offerPriceClass+"' tabindex='0' role ='text' aria-label='"+salePriceMessage.replace('{0}',minOP)+"'>" + minOP + "</span>";
                       }
                   }
              }


              document.getElementById(priceDisplayId).innerHTML = priceHTML;

        },

        pushFacetValue : function(facetValue, isColorFlag) {
            /* this is called twice for some reason when a facet is selected toneed to make sure it's not already added */
            if (this.facetValuesArray.indexOf(facetValue) == -1 && facetValue != "") {
                this.facetValuesArray.push(facetValue);
                if(isColorFlag){
                    var colorIndex = this.filterAppliedColors.indexOf(facetValue);
                    if (colorIndex == -1) {
                        this.filterAppliedColors.push(facetValue);
                    }
                }
            }

        },
        pushFacetId : function(facetId) {
             var push = true;
             if(facetId.indexOf("_") != -1 && facetId != "") {
                 facetId = facetId.split("_")[0];
                 if(this.appliedFacetIdsArray.indexOf(facetId) != -1) {
                     push = false;
                 }
             }
             if(push)
                 this.appliedFacetIdsArray.push(facetId);
        },

        popFacetValue : function(facetValue) {
            var index = this.facetValuesArray.indexOf(facetValue);

            if (index > -1) {
                this.facetValuesArray.splice(index, 1);
            }
            var colorIndex = this.filterAppliedColors.indexOf(facetValue);
            if (colorIndex > -1) {
                this.filterAppliedColors.splice(colorIndex, 1);
            }
        },
        popFacetId : function(facetId) {
            var pop = true;
            if(facetId.indexOf("_") != -1 && facetId != "") {
                facetId = facetId.split("_")[0];
                if(this.appliedFacetIdsArray.indexOf(facetId) == -1) {
                    pop = false;
                }
            }
            if(pop)
                this.appliedFacetIdsArray.splice(this.appliedFacetIdsArray.indexOf(facetId), 1);
       },
        trackCategory: function(categoryIdentifier){
            var catIds1 = dojo.query("[id^='MiniGridQuickView']").map(function(obj){return obj.id.split("_")[1]})
            var catIds2 = dojo.query("[id^='CatalogEntryProdImg']").map(function(obj){return obj.id.split("_")[1]});
            var category = categoryIdentifier;
            MonetateJS.trackCategory([category], catIds1.concat(catIds2));
        }
    };
}
