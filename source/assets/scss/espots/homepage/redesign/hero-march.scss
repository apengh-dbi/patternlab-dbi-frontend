@import "../../../variables";
@import "../../../mixins";


$max-width: 1600px;


.ctnr-maincontent .row {
  margin: 0 auto;
}
.ctnr-maincontent a {
  text-decoration: none;
}





.hero {
  //@include red-bg();
  display: block;
  position: relative;
  width: 100%;
  max-width: $max-width;
  margin: 0 auto;
  margin-top: -5px;

  a {
    text-decoration: none;
  }

  .column_full {
    margin-left: -5px;
    margin-right: -5px;
  }



  img {
    width: 100%;
  }




  .flex {
    //@include green-bg();
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    align-items: stretch;
    align-content: stretch;
    max-width: $max-width;
    margin-left: auto;
    margin-right: auto;


    // --------------------------------------------------------
    // .flex__row
    // --------------------------------------------------------
    &__row {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      width: 100%;

        &:first-child {
          @media screen and (min-width: 768px) {
            width: 37.2%;
          }
          @media screen and (min-width: 1024px) {
            width: 37%;
          }
          @media screen and (min-width: 1150px) {
            width: 36.8322981366%;
          }
        }
        &:last-child {
          @media screen and (min-width: 768px) {
            width: 62.8%;
          }
          @media screen and (min-width: 1024px) {
            width: 63%;
          }
          @media screen and (min-width: 1150px) {
            width: 63.1677018634%;
          }


          .flex__cell {
            &:first-child {
              @media screen and (min-width: 768px) {
                width: 58.2%;
              }
              @media screen and (min-width: 1024px) {
                width: 58.2%;
              }
              @media screen and (min-width: 1150px) {
                width: 58.3087512291%;
              }
            }
            &:nth-child(2) {
              @media screen and (min-width: 768px) {
                width: 41.8%;
              }
              @media screen and (min-width: 1024px) {
                width: 41.8%;
              }
              @media screen and (min-width: 1150px) {
                width: 41.6912487709%;
              }
            }
          }
        }
        @media screen and (min-width: 768px) {
          &:last-of-type {
            flex-flow: wrap;
          }
        }
    } // .flex__row


    // --------------------------------------------------------
    // .flex__cell
    // --------------------------------------------------------
    &__cell {
      //@include green-bg();
      display: block;
      position: relative;
      padding: 5px;
      width: 100%;

      // --------------------------------------------------------
      // .flex__cell-img
      // --------------------------------------------------------
      &-img {
        display: block;
        position: relative;
        padding-left: 10px;
        padding-right: 10px;
        &.full-width {
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
        @media screen and (min-width: 768px) {
          padding-left: 0;
          padding-right: 0;
        }

        a {
          display: block;
          position: relative;
        }

        .btn {
          font-family: "Open Sans",Helvetica Neue,Arial,sans-serif;
          display: inline-block;
          text-decoration: none;
          text-align: center;
          text-transform: uppercase;
          vertical-align: top;
          white-space: nowrap;
          border: none;
          padding: 0;
          cursor: pointer;
          width: auto;
          position: absolute;
          bottom: 10px;
          left: 10px;
          right: 10px;
          background: none;
          margin-left: 10px;
          margin-right: 10px;

          &-secondary a {
            display: inline-block;
            padding: 11px 14px;
            border: 1px solid $purple-gray-dk;
            color: $black;
            background-color: rgba(255, 255, 255, 0.8);
            outline: none;
            backface-visibility: hidden;
            font-size: 11px;
            letter-spacing: 0.605px;
            width: 100%;
            &:hover,
            &:active,
            &focus {
              border: 1px solid $gray-lt;
              background-color: rgba(255, 255, 255, 0.9);
            }
            @media screen and (min-width: 768px) {
              padding: 9px 14px;
            }
          }
        }

      } // .flex__cell-img


      // --------------------------------------------------------
      // .flex__cell-overlay
      // --------------------------------------------------------
      &-overlay {
        display: block;
        position: relative;
        margin-left: 10px;
        margin-right: 10px;
        padding: 5px 0 5px 5px;
        background: rgba(248,246,240,0.85);
        @media screen and (min-width: 768px) {
          margin-left: 0;
          margin-right: 0;
          position: absolute;
          top: 5px;
          left: 5px;
          right: 5px;
          bottom: 5px;
          padding: 0;
          background: rgba(0,0,0,0.5);
          opacity: 0;
          transition: all 0.4s ease-in;
          cursor: pointer;
          display: flex;
          justify-content: center;
          align-items: center;
          color: $white;

        }
        @media screen and (min-width: 1024px) {
          &:hover,
          &:active,
          &:focus {
            opacity: 1;
          }
        }

        &.lead {
          margin-left: 0;
          margin-right: 0;
          padding-left: 15px;
          @media screen and (min-width: 768px) {
            padding-left: 0;
          }
        }
      } // .flex__cell-overlay

    } // .flex__cell
  } // .flex



  // ========================================================
  // Hero Copy
  // ========================================================
  &__copy {
    display: block;
    text-align: center;
    position: relative;
    background: rgba(248,246,240, 1);
    margin-left: 10px;
    margin-right: 10px;
    margin-top: 10px;
    padding-bottom: 30px;
    transition: all 0.3s ease;
    @media screen and (min-width: 768px) {
      background: rgba(248,246,240, 0.75);
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      transform-style: preserve-3d;
      z-index: 10;
      padding: 10px 10px;
      width: 230px;
      left: calc(100% - 120px);
      margin: 0;
      margin-top: 0;
    }
    @media screen and (min-width: 1024px) {
      background: rgba(248,246,240, 0.85);
      margin-top: -1px;
      padding: 10px 10px 20px;
      width: 260px;
      left: calc(100% - 135px);
    }

    &-offer {
      //@include green-bg();
      padding-top: 15px;
      padding-bottom: 10px;
      @media screen and (min-width: 768px) {
        padding-top: 8px;
        padding-bottom: 8px;
      }
      @media screen and (min-width: 1024px) {
        padding-top: 8px;
        padding-bottom: 8px;
      }
      // @media screen and (min-width: 1024px) {
      //   padding-top: 12px;
      //   padding-bottom: 10px;
      // }
    }

    p {
      margin: 0;

      &.sans-l {
        //margin-bottom: 10px;
      }
      &.serif-xl {
        margin-top: 5px;
        margin-bottom: 5px;
        @media screen and (min-width: 768px) {
          margin-top: 0;
          margin-bottom: 0;
        }
        @media screen and (min-width: 1024px) {
          margin-top: 0;
          margin-bottom: 5px;
        }
        @media screen and (min-width: 1100px) {
          margin-top: 5px;
          margin-bottom: 5px;
        }
      }
      &.sans-bold-uppercase {
        margin-top: 10px;
        @media screen and (min-width: 768px) {
          margin-top: 5px;
        }
        @media screen and (min-width: 1024px) {
          margin-top: 10px;
        }
        @media screen and (min-width: 1100px) {
          margin-top: 10px;
        }
      }
      &.btn {
        margin-top: 10px;
        //margin-bottom: 10px;
        @media screen and (min-width: 768px) {
          margin-top: 6px;
        }
        @media screen and (min-width: 1024px) {
          margin-top: 10px;
        }
        @media screen and (min-width: 1100px) {
          margin-top: 10px;
        }
      }
    }

  }


  .serif-xl {
    font-family: 'Domaine Display', 'Bodoni 72', 'Times New Roman', Times, 'Droid Serif', serif;
    font-size: 36px;
    line-height: 1;
    font-weight: normal;
      @media screen and (min-width: $breakpoint-md) {
        font-size: 26px;
      }
      @media screen and (min-width: $breakpoint-lg) {
        font-size: 36px;
      }
    }

  .sans-l {
    font-family: 'Open Sans', Verdana, Helvetica, Arial, 'Droid Sans', sans-serif;
    font-size: 15px;
    line-height: 20px;
    font-weight: normal;
      @media screen and (min-width: $breakpoint-md) {
        font-size: 13px;
        line-height: 18px;
      }
      @media screen and (min-width: $breakpoint-lg) {
        font-size: 15px;
        line-height: 20px;
      }
    }



  // ----------------------------------------------------------------
  // Buttons
  // ----------------------------------------------------------------
  .btn {
    font-family: "Open Sans",Helvetica Neue,Arial,sans-serif;
    display: inline-block;
    text-decoration: none;
    text-align: center;
    text-transform: uppercase;
    vertical-align: top;
    white-space: nowrap;
    border: none;
    padding: 0;
    cursor: pointer;
    width: auto;

    &-secondary a {
      display: inline-block;
      padding: 11px 14px;
      border: 1px solid $purple-gray-dk;
      color: $black;
      background-color: $white;
      outline: none;
      backface-visibility: hidden;
      &:hover,
      &:active,
      &focus {
        border: 1px solid $gray-lt;
      }
      @media screen and (min-width: 768px) {
        padding: 9px 14px;
      }
    }
  }



  // ========================================================
  // Bottom Cover
  // ========================================================
  &__bottom-cover {
    display: none;
    @media screen and (min-width: 768px) {
      display: block;
      width: 100%;
      height: 5px;
      background: white;
      position: absolute;
      bottom: 0;
      z-index: 10;
    }
    @media screen and (min-width: 1024px) {
      height: 10px;
    }
  }


  &__disclaimer {
    z-index: 11;
    margin-left: 10px;
    margin-right: 10px;
    padding-bottom: 10px;
    @media screen and (min-width: 768px) {
      text-align: right;
      margin-right: 5px;
      margin-top: -3px;
    }
  }

} // .hero





// ========================================================
// Animation
// ========================================================
@keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}

$delay: 0.5s;

@media screen and (min-width: 768px) {
  .fade-in {
    opacity: 0;
    animation: fadeIn ease-in 1;
    animation-fill-mode: forwards;
    animation-duration: 1s;

    &.one {
      animation-delay: $delay;
    }
    &.two {
      animation-delay: $delay * 2;
    }
    &.three {
      animation-delay: $delay * 3;
    }
    &.four {
      animation-delay: $delay * 4;
    }
    &.last {
      animation-delay: $delay * 5;
    }

  } // .fade-in
}





// ==========================================================================
// Visibility
// ==========================================================================
.sm-hidden {
  @media screen and (max-width: $breakpoint-md - 1) {
    display: none !important;
  }
}

.md-hidden {
  @media screen and (min-width: $breakpoint-md) and (max-width: ($breakpoint-lg - 1)) {
    display: none !important;
  }
}

.lg-hidden {
  @media screen and (min-width: $breakpoint-lg) {
    display: none !important;
  }
}
