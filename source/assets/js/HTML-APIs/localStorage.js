if (typeof(Storage) !== "undefined") {
    // Code for localStorage/sessionStorage.
    console.log('localStorage available');

    // Store
    localStorage.setItem("firstName", "Adam");
    localStorage.setItem("lastName", "Pengh");

    // Retrieve
    var user = localStorage.getItem("firstName") + ' ' + localStorage.getItem("lastName");
    var html = '<span class="header__personal-nav-account-text sans-xs">' + user + '</span>';
    $('.signup-login.signup-login__desktop').html(html);

    // Remove


} else {
    // Sorry! No Web Storage support..
  console.log('Sorry! No Web Storage support..');
}
