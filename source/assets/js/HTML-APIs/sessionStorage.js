if (typeof(Storage) !== "undefined") {
    // Code for sessionStorage/sessionStorage.
    console.log('sessionStorage available');

    // Store
    sessionStorage.setItem("firstName", "Adam");
    sessionStorage.setItem("lastName", "Pengh");

    // Retrieve
    var user = sessionStorage.getItem("firstName") + ' ' + sessionStorage.getItem("lastName");
    var html = '<span class="header__personal-nav-account-text sans-xs">' + user + '</span>';
    $('.signup-login.signup-login__desktop').html(html);

    // Remove


} else {
    // Sorry! No Web Storage support..
  console.log('Sorry! No Web Storage support..');
}
