(function($) {
  console.log('Add BazaarVoice Ratings to Product Images');

  var passkey = 'caM5iAOQAcSOWx0vOkU41xOJsFLjxeBlvVVjrWj4VnWWw';
  var api = 'https://api.bazaarvoice.com/data/products.json?apiversion=5.4&passkey=' + passkey + '&stats=reviews&limit=30&sort=isActive:asc&filter=id:';

  $.each( $('.bv-ratings'), function(index) {

    var productIdArray = [];
    var products = $(this).find('[data-product-id]');

    $.each(products , function() {
      var productId = $(this).attr('data-product-id');
      productId = parseInt(productId);
      productIdArray.push(productId);
    });
    var productIdString =  productIdArray.toString();


    var request = api + productIdString;
    $.ajax({
      url: request,
      method: 'GET'
    }).success(function(data) {
      var results = data.Results;

      products.each(function() {
        var productId = $(this).attr('data-product-id');
        productId = parseInt(productId);

        for(var i = 0; i < results.length; i++) {
          var id = results[i].Id;
          var rating = results[i].ReviewStatistics.AverageOverallRating;
          var totalReviews = results[i].TotalReviewCount;
          if(productId == id && rating != null) {
            var roundedRating = Math.round( rating * 10 ) / 10;
            var width = Math.round( (roundedRating / 5) * 100 * 10 ) / 10;
            var html = '<div class="listing__grid-item-rating-stars" title="' + roundedRating + ' out of 5 stars">\
                <div class="listing__grid-item-rating-stars-top" style="width: ' + width + '%;"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>\
                <div class="listing__grid-item-rating-stars-bottom"><span>★</span><span>★</span><span>★</span><span>★</span><span>★</span></div>\
              </div>';
            $(this).find('.listing__grid-item-rating').html(html);
          }
        }
      });

    }).error(function() {
      console.error('AJAX ERROR');
    });

  });
}(jQuery));
