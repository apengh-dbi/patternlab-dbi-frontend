var slickOptions = {
  autoplay: false,
  autoplaySpeed: 3000,
  slidesToShow: 6,
  variableWidth: true,
  infinite: true,
  centerMode: false,
  dots: false
};
jQuery('.slick').slick(slickOptions);
jQuery('.slick-container button').on('click', function(event){
  event.preventDefault();
});

jQuery(document).ready(function(){
  var windowWidth = $(window).width();
  var bottomCarouselWidth = $('.slick').width();

  var responsiveCarousel = {
    "brkptSmall": {
      "viewport": 500,
      "slides": 2
    },
    "brkptMedium": {
      "viewport": 650,
      "slides": 3
    },
    "brkptLarge": {
      "viewport": 900,
      "slides": 4
    }
  };

  if(windowWidth < responsiveCarousel.brkptSmall.viewport) {
    $('.palette-carousel .slick-slide').width((bottomCarouselWidth / responsiveCarousel.brkptSmall.slides) - 10);
  } else if (windowWidth < responsiveCarousel.brkptMedium.viewport) {
    $('.palette-carousel .slick-slide').width((bottomCarouselWidth / responsiveCarousel.brkptMedium.slides) - 10);
  } else if (windowWidth < responsiveCarousel.brkptLarge.viewport) {
    $('.palette-carousel .slick-slide').width((bottomCarouselWidth / responsiveCarousel.brkptLarge.slides) - 10);
  } else {
    $('.palette-carousel .slick-slide').width((bottomCarouselWidth / slickOptions.slidesToShow) - 10);
  }

  $(window).resize(function(){
    var windowWidthResize = $(window).width();
    var bottomCarouselWidthResize = $('.slick').width();

    if(windowWidthResize < responsiveCarousel.brkptSmall.viewport) {
      $('.palette-carousel .slick-slide').width((bottomCarouselWidthResize / responsiveCarousel.brkptSmall.slides) - 10);
    } else if (windowWidthResize < responsiveCarousel.brkptMedium.viewport) {
      $('.palette-carousel .slick-slide').width((bottomCarouselWidthResize / responsiveCarousel.brkptMedium.slides) - 10);
    } else if (windowWidthResize < responsiveCarousel.brkptLarge.viewport) {
      $('.palette-carousel .slick-slide').width((bottomCarouselWidthResize / responsiveCarousel.brkptLarge.slides) - 10);
    } else {
      $('.palette-carousel .slick-slide').width((bottomCarouselWidthResize / slickOptions.slidesToShow) - 10);
    }
  });
});


var pinterest = '<svg class="article__share-bar-item-image" id="Layer_1" version="1.1" viewBox="0 0 56.693 56.693" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' +
'<path d="M30.374,4.622c-13.586,0-20.437,9.74-20.437,17.864c0,4.918,1.862,9.293,5.855,10.922c0.655,0.27,1.242,0.01,1.432-0.715  c0.132-0.5,0.445-1.766,0.584-2.295c0.191-0.717,0.117-0.967-0.412-1.594c-1.151-1.357-1.888-3.115-1.888-5.607  c0-7.226,5.407-13.695,14.079-13.695c7.679,0,11.898,4.692,11.898,10.957c0,8.246-3.649,15.205-9.065,15.205  c-2.992,0-5.23-2.473-4.514-5.508c0.859-3.623,2.524-7.531,2.524-10.148c0-2.34-1.257-4.292-3.856-4.292  c-3.058,0-5.515,3.164-5.515,7.401c0,2.699,0.912,4.525,0.912,4.525s-3.129,13.26-3.678,15.582  c-1.092,4.625-0.164,10.293-0.085,10.865c0.046,0.34,0.482,0.422,0.68,0.166c0.281-0.369,3.925-4.865,5.162-9.359  c0.351-1.271,2.011-7.859,2.011-7.859c0.994,1.896,3.898,3.562,6.986,3.562c9.191,0,15.428-8.379,15.428-19.595  C48.476,12.521,41.292,4.622,30.374,4.622z"/>'+
'</svg>';

var facebook = '<svg class="article__share-bar-item-image" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18px" height="18px" viewBox="0 0 275.3 276.1" style="enable-background:new 0 0 275.3 276.1;" xml:space="preserve"><path id="Blue_1_" fill="#333" d="M256.4,3.7H18.9c-8.1,0-14.7,6.6-14.7,14.7v237.4c0,8.1,6.6,14.7,14.7,14.7h127.8V167.3H112V127 h34.8V97.3c0-34.5,21.1-53.2,51.8-53.2c14.7,0,27.4,1.1,31.1,1.6v36l-21.3,0c-16.7,0-20,7.9-20,19.6V127h39.9l-5.2,40.3h-34.7v103.4 h68c8.1,0,14.7-6.6,14.7-14.7V18.5C271.1,10.3,264.5,3.7,256.4,3.7z"></path></svg>';

var twitter = '<svg class="article__share-bar-item-image" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="18px" viewBox="0 0 288 247.5" style="enable-background:new 0 0 288 247.5;" xml:space="preserve"><path fill="#333" d="M282.3,40.2c-10.1,4.5-20.9,7.5-32.2,8.8c11.6-6.9,20.5-17.9,24.7-31c-10.8,6.4-22.8,11.1-35.6,13.6 c-10.2-10.9-24.8-17.7-40.9-17.7c-31,0-56.1,25.1-56.1,56.1c0,4.4,0.5,8.7,1.5,12.8C96.9,80.5,55.6,58.1,27.9,24.2 c-4.8,8.3-7.6,17.9-7.6,28.2c0,19.5,9.9,36.6,25,46.7c-9.2-0.3-17.8-2.8-25.4-7c0,0.2,0,0.5,0,0.7c0,27.2,19.3,49.8,45,55 c-4.7,1.3-9.7,2-14.8,2c-3.6,0-7.1-0.4-10.6-1c7.1,22.3,27.9,38.5,52.4,39c-19.2,15-43.4,24-69.7,24c-4.5,0-9-0.3-13.4-0.8 c24.8,15.9,54.3,25.2,86,25.2c103.2,0,159.6-85.5,159.6-159.6c0-2.4-0.1-4.9-0.2-7.3C265.2,61.4,274.7,51.5,282.3,40.2z"></path></svg>';

var linkBack = "http://www.davidsbridal.com/Content_StyleandFashionGuide_menswearhousecolorcoordination";
var title = "Color Coordination | Men's Wearhouse | David's Bridal";
var description = "Men's Wearhouse tuxedos and accessories coordinate perfectly with David's Bridal bridesmaid dresses. Coordinate your party below.";
var buttonTemplate = '<li class="social-share__icon"><a onclick="$onClick" data-img="$iconImg">$svg</a></li>';
var shareButtons = [{
  provider: 'pinterest',
  iconImgUp: 'pinterest',
  svg: pinterest
}, {
  provider: 'facebook',
  iconImgUp: 'facebook',
  svg: facebook
}, {
  provider: 'twitter',
  iconImgUp: 'twitter',
  svg: twitter
}];


pastelsLarge = new gigya.services.socialize.UserAction();
pastelsLarge.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_pastelLGvert.jpg',
  href: linkBack
};
pastelsLarge.setTitle(title);
pastelsLarge.addMediaItem(image);
pastelsLarge.setDescription(description);
var shareBarParams = {
  userAction: pastelsLarge,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--pastels-large',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);


pastelsSmall = new gigya.services.socialize.UserAction();
pastelsSmall.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_pastelSMvert.jpg',
  href: linkBack
};
pastelsSmall.setTitle(title);
pastelsSmall.addMediaItem(image);
pastelsSmall.setDescription(description);
var shareBarParams = {
  userAction: pastelsSmall,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--pastels-small',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);


brightsLarge = new gigya.services.socialize.UserAction();
brightsLarge.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_brightsLGvert.jpg',
  href: linkBack
};
brightsLarge.setTitle(title);
brightsLarge.addMediaItem(image);
brightsLarge.setDescription(description);
var shareBarParams = {
  userAction: brightsLarge,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--brights-large',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);


brightsSmall = new gigya.services.socialize.UserAction();
brightsSmall.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_brightsSMvert.jpg',
  href: linkBack
};
brightsSmall.setTitle(title);
brightsSmall.addMediaItem(image);
brightsSmall.setDescription(description);
var shareBarParams = {
  userAction: brightsSmall,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--brights-small',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);


jewelsLarge = new gigya.services.socialize.UserAction();
jewelsLarge.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_jewelsLGvert.jpg',
  href: linkBack
};
jewelsLarge.setTitle(title);
jewelsLarge.addMediaItem(image);
jewelsLarge.setDescription(description);
var shareBarParams = {
  userAction: jewelsLarge,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--jewels-large',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);


jewelsSmall = new gigya.services.socialize.UserAction();
jewelsSmall.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_jewelsSMvert.jpg',
  href: linkBack
};
jewelsSmall.setTitle(title);
jewelsSmall.addMediaItem(image);
jewelsSmall.setDescription(description);
var shareBarParams = {
  userAction: jewelsSmall,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--jewels-small',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);


darkLarge = new gigya.services.socialize.UserAction();
darkLarge.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_neutralLGvert.jpg',
  href: linkBack
};
darkLarge.setTitle(title);
darkLarge.addMediaItem(image);
darkLarge.setDescription(description);
var shareBarParams = {
  userAction: darkLarge,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--dark-large',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);


darkSmall = new gigya.services.socialize.UserAction();
darkSmall.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/mens-wearhouse/MW_ColorCoord_neutralSMvert.jpg',
  href: linkBack
};
darkSmall.setTitle(title);
darkSmall.addMediaItem(image);
darkSmall.setDescription(description);
var shareBarParams = {
  userAction: darkSmall,
  shareButtons: shareButtons,
  buttonTemplate: buttonTemplate,
  containerID: 'social-share-icons--dark-small',
  showCounts: 'none',
  sessionExpiration: '0'
};
gigya.services.socialize.showShareBarUI(shareBarParams);
