jQuery(document).ready(function() {
  function fewFavoritesCarousel(element) {
    element.slick({
      autoplay: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      variableWidth: false,
      infinite: true,
      centerMode: false,
      dots: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    });
    element.on("click", function(event){
      event.preventDefault();
    });
  }

  fewFavoritesCarousel($('#topCarousel'));
  fewFavoritesCarousel($('#underTheDress'));

  $('#socialShareIcons').gigyaShareBar({
    debug: true,
    image: '/wcsstore/images/wwcm/sfg/versa-main/767_Hero_Versa_Hub.jpg'
  });
});
