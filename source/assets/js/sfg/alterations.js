jQuery(window).on('load', function() {
  $('.accordion__header').on('click', function(){
    if($(this).parent().attr('data-nav-open') == 'false') {
      $(this).parent().attr('data-nav-open', 'true');
      $(this).parent().find('.accordion__content').slideToggle('slow');
    } else {
      $(this).parent().attr('data-nav-open', 'false');
      $(this).parent().find('.accordion__content').slideToggle('slow');
    }
  });
  $('.accordion__item').on('keydown', function (e) {
    var which = e.which;
    var target = e.target;
    var focused = $(':focus').attr('class');
    if ( which === 13 && focused.indexOf('accordion__item') >= 0 ) {
      if ( $(this).find('.accordion__item').attr('data-nav-open') == 'false' ) {
        $(this).find('.accordion__item').attr('data-nav-open', 'true');
        $(this).find('.accordion__content').slideToggle('slow');
      } else {
        $(this).find('.accordion__item').attr('data-nav-open', 'false');
        $(this).find('.accordion__content').slideToggle('slow');
      }
    }
  });


  $('#socialShareIcons').gigyaShareBar({
    debug: true,
    image: '/wcsstore/images/wwcm/sfg/alterations/Alterations_Bustles.jpg'
  });
});
