$(document).ready(function(){
  var fewFavoritesCarousel = $('.few-favorites-carousel__slick');
  fewFavoritesCarousel.slick({
    autoplay: false,
    slidesToShow: 6,
    variableWidth: true,
    infinite: true,
    centerMode: false,
    dots: false
  });
  $('.few-favorites-carousel-button').on("click", function(event){
    event.preventDefault();
  });

  function resizeCarousel(a, b) {
    var slides = $('.few-favorites-carousel .slick-slide');
    if(a < 600) {
      slides.width((b / 2) - 10);
    } else if (a < 768) {
      slides.width((b / 3) - 10);
    } else if (a < 1024) {
      slides.width((b / 4) - 10);
    } else {
      slides.width((b / 6) - 10);
    }
  }

  var windowWidth = $(window).width();
  var carouselWidth = fewFavoritesCarousel.width();
  resizeCarousel(windowWidth, carouselWidth);

  $(window).resize(function(){
    var windowWidthResize = $(window).width();
    var carouselWidthResize = fewFavoritesCarousel.width();
    resizeCarousel(windowWidthResize, carouselWidthResize);
  });
});
