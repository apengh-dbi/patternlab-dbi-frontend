jQuery(document).ready(function() {

  function fewFavoritesCarousel(element) {
    var id = element.prop('id');
    var prev = '#' + id.replace('Carousel', '') + 'Prev';
    var next = '#' + id.replace('Carousel', '') + 'Next';
    element.slick({
      autoplay: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      variableWidth: false,
      infinite: true,
      centerMode: false,
      dots: false,
      prevArrow: prev,
      nextArrow: next,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
    });
    $(next).on("click", function(event){
      event.preventDefault();
    });
    $(prev).on("click", function(event){
      event.preventDefault();
    });
  }

  var carousels = $('.few-favorites-carousel__slick');
  $.each(carousels, function() {
    var id = '#' + $(this).prop('id');
    fewFavoritesCarousel( $(id) );
  });

  $('#socialShareIcons').gigyaShareBar({
    debug: true,
    image: '/wcsstore/images/wwcm/sfg/versa-new-styles/767_Hero_Versa_New.jpg'
  });

});
