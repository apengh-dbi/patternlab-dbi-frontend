// GIGYA
var linkBack = "http://www.davidsbridal.com/Content_StyleandFashionGuide_braguide";
var title = "Bra Guide | David's Bridal";
var description = "";

ua = new gigya.services.socialize.UserAction();
ua.setLinkBack(linkBack);
var image = {
  type: 'image',
  src: 'http://www.davidsbridal.com/wcsstore/images/wwcm/sfg/braguide/BraGuide-PlungeBra_614x462.gif',
  href: linkBack
};
ua.setTitle(title);
ua.addMediaItem(image);
ua.setDescription(description);

var shareBarParams = {
  userAction: ua,
  shareButtons: [{
      provider: 'pinterest',
      iconImgUp: 'pinterest',
      svg: '<svg class="article__share-bar-item-image" xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" viewBox="0 0 144 144"><path fill="#FFF" d="M144 72c0 39.8-32.2 72-72 72S0 111.8 0 72 32.2 0 72 0s72 32.2 72 72"></path><path fill="#BD081C" d="M71.9 5.4C35.1 5.4 5.3 35.2 5.3 72c0 28.2 17.5 52.3 42.3 62-.6-5.3-1.1-13.3.2-19.1 1.2-5.2 7.8-33.1 7.8-33.1s-2-4-2-9.9c0-9.3 5.4-16.2 12-16.2 5.7 0 8.4 4.3 8.4 9.4 0 5.7-3.6 14.3-5.5 22.2-1.6 6.6 3.3 12 9.9 12 11.8 0 20.9-12.5 20.9-30.5 0-15.9-11.5-27.1-27.8-27.1-18.9 0-30.1 14.2-30.1 28.9 0 5.7 2.2 11.9 5 15.2.5.7.6 1.2.5 1.9-.5 2.1-1.6 6.6-1.8 7.5-.3 1.2-1 1.5-2.2.9-8.3-3.9-13.5-16-13.5-25.8 0-21 15.3-40.3 44-40.3 23.1 0 41 16.5 41 38.4 0 22.9-14.5 41.4-34.5 41.4-6.7 0-13.1-3.5-15.3-7.6 0 0-3.3 12.7-4.1 15.8-1.5 5.8-5.6 13-8.3 17.5 6.2 1.9 12.8 3 19.7 3 36.8 0 66.6-29.8 66.6-66.6 0-36.7-29.8-66.5-66.6-66.5z"></path></svg>'
    }, {
      provider: 'facebook',
      iconImgUp: 'facebook',
      svg: '<svg class="article__share-bar-item-image" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18px" height="18px" viewBox="0 0 275.3 276.1" style="enable-background:new 0 0 275.3 276.1;" xml:space="preserve"><path id="Blue_1_" fill="#3B5999" d="M256.4,3.7H18.9c-8.1,0-14.7,6.6-14.7,14.7v237.4c0,8.1,6.6,14.7,14.7,14.7h127.8V167.3H112V127 h34.8V97.3c0-34.5,21.1-53.2,51.8-53.2c14.7,0,27.4,1.1,31.1,1.6v36l-21.3,0c-16.7,0-20,7.9-20,19.6V127h39.9l-5.2,40.3h-34.7v103.4 h68c8.1,0,14.7-6.6,14.7-14.7V18.5C271.1,10.3,264.5,3.7,256.4,3.7z"></path></svg>'
    }, {
      provider: 'twitter',
      iconImgUp: 'twitter',
      svg: '<svg class="article__share-bar-item-image" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="18px" viewBox="0 0 288 247.5" style="enable-background:new 0 0 288 247.5;" xml:space="preserve"><path fill="#55ACEE" d="M282.3,40.2c-10.1,4.5-20.9,7.5-32.2,8.8c11.6-6.9,20.5-17.9,24.7-31c-10.8,6.4-22.8,11.1-35.6,13.6 c-10.2-10.9-24.8-17.7-40.9-17.7c-31,0-56.1,25.1-56.1,56.1c0,4.4,0.5,8.7,1.5,12.8C96.9,80.5,55.6,58.1,27.9,24.2 c-4.8,8.3-7.6,17.9-7.6,28.2c0,19.5,9.9,36.6,25,46.7c-9.2-0.3-17.8-2.8-25.4-7c0,0.2,0,0.5,0,0.7c0,27.2,19.3,49.8,45,55 c-4.7,1.3-9.7,2-14.8,2c-3.6,0-7.1-0.4-10.6-1c7.1,22.3,27.9,38.5,52.4,39c-19.2,15-43.4,24-69.7,24c-4.5,0-9-0.3-13.4-0.8 c24.8,15.9,54.3,25.2,86,25.2c103.2,0,159.6-85.5,159.6-159.6c0-2.4-0.1-4.9-0.2-7.3C265.2,61.4,274.7,51.5,282.3,40.2z"></path></svg>'
    },
    {
      provider: 'email',
      iconImgUp: 'email',
      svg: '<svg class="article__share-bar-item-image" xmlns="http://www.w3.org/2000/svg" width="22px" height="18px" viewBox="-491 495 16.8 12" enable-background="new -491 495 16.8 12"><g fill="#333"><path d="m-482.5 503.9l-7.5-5.8v7.9c0 .6.6 1 .8 1h12.9c.3 0 .6-.1.8-.3.2-.2.4-.4.4-.7v-7.9l-7.4 5.8"></path><path d="m-475 496.5c0-.3-.1-.7-.3-.9-.2-.2-.4-.6-.7-.6h-13.1c-.3 0-.5.4-.7.6-.2.2-.3.5-.3.8l7.5 5.7 7.6-5.6"></path></g></svg>'
    }
  ],
  buttonTemplate: '<li class="article__share-bar-item"><a onclick="$onClick" data-img="$iconImg">$svg</a></li>',
  containerID: 'socialShareIcons',
  showCounts: 'none',
  sessionExpiration: '0'
};

gigya.services.socialize.showShareBarUI(shareBarParams);
