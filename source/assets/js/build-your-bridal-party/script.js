(function($) {
  $(window).on('load', function() {

    // ========================================================
    // VARIABLES
    // ========================================================
    var debug = true;
    var localStorageKey = 'buildYourBridalParty_v1.0';
    
    var $addBridesmaidButton = $('.add-bridesmaid__button');
    
    // Classes
    var productImage = '.product-image';
    var colorSelect = '.chosen.color';
    var sizeSelect = '.chosen.size';
    
  
  
    // --------------------------------------------------------
    // Custom logging functions
    // --------------------------------------------------------
    var log = {
      info: function(message) {
        if(debug) {console.log(message);}
      },
      error: function(message) {
        if(debug) {console.error(message);}
      }
    };
    log.info('BUILD YOUR BRIDAL PARTY');
    
    
    // ========================================================
    // HELPER FUNCTIONS
    // Used to get information from PDP
    // ========================================================
      // --------------------------------------------------------
      // Get color names and values from PDP
      // --------------------------------------------------------
      function getColorAndHexValues() {
        var colors = [];
        var name, hex;
        $.each( $('.detail__color-swatch-item'), function(index) {
          name = $(this).find('.detail__color-swatch-item-input').attr('swatch-value');
          hex = '#' + $(this).attr('data-color').toUpperCase();
          console.log(index + 1, name, hex);
          colors.push({name: name, hex: hex});
        });
        // sort by name
        var byName = colors.slice(0);
        byName.sort(function(a,b) {
          var x = a.name.toLowerCase();
          var y = b.name.toLowerCase();
          return x < y ? -1 : x > y ? 1 : 0;
        });
        console.log(byName);
        copy(byName); // copy to clipboard (Chrome)
      }
      
      // --------------------------------------------------------
      // Get color names from PDP
      // --------------------------------------------------------
      function getColorsNames() {
        var colorsNames = [];
        var name;
        $.each( $('.detail__color-swatch-item'), function(index) {
          name = $(this).find('.detail__color-swatch-item-input').attr('swatch-value');
          console.log(index + 1, name);
          colorsNames.push(name);
        });
        colorsNames = colorsNames.sort();
        console.log(colorsNames);
        copy(colorsNames); // copy to clipboard (Chrome)
      }
      
      // --------------------------------------------------------
      // Get hex values from PDP
      // --------------------------------------------------------
      function getHexValues() {
        var hexValues = [];
        var hex;
        $.each( $('.detail__color-swatch-item'), function(index) {
          hex = '#' + $(this).attr('data-color').toUpperCase();
          console.log(index + 1, hex);
          hexValues.push(hex);
        });
        hexValues = hexValues.sort();
        console.log(hexValues);
        copy(hexValues); // copy to clipboard (Chrome)
      }
    
  
    // ========================================================
    // Array.prototype.find() Polyfill
    // https://tc39.github.io/ecma262/#sec-array.prototype.find
    // ========================================================
    if (!Array.prototype.find) {
      Object.defineProperty(Array.prototype, 'find', {
        value: function(predicate) {
         // 1. Let O be ? ToObject(this value).
          if (this == null) {
            throw new TypeError('"this" is null or not defined');
          }
          var o = Object(this);
          // 2. Let len be ? ToLength(? Get(O, "length")).
          var len = o.length >>> 0;
          // 3. If IsCallable(predicate) is false, throw a TypeError exception.
          if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
          }
          // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
          var thisArg = arguments[1];
          // 5. Let k be 0.
          var k = 0;
          // 6. Repeat, while k < len
          while (k < len) {
            // a. Let Pk be ! ToString(k).
            // b. Let kValue be ? Get(O, Pk).
            // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
            // d. If testResult is true, return kValue.
            var kValue = o[k];
            if (predicate.call(thisArg, kValue, k, o)) {
              return kValue;
            }
            // e. Increase k by 1.
            k++;
          }
          // 7. Return undefined.
          return undefined;
        },
        configurable: true,
        writable: true
      });
    }
    
    
    // ========================================================
    // Array.prototype.includes() Polyfill
    // https://tc39.github.io/ecma262/#sec-array.prototype.includes
    // ========================================================
    if (!Array.prototype.includes) {
      Object.defineProperty(Array.prototype, 'includes', {
        value: function(searchElement, fromIndex) {
          if (this == null) {
            throw new TypeError('"this" is null or not defined');
          }
          // 1. Let O be ? ToObject(this value).
          var o = Object(this);
          // 2. Let len be ? ToLength(? Get(O, "length")).
          var len = o.length >>> 0;
          // 3. If len is 0, return false.
          if (len === 0) {
            return false;
          }
          // 4. Let n be ? ToInteger(fromIndex).
          //    (If fromIndex is undefined, this step produces the value 0.)
          var n = fromIndex | 0;
          // 5. If n ≥ 0, then
          //  a. Let k be n.
          // 6. Else n < 0,
          //  a. Let k be len + n.
          //  b. If k < 0, let k be 0.
          var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
          function sameValueZero(x, y) {
            return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
          }
          // 7. Repeat, while k < len
          while (k < len) {
            // a. Let elementK be the result of ? Get(O, ! ToString(k)).
            // b. If SameValueZero(searchElement, elementK) is true, return true.
            if (sameValueZero(o[k], searchElement)) {
              return true;
            }
            // c. Increase k by 1.
            k++;
          }
          // 8. Return false
          return false;
        }
      });
    }
    
    
    // ========================================================
    // Cookie Functions
    // ========================================================
    function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    
    
    function getCookie(cname) {
      var name = cname + "="; //Create the cookie name variable with cookie name concatenate with = sign
      var cArr = window.document.cookie.split(';'); //Create cookie array by split the cookie by ';'
      
      //Loop through the cookies and return the cooki value if it find the cookie name
      for(var i=0; i<cArr.length; i++) {
        var c = cArr[i].trim();
        //If the name is the cookie string at position 0, we found the cookie and return the cookie value
        if (c.indexOf(name) == 0)
        return c.substring(name.length, c.length);
      }
      //If we get to this point, that means the cookie wasn't find in the look, we return an empty string.
      return "";
    }
    
    
    // ========================================================
    // Set Cookie for User Selections (Color/Size) for PDP
    // ========================================================
    function linkToPDP(added) {
      var productId = added.attr('data-product-id');
      var product = getProductById(productId);
      var color = getSelectedColor(added) || '';
      var size = getSelectedSize(added) || '';
      
      var cookieValue = color + '$$' + size;
      setCookie('PLP_USER_SELECTION', cookieValue);
      getCookie('PLP_USER_SELECTION');
      var path = product[0].url;
      window.open(window.location.origin + path);
    }
    


    // ========================================================
    // Get Product by ID - from products array
    // ========================================================
    function getProductById(pid) {
      return products.filter(function(object) {
        return object.productId === parseInt(pid);
      });
    }
    
    
    
    // ========================================================
    // Get Products By Attribute
    // ========================================================
    function getProductsByAttribute(attribute, value) {
      var results = products.filter(function(obj) {
        return obj[attribute] === value;
      });
      return results;
    }
    
    
    
    // ========================================================
    // Update data attributes
    // ========================================================
    function updateDataAttributes(added, productId, style) {
      added.attr('data-product-id', productId).attr('data-style-number', style);
    }
    
    
    
    // ========================================================
    // LENGTH FUNCTIONS
    // ========================================================
    function generateLengths(added) {
      for(var i = 0; i < config.lengths.length; i++) {
        var length = config.lengths[i].length;
        
        // Check if length exists in products array
        var products = getProductsByAttribute('length', length);
        if(products.length !== 0) {
          var svg = config.lengths[i].svg;
          var html = '<div class="length">\
          <input type="radio" name="length' + counter + '" id="' + length.toLowerCase() + counter + '" value="' + length + '">\
          <label for="' + length.toLowerCase() + counter + '">\
          ' + svg + '\
          <span class="sans-bold-uppercase">'+ length + '</span>\
          </label>\
          </div>';
          added.find('.lengths').append(html);
        }
      }
    }
    
    
    function getSelectedLength(added) {
      return added.find('.lengths').find('input:checked').val();
    }
    
    
    function setSelectedLength(added, value) {
      added.find('input[value="'+value+'"]').click();
    }
    
    
    
    
    // ========================================================
    // NECKLINE FUNCTIONS
    // ========================================================
    function getSelectedNeckline(added) {
      return added.find('.necklines').find('input:checked').val();
    }
    
    
    function setSelectedNeckline(added, value) {
      added.find('input[value="'+value+'"]').click();
    }
    
    
    function generateNecklines(added) {
      for(var i = 0; i < config.necklines.length; i++) {
        var neckline = config.necklines[i].neckline;
        
        // Check if neckline exists in products array
        var products = getProductsByAttribute('neckline', neckline);
        if(products.length !== 0) {
          var svg = config.necklines[i].svg;
          var html = '<div class="neckline">\
          <input type="radio" name="neckline' + counter + '" id="' + neckline.toLowerCase() + counter + '" value="' + neckline + '">\
          <label for="' + neckline.toLowerCase() + counter + '">\
          ' + svg + '\
          <span class="sans-bold-uppercase">'+ neckline + '</span>\
          </label>\
          </div>';
          added.find('.necklines').append(html);
        }
      }
    }
    
    
    function updateAvailableNecklines(added) {
      var selectedLength = getSelectedLength(added);
      var availableNecklines = getProductsByAttribute('length', selectedLength);
      var necklineValues = [];
      availableNecklines.forEach(function(element) {
        necklineValues.push(element.neckline);
      });
      
      var necklines = added.find('.neckline');
      necklines.each(function() {
        var neckline = $(this).find('input').val();
        if( !necklineValues.includes(neckline) ) {
          $(this).find('input').attr('disabled', 'disabled').attr('checked', false);
        } else {
          $(this).find('input').attr('disabled', false).attr('checked', false);
        }
      });
      
      return added.find('.necklines').find('input:enabled');
    }
    
    
    
    
    // ========================================================
    // COLOR FUNCTIONS
    // ========================================================
    function getSelectedColor(added) {
      return added.find(colorSelect).val();
    }
    
    
    function setSelectedColor(added, color) {
      added.find(colorSelect).val(color).trigger('chosen:updated');
    }
    
    
    function setColorSwatch(added) {
      var selectedColor = getSelectedColor(added);
      var swatch = added.find(colorSelect).next('.chosen-container').find('a.chosen-single').find('.swatch');
      var color = config.colors.find(function(color) {
        return color.name === selectedColor;
      });
      if(swatch.length === 0) {
        added.find(colorSelect).next('.chosen-container').find('a.chosen-single').prepend('<p class="swatch color-swatch" style="background-color:'+color.hex+'"></p>');
      } else {
        added.find(colorSelect).next('.chosen-container').find('a.chosen-single').find('.swatch').css('background-color', color.hex);
      }
    }
    
    
    function generateColorsDropdown(added) {
      // Generate select > option elements
      for(var i = 0; i < config.colors.length; i++) {
        added.find(colorSelect).append('<option data-hex="'+config.colors[i].hex+'" value="' + config.colors[i].name + '">' + config.colors[i].name + '</option>');
      }
      
      // Initialize chosen dropdown
      added.find(colorSelect).chosen({
        disable_search: true,
        placeholder_text_single: 'Select a color'
      });
      
      // On open, add color swatches
      added.find(colorSelect).on('chosen:showing_dropdown', function(evt, params) {
        var options = $(this).find('option');
        $.each(options, function(index) {
          added.find(colorSelect).next('.chosen-container').find('[data-option-array-index='+index+']').prepend('<span class="color-swatch" style="background-color:'+$(this).data('hex')+'"></span>');
        });
      });
      
      // On close, set selected color swatch
      added.find(colorSelect).on('chosen:hiding_dropdown', function() {
        setColorSwatch(added);
      });
    }
    
    
    function updateAvailableColors(added, bridalPartyAdded, product) {
      // Get selected color
      var selectedColor = getSelectedColor(added);
      
      // Create array of all product's color names
      var productColors = [];
      product.colors.forEach(function(color) {
        productColors.push(color);
      });
      
      // Get all colors
      var allColors = config.colors;
      
      // Set each color to enabled or disabled
      allColors.forEach(function(color) {
        if(productColors.indexOf(color.name) === -1) {
          added.find(colorSelect).find('option:contains('+color.name+')').attr('disabled', true);
        } else {
          added.find(colorSelect).find('option:contains('+color.name+')').attr('disabled', false);
        }
      });
      
      // If currently selected color is disabled, set to first available color
      if( added.find(colorSelect).find('option:contains('+selectedColor+')').is(':disabled') ) {
        var firstAvailableColor = added.find(colorSelect).find('option:enabled').eq(0).val();
        added.find(colorSelect).find('option:enabled').eq(0).attr('selected', 'selected');
        setColorSwatch(added);
        var url = config.sceneSevenURL + added.attr('data-style-number') + '-' + added.attr('data-product-id') + '-' + firstAvailableColor + config.sceneSevenParams;
        setProductImage(added, firstAvailableColor)
        setBridalPartyImage(bridalPartyAdded, url)
      }
      
      // Trigger color dropdown update
      added.find(colorSelect).trigger('chosen:updated');
    }
    
    
    
    
    // ========================================================
    // SIZE FUNCTIONS
    // ========================================================
    function getSelectedSize(added) {
      return added.find(sizeSelect).val();
    }
    
    
    function setSelectedSize(added, size) {
      added.find(sizeSelect).val(size).trigger('chosen:updated');
    }
    
    
    function setSizeSwatch(added) {
      var selectedSize = getSelectedSize(added);
      var swatch = added.find(sizeSelect).next('.chosen-container').find('a.chosen-single').find('.swatch');
      if(swatch.length === 0) {
        added.find(sizeSelect).next('.chosen-container').find('a.chosen-single').prepend('<p class="swatch size-swatch">'+selectedSize+'</p>');
      } else {
        added.find(sizeSelect).next('.chosen-container').find('a.chosen-single').find('.size-swatch').text(selectedSize);
      }
    }
    
    
    function generateSizesDropdown(added) {
      for(var i = 0; i < config.sizes.length; i++) {
        added.find(sizeSelect).append('<option data-size="'+config.sizes[i]+'" value="'+config.sizes[i]+'">Size '+config.sizes[i]+'</option>');
      }
      added.find(sizeSelect).chosen({
        disable_search: true,
        placeholder_text_single: 'Select a size'
      });
      added.find(sizeSelect).on('chosen:showing_dropdown', function(evt, params) {
        var options = $(this).find('option');
        $.each(options, function(index) {
          added.find(sizeSelect).next('.chosen-container').find('[data-option-array-index='+index+']').prepend('<span class="size-swatch">'+$(this).data('size')+'</span>');
        });
      });
      added.find(colorSelect).on('chosen:hiding_dropdown', function() {
        setSizeSwatch(added);
      });
    }
    
    
    function updateAvailableSizes(added, bridalPartyAdded, product) {
      // Get selected color
      var selectedSize = getSelectedSize(added);
      
      // Create array of all product's sizes
      var productSizes = [];
      product.sizes.forEach(function(size) {
        productSizes.push(size);
      });
      
      var allSizes = config.sizes;
      allSizes.forEach(function(size) {
        if(productSizes.indexOf(size) === -1) {
          added.find(sizeSelect).find('option:contains(Size '+size+')').attr('disabled', true);
        } else {
          added.find(sizeSelect).find('option:contains(Size '+size+')').attr('disabled', false);
        }
      });
      
      if( added.find(sizeSelect).find('option:contains(Size '+selectedSize+')').is(':disabled') ) {
        var firstAvailableSize = added.find(sizeSelect).find('option:enabled').eq(0).val();
        added.find(sizeSelect).find('option:enabled').eq(0).attr('selected', 'selected');
      }
      added.find(sizeSelect).trigger('chosen:updated');
    }
    
    
    
    
    // ========================================================
    // PRODUCT IMAGE FUNCTIONS
    // ========================================================
    function setProductImage(added, color) {
      var urlPrefix = 'https://img.davidsbridal.com/is/image/DavidsBridalInc/';
      var urlParams = '?wid=407&hei=562&bgc=255,255,255&defaultImage=DavidsBridalInc/PDP_No_Image_Available_Message&id=aNab01&fmt=jpg&fit=constrain,1&wid=621&hei=932&resmode=sharp2&op_usm=2.5,0.3,4';
      
      var $primaryImage = added.find('.primary-image > img');
      var $altImage = added.find('.alt-image > img');
      
      var productId = added.attr('data-product-id');
      var styleNum = added.attr('data-style-number');
      var color = color || getSelectedColor(added);
      
      // https://img.davidsbridal.com/is/image/DavidsBridalInc/Set-VW360214-10277801-Amethyst?req=set,json,UTF-8
      var mixedMediaSetUrl = urlPrefix + '/Set-' + styleNum + '-' + productId + '-' + color + '?req=set,json,UTF-8';
      var primaryImageSrc = urlPrefix + '/Set-' + styleNum + '-' + productId + '-' + color + urlParams;
      
      $.ajax({
        url: mixedMediaSetUrl,
        method: 'GET',
        dataType: 'text'
      }).done(function(response) {
        // Good response
        if( response.indexOf('s7jsonResponse') !== -1 ) {
          var response = response.replace('/*jsonp*/s7jsonResponse(', '');
          var response = response.replace(',"");', '');
          var json = JSON.stringify(eval("(" + response + ")"));
          var obj = JSON.parse(json);
          if (obj.set.item.length != undefined) {
            var altImageSrc = obj.set.item[1].i.n;
            altImageSrc = altImageSrc.replace('DavidsBridalInc/', '');
            altImageSrc = urlPrefix + altImageSrc + urlParams;
            $primaryImage.attr('src', primaryImageSrc);
            $altImage.attr('src', altImageSrc);
          }
        // Error response
        } else if ( response.indexOf('s7jsonError') !== -1 ) {
          var response = response.replace('/*jsonp*/s7jsonError(', '');
          var response = response.replace(',"");', '');
          var json = JSON.stringify(eval("(" + response + ")"));
          var obj = JSON.parse(json);
          log.info('s7jsonError : ' + obj.message);
        }
      }).error(function(response) {
        log.error('AJAX ERROR | Status: ' + response.status + ' | ' + 'Message: ' + response.responseText);
      });
      
      return primaryImageSrc;
    }
    
    
    function setProductImageLink(added) {
      var productId = added.attr('data-product-id');
      var product = getProductById(productId);
      added.find('.product-image__link').attr('href', product[0].url);
    }
    
    
    function setBridalPartyImage(bridalPartyAdded, imageUrl) {
      bridalPartyAdded.find('.image > img').attr('src', imageUrl);
    }
    
    
    
    
    // ========================================================
    // SELECTIONS FUNCTIONS
    // ========================================================
    function setSelections(added, brand, title, style) {
      var element = added.find('.selections')
      element.find('.brand').text(brand);
      element.find('.title').text(title);
      element.find('.style').text(style);
    }
    
    
    
    
    // ========================================================
    // LOCALSTORAGE FUNCTIONS
    // ========================================================
    function getLocalStorage(key) {
      if(localStorage) {
        return localStorage.getItem(localStorageKey);
      }
    }
    
    function setLocalStorage() {
      var data = [];
      // loop through bridesmaids
      var bridesmaids = $('main').find('section.bridesmaid');
      $.each(bridesmaids, function() {
        var name = $(this).find('input[name="name"]').val();
        var email = $(this).find('input[name="email"]').val();
        var product = $(this).attr('data-product-id');
        var color = getSelectedColor($(this));
        var size =  getSelectedSize($(this));
        var user = {
          name: name,
          email: email,
          product: product,
          color: color,
          size: size
        };
        data.push(user);
      });
      if(localStorage) {
        localStorage.setItem(localStorageKey, JSON.stringify(data));
      }
    }
    
    
    
    // ========================================================
    // ADD BRIDAL PARTY
    // ========================================================
    function addBridalParty(added, productDetail, imageUrl) {
      var bridalPartyTemplate = $('.bridal-party-template').html();
      $('.bridal-party__members').append(bridalPartyTemplate);
      var bridalPartyAdded = $('.bridal-party__members').find('.column').last();
      bridalPartyAdded.find('.name > p').html('&nbsp;');
      bridalPartyAdded.find('.image > img').attr('src', imageUrl);
      bridalPartyAdded.find('.brand').text(productDetail.brand);
      bridalPartyAdded.find('.title').text(productDetail.title);
      bridalPartyAdded.find('.style').text(productDetail.style);
      bridalPartyAdded.find('a').attr('href', productDetail.url);
      bridalPartyAdded.find('.color').text( getSelectedColor(added) );
      bridalPartyAdded.find('.size').text( getSelectedSize(added) );
      return bridalPartyAdded;
    }
    
    
    function updateBridalParty(added, bridalPartyAdded, product) {
      bridalPartyAdded.find('.brand').text(product.brand);
      bridalPartyAdded.find('.title').text(product.title);
      bridalPartyAdded.find('.style').text(product.style);
      bridalPartyAdded.find('a').attr('href', product.url);
      bridalPartyAdded.find('.color').text( getSelectedColor(added) );
      bridalPartyAdded.find('.size').text( getSelectedSize(added) );
    }
  
    
    // ========================================================
    // EVENT LISTENERS
    // ========================================================
    function addEventListeners(added, bridalPartyAdded) {
      
      // --------------------------------------------------------
      // Remove bridesmaid on close click
      // --------------------------------------------------------
      added.find('.bridesmaid__header-close').on('click', function(event) {
        event.preventDefault();
        var element = $(this).closest('.bridesmaid');
        var bridesmaids = $('main').find('.bridesmaid');
        var index = bridesmaids.index(element);
        $('.bridal-party__members').find('.column').eq(index).remove();
        element.remove();
        setLocalStorage();
      });
      
      // --------------------------------------------------------
      // Inputs
      // --------------------------------------------------------
      var formGroups = added.find('.form-group')
      $.each(formGroups, function(index) {
        var $label = $(this).find('label');
        var $input = $(this).find('input');
        var $span = $(this).find('span');
        
        $label.on('click', function() {
          $span.addClass('hidden');
          $input.removeClass('hidden').focus();
        });
        $span.on('click', function() {
          $span.addClass('hidden');
          $input.removeClass('hidden').focus();
        });
        $input.on('blur', function() {
          if( $(this).val() !== '' ) {
            $(this).addClass('hidden');
            $span.removeClass('hidden');
            setLocalStorage();
          }
        });
        $input.on('keydown', function(e) {
          if(e.which === 13) {
            $(this).addClass('hidden');
            $span.removeClass('hidden');
            setLocalStorage();
          }
        });
        $input.on('input', function() {
          var text = $(this).val();
          $span.text(text);
          if( $(this).attr('name') === 'name' ) {
            bridalPartyAdded.find('.name > p').html(text);
          }
          setLocalStorage();
        });
      });
      
      
      // --------------------------------------------------------
      // Length click
      // --------------------------------------------------------
      var $lengths = added.find('.lengths').find('input');
      $lengths.on('click', function() {
        var selectedNeckline = getSelectedNeckline(added);
        var length = $(this).val();
        
        // Get available necklines
        var necklines = getProductsByAttribute('length', length);
        
        // Update Necklines
        updateAvailableNecklines(added);
        if( added.find('input[value="'+selectedNeckline+'"]').is(':enabled') === true ) {
          added.find('input[value="'+selectedNeckline+'"]').click();
        } else {
          added.find('input[name*="neckline"]:enabled').eq(0).click();
        }
        
        setLocalStorage();
      });
      
      
      
      // --------------------------------------------------------
      // Neckline click
      // --------------------------------------------------------
      var $necklines = added.find('.necklines').find('input');
      $necklines.on('click', function() {
        
        var neckline = $(this).val();
        var length = getSelectedLength(added);
        var color = getSelectedColor(added);
        
        // Find product with neckline and length
        var result = products.filter(function(obj) {
          return obj.neckline === neckline && obj.length === length;
        });
        
        if(result) {
          var product = result[0];
          var productId = product.productId;
          var style = product.style;
          
          // Update data-style-number
          updateDataAttributes(added, productId, style);
          
          // Update product image
          var imageUrl = setProductImage(added, color);
          setProductImageLink(added);
          
          // Update available colors
          updateAvailableColors(added, bridalPartyAdded, product);
          
          // Update available sizes
          updateAvailableSizes(added, bridalPartyAdded, product);
          setSizeSwatch(added);
          
          // Update selections
          setSelections(added, product.brand, product.title, style);
          
          // Update bridal party details
          setBridalPartyImage(bridalPartyAdded, imageUrl)
          updateBridalParty(added, bridalPartyAdded, product);
        }
        
        setLocalStorage();
      });
      
      
      // --------------------------------------------------------
      // Color Change - Update Product Images
      // --------------------------------------------------------
      added.find(colorSelect).on('change', function() {
        var color = $(this).val();
        // Main image
        var imageUrl = setProductImage(added, color);
        // Bridal Party
        setBridalPartyImage(bridalPartyAdded, imageUrl)
        setLocalStorage();
      });
      
      
      // --------------------------------------------------------
      // Size Change
      // --------------------------------------------------------
      added.find(sizeSelect).on('change', function() {
        var size = $(this).val();
        setSizeSwatch(added);
        setLocalStorage();
      });
      
      
      // --------------------------------------------------------
      // Main Product Image Click
      // --------------------------------------------------------
      added.find('.product-image__link').on('click', function(event) {
        event.preventDefault();
        linkToPDP(added);
      });
      
      
      // --------------------------------------------------------
      // Shop This Style Button Click
      // --------------------------------------------------------
      added.find('button.shop-this-style').on('click', function(event) {
        event.preventDefault();
        linkToPDP(added);
      });
      
      // --------------------------------------------------------
      // Bridesmaid Image Click
      // --------------------------------------------------------
      bridalPartyAdded.find('a').on('click', function(event) {
        event.preventDefault();
        linkToPDP(added);
      });
      
    } // addEventListeners
    
    
    
    
    var counter = 1; // Needed to append to form radio button IDs
    // ========================================================
    // ADD BRIDESMAID(S)
    // ========================================================
    function addBridesmaid() {
      
      // Insert HTML before Add Bridesmaid Button
      var bridesmaidTemplate = $('.bridesmaid-module-template').html();
      $addBridesmaidButton.before(bridesmaidTemplate);
      
      // Get last bridesmaid added
      var added = $('.bridesmaid').last();
      
      // Add Options
      generateLengths(added);
      generateNecklines(added);
      generateColorsDropdown(added);
      setColorSwatch(added);
      generateSizesDropdown(added);
      setSizeSwatch(added);
      
      // Get first product in products array
      var productDetail = products[0];
      
      // Set data-product-id and data-style-number
      updateDataAttributes(added, productDetail.productId, productDetail.style);
      
      // Set length
      setSelectedLength(added, productDetail.length);
      
      // Set neckline
      updateAvailableNecklines(added);
      setSelectedNeckline(added, productDetail.neckline);
      
      // Set product image
      var imageUrl = setProductImage(added);
      setProductImageLink(added);
      
      // Set Selections
      setSelections(added, productDetail.brand, productDetail.title, productDetail.style);
      
      // Add to Bridal Party
      var bridalPartyAdded = addBridalParty(added, productDetail, imageUrl);
      
      // Attach Event Listeners
      addEventListeners(added, bridalPartyAdded);
      
      // Increment Counter
      counter++;
      
      setLocalStorage();
    } // addBridesmaid
    
    
    
    
    // ========================================================
    // ADD BRIDESMAID(S) FROM LOCALSTORAGE
    // ========================================================
    function addBridesmaidFromStorage(user) {
      
      log.info(user);
      
      // Insert HTML before Add Bridesmaid Button
      var bridesmaidTemplate = $('.bridesmaid-module-template').html();
      $addBridesmaidButton.before(bridesmaidTemplate);
      
      // Get last bridesmaid added
      var added = $('.bridesmaid').last();
      
      // Add Options
      generateLengths(added);
      generateNecklines(added);
      generateColorsDropdown(added);
      generateSizesDropdown(added);
      
      // Set Data
      var name = user.name
      var email = user.email;
      var product = user.product;
      var color = user.color;
      var size = user.size;
      
      var productDetail = getProductById(product)[0];
      
      // Set data-product-id and data-style-number
      updateDataAttributes(added, productDetail.productId, productDetail.style);
      
      // Set name
      if(name !== '') {
        var $name = added.find('input[name="name"]');
        $name.val(user.name).addClass('hidden');
        $name.next('span').text(user.name).removeClass('hidden');
      }
      
      // Set email
      if(email !== '') {
        var $email = added.find('input[name="email"]');
        $email.val(user.email).addClass('hidden');
        $email.next('span').text(user.email).removeClass('hidden');
      }
      
      // Set length
      setSelectedLength(added, productDetail.length);
      
      // Set neckline
      updateAvailableNecklines(added);
      setSelectedNeckline(added, productDetail.neckline);
      
      // Set color
      setSelectedColor(added, user.color);
      setColorSwatch(added);
      
      // Set size
      setSelectedSize(added, user.size);
      setSizeSwatch(added);
      
      // Set product image
      var imageUrl = setProductImage(added, user.color);
      setProductImageLink(added);
      
      // Set Selections
      setSelections(added, productDetail.brand, productDetail.title, productDetail.style);
      
      // Add to Bridal Party
      var bridalPartyAdded = addBridalParty(added, productDetail, imageUrl);
      bridalPartyAdded.find('.name > p').html(name);
      
      // Attach Event Listeners
      addEventListeners(added, bridalPartyAdded);
      
      // Increment Counter
      counter++;
      
    } // addBridesmaidFromStorage
    
    
    
    // ========================================================
    // Add bridesmaid on add bridesmaid button click
    // ========================================================
    $('.add-bridesmaid__button').on('click', function(event) {
      event.preventDefault();
      var element = $(this);
      addBridesmaid(element);
    });
    
    
    
    // ========================================================
    // If preferences have been saved in localStorage
    //   load from localStorage and add bridesmaids
    // else
    //   load initial bridesmaid
    // ========================================================
    if(localStorage && ( getLocalStorage(localStorageKey) !== null )) {
      var data = getLocalStorage(localStorageKey);
      data = JSON.parse(data);
      log.info(data);
      data.forEach(function(user) {
        addBridesmaidFromStorage(user);
      });
      
    } else {
      addBridesmaid();
    }
    
    
    
    // ========================================================
    // GIGYA SHARE BAR
    // ========================================================
    var gigyaObject = {
      linkBack: window.location.href,
      title: $('title').eq(0).text(),
      description: $('meta[name="description"]').attr('content'),
      icons: {
        "pinterest": {
          provider: 'pinterest',
          iconImgUp: 'pinterest',
          svg: '<svg class="article__share-bar-item-image" xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" viewBox="0 0 144 144"><path fill="#FFF" d="M144 72c0 39.8-32.2 72-72 72S0 111.8 0 72 32.2 0 72 0s72 32.2 72 72"></path><path fill="#BD081C" d="M71.9 5.4C35.1 5.4 5.3 35.2 5.3 72c0 28.2 17.5 52.3 42.3 62-.6-5.3-1.1-13.3.2-19.1 1.2-5.2 7.8-33.1 7.8-33.1s-2-4-2-9.9c0-9.3 5.4-16.2 12-16.2 5.7 0 8.4 4.3 8.4 9.4 0 5.7-3.6 14.3-5.5 22.2-1.6 6.6 3.3 12 9.9 12 11.8 0 20.9-12.5 20.9-30.5 0-15.9-11.5-27.1-27.8-27.1-18.9 0-30.1 14.2-30.1 28.9 0 5.7 2.2 11.9 5 15.2.5.7.6 1.2.5 1.9-.5 2.1-1.6 6.6-1.8 7.5-.3 1.2-1 1.5-2.2.9-8.3-3.9-13.5-16-13.5-25.8 0-21 15.3-40.3 44-40.3 23.1 0 41 16.5 41 38.4 0 22.9-14.5 41.4-34.5 41.4-6.7 0-13.1-3.5-15.3-7.6 0 0-3.3 12.7-4.1 15.8-1.5 5.8-5.6 13-8.3 17.5 6.2 1.9 12.8 3 19.7 3 36.8 0 66.6-29.8 66.6-66.6 0-36.7-29.8-66.5-66.6-66.5z"></path></svg>'
        },
        "facebook": {
          provider: 'facebook',
          iconImgUp: 'facebook',
          svg: '<svg class="article__share-bar-item-image" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="18px" height="18px" viewBox="0 0 275.3 276.1" style="enable-background:new 0 0 275.3 276.1;" xml:space="preserve"><path id="Blue_1_" fill="#3B5999" d="M256.4,3.7H18.9c-8.1,0-14.7,6.6-14.7,14.7v237.4c0,8.1,6.6,14.7,14.7,14.7h127.8V167.3H112V127 h34.8V97.3c0-34.5,21.1-53.2,51.8-53.2c14.7,0,27.4,1.1,31.1,1.6v36l-21.3,0c-16.7,0-20,7.9-20,19.6V127h39.9l-5.2,40.3h-34.7v103.4 h68c8.1,0,14.7-6.6,14.7-14.7V18.5C271.1,10.3,264.5,3.7,256.4,3.7z"></path></svg>'
        },
        "email": {
          provider: 'email',
          iconImgUp: 'email',
          svg: '<svg class="article__share-bar-item-image" xmlns="http://www.w3.org/2000/svg" width="22px" height="18px" viewBox="-491 495 16.8 12" enable-background="new -491 495 16.8 12"><g fill="#333"><path d="m-482.5 503.9l-7.5-5.8v7.9c0 .6.6 1 .8 1h12.9c.3 0 .6-.1.8-.3.2-.2.4-.4.4-.7v-7.9l-7.4 5.8"></path><path d="m-475 496.5c0-.3-.1-.7-.3-.9-.2-.2-.4-.6-.7-.6h-13.1c-.3 0-.5.4-.7.6-.2.2-.3.5-.3.8l7.5 5.7 7.6-5.6"></path></g></svg>'
        },
        "twitter": {
          provider: 'twitter',
          iconImgUp: 'twitter',
          svg: '<svg class="article__share-bar-item-image" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="21px" height="18px" viewBox="0 0 288 247.5" style="enable-background:new 0 0 288 247.5;" xml:space="preserve"><path fill="#55ACEE" d="M282.3,40.2c-10.1,4.5-20.9,7.5-32.2,8.8c11.6-6.9,20.5-17.9,24.7-31c-10.8,6.4-22.8,11.1-35.6,13.6 c-10.2-10.9-24.8-17.7-40.9-17.7c-31,0-56.1,25.1-56.1,56.1c0,4.4,0.5,8.7,1.5,12.8C96.9,80.5,55.6,58.1,27.9,24.2 c-4.8,8.3-7.6,17.9-7.6,28.2c0,19.5,9.9,36.6,25,46.7c-9.2-0.3-17.8-2.8-25.4-7c0,0.2,0,0.5,0,0.7c0,27.2,19.3,49.8,45,55 c-4.7,1.3-9.7,2-14.8,2c-3.6,0-7.1-0.4-10.6-1c7.1,22.3,27.9,38.5,52.4,39c-19.2,15-43.4,24-69.7,24c-4.5,0-9-0.3-13.4-0.8 c24.8,15.9,54.3,25.2,86,25.2c103.2,0,159.6-85.5,159.6-159.6c0-2.4-0.1-4.9-0.2-7.3C265.2,61.4,274.7,51.5,282.3,40.2z"></path></svg>'
        }
      }
    };


    // --------------------------------------------------------
    // Article Share Bar (Top)
    // --------------------------------------------------------
    var socialShare = new gigya.socialize.UserAction();
    socialShare.setLinkBack(gigyaObject.linkBack);
    var image = {
      type: 'image',
      src: 'https://www.davidsbridal.com/wcsstore/images/wwcm/sfg/neckline-jewelry/Hero_Neckline_Jewelry_Guide_767.jpg',
      href: gigyaObject.linkBack
    };
    socialShare.setTitle(gigyaObject.title);
    socialShare.addMediaItem(image);
    socialShare.setDescription(gigyaObject.description);
    var shareBarParams = {
      userAction: socialShare,
      shareButtons: [
        gigyaObject.icons.pinterest,
        gigyaObject.icons.facebook,
        gigyaObject.icons.email,
        gigyaObject.icons.twitter
      ],
      buttonTemplate: '<li class="article__share-bar-item"><a onclick="$onClick" data-img="$iconImg">$svg</a></li>',
      containerID: 'socialShareIcons',
      showCounts: 'none',
      sessionExpiration: '0'
    };
    gigya.socialize.showShareBarUI(shareBarParams);
    
    
    
    // --------------------------------------------------------
    // Share with your party: (bottom)
    // --------------------------------------------------------
    function getBridesmaidInfo() {
      var bridesmaids = $('main').find('.bridesmaid');
      $.each(bridesmaids, function() {
        var productId = $(this).attr('data-product-id');
        var style = $(this).attr('data-style-number');
        var name = $(this).find('input[name="name"]').val();
        var email = $(this).find('input[name="email"]').val();
        var image = $(this).find('.primary-image > img').attr('src');
        
        var product = getProductById(productId)[0];
        var length = product["length"];
        var neckline = product["neckline"];
        var url = 'https://www.davidsbridal.com' + product["url"];
        
        var data = {
          productId: productId,
          style: style,
          name: name,
          email: email,
          image: image,
          length: length,
          url: url
        };
        console.log(data);
      });
    }
    
    function showShareUI(operationMode) {
      getBridesmaidInfo();
      // Constructing a UserAction Object
      var emailShare = new gigya.socialize.UserAction();
      // Setting the title and description
      // (will be presented in the preview on the Share UI)
      emailShare.setTitle('Build Your Lace/Mesh Party!');
      emailShare.setDescription("Pick a dress for each of your bridesmaids, see how the lineup looks, and easily email everyone the style details");
      // Setting a link back to the publishing source
      emailShare.setLinkBack(window.location.href);
      // Adding an image (will be presented in the preview on the Share UI)
      var image = {
        type: 'image',
        src: 'https://www.davidsbridal.com/wcsstore/images/wwcm/sfg/neckline-jewelry/Hero_Neckline_Jewelry_Guide_767.jpg',
        href: window.location.href
      };
      emailShare.addMediaItem(image);
      // Parameters for the showShareUI method, including the UserAction object
      var params = {
        userAction: emailShare  // The UserAction object enfolding the newsfeed data.
        ,operationMode: 'Simple'
        ,showEmailButton: true // Enable the "Email" button and screen
        //,emailBody: "This email is from: $sender$ <br/> Check this out: $URL$ <br/> $userMsg$ <userMsg> <br/> The title is: $title$ <br/> The description is: $description$"
        ,useHTML: true  // Use the HTML implementation of the add-on
        ,initialView: 'email'
      };
      // Show the "Share" dialog
      gigya.socialize.showShareUI(params);
    }
  
    
    $('#emailShare').on('click', function(event) {
      event.preventDefault();
      showShareUI();
    });

  });
}(jQuery));
