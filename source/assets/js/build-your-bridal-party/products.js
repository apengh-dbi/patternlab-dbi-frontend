// ========================================================
// PRODUCTS
// ========================================================
var products = [
  {
    style: 'F19328',
    productId: 10639812,
    neckline: 'Cap Sleeves',
    length: 'Long',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Gold Metallic", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Parfait", "Oasis", "Persimmon", "Pewter", "Petal", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Rose Gold Metallic", "Sangria", "Sienna", "Steel Blue Metallic", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_long-bridesmaid-dress-with-lace-bodice-f19328',
    title: 'Long Bridesmaid Dress with Lace Bodice',
    brand: "David's Bridal"
  },
  {
    style: 'F17019',
    productId: 10407964,
    neckline: 'Cap Sleeves',
    length: 'Short',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "NBChampagne", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_short-lace-and-mesh-dress-with-illusion-neckline-f17019',
    title: 'Short Lace and Mesh Dress with Illusion Neckline',
    brand: "David's Bridal"
  },
  {
    style: 'F17063',
    productId: 10416672,
    neckline: 'One-Shoulder',
    length: 'Long',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Gold Metallic", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Rose Gold Metallic", "Sangria", "Sienna", "Spa", "Steel Blue", "Steel Blue Metallic", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_long-one-shoulder-lace-bridesmaid-dress-f17063',
    title: 'Long One Shoulder Lace Bridesmaid Dress',
    brand: "David's Bridal"
  },
  {
    style: 'F15711',
    productId: 10235635,
    neckline: 'One-Shoulder',
    length: 'Short',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_short-one-shoulder-contrast-corded-dress-f15711',
    title: 'Short One Shoulder Corded Lace Dress',
    brand: "David's Bridal"
  },
  {
    style: 'F19608',
    productId: 10850544,
    neckline: 'High Neck',
    length: 'Long',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_open-back-lace-and-mesh-bridesmaid-dress-f19608',
    title: 'Long One Shoulder Lace Bridesmaid Dress',
    brand: "David's Bridal"
  },
  {
    style: 'F19752',
    productId: 10929328,
    neckline: 'High Neck',
    length: 'Short',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_open-back-lace-and-mesh-short-bridesmaid-dress-f19752',
    title: 'Open-Back Lace and Mesh Short Bridesmaid Dress',
    brand: "David's Bridal"
  },
  {
    style: 'F18095',
    productId: 10524798,
    neckline: 'Strapless',
    length: 'Long',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover",   "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Parfait", "Oasis", "Persimmon", "Pewter", "Petal", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_lace-and-mesh-long-strapless-dress-f18095',
    title: 'Lace and Mesh Long Strapless Dress',
    brand: "David's Bridal"
  },
  {
    style: 'F19025',
    productId: 10573224,
    neckline: 'Halter',
    length: 'Long',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_long-mesh-dress-with-lace-halter-bodice-f19025',
    title: 'Long Mesh Dress with Lace Halter Bodice',
    brand: "David's Bridal"
  },
  {
    style: 'JB9479',
    productId: 10776087,
    neckline: 'Cap Sleeves',
    length: 'Junior',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "White", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_cap-sleeve-lace-and-mesh-long-girls-dress-jb9479',
    title: 'Cap Sleeve Lace and Mesh Long Girls Dress',
    brand: "David's Bridal"
  },
  {
    style: 'JB9014',
    productId: 10532702,
    neckline: 'One-Shoulder',
    length: 'Junior',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Gold Metallic", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Rose Gold Metallic", "Sangria", "Sienna", "Spa", "Steel Blue", "Steel Blue Metallic", "Sunflower", "Teal Blue", "Tickled", "Truffle", "White", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_one-shoulder-long-lace-bodice-dress-jb9014',
    title: 'One Shoulder Long Lace Bodice Dress',
    brand: "David's Bridal"
  },
  {
    style: 'JB9612',
    productId: 10954908,
    neckline: 'High Neck',
    length: 'Junior',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "White", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_keyhole-back-lace-and-mesh-junior-bridesmaid-dress-jb9612',
    title: 'Keyhole Back Lace and Mesh Junior Bridesmaid Dress',
    brand: "David's Bridal"
  },
  {
    style: 'JB9477',
    productId: 10573224,
    neckline: 'Cap Sleeves',
    length: 'Short',
    colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "White", "Wine", "Wisteria"],
    sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
    url: '/Product_cap-sleeve-lace-and-mesh-girls-dress-jb9477',
    title: 'Cap Sleeve Lace and Mesh Girls Dress',
    brand: "David's Bridal"
  }
  // {
  //   style: 'F19950',
  //   productId: 10573223,
  //   neckline: 'Off-The-Shoulder',
  //   length: 'Long',
  //   colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
  //   sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
  //   url: '',
  //   title: '',
  //   brand: "David's Bridal"
  // },
  // {
  //   style: 'F19908',
  //   productId: ,
  //   neckline: 'Long Sleeve',
  //   length: 'Long',
  //   colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
  //   sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
  //   url: '',
  //   title: '',
  //   brand: "David's Bridal"
  // },
  // {
  //   style: 'F19954',
  //   productId: ,
  //   neckline: 'Spaghetti Strap',
  //   length: 'Long',
  //   colors: ["Apple", "Begonia", "Bellini", "Biscotti", "Black", "Cameo", "Canary", "Champagne", "Chianti", "Clover", "Cobalt", "Graphite", "Grey", "Guava", "Horizon", "Ice Blue", "Iris", "Ivory", "Juniper", "Lapis", "Malibu", "Marine", "Meadow", "Mint", "Mystic", "Oasis", "Parfait", "Persimmon", "Petal", "Pewter", "Plum", "Portobello", "Quartz", "Raspberry", "Regency", "Sangria", "Sienna", "Spa", "Steel Blue", "Sunflower", "Teal Blue", "Tickled", "Truffle", "Wine", "Wisteria"],
  //   sizes: [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30],
  //   url: '',
  //   title: '',
  //   brand: "David's Bridal"
  // }
];