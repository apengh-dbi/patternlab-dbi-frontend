jQuery(document).ready(function(){
  var page_name = utag_data["page_name"];
  $('#widget_left_nav').each(function(index){
    $(this).find('[data-analytics-tagging]').each(function(index){
      var numOfLinks = $('#widget_left_nav').find('[data-analytics-tagging]').length;
      var current_internal_promotion_name = [page_name];
      var current_internal_promotion_id = [$(this).attr('data-analytics-tagging')];
      var current_internal_promotion_creative = [$(this).attr('data-alt')];
      var current_internal_promotion_position = [(index + 1) + 'of' + numOfLinks];

      // ================================================================
      // IMPRESSIONS
      // ================================================================
        // internal_promotion_Name
        if(undefined != utag_data["internal_promotion_Name"]) {
          utag_data["internal_promotion_Name"] = $.merge(utag_data["internal_promotion_Name"], current_internal_promotion_name);
        } else {
          utag_data["internal_promotion_Name"] = current_internal_promotion_name;
        }

        // internal_promotion_id
        if(undefined != utag_data["internal_promotion_id"]) {
          utag_data["internal_promotion_id"] = $.merge(utag_data["internal_promotion_id"], current_internal_promotion_id);
        } else {
          utag_data["internal_promotion_id"] = current_internal_promotion_id;
        }

        // internal_promotion_creative
        if(undefined != utag_data["internal_promotion_creative"]) {
          utag_data["internal_promotion_creative"] = $.merge(utag_data["internal_promotion_creative"], current_internal_promotion_creative);
        } else {
          utag_data["internal_promotion_creative"] = current_internal_promotion_creative;
        }

        // internal_promotion_position
        if(undefined != utag_data["internal_promotion_position"]) {
          utag_data["internal_promotion_position"] = $.merge(utag_data["internal_promotion_position"], current_internal_promotion_position);
        } else {
          utag_data["internal_promotion_position"] = current_internal_promotion_position;
        }


      // ================================================================
      // CLICKS
      // ================================================================
      $(this).on('click', function(){
        var options = {
          "event_type":"promo_click",
          "internal_promotion_id": current_internal_promotion_id,
          "internal_promotion_Name": current_internal_promotion_name,
          "internal_promotion_position": current_internal_promotion_position,
          "internal_promotion_creative": current_internal_promotion_creative
        };
        console.log(options);
        utag.link(options);
      });
    });
  });
});
