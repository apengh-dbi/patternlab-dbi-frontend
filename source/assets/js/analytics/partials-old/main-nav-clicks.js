(function($) {
  $(document).ready(function() {
    $('.header__main-nav-topic-heading-link, .header__main-nav-topic-section-item-link').on('click', function(event) {
      var eventLabel, manualCMSP;

      // L1 Category Links
      if ( $(event.target).hasClass('header__main-nav-topic-heading-link') ) {
        var L1Category = $(this).text().trim();
        eventLabel = '';
        manualCMSP = "Header-_-" + L1Category + "-_-" + L1Category + "|" + L1Category;

      // L3 Category Links
      } else if( $(event.target).hasClass('header__main-nav-topic-section-item-link') ) {
        var L3Category = $(this).text().trim();
        var L2Category = $(this).closest('.header__main-nav-topic-section').find('.header__main-nav-topic-section-heading-link').text().trim();
        var L1Category = $(this).closest('.header__main-nav-topic').find('.header__main-nav-topic-heading-link').text().trim();
        eventLabel = L2Category + "|" + L3Category;
        manualCMSP = "Header-_-" + L1Category + "-_-" + eventLabel;
      }

      var params = {
        "EventCategory": "Header",
        "EventAction": L1Category,
        "EventLabel": eventLabel,
        "EventPage": pageNameValue,
        "Manual_cm_sp": manualCMSP
      };
      utag.link(params);

    });
  });
}(jQuery));



(function($) {
  $(document).ready(function() {
    $('.header__main-nav-topic-heading-link, .header__main-nav-topic-section-item-link, .header__main-nav-topic-section-image').on('click', function(event) {
      var eventLabel
      if ($(event.target).hasClass('header__main-nav-topic-heading-link')) {
        var L1Category = $(this).text().trim();
        eventLabel = '';
      } else if ($(event.target).hasClass('header__main-nav-topic-section-item-link')) {
        var L3Category = $(this).text().trim();
        var L2Category = $(this).closest('.header__main-nav-topic-section').find('.header__main-nav-topic-section-heading-link').text().trim();
        var L1Category = $(this).closest('.header__main-nav-topic').find('.header__main-nav-topic-heading-link').text().trim();
        eventLabel = L2Category + "|" + L3Category;
      } else if ($(event.target).hasClass('header__main-nav-topic-section-image')) {
        var imgAltTxt = $(this).attr('alt');
        var L2Category = $(this).closest('.header__main-nav-topic-section').find('.header__main-nav-topic-section-heading-link').text().trim();
        var L1Category = $(this).closest('.header__main-nav-topic').find('.header__main-nav-topic-heading-link').text().trim();
        eventLabel = "Espot|" + imgAltTxt
      }
      var params = {
        "EventCategory": "Header",
        "EventAction": L1Category,
        "EventLabel": eventLabel,
        "EventPage": pageNameValue,
      };
      utag.link(params);
    });
  });
}(jQuery));
