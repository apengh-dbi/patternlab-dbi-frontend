// ============================================
// PLP - TOP PROMO BANNERS - ANALYTICS
// ============================================
jQuery(document).ready(function() {
  var topPromoBanner = $('.page-top-promo').find("a");
  topPromoBanner.on("click", function() {

    var topPromoBannerMessage = $('.page-top-promo').find("img").attr("alt");
    if (undefined === topPromoBannerMessage) {
      topPromoBannerMessage = $(this).attr("data-toppromo-message");
    }
    function getWords(str) {
      return str.split(/\s+/).slice(0,3).join(" ");
    }
    topPromoBannerMessage = getWords(topPromoBannerMessage);
    topPromoBannerMessage = topPromoBannerMessage.replace(/\s/g, '');

    var L1Cat = utag_data['product_category_level1'];
    var L2Cat = utag_data['product_category_level2'];
    var L3Cat = utag_data['product_category_level3'];
    var Manual_cm_sp = "TopPromo-_-Espot-_-"+L1Cat+"ListingEspot";
    Manual_cm_sp = Manual_cm_sp.replace(/\s/g, '');

    var options = {
      "EventCategory":"Top Promo Click",
      "EventAction":"Banner Click",
      "EventLabel":L1Cat+"|"+L2Cat+"|"+L3Cat+"|page-top-promo",
      "event_type":"promo_click",
      "Manual_cm_sp":Manual_cm_sp,
      "internal_promotion_id":[L1Cat+"|"+L2Cat+"|"+L3Cat+"|page-top-promo"],
      "internal_promotion_Name":["TopPromo"],
      "internal_promotion_position":["Top Banner"],
      "internal_promotion_creative":[topPromoBannerMessage],
      "EventPage":pageNameValue
    };
    console.log(options);
    utag.link(options);
  });
});
