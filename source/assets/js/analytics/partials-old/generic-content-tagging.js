jQuery(document).ready(function(){
  // ================================================================
  // Element to target
  // ================================================================
  var taggingTargetElement = $('#monetate_selectorBanner_46c2be04_00Map area');

  if(undefined !== taggingTargetElement) {

    // Check if mobile or desktop/tablet
    var deviceType = '';
    if(window.innerWidth < 768) {
      deviceType = 'Mobile';
    } else {
      deviceType = 'Desktop';
    }

    // ================================================================
    // Set variable values as an array
    // ================================================================
    var current_internal_promotion_name = ["Monetate Closest Store Banner"];
    var current_internal_promotion_id = ["Closest Store-" + deviceType];
    var current_internal_promotion_creative = ["Visit your store"];
    var current_internal_promotion_position = ["1of1"];

    // ================================================================
    // IMPRESSIONS
    // ================================================================
    if(undefined != utag_data["internal_promotion_Name"]) {
      utag_data["internal_promotion_Name"] = $.merge(utag_data["internal_promotion_Name"], current_internal_promotion_name);
    }
    if(undefined != utag_data["internal_promotion_id"]) {
      utag_data["internal_promotion_id"] = $.merge(utag_data["internal_promotion_id"], current_internal_promotion_id);
    }
    if(undefined != utag_data["internal_promotion_creative"]) {
      utag_data["internal_promotion_creative"] = $.merge(utag_data["internal_promotion_creative"], current_internal_promotion_creative);
    }
    if(undefined != utag_data["internal_promotion_position"]) {
      utag_data["internal_promotion_position"] = $.merge(utag_data["internal_promotion_position"], current_internal_promotion_position);
    }

    // ================================================================
    // CLICKS
    // ================================================================
    taggingTargetElement.on('click', function(){
      var options = {
        "event_type":"promo_click",
        "internal_promotion_id": current_internal_promotion_id,
        "internal_promotion_Name": current_internal_promotion_name,
        "internal_promotion_position": current_internal_promotion_position,
        "internal_promotion_creative": current_internal_promotion_creative
      };
      console.log(options);
      utag.link(options);
    });
  }
});
