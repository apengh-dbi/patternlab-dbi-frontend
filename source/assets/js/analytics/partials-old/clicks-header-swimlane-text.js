// ============================================
// HEADER - SWIMLANE CTA LINKS - ANALYTICS
// ============================================
jQuery(document).ready(function() {
  // Swimlane CTA
  var swimlaneEspotCta = $('[data-swimlane-position="2"]');
  swimlaneEspotCta.on('click', function(){

    var swimlaneImageCtaText = $(this).html();
    function getWords(str) {
      return str.split(/\s+/).slice(0,3).join(" ");
    }
    swimlaneImageCtaText = getWords(swimlaneImageCtaText);
    swimlaneImageCtaText = swimlaneImageCtaText.replace(/\s/g, '');

    var L1Cat = $(this).attr('data-swimlane-category');
    var L2Cat = "SwimLane";
    var Manual_cm_sp = L2Cat+"-_-"+L1Cat+"-_-"+L2Cat;
    Manual_cm_sp = Manual_cm_sp.replace(/\s/g, '');

    var options = {
      "EventCategory":"Swim Lane Espot",
      "EventAction":L1Cat,
      "EventLabel":L1Cat+"|"+L2Cat+"|"+swimlaneImageCtaText,
      "event_type":"promo_click",
      "Manual_cm_sp":Manual_cm_sp,
      "internal_promotion_id":[L1Cat+"|"+L2Cat],
      "internal_promotion_Name":[swimlaneImageCtaText],
      "internal_promotion_position":["2"],
      "internal_promotion_creative":[swimlaneImageCtaText],
      "EventPage":pageNameValue
    };
    console.log(options);
    utag.link(options);
  });

});
