// ============================================
// PLP - MINI-NAV - CLICK TRACKING
// ============================================
jQuery(document).ready(function() {
  // Find all mini-nav sections --> loop through them
  $('.carousel-nav_item').each(function(){
    var sectionHeading = $(this).find("h3").text().trim();
    var L1Cat = utag_data['product_category_level1'];
    if(undefined === L1Cat) {
      L1Cat = ["not set"];
    }
    L1Cat = L1Cat.toString();

    // Find all links --> Loop through them
    $(this).find("a").on('click', function(){
      var linkText = $(this).text().trim();
      var options = {
        "EventCategory": "Mini-Navigation",
        "EventAction": L1Cat,
        "EventLabel": sectionHeading + " | " + linkText
      };
      console.log(options);
      utag.link(options);
    });
  });
});
