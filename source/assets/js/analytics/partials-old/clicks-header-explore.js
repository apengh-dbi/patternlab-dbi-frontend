// ============================================
// HEADER - EXPLORE - TEALIUM ANALYTICS
// ============================================
function headerExploreClickEvent(espotName, L1Category, exploreLinkMessage) {
  var promotion_position = "";
  if(window.innerWidth >= 1024) {
    promotion_position = "Header";
  } else {
    promotion_position = "Collapsible Left Menu";
  }
  var L2Cat = "Explore";
  var Manual_cm_sp = L2Cat+"-_-"+L1Category+"-_-"+L2Cat+"|"+exploreLinkMessage;
  Manual_cm_sp = Manual_cm_sp.replace(/\s/g, '');

  var options = {
    "EventCategory":L2Cat,
    "EventAction":L1Category+"-"+promotion_position,
    "EventLabel":L2Cat+"|"+exploreLinkMessage,
    "Manual_cm_sp":Manual_cm_sp,
    "EventPage":pageNameValue
  };
  console.log(options);
  utag.link(options);
}
