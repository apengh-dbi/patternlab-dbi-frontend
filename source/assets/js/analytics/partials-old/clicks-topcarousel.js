// ============================================
// HEADER - TOP CAROUSEL - ANALYTICS TAGGING
// ============================================
jQuery(document).ready(function() {
  var headerPromoBarEspot = $('.header__promo-list-item').find("a");
  headerPromoBarEspot.on("click", function() {
    var dataPromoName = $(this).parent('span').parent('li').attr('data-promo-name');
    function getWords(str) {
      return str.split(/\s+/).slice(0,3).join(" ");
    }
    dataPromoName = getWords(dataPromoName);
    var dataPromoNameFirstWord = dataPromoName.substr(0, dataPromoName.indexOf(" "));
    var dataPromoPosition = $(this).parent('span').parent('li').attr('data-promo-position');

    var L1Cat = utag_data['product_category_level1'];
    var L2Cat = utag_data['product_category_level2'];
    var L3Cat = utag_data['product_category_level3'];
    var Manual_cm_sp = "TopCarousel-_-"+dataPromoPosition+"-_-"+dataPromoNameFirstWord;
    Manual_cm_sp = Manual_cm_sp.replace(/\s/g, '');

    var options = {
      "EventCategory":"Top Carousel Click",
      "EventAction":"Banner Click",
      "EventLabel":pageNameValue,
      "event_type":"promo_click",
      "Manual_cm_sp":Manual_cm_sp,
      "internal_promotion_id":["Top Carousel | "+pageNameValue],
      "internal_promotion_Name":["TopCarousel"],
      "internal_promotion_position":[dataPromoPosition],
      "internal_promotion_creative":[dataPromoName],
      "EventPage":pageNameValue
    };
    console.log(options);
    utag.link(options);
  });
});
