// ============================================
// HEADER - SWIMLANE IMAGES - ANALYTICS
// ============================================
jQuery(document).ready(function() {

  // SwimLane Image
  var swimlaneEspotImage = $('[data-swimlane-position="1"]');
  swimlaneEspotImage.on('click', function(){

    var swimlaneImageAltText = $(this).find('img').attr('alt');
    function getWords(str) {
      return str.split(/\s+/).slice(0,3).join(" ");
    }
    swimlaneImageAltText = getWords(swimlaneImageAltText);
    swimlaneImageAltText = swimlaneImageAltText.replace(/\s/g, '');

    var L1Cat = $(this).attr('data-swimlane-category');
    var L2Cat = "SwimLane";
    var Manual_cm_sp = L2Cat+"-_-"+L1Cat+"-_-"+L2Cat;
    Manual_cm_sp = Manual_cm_sp.replace(/\s/g, '');

    var options = {
      "EventCategory":"Swim Lane Espot",
      "EventAction":L1Cat,
      "EventLabel":L1Cat+"|"+L2Cat+"|"+swimlaneImageAltText,
      "event_type":"promo_click",
      "Manual_cm_sp":Manual_cm_sp,
      "internal_promotion_id":[L1Cat+"|"+L2Cat],
      "internal_promotion_Name":[swimlaneImageAltText],
      "internal_promotion_position":["1"],
      "internal_promotion_creative":[swimlaneImageAltText],
      "EventPage":pageNameValue
    };
    console.log(options);
    utag.link(options);
  });
});
