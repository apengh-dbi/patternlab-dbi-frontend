// ============================================
// PLP - INLINE - ANALYTICS
// ============================================
jQuery(document).on('click', '[data-inline-message]', function(){

  var inlineEspotMessage = $('.listing__grid-promo-slot-image').attr('alt');
  if (undefined === inlineEspotMessage) {
    inlineEspotMessage = inlineEspot.attr("data-inline-message");
  }
  function getWords(str) {
    return str.split(/\s+/).slice(0,3).join(" ");
  }
  inlineEspotMessage = getWords(inlineEspotMessage);
  inlineEspotMessage = inlineEspotMessage.replace(/\s/g, '');

  var L1Cat = utag_data['product_category_level1'];
  var L2Cat = utag_data['product_category_level2'];
  var L3Cat = utag_data['product_category_level3'];
  var Manual_cm_sp = "PLP-_-Espot-_-"+L1Cat+"|"+L2Cat+"|"+inlineEspotMessage;
  Manual_cm_sp = Manual_cm_sp.replace(/\s/g, '');

  var options = {
    "EventCategory":"PLP Click",
    "EventAction":"Espot Click",
    "EventLabel":L1Cat+"|"+L2Cat+"|"+L3Cat,
    "event_type":"promo_click",
    "Manual_cm_sp":Manual_cm_sp,
    "internal_promotion_id":[L1Cat+"|"+L2Cat+"|"+L3Cat+"|"+inlineEspotMessage],
    "internal_promotion_Name":["SingleSlot"],
    "internal_promotion_position":["inline"],
    "internal_promotion_creative":[inlineEspotMessage],
    "EventPage":pageNameValue
  };
  console.log(options);
  utag.link(options);
});
