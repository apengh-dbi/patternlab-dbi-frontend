// ============================================
// PLP - BOOK APPOINTMENT - ANALYTICS
// ============================================
jQuery(document).ready(function() {
  var bookAppointmentEspot = $('.listing__grid-promo-bar').find("a");
  function getWords(str) {
    return str.split(/\s+/).slice(0,3).join(" ");
  }
  bookAppointmentEspot.on("click", function() {
    var bookAppointmentMessage = bookAppointmentEspot.attr("data-book-appointment-message");
    bookAppointmentMessage = getWords(bookAppointmentMessage);
    bookAppointmentMessage = bookAppointmentMessage.replace(/\s/g, '');

    var L1Cat = utag_data['product_category_level1'];
    var L2Cat = utag_data['product_category_level2'];
    var L3Cat = utag_data['product_category_level3'];
    var Manual_cm_sp = "PLP-_-Espot-_-"+L1Cat+"-_-"+L2Cat+"-_-"+L3Cat+"-_-Bottom";
    Manual_cm_sp = Manual_cm_sp.replace(/\s/g, '');

    var options = {
      "EventCategory":"PLP Click",
      "EventAction":"Bottom Espot",
      "EventLabel":L1Cat+"|"+L2Cat+"|"+L3Cat+"|"+bookAppointmentMessage,
      "event_type":"promo_click",
      "Manual_cm_sp":Manual_cm_sp,
      "internal_promotion_id":[L1Cat+"|"+L2Cat+"|"+L3Cat+"|"+bookAppointmentMessage],
      "internal_promotion_Name":["BottomEspot"],
      "internal_promotion_position":["Bottom PLP eSpot"],
      "internal_promotion_creative":[bookAppointmentMessage],
      "EventPage":pageNameValue
    };
    console.log(options);
    utag.link(options);
  });
});
