// ============================================
// AJAX IMPRESSIONS - INLINE ESPOTS - After Page 1 Results
// ============================================
require(["dojo/request", "dojo/request/notify"], function(request, notify){

  // DOJO AJAX Call - Done
  notify("done", function(responseOrError){
    // If error, log error
    if(responseOrError instanceof Error){
      console.log('Dojo - Notify - Failed');
    } else {
      // Get Current Listing Page Number
      var impressionsPageNumber = dojo.byId("listing-page")[dojo.byId("listing-page").selectedIndex].value;

      // If Not Page 1
      if(impressionsPageNumber != 1) {
        console.log('Dojo - Notify - Page Number: '+impressionsPageNumber);

        // Look for Inline E-spot
        var plpInlineEspot = $(".listing__grid-promo-slot-text").find("a").attr("data-inline-message");
        if (undefined != plpInlineEspot) {

          var L1Cat = utag_data['product_category_level1'];
          var L2Cat = utag_data['product_category_level2'];
          var L3Cat = utag_data['product_category_level3'];

          // Get data-inline-message, trim to 3 words, stip spaces
          var inlineEspotMessage = $('.listing__grid-promo-slot-image').attr('alt');
          if (undefined === inlineEspotMessage) {
            var inlineEspotMessage = inlineEspot.attr("data-inline-message");
          }
          function getWords(str) {
            return str.split(/\s+/).slice(0,3).join(" ");
          }
          inlineEspotMessage = getWords(inlineEspotMessage);
          inlineEspotMessage = inlineEspotMessage.replace(/\s/g, '');

          // internal_promotion_id
          var internal_promotion_id = [L1Cat+"|"+L2Cat+"|"+L3Cat+"|"+inlineEspotMessage];

          // internal_promotion_Name
          var internal_promotion_Name = ["SingleSlot"];

          // internal_promotion_position
          var internal_promotion_position = ["inline"];

          // internal_promotion_creative
          var internal_promotion_creative = [inlineEspotMessage];

          // Build json object
          options = {
            "internal_promotion_id":internal_promotion_id,
            "internal_promotion_Name":internal_promotion_Name,
            "internal_promotion_position":internal_promotion_position,
            "internal_promotion_creative":internal_promotion_creative
          }
          console.log(options);
          utag.view(options);
        }
      }

    }
  });
});
