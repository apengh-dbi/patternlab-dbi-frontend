// ============================================
// PAGE LOAD IMPRESSIONS
// ============================================
jQuery(document).ready(function() {
  function getWords(str) {
    return str.split(/\s+/).slice(0,3).join(" ");
  }

  var L1Cat = utag_data['product_category_level1'];
  var L2Cat = utag_data['product_category_level2'];
  var L3Cat = utag_data['product_category_level3'];

  if(undefined === internal_promotion_Name) {
    var internal_promotion_Name = [];
  }
  if(undefined === internal_promotion_id) {
    var internal_promotion_id = [];
  }
  if(undefined === internal_promotion_creative) {
    var internal_promotion_creative = [];
  }
  if(undefined === internal_promotion_position) {
    var internal_promotion_position = [];
  }


  // ============================================
  // HEADER - TOP CAROUSEL
  // ============================================
  if( $('.header__promo-list-item[data-promo-status="active"]').attr('data-promo-viewed') == "false" ) {
    // Get Top Carousel Data
    var dataPromoName = $('.header__promo-list-item[data-promo-status="active"]').attr('data-promo-name');
    dataPromoName = getWords(dataPromoName);

    // Top Carousel internal_promotion_position
    var dataPromoPosition = $('.header__promo-list-item[data-promo-status="active"]').attr('data-promo-position');

    // Set Array Values
    var topCarouselPromoId = ["Top Carousel | " + pageNameValue];
    var topCarouselPromoName = ["TopCarousel"];
    var topCarouselPromoPosition = [dataPromoPosition];
    var topCarouselPromoCreative = [dataPromoName];

    // Add to internal_promotion Arrays
    internal_promotion_id = $.merge(internal_promotion_id, topCarouselPromoId);
    internal_promotion_Name = $.merge(internal_promotion_Name, topCarouselPromoName);
    internal_promotion_position = $.merge(internal_promotion_position, topCarouselPromoPosition);
    internal_promotion_creative = $.merge(internal_promotion_creative, topCarouselPromoCreative);

    // Set First Carousel data-promo-viewed = true
    $('.header__promo-list-item[data-promo-status="active"]').attr('data-promo-viewed','true');
  }


  // ============================================
  // HEADER - SWIMLANE
  // ============================================
  // var swimlaneEspot = $('li[espot-name*="ES_SWIMLANE"]');
  // var L2SwimLaneCategory = "SwimLane";
  //
  // $.each(swimlaneEspot, function() {
  //   // Get Swimlane Category
  //   var L1SwimLaneCategory = $(this).find('figure').attr('data-swimlane-category');
  //
  //   // --------------------------------------------
  //   // SWIMLANE - IMAGE
  //   // --------------------------------------------
  //   // Get Image Alt Text, Trim to first 3 words, remove spaces
  //   var swimlaneImageAltText = $(this).find('a[data-swimlane-position*="1"]').find('img').attr('alt');
  //   swimlaneImageAltText = getWords(swimlaneImageAltText);
  //   swimlaneImageAltText = swimlaneImageAltText.replace(/\s/g, '');
  //
  //   // internal_promotion_id
  //   var swimlanePromoIdText = [L1SwimLaneCategory+'|Swimlane'];
  //
  //   // internal_promotion_Name
  //   swimlaneImageAltText = [swimlaneImageAltText];
  //
  //   // internal_promotion_position
  //   swimlanePromoPosition = ["1"];
  //
  //   // internal_promotion_creative
  //   swimlanePromoCreative = swimlaneImageAltText;
  //
  //   // Add to internal_promotion Arrays
  //   internal_promotion_id = $.merge(internal_promotion_id, swimlanePromoIdText);
  //   internal_promotion_Name = $.merge(internal_promotion_Name, swimlaneImageAltText);
  //   internal_promotion_position = $.merge(internal_promotion_position, swimlanePromoPosition);
  //   internal_promotion_creative = $.merge(internal_promotion_creative, swimlanePromoCreative);
  //
  //
  //   // --------------------------------------------
  //   // SWIMLANE - CTA
  //   // --------------------------------------------
  //   // Get CTA Text, Trim to first 3 words, remove spaces
  //   var swimlaneCtaText = $(this).find('a[data-swimlane-position*="2"]').html();
  //   swimlaneCtaText = getWords(swimlaneCtaText);
  //   swimlaneCtaText = swimlaneCtaText.replace(/\s/g, '');
  //
  //   // internal_promotion_id
  //   var swimlanePromoIdText = [L1SwimLaneCategory+'|Swimlane'];
  //
  //   // internal_promotion_Name
  //   swimlaneCtaText = [swimlaneCtaText];
  //
  //   // internal_promotion_position
  //   swimlanePromoPosition = ["2"];
  //
  //   // internal_promotion_creative
  //   swimlanePromoCreative = swimlaneCtaText;
  //
  //   // Add to internal_promotion Arrays
  //   internal_promotion_id = $.merge(internal_promotion_id, swimlanePromoIdText);
  //   internal_promotion_Name = $.merge(internal_promotion_Name, swimlaneCtaText);
  //   internal_promotion_position = $.merge(internal_promotion_position, swimlanePromoPosition);
  //   internal_promotion_creative = $.merge(internal_promotion_creative, swimlanePromoCreative);
  //
  // });


  // ============================================
  // PLP - TOP PROMO BANNERS
  // ============================================
    // --------------------------------------------
    // OMNI - TOP BANNERS
    // --------------------------------------------
    // Look for Omni banner first
    // Only if there is an <a> tag
    var plpTopPromoEspot = $('.page-top-promo').find("a").attr("data-toppromo-message");
    if (undefined !== plpTopPromoEspot) {

      // internal_promotion_id
      var topPromoId = [L1Cat+"|"+L2Cat+"|"+L3Cat+"|page-top-promo"];

      // internal_promotion_Name
      var topPromoName = ["TopPromo"];

      // internal_promotion_position
      var topPromoPosition = ["Top Banner"];

      // internal_promotion_creative
      // Get data-toppromo-message, trim to 3 words, strip spaces, convert to array
      var topPromoBannerMessage = $('.page-top-promo').find("a").attr("data-toppromo-message");
      if (undefined === topPromoBannerMessage) {
        var topPromoBannerMessage = "";
      }
      topPromoBannerMessage = getWords(topPromoBannerMessage);
      topPromoBannerMessage = topPromoBannerMessage.replace(/\s/g, '');
      topPromoBannerMessage = [topPromoBannerMessage];

      // Add to internal_promotion Arrays
      internal_promotion_id = $.merge(internal_promotion_id, topPromoId);
      internal_promotion_Name = $.merge(internal_promotion_Name, topPromoName);
      internal_promotion_position = $.merge(internal_promotion_position, topPromoPosition);
      internal_promotion_creative = $.merge(internal_promotion_creative, topPromoBannerMessage);

    }

    // --------------------------------------------
    // LEGACY - TOP BANNERS
    // --------------------------------------------
    // No Omni banner, look for Legacy banner
    var legacyTopBanner = $('.widget_hero_image_container').find('a').attr('data-toppromo-message');
    if (undefined !== legacyTopBanner) {

      // internal_promotion_id
      var legacyTopBannerPromoId = [L1Cat+"|"+L2Cat+"|"+L3Cat+"|page-top-promo"];

      // internal_promotion_Name
      var legacyTopBannerPromoName = ["TopPromo"];

      // internal_promotion_position
      var legacyTopBannerPromoPosition = ["Top Banner"];

      // internal_promotion_creative
      // Get data-toppromo-message, trim to 3 words, strip spaces, convert to array
      var legacyTopBannerMessage = $('.widget_hero_image_container').find('a').attr("data-toppromo-message");
      if (undefined === legacyTopBannerMessage) {
        var legacyTopBannerMessage = "";
      }
      legacyTopBannerMessage = getWords(legacyTopBannerMessage);
      legacyTopBannerMessage = legacyTopBannerMessage.replace(/\s/g, '');
      legacyTopBannerMessage = [legacyTopBannerMessage];

      // Add to internal_promotion Arrays
      internal_promotion_id = $.merge(internal_promotion_id, legacyTopBannerPromoId);
      internal_promotion_Name = $.merge(internal_promotion_Name, legacyTopBannerPromoName);
      internal_promotion_position = $.merge(internal_promotion_position, legacyTopBannerPromoPosition);
      internal_promotion_creative = $.merge(internal_promotion_creative, legacyTopBannerMessage);
    }



  // ============================================
  // PLP - INLINE
  // ============================================
  var plpInlineEspot = $(".listing__grid-promo-slot-text").find("a").attr("data-inline-message");
  if (undefined !== plpInlineEspot) {

    // Get data-inline-message, trim to 3 words, strip spaces
    var inlineEspotMessage = $('.listing__grid-promo-slot-image').attr('alt');
    if (undefined === inlineEspotMessage) {
      var inlineEspotMessage = inlineEspot.attr("data-inline-message");
    }
    inlineEspotMessage = getWords(inlineEspotMessage);
    inlineEspotMessage = inlineEspotMessage.replace(/\s/g, '');

    // internal_promotion_id
    var inlinePromoId = [L1Cat+"|"+L2Cat+"|"+L3Cat+"|"+inlineEspotMessage];

    // internal_promotion_Name
    var inlinePromoName = ["SingleSlot"];

    // internal_promotion_position
    var inlinePromoPosition = ["inline"];

    // internal_promotion_creative
    var inlinePromoCreative = [inlineEspotMessage];

    // Add to internal_promotion Arrays
    internal_promotion_id = $.merge(internal_promotion_id, inlinePromoId);
    internal_promotion_Name = $.merge(internal_promotion_Name, inlinePromoName);
    internal_promotion_position = $.merge(internal_promotion_position, inlinePromoPosition);
    internal_promotion_creative = $.merge(internal_promotion_creative, inlinePromoCreative);
  }

  // ============================================
  // PLP - BOOK APPOINTMENT
  // ============================================
  var plpBookAppointmentEspot = $('p[data-espot-name="PLP_BOOK_APPOINTMENT"]').attr("data-espot-name");
  if (undefined != plpBookAppointmentEspot) {

    // Get data-book-appointment-message, trim to 3 words, strip spaces
    var bookAppointmentMessage = $('.listing__grid-promo-bar').find("a").attr("data-book-appointment-message");
    bookAppointmentMessage = getWords(bookAppointmentMessage);
    bookAppointmentMessage = bookAppointmentMessage.replace(/\s/g, '');

    // internal_promotion_id
    var bookAppointmentPromoId = [L1Cat+"|"+L2Cat+"|"+L3Cat+"|"+bookAppointmentMessage];

    // internal_promotion_Name
    var bookAppointmentPromoName = ["BottomEspot"];

    // internal_promotion_position
    var bookAppointmentPosition = ["Bottom PLP eSpot"];

    // internal_promotion_creative
    var bookAppointmentCreative = [bookAppointmentMessage];

    // Add to internal_promotion Arrays
    internal_promotion_id = $.merge(internal_promotion_id, bookAppointmentPromoId);
    internal_promotion_Name = $.merge(internal_promotion_Name, bookAppointmentPromoName);
    internal_promotion_position = $.merge(internal_promotion_position, bookAppointmentPosition);
    internal_promotion_creative = $.merge(internal_promotion_creative, bookAppointmentCreative);
  }




// ================================================================
// GA Tagging - OMNI Release 2 - Homepage & Category Landing Pages
// ================================================================
  var page_name = utag_data["page_name"];

  // ================================================================
  // Only tag content between the Header and Footer
  // ================================================================
  $("#contentWrapper").each(function(index) {

    // ================================================================
    // Carousel
    // ================================================================
    $('.slick').each(function(index) {

      // Set all slides to data-slide-viewed = false
      $('.slick-slide').attr('data-slide-viewed', 'false');

      // ----------------------------------------------------------------
      // Send Impressions for First Slide
      // ----------------------------------------------------------------
        // Set current slide to viewed
        $(".slick-slide.slick-active").attr('data-slide-viewed', 'true');

        // Find all links, loop through them
        var number_of_links = $(".slick-slide.slick-active").find("a").length;
        $(".slick-slide.slick-active").find("a").each(function( index ){
          var link_index = index + 1;

          // internal_promotion_name
            var current_internal_promotion_name = [page_name];

          // internal_promotion_creative - image alt tag or link text
            // Search for an image and strip alt tag (if no image, use link text)
            var current_link_image = $(this).find("img").attr("alt");
            var current_link_text = $(this).text().trim();

            if(undefined !== current_link_image) {
              current_internal_promotion_creative = current_link_image;
            } else {
              current_internal_promotion_creative = current_link_text;
            }


            if(current_internal_promotion_creative !== "") {
              current_internal_promotion_creative = [current_internal_promotion_creative];

              // internal_promotion_id
              var current_internal_promotion_id = ["Carousel"];

              // internal_promotion_position
              var current_internal_promotion_position = [link_index + "of" + number_of_links];

              // Add to internal_promotion Arrays
              internal_promotion_id = $.merge(internal_promotion_id, current_internal_promotion_id);
              internal_promotion_Name = $.merge(internal_promotion_Name, current_internal_promotion_name);
              internal_promotion_position = $.merge(internal_promotion_position, current_internal_promotion_position);
              internal_promotion_creative = $.merge(internal_promotion_creative, current_internal_promotion_creative);
            }


          // ----------------------------------------------------------------
          // Click Tracking
          // ----------------------------------------------------------------
            $(this).on("click", function(){
              var click_internal_promotion_creative = $(this).text().trim();
              click_internal_promotion_creative = [click_internal_promotion_creative];
              var options = {
                "event_type":"promo_click",
                "internal_promotion_id": current_internal_promotion_id,
                "internal_promotion_Name": current_internal_promotion_name,
                "internal_promotion_position": current_internal_promotion_position,
                "internal_promotion_creative": click_internal_promotion_creative
              };
              console.log(options);
              utag.link(options);
            });
        });



      // ----------------------------------------------------------------
      // Send Impressions for All Other Slides on First View
      // ----------------------------------------------------------------
        $(this).on("afterChange", function(){
          // Slide NOT Viewed - Send Impressions
          if($(".slick-slide.slick-active").attr('data-slide-viewed') == 'false') {

            // Define variables
            var slides_internal_promotion_id = [];
            var slides_internal_promotion_Name = [];
            var slides_internal_promotion_position = [];
            var slides_internal_promotion_creative = [];

            // Find all links, loop through them
            var number_of_links = $(".slick-slide.slick-active").find("a").length;
            $(".slick-slide.slick-active").find("a").each(function( index ){
              var link_index = index + 1;

              // internal_promotion_name
                var current_slide_internal_promotion_name = [page_name];

              // internal_promotion_creative - image alt tag or link text
                // Search for an image and strip alt tag (if no image, use link text)
                var current_link_image = $(this).find("img").attr("alt");
                var current_link_text = $(this).text().trim();

                if(undefined !== current_link_image) {
                  current_slide_internal_promotion_creative = current_link_image;
                } else {
                  current_slide_internal_promotion_creative = current_link_text;
                }

                if(current_slide_internal_promotion_creative !== "") {
                  current_slide_internal_promotion_creative = [current_slide_internal_promotion_creative];

                  // internal_promotion_id
                  var current_slide_internal_promotion_id = ["Carousel"];

                  // internal_promotion_position
                  var current_slide_internal_promotion_position = [link_index + "of" + number_of_links];

                  // Add to internal_promotion Arrays
                  slides_internal_promotion_id = $.merge(slides_internal_promotion_id, current_slide_internal_promotion_id);
                  slides_internal_promotion_Name = $.merge(slides_internal_promotion_Name, current_slide_internal_promotion_name);
                  slides_internal_promotion_position = $.merge(slides_internal_promotion_position, current_slide_internal_promotion_position);
                  slides_internal_promotion_creative = $.merge(slides_internal_promotion_creative, current_slide_internal_promotion_creative);
                }


              // ----------------------------------------------------------------
              // Click Tracking
              // ----------------------------------------------------------------
                $(this).on("click", function(){
                  var click_internal_promotion_creative = $(this).text().trim();
                  click_internal_promotion_creative = [click_internal_promotion_creative];
                  var options = {
                    "event_type":"promo_click",
                    "internal_promotion_id": current_slide_internal_promotion_id,
                    "internal_promotion_Name": current_slide_internal_promotion_name,
                    "internal_promotion_position": current_slide_internal_promotion_position,
                    "internal_promotion_creative": click_internal_promotion_creative
                  };
                  console.log(options);
                  utag.link(options);
                });
            });

            var options = {
              "internal_promotion_id": slides_internal_promotion_id,
              "internal_promotion_Name": slides_internal_promotion_Name,
              "internal_promotion_position": slides_internal_promotion_position,
              "internal_promotion_creative": slides_internal_promotion_creative
            };
            console.log(options);
            utag.view(options);

            // Set current slide to viewed
            $(".slick-slide.slick-active").attr('data-slide-viewed', 'true');
          }
        });
    });



    // ----------------------------------------------------------------
    // Find all eSpot containers (.left_espot) and loop through them
    // ----------------------------------------------------------------
    $(".left_espot").each(function( index ) {

      // ----------------------------------------------------------------
      // Find all section[data-analytics-tagging], loop through them
      // ----------------------------------------------------------------
      $(this).find("section[data-analytics-tagging]").each(function( index ){

        // ----------------------------------------------------------------
        // internal_promotion_id
        // ----------------------------------------------------------------
          var current_internal_promotion_id = [$(this).attr('data-analytics-tagging')];


        // ----------------------------------------------------------------
        // Search for any links with section[data-analytics-tagging] and loop through them
        // ----------------------------------------------------------------
        var number_of_links = $(this).find("a").length;
        $(this).find("a").each(function( index ) {
          var link_index = index + 1;

          // ----------------------------------------------------------------
          // internal_promotion_Name
          // ----------------------------------------------------------------
            var current_internal_promotion_Name = [page_name];

          // ----------------------------------------------------------------
          // internal_promotion_creative - image alt tag or link text
          // ----------------------------------------------------------------
            // Search for an image and strip alt tag (if no image, use link text)
            var current_link_image = $(this).find("img").attr("alt");
            var current_link_text = $(this).text().trim();
            current_link_text = current_link_text.replace(/(\r\n|\n|\r)/gm,''); // Remove an line breaks
            current_link_text = current_link_text.replace(/ +/g, ' '); // Remove spaces

            if(undefined !== current_link_image) {
              current_internal_promotion_creative = current_link_image;
            } else {
              current_internal_promotion_creative = current_link_text;
            }

            // As long as current_internal_promotion_creative is not an empty string, proceed
            if(current_internal_promotion_creative !== "") {
              current_internal_promotion_creative = [current_internal_promotion_creative];

              // ----------------------------------------------------------------
              // internal_promotion_position
              // ----------------------------------------------------------------
              var current_internal_promotion_position = [link_index + "of" + number_of_links];

              // ================================================================
              // Update internal_promotion variables
              // ================================================================
                internal_promotion_Name = $.merge(internal_promotion_Name, current_internal_promotion_Name);
                internal_promotion_creative = $.merge(internal_promotion_creative, current_internal_promotion_creative);
                internal_promotion_id = $.merge(internal_promotion_id, current_internal_promotion_id);
                internal_promotion_position = $.merge(internal_promotion_position, current_internal_promotion_position);

              // ================================================================
              // CLICK TRACKING
              // ================================================================
              $(this).on("click", function( event ) {

                // Search for an image and strip alt tag (if no image, use link text)
                var click_link_image = $(this).find("img").attr("alt");
                var click_link_text = $(this).text().trim();
                click_link_text = click_link_text.replace(/(\r\n|\n|\r)/gm,''); // Remove an line breaks
                click_link_text = click_link_text.replace(/ +/g, ' '); // Remove spaces
                if(undefined !== click_link_image) {
                  click_internal_promotion_creative = [click_link_image];
                } else {
                  click_internal_promotion_creative = [click_link_text];
                }

                internal_promotion_object = {
                  "event_type":"promo_click",
                  "internal_promotion_Name":current_internal_promotion_Name,
                  "internal_promotion_creative":click_internal_promotion_creative,
                  "internal_promotion_id":current_internal_promotion_id,
                  "internal_promotion_position":current_internal_promotion_position,
                };
                console.log(internal_promotion_object);
                utag.link(internal_promotion_object);
              });
            }
        });
      });
    });
  });

  // ============================================
  // BUILD utag_data VARIABLES
  // ============================================
    utag_data["internal_promotion_Name"] = internal_promotion_Name;
    utag_data["internal_promotion_creative"] = internal_promotion_creative;
    utag_data["internal_promotion_id"] = internal_promotion_id;
    utag_data["internal_promotion_position"] = internal_promotion_position;
});
