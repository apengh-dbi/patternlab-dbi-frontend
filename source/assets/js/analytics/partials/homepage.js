(function($) {
  // ========================================================
  // Variables
  // ========================================================

    // set to true to enable logging to the console
    var debug = true;
    
    // pages to target (Homepage, CLPs)
    var targetPageTypes = ['Home', 'Landing Page'];
    
    // data attributes
    var dataAnalyticsTitle = 'data-analytics-tagging'; // added to parent container(s) inside of espots ( <div class="left_espot">)
    var dataAnalyticsPosition = 'data-analytics-position'; // added to link(s) parent container to get more granular metrics
    var dataImpressionsSent = 'data-impression-sent'; // dynamically added by script to track impressions sent/not sent
    var dataLinkType = 'data-link-type'; // dynamically added to links (CTA / IMAGE / SVG)
    
    // class to exclude for CLP mini-nav
    var clpMiniNav = '.carousel-nav';

    var page_name;
    if(utag_data["page_name"] !== undefined) {
      page_name = utag_data["page_name"];
    }
    var page_type;
    if(utag_data["page_type"] !== undefined) {
      page_type = utag_data["page_type"];
    }


  // ========================================================
  // Functions
  // ========================================================
  
    // --------------------------------------------------------
    // Custom logging functions
    // --------------------------------------------------------
    var log = {
      info: function(message) {
        if(debug) { console.log(message); }
      },
      error: function(message) {
        if(debug) { console.error(message); }
      },
      table: function(data) {
        if(debug) { console.table(data); }
      }
    }
    
    log.info('//' + '='.repeat(70) + '\n// HOMEPAGE / CLP TAGGING' + '\n//' + '='.repeat(70));

    // --------------------------------------------------------
    // Format text
    // trim whitespace, remove returns/new lines/tabs, replace multiples spaces with single space
    // --------------------------------------------------------
    function formatText(string) {
      return string.text().trim().replace(/(\r\n\t|\n|\r\t)/gm,"").replace(/\s+/g, ' ');
    }

    // --------------------------------------------------------
    // Does a link have visible text (true || false)
    // --------------------------------------------------------
    function hasVisibleText(link) {
      return ((link.is(':visible') || link.has(":visible").length !== 0) && formatText(link) !== '') && true || false;
    }

    // --------------------------------------------------------
    // Does a link have a visible image (true || false)
    // --------------------------------------------------------
    function hasVisibleImage(link) {
      return link.find('img:visible').is(':visible') && true || false;
    }
    
    // --------------------------------------------------------
    // Does a link have inline SVG image (true || false)
    // --------------------------------------------------------
    function hasInlineSVGImage(link) {
      return link.find('svg').is(':visible') && true || false;
    }
    
    // --------------------------------------------------------
    // Exclude CLP Mini-Nav links (true || false)
    // --------------------------------------------------------
    function isNotMiniNav(link) {
      return link.closest(clpMiniNav).length === 0 && true || false;
    }
    

    // --------------------------------------------------------
    // Get All Visible Links from Array of Links
    // add data-link-type and data-impression-sent attributes to links
    // --------------------------------------------------------
    function getAllVisibleLinks(links) {
      var visibleLinks = [];
      links.each(function(index) {
        if( hasVisibleText($(this)) && isNotMiniNav($(this)) && hasInlineSVGImage($(this))===false ) {
          $(this).attr(dataLinkType, 'CTA');
          $(this).attr(dataImpressionsSent, 'false');
        }
        if( hasVisibleImage($(this)) && isNotMiniNav($(this)) && hasInlineSVGImage($(this))===false ) {
          $(this).attr(dataLinkType, 'IMAGE');
          $(this).attr(dataImpressionsSent, 'false');
        }
        if( hasInlineSVGImage($(this)) ) {
          $(this).attr(dataLinkType, 'SVG');
          $(this).attr(dataImpressionsSent, 'false');
        }
        
        if( (hasVisibleText($(this)) || hasVisibleImage($(this)) || hasInlineSVGImage($(this))) && isNotMiniNav($(this)) ) {
          visibleLinks.push($(this));
        }
      });
      return visibleLinks;
    }


    // --------------------------------------------------------
    // Send click event data
    // --------------------------------------------------------
    function sendClick(promotion_id, promotion_name, promotion_creative, promotion_position) {
      var data = {
        'id': promotion_id,
        'name': promotion_name,
        'creative': promotion_creative,
        'position': promotion_position
      }
      log.info(data);
    }



    // --------------------------------------------------------
    // Send Impression
    // --------------------------------------------------------
    function sendImpression(element) {
      

      var title = element.attr(dataAnalyticsTitle);
      var impressionsData;

      log.info('\n' + '//' + '-'.repeat(70) + '\n// VIEWED: ' + title + '\n' + '//' + '-'.repeat(70));


      // --------------------------------------------------------
      // Conditions to look for
      // Has data-analytics-promotion attribute
      // --------------------------------------------------------
      // Find all data-analytics-position
      var positions = element.find('['+dataAnalyticsPosition+']');
      if(positions.length > 0) {
        positions.each(function(index) {
          
          var position = $(this).attr(dataAnalyticsPosition);
    
          // If current position is visible
          if( $(this).is(":visible") ) {
            
            // get all links
            var visibleLinks = [];
            var links = $(this).find('a');
            links.each(function(index) {
              var link = $(this);
              if ( (hasVisibleText(link) || hasVisibleImage(link)) && isNotMiniNav(link) ) {
                visibleLinks.push(link);
              };
            });
            
            $.each(visibleLinks, function(index) {
              // Internal Promotion Name
              var promotion_name = page_name;
        
              // Internal Promotion ID
              var promotion_id = title;
        
              // Internal Promotion Position
              var promotion_position = position;
        
              // Internal Promotion Creative (Image or Text CTA)
              var promotion_creative = $(this).find('img').attr('src');
              if(promotion_creative === undefined) {
                promotion_creative = formatText( $(this) );
              }
              
              // Send Enhanced Commerce Promotion Impression
              impressionsData = {
                'id': promotion_id,
                'name': promotion_name,
                'creative': promotion_creative,
                'position': promotion_position
              };
              log.info(impressionsData);
              ga('ec:addPromo', impressionsData);
        
              // Bind 'click' event
              $(this).on('click', function() {
                sendClick(promotion_name, promotion_id, promotion_position, promotion_creative);
              });
            });
          }
        });
      
      // --------------------------------------------------------
      // Doesn't have data-analytics-position attribute
      // Find all visible links
      // --------------------------------------------------------
      } else {
        var links = element.find('a');
        log.info('ALL LINKS: ' + links.length);
        var visibleLinks = getAllVisibleLinks(links);
        log.info('VISIBLE LINKS: ' + visibleLinks.length);
        
        var imageCount = 1, ctaCount = 1, svgCount = 1;
        $.each(visibleLinks, function(index) {
                
          // Get data-link-type
          var linkType = $(this).attr(dataLinkType);
          if( linkType === 'IMAGE' ) {
            $(this).attr(dataLinkType, 'IMAGE_' + imageCount);
            var promotion_creative = $(this).find('img').attr('src');
            imageCount++;
          } else if ( linkType === 'CTA' ) {
            $(this).attr(dataLinkType, 'CTA_' + ctaCount);
            var promotion_creative = formatText( $(this) );
            ctaCount++;
          } else if ( linkType === 'SVG' ) {
            $(this).attr(dataLinkType, 'SVG_' + svgCount);
            var promotion_creative = 'SVG or BUTTON';
            svgCount++;
          }
          
          if(promotion_creative) {
            // Internal Promotion Name
            var promotion_name = page_name;
        
            // Internal Promotion ID
            var promotion_id = title;
        
            // Internal Promotion Position
            var promotion_position = $(this).attr(dataLinkType);
            
            // Send Enhanced Commerce Promotion Impression
            impressionsData = {
              'id': promotion_id,
              'name': promotion_name,
              'creative': promotion_creative,
              'position': promotion_position
            };
            log.info(impressionsData);
            ga('ec:addPromo', impressionsData);
            
            // Set data-impression-sent = true
            $(this).attr(dataImpressionsSent, 'true');
        
            // Bind 'click' event
            $(this).on('click', function() {
              sendClick(promotion_name, promotion_id, promotion_position, promotion_creative);
            });
          }
        });
      }
      
      log.info('//' + '-'.repeat(70) + '\n// SENT IMPRESSION: ' + title + '\n' + '//' + '-'.repeat(70) + '\n\n');
    }




    // ========================================================
    // Homepage or Category Landing Page
    // ========================================================
    if( targetPageTypes.includes(page_type)) {
      
      // --------------------------------------------------------
      // Loop through eSpots to add data-analytics-tagging attribute if not present
      // --------------------------------------------------------
      // Get all visible espots
      var allEspots = $('.left_espot').filter(":visible").filter(function(){
        return $(this).height() > 0;
      });
      // exclude espots that have data-analytics-tagging attribute
      allEspots = allEspots.not(':has(['+dataAnalyticsTitle+'])');
         
      // loop through espots
      $.each(allEspots, function() {
        // has visible children with links
        var hasVisibleChildren = $(this).children(":visible").find('a').length !== 0 && true || false;
        if(hasVisibleChildren) {
          // Get all visible children
          var children = $(this).children(":visible");
          $.each(children, function(index) {
            // Get classname
            var className = $(this).attr('class').replace(/\s+/g, '.');
            // Add data-analytics-tagging attribute = class name
            $(this).attr(dataAnalyticsTitle, className);
          });
        }
      });
      
      // --------------------------------------------------------
      // Loop through eSpots with data-analytics-tagging attribute
      // then loop through content
      // --------------------------------------------------------
      var espots = $('.left_espot').has('['+dataAnalyticsTitle+']').filter(":visible").filter(function(){
        return $(this).height() > 0;
      });
      espots.each(function(index) {
        var row = index + 1;
        var contents = $(this).find('['+dataAnalyticsTitle+']');
        contents.each(function(index) {
          $(this).attr(dataImpressionsSent, 'false'); // set data-impression-sent = false
        });
        return contents;
      });


      // --------------------------------------------------------
      // Detect Visible
      // --------------------------------------------------------
      function detectVisible() {
        var winHeight = $(window).height();
        var winOffset = $(document).scrollTop();
        var minY = winOffset;
        var maxY = winOffset + winHeight;

        espots.each(function(index) {
          var contents = $(this).find('['+dataAnalyticsTitle+']');
          contents.each(function(index) {
            var visible = false;
            var itemTop = $(this).offset().top;
            var itemBottom = itemTop + $(this).height();
            var analyticsTitle = $(this).attr(dataAnalyticsTitle);
            // If element is visible
            if ((itemTop >= minY && itemTop < maxY) || (itemBottom >= minY && itemBottom < maxY)) {
              if( $(this).attr(dataImpressionsSent) === 'false' ) {
                $(this).attr(dataImpressionsSent, 'true');
                var element = $(this);
                sendImpression(element);
              }
            }
          });
        });
      }

      $(document).on('scroll', function() {
        detectVisible();
      });
      detectVisible();
    }
    
    
    // ========================================================
    // Category Landing Page Mini-Nav
    // ========================================================
    if(page_type === 'Landing Page') {
      log.info('//' + '='.repeat(70) + '\n// CATEGORY LANDING PAGE - MINI-NAV' + '\n//' + '='.repeat(70));

      var $miniNav = $('.carousel-nav'),
      $miniNavItems = $('.carousel-nav_items'),
      $miniNavItem = $('.carousel-nav_item');
      
      $.each($miniNavItem, function(index) {
        $(this).attr(dataImpressionsSent, 'false');
      });
      
      $.each($miniNavItems, function(index) {
        $(this).find('a').attr(dataImpressionsSent, 'false');
      });
      
      
      function getMiniNavImpressions() {
        log.info('getMiniNavImpressions');
        $.each($miniNavItem, function(index) {
          
          var header = $(this).find('.carousel-nav_item_header h3').text();
          var dataNavOpen = $(this).attr('data-nav-open');
          var impressionsSent = $(this).attr(dataImpressionsSent);
          log.info(header + ' : ' + dataNavOpen + ' : ' + impressionsSent);
          
          if(dataNavOpen === 'true' && impressionsSent === 'false') {
            // Find links that haven't had impressions sent
            var links = $(this).find('a');
            log.info(links);
            
            $.each(links, function(index) {
              // Internal Promotion Name
              var promotion_name = page_name;
            
              // Internal Promotion ID
              var promotion_id = 'Mini-Nav';
            
              // Internal Promotion Position
              var promotion_position = header.toUpperCase().replace(/\s+/g, '-') + '__CTA_' + (index + 1);
            
              // Internal Promotion Creative (Text)
              var promotion_creative = formatText( $(this) );

              // Send Enhanced Commerce Promotion Impression
              impressionsData = {
                'id': promotion_id,
                'name': promotion_name,
                'creative': promotion_creative,
                'position': promotion_position
              };
              log.info(impressionsData);
              ga('ec:addPromo', impressionsData);

              // Set data-impression-sent = true
              $(this).attr(dataImpressionsSent, 'true');
            
              // Bind 'click' event to each link
              $(this).on('click', function() {
                sendClick(promotion_name, promotion_id, promotion_position, promotion_creative);
              });
            });

            $(this).attr(dataImpressionsSent, 'true');
          }
        });
      }

      $miniNavItem.find('.carousel-nav_item_header').on('click', function() {
        getMiniNavImpressions();
      });
      getMiniNavImpressions();
    }
    
}(jQuery));
