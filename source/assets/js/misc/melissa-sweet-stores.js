/*
* List of Melissa Sweet Stores
* http://dbstage.davidsbridal.com/Content_Bridal_msexpstores
*
*/

var table = '<table width="100%" border="1" cellpadding="3" cellspacing="0">\
  <thead>\
    <tr>\
      <th></th>\
      <th>State</th>\
      <th>Address 1</th>\
      <th>Address 2</th>\
      <th>Address 3</th>\
      <th>City</th>\
      <th>State Abbr</th>\
      <th>Zip</th>\
    </tr>\
  </thead>\
  <tbody>';



//var table = [];
$('.MSstore_block').each(function(index) {
  //var row = [];
  var tableRow = '<tr>';
  tableRow += '<td>'+index+'</td>';

  var state = $(this).find('.MSstore_state').text();
  //row.push(state);
  tableRow += '<td>'+state+'</td>';

  var address = $(this).find('.MSstore_address').html();
  address = address.split('<br>');
  // console.log(address);

  if(address.length == 2) {
    var address1 = address[0];
    var address2 = '';
    var address3 = '';
    var cityStateZip = address[1];

  } else if (address.length == 3) {
    var address1 = address[0];
    var address2 = address[1];
    var address3 = '';
    var cityStateZip = address[2];

  } else if (address.length == 4) {
    var address1 = address[0];
    var address2 = address[1];
    var address3 = address[2];
    var cityStateZip = address[3];
  }

  //row.push(address1);
  //row.push(address2);
  //row.push(address3);
  //row.push(cityStateZip);

  tableRow += '<td>'+address1+'</td>';
  tableRow += '<td>'+address2+'</td>';
  tableRow += '<td>'+address3+'</td>';

  var stateZip = cityStateZip.split(',');
  var city = stateZip[0];
  //row.push(city);
  tableRow += '<td>'+city+'</td>';

  max = stateZip.length - 1;
  var stateZipSplit = stateZip[max].trim().split(' ');
  for (var i = 0; i < stateZipSplit.length; i++) {
    //row.push(stateZipSplit[i]);
    tableRow += '<td>'+stateZipSplit[i]+'</td>';
  }

  //table.push(row);
  tableRow += '</tr>';
  table += tableRow;

});

// console.table(table);

table += '</tbody>\
</table>';

console.log(table);

$('body').html(table);
