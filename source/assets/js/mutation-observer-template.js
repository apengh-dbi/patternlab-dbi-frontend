/*
* Mutation Observer Template
* Watches for changes being made to the DOM tree
* https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
*/


// Select the node that will be observed for mutations
var targetNode = document.getElementById('ProcessedOrdersStatusDisplay');

// Options for the observer (which mutations to observe)
var config = {
  attributes: true,
  childList: true,
  characterData: false,
  subtree: false
};

// Callback function to execute when mutations are observed
function callback(mutationsList) {
  console.log(mutationsList);

  for(var i = 0; i < mutationsList.length; i++) {
    var mutation = mutationsList[i];

    if (mutation.type == 'attributes') {
      console.log('The ' + mutation.attributeName + ' attribute was modified.');
    } else if(mutation.type == 'childList') {
      console.log('A child node has been added or removed.');
    } else if(mutation.type == 'characterData') {
      console.log('The ' + mutation.attributeName + ' data was modified.');
    } else if(mutation.type == 'subtree') {
      console.log('The ' + mutation.attributeName + ' descendant was modified.');
    }

  }

};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(targetNode, config);

// Later, you can stop observing
//observer.disconnect();
