(function($) {
  var carousel = '.few-favorites-carousel';
  var heading = '.few-favorites-carousel__inner--heading > p.serif-m-italic';
  
  var $bridesCarousel = $('#bridesCarouselSlick').closest(carousel);
  var $bridesmaidsCarousel = $('#bridesmaidsCarouselSlick').closest(carousel);
  var $dressesCarousel = $('#dressesCarouselSlick').closest(carousel);
  var $accessoriesCarousel = $('#accessoriesCarouselSlick').closest(carousel);
  var $shoesCarousel = $('#shoesCarouselSlick').closest(carousel);
  var $decorationsGiftsCarousel = $('#decorationsGiftsSlick').closest(carousel);
  
  // Brides
  var bridesHeading = $bridesCarousel.find('.few-favorites-carousel__inner--heading > p.serif-m-italic');
  bridesHeading.html('brides');
  bridesHeading.after('<p class="sans-bold-uppercase"><a href="/wedding-dresses/all-wedding-dresses">View All&nbsp;&gt;</a></p>');
  
  
  // Bridesmaids
  var bridesmaidsHeading = $bridesmaidsCarousel.find(heading);
  bridesmaidsHeading.html('bridesmaids');
  bridesmaidsHeading.after('<p class="sans-bold-uppercase"><a href="/bridesmaid-dresses/all-bridesmaid-dresses">View All&nbsp;&gt;</a></p>');
  
  
  // Dresses
  var dressesHeading = $dressesCarousel.find(heading);
  dressesHeading.html('dresses');
  dressesHeading.after('<p class="sans-bold-uppercase"><a href="/dresses/all-dresses">View All&nbsp;&gt;</a></p>');
  
  
  // Accessories
  var accessoriesHeading = $accessoriesCarousel.find(heading);
  accessoriesHeading.html('accessories');
  accessoriesHeading.after('<p class="sans-bold-uppercase"><a href="/accessories/all-accessories">View All&nbsp;&gt;</a></p>');
  
  
  // Shoes
  var shoesHeading = $shoesCarousel.find(heading);
  shoesHeading.html('shoes');
  shoesHeading.after('<p class="sans-bold-uppercase"><a href="/shoes/all-shoes">View All&nbsp;&gt;</a></p>');
  
  
  // Gifts & Decorations
  var decorationsGiftsHeading = $decorationsGiftsCarousel.find(heading);
  decorationsGiftsHeading.html('gifts & decor');
  decorationsGiftsHeading.after('<p class="sans-bold-uppercase"><a href="/wedding-gifts/all-wedding-gifts">View All&nbsp;&gt;</a></p>');
  

}(jQuery));