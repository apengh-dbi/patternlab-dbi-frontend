(function( $ ){

  var $countdown = $('.weekend-sale__countdown');
  var $countdownText = $('.weekend-sale__countdown--text');
  var endDate = new Date($countdown.attr('data-end-date'));
  var today = new Date();
  //var endDate = new Date('May 28, 2018 11:59:59 PM');
  //var today = new Date('May 28, 2018 12:00:00 AM');
  var secondsInDay = 1000 * 60 * 60 * 24;

  // Adjust start/end date/time to Eastern time
  function timezoneAdjust(date, timezoneOffest) {
    var timezoneOffest = timezoneOffest || -4;
    return new Date( new Date(date).getTime() + timezoneOffest * 3600 * 1000).toUTCString().replace( / GMT$/, "" );
  }

  var endTime = new Date( timezoneAdjust(endDate) ).getTime();
  var currentTime = new Date( timezoneAdjust(today) ).getTime();
  var daysLeft = Math.ceil((endTime - currentTime) / secondsInDay);

  var text = '';
  if(daysLeft >= 4) {
    text = '4 Days<br>Only!';
  } else if(daysLeft === 3 ) {
    text = '3 Days<br>Left!';
  } else if(daysLeft === 2 ) {
    text = '2 Days<br>Left!';
  } else if(daysLeft === 1 ) {
    text = 'Last<br>Day!'
  } else {
    text = '';
  }

  $countdownText.html(text);

}( jQuery ));
