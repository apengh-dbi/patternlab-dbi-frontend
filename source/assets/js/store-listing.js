(function($) {

  $('body').prepend('<style>\
  .overlay {\
    display: block;\
    position: fixed;\
    top: 0;\
    left: 0;\
    width: 100%;\
    height: 100%;\
    background: rgba(255, 255, 255, 0.5);\
    z-index: 999;\
  }\
  .overlay-loader {\
    position: absolute;\
    top: 50%;\
    left: 50%;\
    transform: translate(-50%, -50%);\
  }\
  </style>\
  <div class="overlay" id="overlay">\
    <img class="overlay-loader" src="http://www.davidsbridal.com/AuroraPlusStorefrontAssetStore/images/colors/color1/loading.gif">\
  </div>');

  var table = '<style>\
    td, th { padding: 5px;}\
    </style>\
    <table id="storeListings" class="display" width="100%" border="1" cellpadding="3" cellspacing="0" style="width: 100%; overflow: auto;">\
    <thead>\
      <tr>\
        <th>Store Number</th>\
        <th>Address 1</th>\
        <th>Address 2</th>\
        <th>City</th>\
        <th>State</th>\
        <th>Country</th>\
        <th>Zip</th>\
        <th>Latitude</th>\
        <th>Longitude</th>\
        <th>Weekday</th>\
        <th>Saturday</th>\
        <th>Sunday</th>\
      </tr>\
    </thead>\
    <tbody></tbody>\
  </table>';
  $('#page').html(table);

  function getStoreListings() {
    var storeNums = [];
    var start = 1;
    var end = 1000;
    var count = start;
    for(var i = start; i < end; i++) {
      var request = 'http://www.davidsbridal.com/wcs/resources/store/10052/storelocator/byStoreId/' + i;

      $.ajax({
        url: request,
        method: 'GET'
      }).done(function(data, textStatus, xhr) {
        if(data.recordSetCount > 0) {
          if(data.PhysicalStore[0] != undefined) {
            var store = data.PhysicalStore[0];
            var storeNum = store.storeName;
            console.log(storeNum);
            if(!storeNums.includes(storeNum)) {
              storeNums.push(storeNum);
              if( store !== undefined ) {
                var row = '';
                row += '<tr>';
                row += '<td>'+parseInt(store.storeName)+'</td>';
                row += '<td>'+store.addressLine[0]+'</td>';
                if(store.addressLine[1] != undefined) {
                  var address2 = store.addressLine[1];
                  var formatted = address2.replace('<br>', ', ');
                  formatted = formatted.replace('<br/>', ', ');
                  row += '<td>'+formatted+'</td>';
                } else {
                  row += '<td></td>';
                }
                row += '<td>'+store.city+'</td>';
                row += '<td>'+store.stateOrProvinceName+'</td>';
                row += '<td>'+store.country+'</td>';
                row += '<td>'+$.trim(store.postalCode)+'</td>';
                row += '<td>'+store.latitude+'</td>';
                row += '<td>'+store.longitude+'</td>';
                for(var i = 0; i < store.Attribute.length; i++) {
                  if(store.Attribute[i].name == 'STOREHOURSWEEKDAY') {
                    row += '<td>'+store.Attribute[i].value+'</td>';
                  }
                }
                for(var i = 0; i < store.Attribute.length; i++) {
                  if(store.Attribute[i].name == 'STOREHOURSSAT') {
                    row += '<td>'+store.Attribute[i].value+'</td>';
                  }
                }
                for(var i = 0; i < store.Attribute.length; i++) {
                  if(store.Attribute[i].name == 'STOREHOURSSUN') {
                    row += '<td>'+store.Attribute[i].value+'</td>';
                  }
                }
                row += '</tr>';
              }
              $('#storeListings').find('tbody').append(row);
            }
          }
        }
        $('body').css('overflow-x', 'auto');
        $('#page').css('overflow', 'auto');
      }).fail(function() {
        console.log('AJAX FAIL');
      }).always(function() {
        count++;
        console.log('AJAX ALWAYS', count);
        if(count === end) {
          $('#overlay').hide();
          console.log('DONE DONE, LIKE FOR REAL');
        }
      });
    }
  }
  getStoreListings();
}(jQuery));



// $.get('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.9.0/js/jquery.dataTables.js', function() {
//   $.get('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.9.0/css/jquery.dataTables.css', function() {
//     $('#storeListings').DataTable();
//   });
// });
