javascript: (function($) {
  var path = window.location.pathname;
  var styleNum = s7DBIData.mfPartNumber;
  var productId = s7DBIData.productId;
  var color = s7DBIData.mainColor;
  var altText = $('h1.detail__meta-title').text().trim();
  var className = 'slick-slide';
  $('.detail__color-swatch-item-input').each(function() {
    if ( $(this).prop("checked") ) {
      color = $(this).attr('swatch-value');
    }
  });
  console.log(color);
  color = color.replace(/\//g,'_').replace(/\ /g,'%20');
  console.log(color);
  var slide = `<!-- ${styleNum} -->\n<div class="${className}">\n\t<a href="${path}">\n\t\t<img src="https://img.davidsbridal.com/is/image/DavidsBridalInc/Set-${styleNum}-${productId}-${color}?$plpproductimgmobile_2up$" alt="${altText}">\n\t</a>\n</div>`;
  console.log(slide);
  var container = `<div>
    <pre id="trendingProductSlide" style="background:rgba(0, 0, 0, 0.7); color:rgba(128, 256, 0, 1); margin:20px; padding:20px; border-radius:8px; box-shadow:2px 2px 4px #000 inset; white-space:pre-wrap;"></pre>
  </div>`;
  $('body').prepend(container);
  $('#trendingProductSlide').text(slide);
}(jQuery));
