javascript: (function() {
  var path = window.location.pathname;
  var styleNum = s7DBIData.mfPartNumber;
  var productId = s7DBIData.productId;
  var color = s7DBIData.mainColor;
  var altText = $('h1.detail__meta-title').text().trim();
  var mainImage = $('.s7staticimage').find('img').eq(0).attr('src');
  var message = '<style>\
    table { font-family: "Open Sans", Helvetica, Arial, sans-serif; white-space:normal; table-layout:fixed; width:100%; }\
    td, th { padding:10px; text-align:left; font-weight:700; }\
    th { background:#333; color:#fff; white-space:nowrap; }\
    td { word-wrap:break-word; }\
  </style>\
  <section style="padding:20px; overflow-x:auto;">\
    <table width="100%" border="1" cellpadding="5" cellspacing="0">\
      <tbody>\
        <tr>\
          <th width="200">STYLE NUM</th>\
          <td>${styleNum}</td>\
        </tr>\
        <tr>\
          <th>CATENTRY ID</th>\
          <td>'+productId+'</td>\
        </tr>\
        <tr>\
          <th>PATH</th>\
          <td>'+path+'</td>\
        </tr>\
        <tr>\
          <th>MIXED MEDIA SET</th>\
          <td>https://img.davidsbridal.com/is/image/DavidsBridalInc/Set-'+styleNum+'-'+productId+'-'+color+'</td>\
        </tr>\
        <tr>\
          <th>MAIN IMAGE</th>\
          <td>'+mainImage+'</td>\
        </tr>\
      </tbody>\
    </table>\
  </section>';
  $('body').prepend(message);
})();
