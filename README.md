# David's Bridal Front-End Pattern Lab
Pattern Lab Node - Gulp Edition



## Prerequisites

The Pattern Lab Node - Gulp Edition uses [Node](https://nodejs.org) for core processing, [npm](https://www.npmjs.com/) to manage project dependencies, and [gulp.js](http://gulpjs.com/) to run tasks and interface with the core library. Node version 4 or higher suffices. You can follow the directions for [installing Node](https://nodejs.org/en/download/) on the Node website if you haven't done so already. Installation of Node will include npm.

It's also highly recommended that you [install gulp](hhttps://github.com/gulpjs/gulp/blob/4.0/docs/getting-started.md) globally.

#### Installing Prerequisites

* Install NodeJS and NPM: [Download Node/NPM](https://nodejs.org/en/)
  
* Install Gulp CLI
  1. Launch Terminal
  1. Run `npm install gulp-cli -g`


## Installing Pattern Lab

1. Launch Terminal
1. cd to the directory where you want to install
1. Clone the repository: `git clone https://apengh-dbi@bitbucket.org/apengh-dbi/patternlab-dbi-frontend.git`
1. cd into patternlab-dbi-frontend directory
1. Run npm install: `npm install`


## Getting Started

#### List all of the available commands

To list all available commands type:

`gulp patternlab:help`


#### Generate Pattern Lab

To generate the front-end for Pattern Lab type:

`gulp patternlab:build`


#### Watch for changes and re-generate Pattern Lab

To watch for changes, re-generate the front-end, and server it via a BrowserSync server,  type:

`gulp patternlab:serve`

BrowserSync should open [http://localhost:3000](http://localhost:3000) in your browser.


#### Custom Gulp Tasks

Delete everything in the Public folder

`gulp clean`


Download WCS Assets (CSS / JS files)

`gulp download-wcs-assets`


Compile SASS files

`gulp sass`
